﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilderPathMode : MonoBehaviour
{
    private InputManager inputScr;








    private Camera levelEditorCam;

    private LevelBuilderSaveLoad builderSaveLoadScr;

    private bool buildingAxisUsed;

    void ReRenderAffectedPathIndexes()
    {
        if (ExtendedMaths.ListTrueEmpty(builderSaveLoadScr.allPathPoints) == false)
        {
            builderSaveLoadScr.RenderBuilderData(builderSaveLoadScr.allPathPoints[0].x, builderSaveLoadScr.allPathPoints[0].y);
            if (builderSaveLoadScr.allPathPoints.Count - 1 >= 0)
            {
                builderSaveLoadScr.RenderBuilderData(builderSaveLoadScr.allPathPoints[builderSaveLoadScr.allPathPoints.Count - 1].x, builderSaveLoadScr.allPathPoints[builderSaveLoadScr.allPathPoints.Count - 1].y);

            }

            if (builderSaveLoadScr.allPathPoints.Count - 2 >= 0)
            {
                builderSaveLoadScr.RenderBuilderData(builderSaveLoadScr.allPathPoints[builderSaveLoadScr.allPathPoints.Count - 2].x, builderSaveLoadScr.allPathPoints[builderSaveLoadScr.allPathPoints.Count - 2].y);

            }
        }
    }

    void Update_PlaceRemovePoints()
    {
        if (FindDependencies())
        {
            if (ExtendedMaths.PointerOverUI() == false && ExtendedMaths.PointerInView() == true)
            {
                Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                Vector3 spawnPos = GridSnap.SnapToGrid(levelEditorCam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, levelEditorCam.nearClipPlane + 0.5f)), true, true, true);

                intVec2 posFound = builderSaveLoadScr.FindPosInBuilderData(new Vector2(spawnPos.x, spawnPos.y));


                if (inputScr.allInputValues[inputScr.GetGroupIndexByName("Building")].HasActivity())
                {
                    if (buildingAxisUsed == false)
                    {
                        if (inputScr.GetInputByName("Building", "Editor_PlaceBlock") > 0)
                        {



                            if (posFound.x != LevelBuilderSaveLoad.invalidIntVec2.x && posFound.y != LevelBuilderSaveLoad.invalidIntVec2.y)
                            {

                                if (!builderSaveLoadScr.allPathPoints.Contains(posFound))
                                {
                                    builderSaveLoadScr.allPathPoints.Add(posFound);
                                }
                                
                                builderSaveLoadScr.RenderBuilderData(posFound.x,posFound.y);
                                ReRenderAffectedPathIndexes();


                            }

                        }


                        if (inputScr.GetInputByName("Building", "Editor_RemoveBlock") > 0)
                        {



                            if (posFound.x != LevelBuilderSaveLoad.invalidIntVec2.x && posFound.y != LevelBuilderSaveLoad.invalidIntVec2.y)
                            {
                                if (builderSaveLoadScr.allPathPoints.Contains(posFound))
                                {
                                    builderSaveLoadScr.allPathPoints.Remove(posFound);
                                }
                                
                                builderSaveLoadScr.RenderBuilderData(posFound.x, posFound.y);
                                ReRenderAffectedPathIndexes();





                            }

                        }
                        buildingAxisUsed = true;
                    }
                }
                else
                {
                    buildingAxisUsed = false;
                }

            }
        }




    }




    private void Update()
    {
        if (FindDependencies())
        {

            Update_PlaceRemovePoints();
        }

    }




    bool FindDependencies()
    {
        bool ret = false;

        if (GameObject.FindObjectOfType<LevelBuilderCameraMovement>() != null)
        {
            levelEditorCam = GameObject.FindObjectOfType<LevelBuilderCameraMovement>().GetComponent<Camera>();
        }

        

        inputScr = GameObject.FindObjectOfType<InputManager>();

        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();
        if (levelEditorCam != null && inputScr != null && builderSaveLoadScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
