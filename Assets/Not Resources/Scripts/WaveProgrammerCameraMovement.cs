﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveProgrammerCameraMovement : MonoBehaviour
{
    private LevelBuilderSaveLoad builderSaveLoadScr;
    [SerializeField] float scrollSpeed;
    private InputManager inputScr;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (FindDependencies())
        {
            Vector3 position_proposed = gameObject.transform.position+ new Vector3(0, scrollSpeed * inputScr.GetInputByName("Camera", "Editor_UpDown") * Time.deltaTime, 0);

            if (builderSaveLoadScr.editorWaves_index >= 0 && builderSaveLoadScr.editorWaves_index < builderSaveLoadScr.editorWaves.Count)
            {
                float halfACanvas = (builderSaveLoadScr.singleWorldGroupCanvasHeight * 0.5f);
                float scrollUpperLimit = halfACanvas;
                float scrollLowerLimit = -(builderSaveLoadScr.singleWorldGroupCanvasHeight * builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count) + halfACanvas;

                position_proposed = new Vector3(position_proposed.x, Mathf.Clamp(position_proposed.y, scrollLowerLimit, scrollUpperLimit), position_proposed.z);
            }
            gameObject.transform.position = position_proposed;

        }
        
    }

    bool FindDependencies()
    {
        bool ret = false;

        inputScr = GameObject.FindObjectOfType<InputManager>();
        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();

        if (inputScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
