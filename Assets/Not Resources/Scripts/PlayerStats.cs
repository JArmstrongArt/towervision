﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this class keeps track of important rolling figures assigned to the player, its largely self-explanatory
public class PlayerStats : MonoBehaviour
{
    [HideInInspector]
    public int money;

    [HideInInspector]
    public float health;
    private InputManager inputScr;

    private void Awake()
    {
        health = 100.0f;
        money = 200;//for some reason i don't understand, declaring and defining this value on the same line refused to work
    }
    private void Update()
    {
        if (FindDependencies())
        {

            if (inputScr.GetInputByName("DEBUG", "DBG_ADDMONEY") > 0)
            {
                money += 100;

            }
        }
    }


    bool FindDependencies()
    {
        bool ret = false;
        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (inputScr != null)
        {

            ret = true;
        }

        return ret;
    }

    public void ShiftMoney(int shiftAmt)
    {
        int money_proposed = money + shiftAmt;

        if (money_proposed < 0)
        {
            money_proposed = 0;
        }
        money = money_proposed;



    }

    public void ShiftHealth(float shiftAmt)
    {
        float health_proposed = health + shiftAmt;
        if (health_proposed < 0)
        {
            health_proposed = 0;

            
        }
        health = health_proposed;

    }
}
