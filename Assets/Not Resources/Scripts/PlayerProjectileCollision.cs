﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectileCollision : MonoBehaviour
{
    private PlayerMovement moveScr;
    [HideInInspector]
    public GameObject sourcePlayer;
    // Start is called before the first frame update
    public void Setup()
    {

        if (FindDependencies())
        {
            if (gameObject.GetComponent<Rigidbody>() != null)
            {
                Destroy(GetComponent(typeof(Rigidbody)));
            }

            if (gameObject.GetComponent<Rigidbody>() == null)
            {
                gameObject.AddComponent<Rigidbody>();
            }

            if (gameObject.GetComponent<Rigidbody>() != null)
            {
                Rigidbody myRb = gameObject.GetComponent<Rigidbody>();
                myRb.isKinematic = true;
                myRb.useGravity = false;
                myRb.constraints = RigidbodyConstraints.FreezeAll;
                myRb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
            }

            //just guess some average values for the collision on this entity, again it can be changed in child awake calls easily
            SphereCollider collComp = null;
            if (gameObject.GetComponent<SphereCollider>() != null)
            {
                collComp = gameObject.GetComponent<SphereCollider>();
            }
            else
            {
                while (gameObject.GetComponent<Collider>() != null)
                {
                    Destroy(GetComponent(typeof(Collider)));
                }
                collComp = gameObject.AddComponent<SphereCollider>();
            }
            collComp.radius = moveScr.playerHeight;
            collComp.center = Vector3.zero;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (sourcePlayer != null)
        {
            moveScr = sourcePlayer.GetComponent<PlayerMovement>();
        }
        

        if (moveScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
