﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpreadTower : TowerInherit
{
    private GameObject ps_spreadBubbles_prefab;
    private GameObject ps_spreadBubbles_inst;
    [SerializeField] float spinRate;
    private float spinRate_orig;
    private float damage_orig;
    private Rotate rotateScr;
    //implementation of how upgrades affect this specific tower, it increases its spin speed and proportionally its damage
    protected override void UpgradeEffectUpdate()
    {
        if (FindResources() && FindDependencies())
        {
            base.damage = damage_orig * base.upgrades[base.upgradeLevel - 1].optionalParameter1;
            spinRate = spinRate_orig * base.upgrades[base.upgradeLevel - 1].optionalParameter1;
        }

    }

    protected override void Attack()
    {
        if (FindResources() && FindDependencies())
        {
            if (ps_spreadBubbles_inst == null)
            {
                ps_spreadBubbles_inst = Instantiate(ps_spreadBubbles_prefab, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1.3f, gameObject.transform.position.z), Quaternion.Euler(0.0f, transform.eulerAngles.y, 0.0f), null);

            }
            else
            {
                if (ps_spreadBubbles_inst.GetComponent<SpreadTowerBubbles>() != null)
                {
                    if (ps_spreadBubbles_inst.GetComponent<SpreadTowerBubbles>().spreadScr == null)
                    {
                        ps_spreadBubbles_inst.GetComponent<SpreadTowerBubbles>().spreadScr = gameObject.GetComponent<SpreadTower>();
                    }

                }

                ps_spreadBubbles_inst.name = gameObject.name.ToString() + "_spreadBubbles";
                if (ps_spreadBubbles_inst.GetComponent<Rotate>() == null)
                {
                    rotateScr = ps_spreadBubbles_inst.AddComponent<Rotate>();
                }
                else
                {
                    rotateScr = ps_spreadBubbles_inst.GetComponent<Rotate>();
                }

                UpdateRotateVal();

                base.animScr.billboardWorldDir_set(ps_spreadBubbles_inst.transform.forward);
            }
        }

    }

    void UpdateRotateVal()
    {
        if (FindResources() && FindDependencies())
        {
            if (rotateScr != null)
            {
                rotateScr.spinVals = new Vector3(0, spinRate, 0);
            }
        }

    }

    protected override bool NearestMonsterInRange()//the attack timer in the base class only runs if this function returns true, and i want this tower to always be atatcking, so i do this
    {
        return true;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (FindResources() && FindDependencies())
        {
            
            if (ps_spreadBubbles_inst != null)
            {
                Destroy(ps_spreadBubbles_inst);
            }
        }

    }

    protected override void Awake()
    {
        if (FindResources() && FindDependencies())
        {
            base.Awake();

            spinRate_orig = spinRate;
            damage_orig = base.damage;
        }

    }

    protected override void Update()
    {
        if (FindResources() && FindDependencies())
        {
            base.Update();

            //UpdateRotateVal();
        }

    }

    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;

        if (ps_spreadBubbles_prefab == null)
        {
            ps_spreadBubbles_prefab = Resources.Load<GameObject>("Particles/pS_spreadBubbles");
        }

        if (parRet==true && ps_spreadBubbles_prefab != null)
        {
            ret = true;
        }
        return ret;
    }

    protected override bool FindDependencies()
    {
        return base.FindDependencies();
    }
}
