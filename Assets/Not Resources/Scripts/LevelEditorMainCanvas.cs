﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class LevelEditorMainCanvas : LevelEditorCanvasInputToggles
{
    [SerializeField] RectTransform levelBuilder_btn;
    [SerializeField] RectTransform metadata_btn;
    [SerializeField] RectTransform waveProgrammer_btn;
    [SerializeField] RectTransform exit_btn;
    [SerializeField] RectTransform open_btn;
    [SerializeField] RectTransform save_btn;
    [SerializeField] RectTransform playtest_btn;
    public TMP_InputField levelName_inp;
    private RectTransform canvRect;

    private LevelBuilderSaveLoad builderSaveLoadScr;

    private LevelBuilderModeManager modeManagerScr;

    [SerializeField] GameObject loadLevelWarning;

    private float loadLevelWarning_visibleTime_current = 0;
    [SerializeField] float loadLevelWarning_visibleTime;

    private float saveLevelWarning_visibleTime_current = 0;
    [SerializeField] float saveLevelWarning_visibleTime;
    [SerializeField] GameObject saveLevelWarning;
    [SerializeField] GameObject saveLevelWarning_spawns;
    [SerializeField] GameObject saveLevelWarning_waves;
    [SerializeField] GameObject saveLevelWarning_path;
    private IDictionary<string, bool> reasonsNotToSave;



    public void LoadLevel()
    {
        if (FindDependencies())
        {

            LevelSaveLoad.folderToLoad = LevelSaveLoadFolder.USER;
            string loadPath = Application.dataPath + "/" + "levels" + "/" + LevelSaveLoad.SaveLoadFolderToString(LevelSaveLoad.folderToLoad) + "/";
            List<System.IO.FileInfo> userFiles = ExtendedMaths.LoadAllLevelsInFolder(loadPath);
            if (!ExtendedMaths.ListTrueEmpty(userFiles))
            {
                SwitchTab((int)LevelEditorTab.LOADLEVEL);
            }
            else
            {
                loadLevelWarning_visibleTime_current = loadLevelWarning_visibleTime;
            }
            
        }
    }
    public void SaveLevel()
    {
        if (FindDependencies())
        {

            reasonsNotToSave = builderSaveLoadScr.SaveLevelOutOfEditor(false, levelName_inp.text);
            saveLevelWarning_visibleTime_current = saveLevelWarning_visibleTime;

        }
    }

    public void LevelName_Refresh()
    {
        if (FindDependencies())
        {
            ExtendedMaths.TMPTextVerify_InputField(levelName_inp);
        }
    }
    public void ExitLevelEditor()
    {
        if (FindDependencies())
        {
            SceneManager.LoadScene("MainMenu");

        }
    }
    public void TestLevel_ButtonPressed()
    {
        if (FindDependencies())
        {
            LevelSaveLoad.levelToLoad = builderSaveLoadScr.testLevelName;
            LevelSaveLoad.folderToLoad = LevelSaveLoadFolder.TEST;
            reasonsNotToSave = builderSaveLoadScr.SaveLevelOutOfEditor(true, levelName_inp.text);
            saveLevelWarning_visibleTime_current = saveLevelWarning_visibleTime;
            bool saveProblems = false;

            foreach (KeyValuePair<string, bool> reason in reasonsNotToSave)
            {
                if (reason.Value == false)
                {
                    saveProblems = true;
                    break;
                }
            }

            if (saveProblems == false)
            {
                SceneManager.LoadScene("LevelArea");

            }
        }

    }

    public void SwitchTab(int tabEnumRep)
    {
        if (FindDependencies())
        {
            if (System.Enum.IsDefined(typeof(LevelEditorTab), tabEnumRep))
            {
                modeManagerScr.ChangeModeAndTab(modeManagerScr.mode, (LevelEditorTab)tabEnumRep);

            }

        }
    }


    private void OnRectTransformDimensionsChange()
    {
        if (FindDependencies())
        {
            TaskbarButtonResize();
        }



    }

    private void Awake()
    {
        if (FindDependencies())
        {
            TaskbarButtonResize();
            LevelName_Refresh();
        }
  
    }
    private void Update()
    {
        if (FindDependencies())
        {
            if (loadLevelWarning_visibleTime_current > 0)
            {
                loadLevelWarning_visibleTime_current -= Time.deltaTime;
                loadLevelWarning.SetActive(true);
            }
            else
            {
                loadLevelWarning.SetActive(false);

            }

            if (saveLevelWarning_visibleTime_current > 0)
            {
                saveLevelWarning_visibleTime_current -= Time.deltaTime;
                if (reasonsNotToSave != null)
                {
                    bool saveProblems = false;

                    foreach (KeyValuePair<string, bool> reason in reasonsNotToSave)
                    {
                        if (reason.Value == false)
                        {
                            saveProblems = true;
                            break;
                        }
                    }

                    if (saveProblems)
                    {
                        saveLevelWarning.SetActive(true);
                        if (reasonsNotToSave.ContainsKey("hasSpawns"))
                        {

                            saveLevelWarning_spawns.SetActive(!reasonsNotToSave["hasSpawns"]);

                        }
                        else
                        {
                            saveLevelWarning_spawns.SetActive(false);

                        }

                        if (reasonsNotToSave.ContainsKey("hasPath"))
                        {

                            saveLevelWarning_path.SetActive(!reasonsNotToSave["hasPath"]);

                        }
                        else
                        {
                            saveLevelWarning_path.SetActive(false);

                        }

                        if (reasonsNotToSave.ContainsKey("hasWaves"))
                        {

                            saveLevelWarning_waves.SetActive(!reasonsNotToSave["hasWaves"]);

                        }
                        else
                        {
                            saveLevelWarning_waves.SetActive(false);

                        }
                    }
                    else
                    {
                        SaveLevelWarning_AllOnOff(false);

                    }
                }
            }
            else
            {
                SaveLevelWarning_AllOnOff(false);
            }
        }
    }

    void SaveLevelWarning_AllOnOff(bool state)
    {
        saveLevelWarning.SetActive(state);
        saveLevelWarning_path.SetActive(saveLevelWarning.activeSelf);
        saveLevelWarning_waves.SetActive(saveLevelWarning.activeSelf);
        saveLevelWarning_spawns.SetActive(saveLevelWarning.activeSelf);
    }
    void TaskbarButtonResize()
    {
        if (FindDependencies())
        {
            float aThird = 0.333333333f;


            float unusedXSpace = exit_btn.sizeDelta.x + open_btn.sizeDelta.x + save_btn.sizeDelta.x + playtest_btn.sizeDelta.x;
            levelBuilder_btn.sizeDelta = new Vector2((canvRect.sizeDelta.x- unusedXSpace)*aThird, levelBuilder_btn.sizeDelta.y);
            waveProgrammer_btn.sizeDelta = levelBuilder_btn.sizeDelta;
            metadata_btn.sizeDelta = waveProgrammer_btn.sizeDelta;

            levelBuilder_btn.anchoredPosition = new Vector2(- levelBuilder_btn.sizeDelta.x, levelBuilder_btn.anchoredPosition.y);
            waveProgrammer_btn.anchoredPosition = new Vector2(0, waveProgrammer_btn.anchoredPosition.y);
            metadata_btn.anchoredPosition = new Vector2(metadata_btn.sizeDelta.x, metadata_btn.anchoredPosition.y);

        }

    }

    bool FindDependencies()
    {
        bool ret = false;
        canvRect = gameObject.GetComponent<RectTransform>();
        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();

        modeManagerScr = GameObject.FindObjectOfType<LevelBuilderModeManager>();



        if (modeManagerScr != null && canvRect != null && builderSaveLoadScr != null)
        {
            ret = true;
        }

        return ret;
    }


}
