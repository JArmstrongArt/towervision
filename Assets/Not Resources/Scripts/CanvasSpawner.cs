﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum CanvasSet
{
    GAMEPLAY,
    LEVELEDITOR,
    MENU,
    STORY
}
public class CanvasSpawner : MonoBehaviour,IEditorFunctionality
{
    private GameObject slimeOverlayCanvas_prefab;
    private GameObject nextWaveMonsterCanvas_prefab;
    private GameObject crosshairCanvas_prefab;
    private GameObject weaponCanvas_prefab;
    private GameObject mainHudCanvas_prefab;
    private GameObject minimapCamera_prefab;
    private GameObject levelEditor_MainCanvas_prefab;
    private GameObject levelEditor_BuilderCanvas_prefab;
    private GameObject waveProgrammerCanvas_prefab;
    private GameObject metadataCanvas_prefab;
    private GameObject winCanvas_prefab;
    private GameObject loseCanvas_prefab;

    private GameObject mainMenuHotbarCanvas_prefab;
    private GameObject titleScreenCanvas_prefab;
    private GameObject optionsCanvas_prefab;
    private GameObject creditsCanvas_prefab;
    private GameObject levelFolderSelectCanvas_prefab;
    private GameObject levelSelectCanvas_prefab;
    private GameObject levelSelectCanvasLevelLoader_prefab;

    private GameObject storyCanvas_prefab;

    private GameObject pauseCanvas_prefab;

    [SerializeField] CanvasSet canvasSetToSpawn;

    [HideInInspector]
    public bool awakeDone;
    void Awake()
    {
        if (FindResources())
        {
            awakeDone = false;
            GameObject levelSelectCanvasObj = null;
            LevelSelectCanvas selectCanvasScr = null;
            switch (canvasSetToSpawn)
            {
                
                case CanvasSet.GAMEPLAY:
                    Instantiate(slimeOverlayCanvas_prefab, null);
                    Instantiate(nextWaveMonsterCanvas_prefab, null);
                    Instantiate(crosshairCanvas_prefab, null);
                    Instantiate(weaponCanvas_prefab, null);
                    Instantiate(mainHudCanvas_prefab, null);
                    Instantiate(minimapCamera_prefab, null);
                    Instantiate(winCanvas_prefab, null);
                    Instantiate(loseCanvas_prefab, null);
                    Instantiate(pauseCanvas_prefab, null);
                    GameObject optionObj= Instantiate(optionsCanvas_prefab, null);
                    Canvas optionScr = optionObj.GetComponent<Canvas>();
                    if (optionScr != null)
                    {
                        optionScr.enabled = false;
                    }

                    break;
                case CanvasSet.LEVELEDITOR:
                    Instantiate(levelEditor_MainCanvas_prefab, null);
                    Instantiate(levelEditor_BuilderCanvas_prefab, null);
                    Instantiate(waveProgrammerCanvas_prefab, null);
                    Instantiate(metadataCanvas_prefab, null);
                    levelSelectCanvasObj = Instantiate(levelSelectCanvas_prefab, null);
                    selectCanvasScr = levelSelectCanvasObj.GetComponent<LevelSelectCanvas>();
                    if (selectCanvasScr != null)
                    {
                        selectCanvasScr.spawnEnum = SpawnMode.LevelEditor;
                    }
                    Instantiate(levelSelectCanvasLevelLoader_prefab, null);

                    break;
                case CanvasSet.MENU:
                    Instantiate(mainMenuHotbarCanvas_prefab, null);
                    Instantiate(titleScreenCanvas_prefab, null);
                    Instantiate(optionsCanvas_prefab, null);
                    Instantiate(creditsCanvas_prefab, null);
                    Instantiate(levelFolderSelectCanvas_prefab, null);
                    levelSelectCanvasObj = Instantiate(levelSelectCanvas_prefab, null);
                    selectCanvasScr = levelSelectCanvasObj.GetComponent<LevelSelectCanvas>();
                    if (selectCanvasScr != null)
                    {
                        selectCanvasScr.spawnEnum = SpawnMode.MainMenu;
                    }
                    Instantiate(levelSelectCanvasLevelLoader_prefab, null);
                    break;

                case CanvasSet.STORY:
                    Instantiate(storyCanvas_prefab, null);
                    break;
            }
            awakeDone = true;

        }
    }
    public void EditorUpdate()
    {
        gameObject.name = "CanvasSpawner";
    }

    bool FindResources()
    {
        bool ret = false;

        switch (canvasSetToSpawn)
        {
            case CanvasSet.GAMEPLAY:


                if (slimeOverlayCanvas_prefab == null)
                {
                    slimeOverlayCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/slimeOverlayCanvas");
                }
                if (nextWaveMonsterCanvas_prefab == null)
                {
                    nextWaveMonsterCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/nextWaveMonsterCanvas");
                }
                if (crosshairCanvas_prefab == null)
                {
                    crosshairCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/crosshairCanvas");
                }
                if (weaponCanvas_prefab == null)
                {
                    weaponCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/weaponCanvas");
                }

                if (mainHudCanvas_prefab == null)
                {
                    mainHudCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/mainHudCanvas");
                }
                if (minimapCamera_prefab == null)
                {
                    minimapCamera_prefab = Resources.Load<GameObject>("Prefabs/UI/minimapCamera");
                }
                if (winCanvas_prefab == null)
                {
                    winCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/winCanvas");
                }

                if (loseCanvas_prefab == null)
                {
                    loseCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/loseCanvas");
                }
                if (pauseCanvas_prefab == null)
                {
                    pauseCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/pauseCanvas");
                }

                if (optionsCanvas_prefab == null)
                {
                    optionsCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/MainMenu/optionsCanvas");
                }

                if (optionsCanvas_prefab!=null && slimeOverlayCanvas_prefab != null && nextWaveMonsterCanvas_prefab!=null && crosshairCanvas_prefab!=null && weaponCanvas_prefab!=null && mainHudCanvas_prefab!=null && minimapCamera_prefab!=null && winCanvas_prefab!=null && loseCanvas_prefab!=null && pauseCanvas_prefab != null)
                {
                    ret = true;
                }
                break;
            case CanvasSet.LEVELEDITOR:


                if (levelEditor_MainCanvas_prefab == null)
                {
                    levelEditor_MainCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/LevelEditor_MainCanvas");
                }

                if (levelEditor_BuilderCanvas_prefab == null)
                {
                    levelEditor_BuilderCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/LevelEditor_BuilderCanvas");
                }


                if (waveProgrammerCanvas_prefab == null)
                {
                    waveProgrammerCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/waveProgrammerCanvas");
                }

                if (metadataCanvas_prefab == null)
                {
                    metadataCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/metadataCanvas");
                }

                if (levelSelectCanvas_prefab == null)
                {
                    levelSelectCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/MainMenu/levelSelectCanvas");
                }

                if (levelSelectCanvasLevelLoader_prefab == null)
                {
                    levelSelectCanvasLevelLoader_prefab = Resources.Load<GameObject>("Prefabs/levelSelectCanvasLevelLoader");
                }

                if (levelSelectCanvas_prefab!=null && levelSelectCanvasLevelLoader_prefab != null && levelEditor_MainCanvas_prefab != null && levelEditor_BuilderCanvas_prefab!=null && waveProgrammerCanvas_prefab!=null && metadataCanvas_prefab != null)
                {
                    ret = true;
                }
                break;
            case CanvasSet.MENU:




                if (mainMenuHotbarCanvas_prefab == null)
                {
                    mainMenuHotbarCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/MainMenu/mainMenuHotbarCanvas");
                }

                if (titleScreenCanvas_prefab == null)
                {
                    titleScreenCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/MainMenu/titleScreenCanvas");
                }

                if (optionsCanvas_prefab == null)
                {
                    optionsCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/MainMenu/optionsCanvas");
                }


                if (creditsCanvas_prefab == null)
                {
                    creditsCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/MainMenu/creditsCanvas");
                }

                if (levelFolderSelectCanvas_prefab == null)
                {
                    levelFolderSelectCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/MainMenu/levelFolderSelectCanvas");
                }

                if (levelSelectCanvas_prefab == null)
                {
                    levelSelectCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/MainMenu/levelSelectCanvas");
                }
                // levelSelectCanvasLevelLoader
                if (levelSelectCanvasLevelLoader_prefab == null)
                {
                    levelSelectCanvasLevelLoader_prefab = Resources.Load<GameObject>("Prefabs/levelSelectCanvasLevelLoader");
                }

                if (levelSelectCanvasLevelLoader_prefab!=null && levelSelectCanvas_prefab != null && mainMenuHotbarCanvas_prefab != null && titleScreenCanvas_prefab != null && optionsCanvas_prefab != null && creditsCanvas_prefab != null && levelFolderSelectCanvas_prefab != null)
                {
                    ret = true;
                }
                break;

            case CanvasSet.STORY:



                if (storyCanvas_prefab == null)
                {
                    storyCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/storyCanvas");
                }

                if (storyCanvas_prefab != null)
                {
                    ret = true;
                }
                break;
        }




        return ret;
    }
}
