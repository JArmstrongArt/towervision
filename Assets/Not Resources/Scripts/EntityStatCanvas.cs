﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//a class i didn't want to make but saw no alternative, perhaps a struct?
//basically i need this in the inspector so i can easily plop in these ui assets
public class EntityStatCanvas : MonoBehaviour
{
    public TextMeshProUGUI healthVal_txt;

    public TextMeshProUGUI entityName_txt;

    public Image bg_img;
    public Image healthbar_bg_img;
}
