﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class MetadataCanvas : LevelEditorCanvasInputToggles
{
    private LevelBuilderSaveLoad builderSaveLoadScr;

    [SerializeField] TMP_InputField startMoney_inp;
    [SerializeField] TMP_InputField startHealth_inp;
    [SerializeField] TMP_InputField skyColour_inp;
    [SerializeField] Image skyColour_img;


    private void Awake()
    {
        if (FindDependencies())
        {
            StartMoney_Get();
            StartHealth_Get();
            SkyColour_Get();
        }
    }

    private void OnEnable()
    {
        if (FindDependencies())
        {
            Awake();
        }
    }

    public void StartMoney_Refresh()
    {
        if (FindDependencies())
        {
            ExtendedMaths.TMPTextVerify_InputField(startMoney_inp);

        }
    }

    public void StartHealth_Refresh()
    {
        if (FindDependencies())
        {
            ExtendedMaths.TMPTextVerify_InputField(startHealth_inp);

        }
    }

    public void SkyColour_Refresh()
    {
        if (FindDependencies())
        {
            ExtendedMaths.TMPTextVerify_InputField(skyColour_inp);

        }
    }


    public void StartMoney_Set()
    {
        if (FindDependencies())
        {
            int setVal = Mathf.RoundToInt(ExtendedMaths.GetNumFromInputField(startMoney_inp,  0, int.MaxValue));




            builderSaveLoadScr.startMoney = setVal;

            StartMoney_Get();


        }

    }

    public void StartHealth_Set()
    {
        if (FindDependencies())
        {
            int setVal = Mathf.RoundToInt(ExtendedMaths.GetNumFromInputField(startHealth_inp, 1, int.MaxValue));




            builderSaveLoadScr.startHealth = setVal;


            StartHealth_Get();

        }

    }

    public int StartMoney_Get()
    {
        int ret = 1;
        if (FindDependencies())
        {

            ret = builderSaveLoadScr.startMoney;
            startMoney_inp.text = ret.ToString();
            ExtendedMaths.TMPTextVerify_InputField(startMoney_inp);




        }
        return ret;
    }

    public int StartHealth_Get()
    {
        int ret = 1;
        if (FindDependencies() )
        {

            ret = builderSaveLoadScr.startHealth;
            startHealth_inp.text = ret.ToString();
            ExtendedMaths.TMPTextVerify_InputField(startHealth_inp);




        }
        return ret;
    }



    public string SkyColour_Get()
    {
        string ret = "000000";
        if (FindDependencies() )
        {



            ret = ExtendedMaths.ColorToHex( builderSaveLoadScr.skyColour);
            skyColour_inp.text = ret.ToString();
            ExtendedMaths.TMPTextVerify_InputField(skyColour_inp);
            skyColour_img.color = builderSaveLoadScr.skyColour;


        }
        return ret;
    }

    public void SkyColour_Set()
    {
        if (FindDependencies())
        {




            if (ExtendedMaths.IsItHex(ExtendedMaths.ReformatHex( skyColour_inp.text)) == true)
            {
                builderSaveLoadScr.skyColour = ExtendedMaths.HexToColor(skyColour_inp.text);
            }
            else
            {
                builderSaveLoadScr.skyColour = Color.black;
            }
            SkyColour_Get();




        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();



        if (builderSaveLoadScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
