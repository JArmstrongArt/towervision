﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;






public class HudCanvas : MonoBehaviour
{
    private GameObject towerIcon_prefab;
    private GameObject placableTower_prefab;

    private PlacableTower placeTowerScr;
    private PlayerStats statScr;
    [SerializeField] TextMeshProUGUI money_txt;
    [SerializeField] TextMeshProUGUI health_txt;
    private MatchManager matchManagerScr;
    [SerializeField] TextMeshProUGUI waveCounter_txt;
    [SerializeField] RectTransform minimapRect;
    private Vector2 minimapSize_orig;
    private Vector2 minimapSize_multipleMax = new Vector2(1.3f, 1.3f);

    private float scaleMinimapElement_lerp;
    private float scaleMinimapElement_lerp_previous;


    private List<GameObject> spawnedTowerIcons = new List<GameObject>();
    private List<GameObject> spawnedNextWaveMonsterIcons = new List<GameObject>();

    [SerializeField] GameObject minimapObj;

    private MinimapCamera minimapCamScr;

    private bool matchManagerLateAwake;
    // Start is called before the first frame update


    void MinimapResizeToFitCameraRenderingIt()
    {
        if (FindResources() && FindDependencies())
        {
            if (minimapObj != null)
            {
                if (minimapObj.GetComponent<RawImage>() != null)
                {
                    if (minimapObj.GetComponent<RectTransform>() != null)
                    {
                        if (minimapObj.GetComponent<RawImage>().texture != null)
                        {
                            int texWidth = minimapObj.GetComponent<RawImage>().texture.width;
                            int texHeight = minimapObj.GetComponent<RawImage>().texture.height;
                            float texAspect = (float)texWidth / (float)texHeight;
                            minimapObj.GetComponent<RectTransform>().sizeDelta = new Vector2(minimapObj.GetComponent<RectTransform>().sizeDelta.y * texAspect, minimapObj.GetComponent<RectTransform>().sizeDelta.y);
                        }
                    }
                }
            }
        }

    }

    void DeleteTowerIcons()
    {
        foreach(GameObject icon in spawnedTowerIcons)
        {
            if (icon != null)
            {
                Destroy(icon);
            }
        }
        spawnedTowerIcons.Clear();
    }

    void DeleteNextWaveMonsterIcons()
    {

        foreach (GameObject icon in spawnedNextWaveMonsterIcons)
        {
            if (icon != null)
            {
                Destroy(icon);
            }
        }
        spawnedNextWaveMonsterIcons.Clear();
    }

    void SetupTowerIcons()
    {
        if (FindResources() && FindDependencies())
        {
            DeleteTowerIcons();

            for (int i = 0; i < placeTowerScr.placableTowerList.Length; i++)
            {
                GameObject iconInst = Instantiate(towerIcon_prefab, gameObject.transform);
                spawnedTowerIcons.Add(iconInst);
                TowerIcon iconScr = iconInst.GetComponent<TowerIcon>();
                if (iconScr != null)
                {

                    GameObject realTowerPrefab = placeTowerScr.FindTowerByType_towerPrefab(placeTowerScr.placableTowerList[i].whatTowerIsThis);

                    if (realTowerPrefab != null)
                    {

                        TowerInherit relevantTowerInfo = realTowerPrefab.GetComponent<TowerInherit>();

                        if (relevantTowerInfo != null)
                        {
                            iconScr.cost_txt.text = "$" + relevantTowerInfo.cost.ToString();
                            ExtendedMaths.TMPTextVerify(iconScr.cost_txt);
                        }
                        


                        SetTheTowerIconImages();




                    }

                    RefreshTowerIconPos();
                }



            }


        }
    }








    // Update is called once per frame
    void OnRectTransformDimensionsChange()//this monodevelop function is called whenever the gameobject this script is attached to has its recttransform changed in any way
    {
        if (FindResources() && FindDependencies())
        {
            RefreshTowerIconPos();

        }

    }



    void RefreshTowerIconPos()
    {
        if (FindResources() && FindDependencies())
        {
            if (spawnedTowerIcons.Count > 0)
            {
                int iconIndex = 0;
                foreach(GameObject icon in spawnedTowerIcons)
                {
                    if (icon != null)
                    {
                        RectTransform iconRect = icon.GetComponent<RectTransform>();
                        if (iconRect != null)
                        {
                            iconRect.anchoredPosition = new Vector2(((gameObject.GetComponent<RectTransform>().sizeDelta.x / 2) + (iconRect.sizeDelta.x / 2) - (iconRect.sizeDelta.x * placeTowerScr.placableTowerList.Length)) + (iconRect.sizeDelta.x * iconIndex), -(gameObject.GetComponent<RectTransform>().sizeDelta.y / 2) + (iconRect.sizeDelta.y / 2));
                        }
                    }
                    iconIndex += 1;
                }

            }



            RectTransform money_txtRect = money_txt.gameObject.GetComponent<RectTransform>();
            if (money_txtRect != null)
            {
                if (spawnedTowerIcons.Count > 0)
                {
                    if (spawnedTowerIcons[0] != null && spawnedTowerIcons[spawnedTowerIcons.Count - 1] != null)
                    {
                        RectTransform iconRect_first = spawnedTowerIcons[0].GetComponent<RectTransform>();
                        RectTransform iconRect_last = spawnedTowerIcons[spawnedTowerIcons.Count - 1].GetComponent<RectTransform>();

                        if (iconRect_first != null && iconRect_last != null)
                        {
                            money_txtRect.anchoredPosition = new Vector2(Mathf.Lerp(iconRect_first.anchoredPosition.x, iconRect_last.anchoredPosition.x, 0.5f), Mathf.Lerp(iconRect_first.anchoredPosition.y, iconRect_last.anchoredPosition.y, 0.5f) + (iconRect_first.sizeDelta.y / 2) + (money_txtRect.sizeDelta.y / 2));
                        }
                    }
                }





            }
        }
    }

    void WaveProcessing_Counter()
    {
        if (FindResources() && FindDependencies())
        {
            if (matchManagerScr.FindDependencies())
            {
                if (matchManagerScr.currentWaveIndex + 1 <= matchManagerScr.matchData.allWaves.Length)
                {
                    waveCounter_txt.text = (matchManagerScr.currentWaveIndex + 1).ToString() + "/" + matchManagerScr.matchData.allWaves.Length.ToString();
                }
                else
                {
                    waveCounter_txt.text = matchManagerScr.matchData.allWaves.Length.ToString() + "/" + matchManagerScr.matchData.allWaves.Length.ToString();
                }
                ExtendedMaths.TMPTextVerify(waveCounter_txt);
            }

        }

    }



    void ScaleMinimapElement()
    {


        bool oneTowerVisible = false;
        foreach (TowerInherit tower in EntityTracker.ALL_TOWERS)
        {
            if (tower != null)
            {
                if (tower.iconScr != null)
                {
                    if (tower.iconScr.towerOnMinimap == true)
                    {
                        oneTowerVisible = true;
                        break;
                    }
                }

            }
        }
        float lerpProposed = (float)minimapCamScr.circleLifetime / (float)minimapCamScr.circleLifetime_orig;
        if (oneTowerVisible==true||(oneTowerVisible==false && lerpProposed<= scaleMinimapElement_lerp_previous))
        {
            scaleMinimapElement_lerp = (float)minimapCamScr.circleLifetime / (float)minimapCamScr.circleLifetime_orig;
        }

        minimapRect.sizeDelta = Vector2.Lerp(minimapSize_orig, new Vector2(minimapSize_orig.x*minimapSize_multipleMax.x, minimapSize_orig.y * minimapSize_multipleMax.y), scaleMinimapElement_lerp);


        scaleMinimapElement_lerp_previous = scaleMinimapElement_lerp;

    }


    void Update()
    {



        if (FindResources() && FindDependencies())
        {
            if (matchManagerLateAwake == false)
            {
                minimapSize_orig = minimapRect.sizeDelta;
                SetupTowerIcons();

                MinimapResizeToFitCameraRenderingIt();
                matchManagerLateAwake = true;
            }

            if (matchManagerLateAwake == true)
            {
                SetTheTowerIconImages();
                money_txt.text = "Money: $" + statScr.money.ToString();
                health_txt.text = "Health: " + Mathf.Ceil(statScr.health).ToString();
                ExtendedMaths.TMPTextVerify(money_txt);

                ExtendedMaths.TMPTextVerify(health_txt);
                WaveProcessing_Counter();
                ScaleMinimapElement();
            }


        }
    }





    void SetTheTowerIconImages()
    {
        if (FindResources() && FindDependencies())
        {
            if (spawnedTowerIcons.Count > 0)
            {

                int iconIndex = 0;
                foreach (GameObject icon in spawnedTowerIcons)
                {
                    if (icon != null)
                    {
                        Sprite iconImg_sprite_proposed = null;
                        GameObject relevantTowerInfo_obj = placeTowerScr.FindTowerByType_towerPrefab(placeTowerScr.placableTowerList[iconIndex].whatTowerIsThis);

                        if (relevantTowerInfo_obj != null)
                        {
                            TowerInherit relevantTowerInfo = relevantTowerInfo_obj.GetComponent<TowerInherit>();
                            if (relevantTowerInfo != null)
                            {

                                if (relevantTowerInfo.cost > statScr.money)
                                {

                                    iconImg_sprite_proposed = placeTowerScr.FindTowerByType_towerHUDIcon_grey(placeTowerScr.placableTowerList[iconIndex].whatTowerIsThis);
                                }
                                else
                                {
                                    iconImg_sprite_proposed = placeTowerScr.FindTowerByType_towerHUDIcon(placeTowerScr.placableTowerList[iconIndex].whatTowerIsThis);
                                }


                            }



                        }
                        TowerIcon towIconScr = icon.GetComponent<TowerIcon>();

                        if (towIconScr != null)
                        {
                            if (towIconScr.towerIcon_img != null)
                            {
                                if (iconImg_sprite_proposed == null)
                                {
                                    towIconScr.towerIcon_img.enabled = false;
                                }
                                else
                                {
                                    towIconScr.towerIcon_img.enabled = true;

                                }
                                towIconScr.towerIcon_img.sprite = iconImg_sprite_proposed;
                            }

                        }

                    }
                    iconIndex += 1;
                }
            }


        }
    }

    bool FindResources()
    {
        bool ret = false;
        if (towerIcon_prefab == null)
        {
            towerIcon_prefab = Resources.Load<GameObject>("Prefabs/towerIcon");
        }

        if (placableTower_prefab == null)
        {
            placableTower_prefab = Resources.Load<GameObject>("Prefabs/Towers/tower_placable");
        }


        
        if(towerIcon_prefab!=null && placableTower_prefab!=null)
        {
            ret = true;
        }
        return ret;
    }

    bool FindDependencies()
    {
        bool ret = false;
        if (statScr == null)
        {
            statScr = GameObject.FindObjectOfType<PlayerStats>();
        }

        if (placeTowerScr == null)
        {
            if(placableTower_prefab != null)
            {
                placeTowerScr = placableTower_prefab.GetComponent<PlacableTower>();
            }
        }

        if (matchManagerScr == null)
        {
            matchManagerScr = GameObject.FindObjectOfType<MatchManager>();
        }

        if (minimapCamScr == null)
        {
            minimapCamScr = GameObject.FindObjectOfType<MinimapCamera>();
        }

        if (statScr != null && placeTowerScr!=null && matchManagerScr!=null && minimapCamScr!=null)
        {
            ret = true;
        }

        return ret;
    }




}
