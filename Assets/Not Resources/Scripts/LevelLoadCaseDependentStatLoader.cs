﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoadCaseDependentStatLoader : MonoBehaviour
{
    private LifeTimer timerScr;
    private bool jobDone = false;

    [HideInInspector]
    public LevelData dataToLoadFrom;
    void Awake()
    {
        timerScr = gameObject.GetComponent<LifeTimer>();
        if (timerScr == null)
        {
            timerScr = gameObject.AddComponent<LifeTimer>();
        }
        timerScr.lifetime = 1.0f;
    }

    private void Update()
    {
        if (jobDone == false)
        {
            PlayerStats statsScr = GameObject.FindObjectOfType<PlayerStats>();
            if (statsScr != null)
            {

                statsScr.money = Mathf.Abs(dataToLoadFrom.startMoney);
                statsScr.health = Mathf.Abs(dataToLoadFrom.startHealth);
                if (statsScr.health <= 0)
                {
                    statsScr.health = 1;
                }

                jobDone = true;
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
