﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//this whole thing is just a way to pass the unity input system into one that accepts the entire controls being disabled with a bool, i pass this script around a lot of my projects.

//a struct that holds the info i may need about any given unity input
[Serializable]
public struct inputValue
{
    public string input_name;


    [HideInInspector]
    public float input_value;

    [HideInInspector]
    public float input_value_raw;

    public bool excludeFromDisabledInput;
    [HideInInspector]
    public float input_value_lerped;
    public float input_value_lerped_strength;



}

//a class that groups inputValue stuff basically just so that if i ever need to know if a group is active or not, i can do it very simply.
[Serializable]
public class inputGroup
{
    public string inputGroup_name;
    public inputValue[] inputsInGroup;

    public bool HasActivity()
    {
        for(int i = 0; i < inputsInGroup.Length; i++)
        {
            if (inputsInGroup[i].input_value != 0)
            {
                return true;
            }
        }
        return false;
    }
}

//the meat and potatoes
public class InputManager : MonoBehaviour
{
    public bool input_enabled;//the bool that lets me disable all controls in one flip-switch
    public inputGroup[] allInputValues;//an array of inputGroup objects that let me of course catalogue more than just one unity input and categorize the inputs by purpose




    // Update is called once per frame
    void Update()
    {
        //for every input i want to be getting
        for(int i = 0; i < allInputValues.Length; i++)
        {
            for(int j = 0; j < allInputValues[i].inputsInGroup.Length; j++)
            {
                if (allInputValues[i].inputGroup_name.ToUpper() != "DEBUG")
                {


                    if (input_enabled == true)
                    {

                        //set the input value to whatever unity has for us and set the lerped value to the same thing but it operates on a user defined lerp strength, i may remove this functionality i don't think this project will need it.
                        allInputValues[i].inputsInGroup[j].input_value = Input.GetAxis(allInputValues[i].inputsInGroup[j].input_name.ToString());
                        allInputValues[i].inputsInGroup[j].input_value_raw = Input.GetAxisRaw(allInputValues[i].inputsInGroup[j].input_name.ToString());
                        allInputValues[i].inputsInGroup[j].input_value_lerped = Mathf.Lerp(allInputValues[i].inputsInGroup[j].input_value_lerped, allInputValues[i].inputsInGroup[j].input_value, allInputValues[i].inputsInGroup[j].input_value_lerped_strength * Time.deltaTime);
                    }
                    else
                    {
                        if (allInputValues[i].inputsInGroup[j].excludeFromDisabledInput == false)
                        {
                            //set both the input value and its lerped counterpart to nothing immediately
                            allInputValues[i].inputsInGroup[j].input_value = 0;
                            allInputValues[i].inputsInGroup[j].input_value_raw = 0;
                            allInputValues[i].inputsInGroup[j].input_value_lerped = 0;
                        }
                        else
                        {
                            //set the input value to whatever unity has for us and set the lerped value to the same thing but it operates on a user defined lerp strength, i may remove this functionality i don't think this project will need it.
                            allInputValues[i].inputsInGroup[j].input_value = Input.GetAxis(allInputValues[i].inputsInGroup[j].input_name.ToString());
                            allInputValues[i].inputsInGroup[j].input_value_raw = Input.GetAxisRaw(allInputValues[i].inputsInGroup[j].input_name.ToString());
                            allInputValues[i].inputsInGroup[j].input_value_lerped = Mathf.Lerp(allInputValues[i].inputsInGroup[j].input_value_lerped, allInputValues[i].inputsInGroup[j].input_value, allInputValues[i].inputsInGroup[j].input_value_lerped_strength * Time.deltaTime);
                        }


                    }
                }
                else
                {
                    if (Constants.DEBUG_MODE == true)
                    {
                        allInputValues[i].inputsInGroup[j].input_value = Input.GetAxis(allInputValues[i].inputsInGroup[j].input_name.ToString());
                        allInputValues[i].inputsInGroup[j].input_value_raw = Input.GetAxisRaw(allInputValues[i].inputsInGroup[j].input_name.ToString());
                        allInputValues[i].inputsInGroup[j].input_value_lerped = Mathf.Lerp(allInputValues[i].inputsInGroup[j].input_value_lerped, allInputValues[i].inputsInGroup[j].input_value, allInputValues[i].inputsInGroup[j].input_value_lerped_strength * Time.deltaTime);
                    }
                    else
                    {
                        if(allInputValues[i].inputsInGroup[j].excludeFromDisabledInput == false)
                        {
                            allInputValues[i].inputsInGroup[j].input_value = 0;
                            allInputValues[i].inputsInGroup[j].input_value_raw = 0;
                            allInputValues[i].inputsInGroup[j].input_value_lerped = 0;
                        }
                        else
                        {
                            allInputValues[i].inputsInGroup[j].input_value = Input.GetAxis(allInputValues[i].inputsInGroup[j].input_name.ToString());
                            allInputValues[i].inputsInGroup[j].input_value_raw = Input.GetAxisRaw(allInputValues[i].inputsInGroup[j].input_name.ToString());
                            allInputValues[i].inputsInGroup[j].input_value_lerped = Mathf.Lerp(allInputValues[i].inputsInGroup[j].input_value_lerped, allInputValues[i].inputsInGroup[j].input_value, allInputValues[i].inputsInGroup[j].input_value_lerped_strength * Time.deltaTime);
                        }

                    }
                }


            }
        }
    }

    //the function other classes use once they get a reference to this class object in order to get input values.
    public float GetInputByName(string groupName, string inputName, bool lerped = false, bool raw = false)
    {
        float ret = 0;
        for(int i = 0; i < allInputValues.Length; i++)
        {
            for (int j = 0; j < allInputValues[i].inputsInGroup.Length; j++)
            {
                if (allInputValues[i].inputGroup_name == groupName)
                {
                    if (allInputValues[i].inputsInGroup[j].input_name == inputName)
                    {
                        if (raw == false)
                        {
                            if (lerped == false)
                            {
                                ret = allInputValues[i].inputsInGroup[j].input_value;
                            }
                            else
                            {
                                ret = allInputValues[i].inputsInGroup[j].input_value_lerped;
                            }
                        }
                        else
                        {
                            ret = allInputValues[i].inputsInGroup[j].input_value_raw;

                        }


                    }
                }

            }

        }
        return ret;
    }

    public int GetGroupIndexByName(string groupName)
    {
        int ret = -1;
        for(int i = 0; i < allInputValues.Length; i++)
        {
            if (allInputValues[i].inputGroup_name == groupName)
            {

                ret = i;
                break;
            }
        }
        return ret;
    }


}
