﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

//this script, childed from billboardcameracalcs, does the actual animating of the sprites using some of the values from billboardcameracalcs

//the possible ways an animation will repeat itself
[Serializable]
public enum BillboardAnimation_AnimType
{
    Once,
    Loop,
    GoToAnim

}

//this is where all 8 angles of a frame of animation are stored
[Serializable]
public struct BillboardAnimation_FrameAngles
{
    public Texture[] angles;//can technically be as big as needed, but will not check beyond 8 frames and shouldn't be any size BUT 8 frames, though 1 frame probably will be fine.
    public GameObject soundEffect_prefab;
    [HideInInspector]
    public GameObject soundEffect_inst;


}



//this class represents a single animation made up of frames which themselves are made up of angles
[Serializable]
public class SingleBillboardAnimation
{
    public string name;//the name of the animation used by other classes to play that animation

    public BillboardAnimation_FrameAngles[] frames;//make a list of frames for this
    public string goToAnim_destination;
    public BillboardAnimation_AnimType animType;
    public float playbackRate;//the higher this value is, the faster the animation will play

    [HideInInspector]
    public int frameIndex;//which frame to render

    [HideInInspector]
    public float nextFrameTimer;//how long until we go forward a frame in seconds

    [HideInInspector]
    public int angleIndex;//which angle of the current frame to render



    //this function keeps the angleindex within a very specific range, never going beyond an index of 7 regardless of array length but being allowed to do whatever below that
    public void ClampAngleIndex()
    {
        if (frames != null)
        {
            if (frames.Length > 0)
            {
                if (frameIndex > 0 && frameIndex < frames.Length)
                {
                    if (frames[frameIndex].angles != null)
                    {
                        if (frames[frameIndex].angles.Length > 0 && frames[frameIndex].angles.Length <= 8)
                        {
                            angleIndex = Mathf.Clamp(angleIndex, 0, frames[frameIndex].angles.Length - 1);
                            if (angleIndex > frames[frameIndex].angles.Length - 1)
                            {
                                angleIndex = frames[frameIndex].angles.Length - 1;
                            }
                        }
                        else if (frames[frameIndex].angles.Length > 8)
                        {
                            angleIndex = Mathf.Clamp(angleIndex, 0, 7);
                        }
                        else
                        {
                            angleIndex = 0;
                        }
                    }

                }
            }
        }




    }


    //returns true if the frame of the current animation is the last one
    public bool OnFinalFrame()
    {
        if (frameIndex >= frames.Length - 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //simply keeps the frame index within the valid length of all frames
    void ClampFrameIndex()
    {
        frameIndex = Mathf.Clamp(frameIndex, 0, frames.Length - 1);
    }







    //this function resets the animation entirely with no loose ends
    public void CleanStart()
    {
        frameIndex = 0;
        nextFrameTimer_reset();
    }

    //sets the nextframetimer to be inversely proportional to the playback rate
    public void nextFrameTimer_reset()
    {
        nextFrameTimer = (float)1/(float)playbackRate;
    }
}


public class BillboardAnimations : BillboardCameraCalcs
{
    [SerializeField] SingleBillboardAnimation[] animations;//every animation this entity has



    private bool playingAnimation;//true if the animation is NOT paused for whatever reason
    private int curAnimIndex=-1;//the index in the 'animations' array of the current animation playing


    public Color renderCol;
    [HideInInspector]
    public Color renderCol_orig;

    private Material billboardMat_inst;
    private Material billboardMat_asset;
    private Mesh billboardMesh_asset;
    private Texture frameMissingTex;
    private Texture frameEmptyTex;
    protected override void Awake()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.Awake();
            SetMat();
            renderCol_orig = renderCol;
            UpdateInstMat();
            UpdateMaterialTexture(true);

        }



    }

    void SetMat()
    {
        if(base.FindDependencies() && FindResources())
        {
            MeshRenderer rend = gameObject.GetComponent<MeshRenderer>();
            if (rend == null)
            {
                rend = gameObject.AddComponent<MeshRenderer>();
            }

            MeshFilter filt = gameObject.GetComponent<MeshFilter>();

            if (filt == null)
            {
                filt = gameObject.AddComponent<MeshFilter>();
            }

            filt.mesh = billboardMesh_asset;
            rend.material = billboardMat_asset;
        }

    }
    void UpdateInstMat()
    {
        if (base.FindDependencies() && FindResources())
        {
            if (gameObject.GetComponent<MeshRenderer>() != null)
            {
                if (billboardMat_inst == null || billboardMat_inst != gameObject.GetComponent<MeshRenderer>().material)
                {
                    billboardMat_inst = gameObject.GetComponent<MeshRenderer>().material;
                }

            }
        }


    }

    protected override void Update()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.Update();
            UpdateInstMat();

            AnimateControl();
            UpdateMaterialColour();
        }


    }



    //used with building towers, this function takes in a value (0 to 1 usually) and sets the y UV offset of the texture to it, to create a scrolling effect
    public void SetVisualBuild(float progress)
    {

        if (base.FindDependencies() && FindResources())
        {
            if (billboardMat_inst != null)
            {
                billboardMat_inst.SetTextureOffset("_MainTex", new Vector2(0, progress));

            }
        }


        
    }

    //this function controls the playback of the current animation
    void AnimateControl()
    {
        if (base.FindDependencies() && FindResources())
        {
            if (billboardMat_inst != null)
            {

                if(curAnimIndex>=0 && curAnimIndex < animations.Length)
                {
                    if (playingAnimation == true)
                    {

                        animations[curAnimIndex].nextFrameTimer -= Time.deltaTime;

                        if (animations[curAnimIndex].nextFrameTimer <= 0)
                        {

                            animations[curAnimIndex].nextFrameTimer_reset();//reset the timer that just ran out

                            if (animations[curAnimIndex].OnFinalFrame())//if the animation was on the final frame when this timer ran out
                            {
                                switch (animations[curAnimIndex].animType)
                                {
                                    case BillboardAnimation_AnimType.Loop:
                                        animations[curAnimIndex].frameIndex = 0;//go back to the start of the animation frames
                                        break;
                                    case BillboardAnimation_AnimType.Once:
                                        playingAnimation = false;//pause the animation
                                        break;

                                    case BillboardAnimation_AnimType.GoToAnim:
                                        int playResult = FindAnimationIndexByName(animations[curAnimIndex].goToAnim_destination);
                                        if (playResult < 0)
                                        {
                                            playingAnimation = false;
                                        }
                                        else
                                        {
                                            PlayAnimation(animations[curAnimIndex].goToAnim_destination);
                                        }
                                        break;





                                }
                            }
                            else
                            {
                                animations[curAnimIndex].frameIndex = animations[curAnimIndex].frameIndex + 1;//go to the next frame
                            }


                        }

                    }
                    animations[curAnimIndex].angleIndex = base.resultingAngleIndex;//this makes sure the angle being rendered is the one calculated to be correct by the parent class
                    animations[curAnimIndex].ClampAngleIndex();//this is not needed if the angle list for the frame is of length 8, but you never know.
                    UpdateMaterialTexture();
                }



            }
        }



    }

    

    void UpdateMaterialTexture(bool firstTimeMissingPrevent =false)
    {
        if (base.FindDependencies() && FindResources())
        {
            if (billboardMat_inst != null)
            {
                Texture targTex = frameMissingTex;
                if (firstTimeMissingPrevent)
                {
                    targTex = frameEmptyTex;
                }


                if (curAnimIndex>=0 && curAnimIndex < animations.Length)
                {
                    
                    if (animations[curAnimIndex].frames[animations[curAnimIndex].frameIndex].angles[animations[curAnimIndex].angleIndex] != null)
                    {
                        targTex = animations[curAnimIndex].frames[animations[curAnimIndex].frameIndex].angles[animations[curAnimIndex].angleIndex];

                        if (animations[curAnimIndex].frames[animations[curAnimIndex].frameIndex].soundEffect_prefab != null)
                        {
                            if (animations[curAnimIndex].frames[animations[curAnimIndex].frameIndex].soundEffect_prefab.GetComponent<GeneralSoundEffect>() != null)
                            {
                                if (animations[curAnimIndex].frames[animations[curAnimIndex].frameIndex].soundEffect_inst == null)
                                {
                                    animations[curAnimIndex].frames[animations[curAnimIndex].frameIndex].soundEffect_inst = Instantiate(animations[curAnimIndex].frames[animations[curAnimIndex].frameIndex].soundEffect_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);

                                }

                            }
                        }
                    }



                }

                if (billboardMat_inst.GetTexture("_MainTex") != targTex)
                {
                    billboardMat_inst.SetTexture("_MainTex", targTex);//actually render the texture that 2 scripts work together to figure out the right indexes for
                }

            }
        }




    }

    public void UpdateMaterialColour()
    {

        if (base.FindDependencies() && FindResources())
        {
            if (billboardMat_inst != null)
            {
                if(billboardMat_inst.GetColor("_Color")!= renderCol)
                {
                    billboardMat_inst.SetColor("_Color", renderCol);
                }
                

            }
        }


    }

    //retrieves the index of an animation in the 'animations' array based on its name
    int FindAnimationIndexByName(string animName)
    {


        int ret = -1;
        if (base.FindDependencies() && FindResources())
        {
            for (int i = 0; i < animations.Length; i++)
            {
                if (animations[i].name == animName)
                {
                    ret = i;
                    break;
                }
            }
        }


        return ret;
    }




    //other classes can use this function to play animations
    public void PlayAnimation(string animName)
    {

        if (base.FindDependencies() && FindResources())
        {

            if (billboardMat_inst != null)
            {
                int foundAnim = FindAnimationIndexByName(animName);
                if (foundAnim >= 0 && curAnimIndex!=foundAnim)//if foundAnim returned a valid value, i.e, the animation exists
                {
                    if (billboardMat_inst != null)//if a material exists to animate upon
                    {
                        animations[foundAnim].CleanStart();//in the likely event the animation finished mid-cycle, reset the animation at foundAnim so that it plays from the beginning properly
                        curAnimIndex = foundAnim;//set the current animation to the one retrieved in foundAnim


                        playingAnimation = true;//make sure this script knows we're not paused anymore in case the last animation set this to false
                    }

                }
            }
        }





    }

    bool FindResources()
    {
        bool ret = false;

        if (billboardMat_asset == null)
        {
            billboardMat_asset = Resources.Load<Material>("Materials/billboard");
        }

        if (billboardMesh_asset == null)
        {
            billboardMesh_asset = Resources.Load<Mesh>("Models/billboard");

        }

        if (frameMissingTex == null)
        {
            frameMissingTex = Resources.Load<Texture>("Textures/frameMissing");
        }

        if (frameEmptyTex == null)
        {
            frameEmptyTex = Resources.Load<Texture>("Textures/frameEmpty");
        }

        if (frameEmptyTex!=null && frameMissingTex != null && billboardMat_asset != null && billboardMesh_asset!=null)
        {
            ret = true;
        }

        return ret;
    }
}
