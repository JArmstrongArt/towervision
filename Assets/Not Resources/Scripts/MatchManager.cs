﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Reflection;

public class MatchManager : MonoBehaviour,IEditorFunctionality
{
    [HideInInspector]
    public MonsterPath matchData;
    public bool automaticWaves;
    [SerializeField] float timeBetweenEachWave;
    private float timeBetweenEachWave_orig;

    [HideInInspector]
    public int currentWaveIndex;
    private int currentMonsterGroupIndex;
    private int currentMonsterGroupSpawnIndex;

    private GameObject monsterInst;
    private float monsterDelayTimer;
    [HideInInspector]
    public bool waveInProgress;
    [HideInInspector]
    public List<MonsterInherit> allMonstersFromCurrentWave = new List<MonsterInherit>();//used to track if a waves monsters are all killed yet to allow the next wave to begin

    private bool monsterDelayBegan = false;

    [HideInInspector]
    public Dictionary<string, int> upcomingWaveMonsters_totalQuantityDict = new Dictionary<string, int>();//dictionaries, like lists, cannot be properties

    [HideInInspector]
    public bool nextWaveMonsterCanvas_setupComplete = false;

    [HideInInspector]
    public System.Type[] acceptableMonsterTypes;

    private Dictionary<string, GameObject> loadedMonsters = new Dictionary<string, GameObject>();

    private MusicManager musicScr;

    private int musicChance = 5;
    private int musicChance_result;
    private int musicSelected;
    private bool musicPlayed = false;
    private void Awake()
    {
        if (FindDependencies())
        {
            LoadRelevantMonsters();
            GenerateAcceptableMonsterTypes();

            timeBetweenEachWave_orig = timeBetweenEachWave;
            UpcomingWaveMonsterCount_Refresh();
            musicScr.PlaySong("ambience");
            musicChance_result = Random.Range(0, musicChance);

        }



    }

    void LoadRelevantMonsters()
    {
        if (FindDependencies())
        {
            for(int i = 0; i < matchData.allWaves.Length; i++)
            {
                for (int j = 0; j < matchData.allWaves[i].allMonsterGroups.Length; j++)
                {
                    string monsterDir = matchData.allWaves[i].allMonsterGroups[j].monster_prefabDirectory;
                    GameObject loadedMonster = Resources.Load<GameObject>(monsterDir);

                    if (loadedMonster != null)
                    {
                        loadedMonsters[monsterDir] = loadedMonster;
                    }
                }
            }
        }
    }

    GameObject FindLoadedMonster(string monsterDir)
    {
        if (loadedMonsters.ContainsKey(monsterDir))
        {
            return loadedMonsters[monsterDir];
        }
        else
        {
            return null;
        }
    }



    void GenerateAcceptableMonsterTypes()
    {
        if (FindDependencies())
        {

            List<System.Type> monType_list = ExtendedMaths.GetAllClassesThatAreSubclassOf(typeof(MonsterInherit));
            acceptableMonsterTypes = new System.Type[monType_list.Count];

            for (int i = 0; i < monType_list.Count; i++)
            {
                acceptableMonsterTypes[i] = monType_list[i];
            }
        }

    }

    public GameObject FindFirstInstanceOfMonsterOnUpcomingWave(System.Type curType)
    {
        GameObject ret = null;

        if (FindDependencies())
        {
            if (currentWaveIndex >= 0 && currentWaveIndex < matchData.allWaves.Length)
            {
                for (int i = 0; i < matchData.allWaves[currentWaveIndex].allMonsterGroups.Length; i++)
                {
                    GameObject monsObj = FindLoadedMonster(matchData.allWaves[currentWaveIndex].allMonsterGroups[i].monster_prefabDirectory);
                    if (monsObj != null)
                    {
                        
                        if (curType != null)
                        {
 

                            if (curType.IsSubclassOf(typeof(MonsterInherit)))
                            {
                                if (monsObj.GetComponent(curType) != null)
                                {
                                    ret = monsObj;
                                }
                            }
                        }
                    }
                }
            }
        }



        return ret;

    }

    void UpcomingWaveMonsterCount_Wipe()
    {
        if (FindDependencies())
        {
            upcomingWaveMonsters_totalQuantityDict.Clear();
        }
        
    }

    void UpcomingWaveMonsterCount_Refresh()
    {
        if (FindDependencies())
        {
            UpcomingWaveMonsterCount_Wipe();

            if (currentWaveIndex >= 0 && currentWaveIndex < matchData.allWaves.Length)
            {
                MonsterWave currentWave = matchData.allWaves[currentWaveIndex];
                for (int i = 0; i < currentWave.allMonsterGroups.Length; i++)
                {
                    GameObject currentMonsterGroupPrefab = FindLoadedMonster(currentWave.allMonsterGroups[i].monster_prefabDirectory);
                    if (currentMonsterGroupPrefab != null && upcomingWaveMonsters_totalQuantityDict != null)
                    {
                        if (currentWave.allMonsterGroups[i].amount > 0)
                        {

                            for (int j = 0; j < acceptableMonsterTypes.Length; j++)
                            {
                                if (currentMonsterGroupPrefab.GetComponent(acceptableMonsterTypes[j]) != null)
                                {
                                    if (acceptableMonsterTypes[j].IsSubclassOf(typeof(MonsterInherit)))
                                    {
                                        if (upcomingWaveMonsters_totalQuantityDict.ContainsKey(acceptableMonsterTypes[j].ToString()))
                                        {
                                            upcomingWaveMonsters_totalQuantityDict[acceptableMonsterTypes[j].ToString()] += currentWave.allMonsterGroups[i].amount;
                                        }
                                        else
                                        {
                                            upcomingWaveMonsters_totalQuantityDict[acceptableMonsterTypes[j].ToString()] = currentWave.allMonsterGroups[i].amount;
                                        }
                                    }
                                    else
                                    {

                                        if (upcomingWaveMonsters_totalQuantityDict.ContainsKey("/unknownmonster"))//i begin with a forward slash since i know a class name cannot have a forward slash in it, so its impossible i could ever accidentally make a class with this name
                                        {
                                            upcomingWaveMonsters_totalQuantityDict["/unknownmonster"] += currentWave.allMonsterGroups[i].amount;
                                        }
                                        else
                                        {
                                            upcomingWaveMonsters_totalQuantityDict["/unknownmonster"] = currentWave.allMonsterGroups[i].amount;
                                        }
                                        print(acceptableMonsterTypes[j].ToString() + "_not a subclass" + "   " + upcomingWaveMonsters_totalQuantityDict["/unknownmonster"].ToString());
                                    }


                                }
                                else
                                {

                                    bool acceptableMonsterTypeVerified = false;

                                    for (int k = 0; k < acceptableMonsterTypes.Length; k++)
                                    {
                                        if (currentMonsterGroupPrefab.GetComponent(acceptableMonsterTypes[k]) != null)
                                        {
                                            acceptableMonsterTypeVerified = true;
                                            break;
                                        }
                                    }

                                    if (acceptableMonsterTypeVerified == false)
                                    {
                                        if (upcomingWaveMonsters_totalQuantityDict.ContainsKey("/unknownmonster"))//i begin with a forward slash since i know a class name cannot have a forward slash in it, so its impossible i could ever accidentally make a class with this name
                                        {
                                            upcomingWaveMonsters_totalQuantityDict["/unknownmonster"] += currentWave.allMonsterGroups[i].amount;
                                        }
                                        else
                                        {
                                            upcomingWaveMonsters_totalQuantityDict["/unknownmonster"] = currentWave.allMonsterGroups[i].amount;
                                        }
                                    }




                                }
                            }

                        }


                    }
                }
            }
            nextWaveMonsterCanvas_setupComplete = true;
        }


    }

    private void Update()
    {
        if (FindDependencies())
        {
            if (currentWaveIndex < matchData.allWaves.Length)
            {
                if (currentMonsterGroupIndex < matchData.allWaves[currentWaveIndex].allMonsterGroups.Length)
                {
                    if (currentMonsterGroupSpawnIndex < matchData.allWaves[currentWaveIndex].allMonsterGroups[currentMonsterGroupIndex].amount)
                    {
                        if (waveInProgress == true)
                        {
                            if (musicPlayed == false)
                            {
                                if (musicChance_result == 0)
                                {
                                    if (musicSelected == 0)
                                    {
                                        musicScr.PlaySong("unsettling");

                                    }
                                    else
                                    {
                                        musicScr.PlaySong("spooky");

                                    }
                                }
                                musicPlayed = true;
                            }

                            
                            if (timeBetweenEachWave > 0 && automaticWaves == true)
                            {
                                timeBetweenEachWave -= Time.deltaTime;
                            }
                            else
                            {
                                if (monsterInst == null && monsterDelayBegan == false)
                                {
                                    GameObject monsObj = FindLoadedMonster(matchData.allWaves[currentWaveIndex].allMonsterGroups[currentMonsterGroupIndex].monster_prefabDirectory);
                                    if (monsObj != null)
                                    {
                                        if (monsObj.GetComponent<MonsterInherit>() != null)
                                        {
                                            if (matchData.allPathCorners.Length > 0)
                                            {
                                                if (matchData.allPathCorners[0] != null)
                                                {
                                                    monsterInst = Instantiate(monsObj, matchData.allPathCorners[0].position, Quaternion.Euler(Vector3.zero), null);
                                                    if (monsterInst.GetComponent<MonsterInherit>() != null)
                                                    {
                                                        monsterInst.GetComponent<MonsterInherit>().matchManageScr = this;
                                                        monsterInst.GetComponent<MonsterInherit>().pathScr = matchData;
                                                        allMonstersFromCurrentWave.Add(monsterInst.GetComponent<MonsterInherit>());
                                                    }
                                                    BillboardAnimations monsterBillScr = monsterInst.GetComponent<BillboardAnimations>();
                                                    if (monsterBillScr != null)
                                                    {
                                                        monsterBillScr.billboardWorldDir_set(matchData.allPathCorners[0].forward);
                                                    }

                                                }
                                            }


                                        }
                                    }



                                    if (currentMonsterGroupSpawnIndex >= matchData.allWaves[currentWaveIndex].allMonsterGroups[currentMonsterGroupIndex].amount - 1)
                                    {
                                        monsterDelayTimer = matchData.allWaves[currentWaveIndex].allMonsterGroups[currentMonsterGroupIndex].postSpawnDelay;
                                    }
                                    else
                                    {
                                        monsterDelayTimer = matchData.allWaves[currentWaveIndex].allMonsterGroups[currentMonsterGroupIndex].betweenSpawnDelay;
                                    }

                                    monsterDelayBegan = true;
                                }

                                if (monsterDelayBegan == true)//using this instead of monsterInst!=null because then the timer resets and you get repeat monsters if you kill a monster before the next one spawns.
                                {
                                    if (monsterDelayTimer > 0)
                                    {
                                        monsterDelayTimer -= Time.deltaTime;
                                    }

                                    if (monsterDelayTimer <= 0)
                                    {

                                        currentMonsterGroupSpawnIndex += 1;
                                        monsterDelayBegan = false;
                                        monsterInst = null;
                                    }
                                }
                            }

                        }

                    }
                    else
                    {
                        currentMonsterGroupIndex += 1;
                        currentMonsterGroupSpawnIndex = 0;
                    }
                }
                else
                {
                    if (allMonstersFromCurrentWave.Count <= 0)//if monsters remain by the time this point in the condition is reached, then wait until theyre all gone to trigger the after wave effect, be it going into the waiting period or automatically moving on
                    {
                        
                        musicScr.PlaySong("ambience");

                        musicChance_result = Random.Range(0, musicChance);
                        musicSelected = Random.Range(0, 2);
                        musicPlayed = false;
                        if (automaticWaves == false)
                        {
                            waveInProgress = false;
                        }

                        timeBetweenEachWave = timeBetweenEachWave_orig;
                        currentWaveIndex += 1;
                        currentMonsterGroupIndex = 0;
                        UpcomingWaveMonsterCount_Refresh();

                        NextWaveMonsterCanvas monstCanvScr = GameObject.FindObjectOfType<NextWaveMonsterCanvas>();
                        if (monstCanvScr != null)
                        {
                            monstCanvScr.SetupNextWaveMonsterIcons();
                        }
                    }

                }
            }
            else
            {
                if (allMonstersFromCurrentWave.Count <= 0)//waits until the final wave is cleared of enemies
                {
                    UpcomingWaveMonsterCount_Wipe();
                }

            }
        }


    }

    public void EditorUpdate()
    {
        gameObject.name = "MatchManager";

    }

    

    public bool FindDependencies()
    {
        bool ret = false;

        matchData = GameObject.FindObjectOfType<MonsterPath>();

        musicScr = GameObject.FindObjectOfType<MusicManager>();

        if (musicScr!=null &&matchData != null)
        {
            ret = true;
        }

        return ret;
    }

    
}
