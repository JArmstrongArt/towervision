﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowdownTower : TowerInherit
{
    [SerializeField] float attackRateSprayExtensionMultiple;
    private float attackRateSprayExtensionMultiple_multiple = 1.0f;
    [HideInInspector]
    public bool sprayMode = false;

    private GameObject slowdownTowerFrost_prefab;
    private GameObject slowdownTowerFrost_inst;
    private float frostDistanceFromFace = 1.0f;
    private ParticleSystem slowdownTowerFrost_pS;
    private float frostHeight = 0.5f;
    private bool lateAwakeComplete = false;
    protected override void Awake()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.Awake();

            base.animScr.PlayAnimation("close");

        }
        
    }

    protected override void Update()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.Update();
            if (buildingProgress >= 1)
            {
                if (lateAwakeComplete == false)
                {
                    slowdownTowerFrost_inst = Instantiate(slowdownTowerFrost_prefab, gameObject.transform.position + ((base.animScr.billboardWorldDir_get().normalized) * frostDistanceFromFace) + new Vector3(0, frostHeight, 0), Quaternion.Euler(Vector3.zero), null);

                    slowdownTowerFrost_inst.transform.forward = base.animScr.billboardWorldDir_get().normalized;


                    slowdownTowerFrost_pS = slowdownTowerFrost_inst.GetComponent<ParticleSystem>();
                    SlowdownTowerFrost frostScr = slowdownTowerFrost_inst.GetComponent<SlowdownTowerFrost>();
                    if (frostScr != null)
                    {
                        frostScr.slowdownScr = this;
                    }
                    lateAwakeComplete = true;
                }
            }
        }
        
    }
    protected override bool NearestMonsterInRange()//the attack timer in the base class only runs if this function returns true, and i want this tower to always be atatcking, so i do this
    {
        return true;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (FindResources() && base.FindDependencies())
        {

            if (slowdownTowerFrost_inst != null)
            {
                Destroy(slowdownTowerFrost_inst);
            }
        }

    }


    protected override void Attack()
    {
        if(FindResources() && base.FindDependencies())
        {
            sprayMode = !sprayMode;
            if (sprayMode)
            {
                base.attackRate = base.attackRate_orig * (attackRateSprayExtensionMultiple * attackRateSprayExtensionMultiple_multiple);
                base.animScr.PlayAnimation("open");
                if (slowdownTowerFrost_pS != null)
                {

                    slowdownTowerFrost_pS.Play();
                }

            }
            else
            {
                base.attackRate = base.attackRate_orig;
                base.animScr.PlayAnimation("close");
                if (slowdownTowerFrost_pS != null)
                {
                    slowdownTowerFrost_pS.Stop();
                }
            }
        }

        
    }

    protected override void UpgradeEffectUpdate()
    {
        if (FindResources() && base.FindDependencies())
        {
            attackRateSprayExtensionMultiple_multiple = base.upgrades[base.upgradeLevel - 1].optionalParameter1;
        }

    }

    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;

        if (slowdownTowerFrost_prefab == null)
        {
            slowdownTowerFrost_prefab = Resources.Load<GameObject>("Particles/pS_slowdownFrost");
        }

        if (slowdownTowerFrost_prefab != null && parRet==true)
        {
            ret = true;
        }

        return ret;
    }
}
