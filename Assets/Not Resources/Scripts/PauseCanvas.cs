﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseCanvas : MonoBehaviour
{
    private PlayerActions actionScr;
    private OptionsCanvas optionsCanvScr;

    public void QuitGame()
    {
        if (FindDependencies())
        {

            if (LevelSaveLoad.folderToLoad != LevelSaveLoadFolder.TEST)
            {
                SceneManager.LoadScene("MainMenu");

            }
            else
            {
                LevelBuilderSaveLoad.resumePreviousSession = true;

                SceneManager.LoadScene("LevelEditor");

            }

        }

    }

    public void OptionsMenu()
    {

        if (FindDependencies())
        {
            Canvas canv = optionsCanvScr.gameObject.GetComponent<Canvas>();
            Canvas myCanv = gameObject.GetComponent<Canvas>();
            if (canv != null && myCanv!=null)
            {
                canv.enabled = true;
                canv.sortingOrder = myCanv.sortingOrder + 1;
                optionsCanvScr.closeButton = true;
            }

        }
    }

    public void ResumeGame()
    {
        if (FindDependencies())
        {
            actionScr.pauseSet(false);

        }
    }

    bool FindDependencies()
    {
        bool ret = false;

        actionScr = GameObject.FindObjectOfType<PlayerActions>();
        optionsCanvScr = GameObject.FindObjectOfType<OptionsCanvas>();
        if (actionScr != null && optionsCanvScr != null)
        {
            ret = true;
        }
        return ret;
    }
}
