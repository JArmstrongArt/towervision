﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TMProInputFieldWorkaround : MonoBehaviour
{

    private TMP_InputField source;
    [HideInInspector]
    public TextMeshProUGUI dest;
    private Color origSourceCol;
    // Start is called before the first frame update
    void Awake()
    {
        source = gameObject.GetComponent<TMP_InputField>();
        origSourceCol = ((TextMeshProUGUI)source.textComponent).color;
    }

    private void OnDestroy()
    {
        if (dest != null)
        {
            if (dest.gameObject != null)
            {
                Destroy(dest.gameObject);
            }

        }

        
    }
    // Update is called once per frame
    void Update()
    {
        if (source != null)
        {
            if (source.textComponent != null)
            {
                if (dest == null)
                {

                    dest = Instantiate(((TextMeshProUGUI)source.textComponent).gameObject, ((TextMeshProUGUI)source.textComponent).transform.position, ((TextMeshProUGUI)source.textComponent).transform.rotation, ((TextMeshProUGUI)source.textComponent).transform.parent).GetComponent<TextMeshProUGUI>();
                    
                }
                else
                {
                    ExtendedMaths.TMPTextVerify(dest);
                    ((TextMeshProUGUI)source.textComponent).color = new Color(1,1,1,0);
                    dest.color = origSourceCol;
                }
            }

        }
    }
}
