﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWinLose : MonoBehaviour
{
    [HideInInspector]
    public bool gameWon;
    [HideInInspector]
    public bool gameLost;

    private PlayerStats statScr;
    private MatchManager matchManageScr;
    private InputManager inputScr;

    private GameObject winCanvasObj;
    private GameObject loseCanvasObj;

    private PlayerActions actionScr;

    private void Awake()
    {
        SetGameWonLost(false, false);
    }


    private void Update()
    {
        if (FindDependencies())
        {




            if (matchManageScr.FindDependencies())
            {

                if (statScr.health <= 0)
                {
                    SetGameWonLost(gameWon, true);
                }

                if (matchManageScr.currentWaveIndex >= matchManageScr.matchData.allWaves.Length)
                {
                    SetGameWonLost(true, gameLost);


                }


            }




        }
    }

    void SetGameWonLost(bool gW=false, bool gL=false)
    {
        if (FindDependencies())
        {
            gameWon = gW;
            gameLost = gL;

            if (gameWon == true)
            {
                gameLost = false;


            }


            if (winCanvasObj.activeSelf != gameWon)
            {
                winCanvasObj.SetActive(gameWon);
            }

            if (loseCanvasObj.activeSelf != gameLost)
            {
                loseCanvasObj.SetActive(gameLost);
            }

            if (gameLost == true || gameWon == true)
            {
                inputScr.input_enabled = false;
            }
            else
            {
                inputScr.input_enabled = true;
            }

        }


    }

    bool FindDependencies()
    {
        bool ret = false;

        if (statScr == null)
        {
            statScr = gameObject.GetComponent<PlayerStats>();
        }

        if (matchManageScr == null)
        {
            matchManageScr = GameObject.FindObjectOfType<MatchManager>();
        }

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (winCanvasObj == null)
        {
            if (GameObject.FindObjectOfType<WinCanvas>() != null)
            {
                winCanvasObj = GameObject.FindObjectOfType<WinCanvas>().gameObject;
            }
            
        }

        if (loseCanvasObj == null)
        {
            if (GameObject.FindObjectOfType<LoseCanvas>() != null)
            {
                loseCanvasObj = GameObject.FindObjectOfType<LoseCanvas>().gameObject;
            }
        }

        actionScr = gameObject.GetComponent<PlayerActions>();

        if (actionScr!=null && winCanvasObj != null && loseCanvasObj!=null && statScr != null && matchManageScr!=null && inputScr!=null)
        {
            ret = true;
        }

        return ret;
    }
}
