﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class LevelSelectCanvasLevelLoader : MonoBehaviour,IEditorFunctionality
{
    private LevelSelectCanvas levSelectScr;
    private List<GameObject> levelEntries = new List<GameObject>();

    private bool canvasActive = false;

    [HideInInspector]
    public int levelLimit;

    private GameObject exampleEntry;
    bool FindDependencies()
    {
        bool ret = false;
        levSelectScr = GameObject.FindObjectOfType<LevelSelectCanvas>();
        
        if (levSelectScr != null)
        {
            ret = true;
        }

        return ret;

    }
    void Update()
    {

        if (FindDependencies())
        {
            if (canvasActive == false)
            {
                exampleEntry = levSelectScr.levelEntryObj;
                RefreshLevelList();
                canvasActive = true;
            }
            
        }
        else
        {
            canvasActive = false;
        }


    }

    public void SelectedLevelVisualUpdate()
    {

        foreach(GameObject entry in levelEntries)
        {
            LevelSelectCanvasLevelEntry entryScr = entry.GetComponent<LevelSelectCanvasLevelEntry>();

            if (entryScr != null)
            {
                entryScr.selectEntryBtn_img.color = new Color(0, 0, 0, 0);

                if (entryScr.associatedFileInfo != null)
                {

                    if (entryScr.associatedFileInfo.Name.ToUpper().Replace("." + LevelSaveLoad.fileType.ToString().ToUpper(), "") == LevelSaveLoad.levelToLoad.ToUpper())
                    {
                        entryScr.selectEntryBtn_img.color = new Color(1, 0, 0, 0.5f);
                    }
                }
            }
        }
    }

    

    

    public void RefreshLevelList(int levelOffset = 0)
    {
        if (FindDependencies())
        {
            if (exampleEntry != null)
            {
                exampleEntry.gameObject.SetActive(false);
                if (ExtendedMaths.ListTrueEmpty(levelEntries) == false)
                {
                    foreach (GameObject entry in levelEntries)
                    {
                        Destroy(entry);
                    }
                }
                levelEntries = new List<GameObject>();
                string loadPath = Application.dataPath + "/" + "levels" + "/" + LevelSaveLoad.SaveLoadFolderToString(LevelSaveLoad.folderToLoad) + "/";
                List<FileInfo> allLevelPaths_levelsOnly = ExtendedMaths.LoadAllLevelsInFolder(loadPath);
                levelLimit = allLevelPaths_levelsOnly.Count - 1;

                levelOffset = Mathf.Clamp(levelOffset, 0, levelLimit);

                if (ExtendedMaths.ListTrueEmpty(allLevelPaths_levelsOnly))
                {
                    levSelectScr.noLevelsObj.SetActive(true);
                    LevelSaveLoad.levelToLoad = "";
                }
                else
                {
                    levSelectScr.noLevelsObj.SetActive(false);


                    for (int i = levelOffset; i < allLevelPaths_levelsOnly.Count; i++)
                    {
                        GameObject currentEntryObj = currentEntryObj = Instantiate(levSelectScr.levelEntryObj, levSelectScr.levelEntryObj.transform.position, levSelectScr.levelEntryObj.transform.rotation, levSelectScr.levelEntryObj.transform.parent);
                        currentEntryObj.SetActive(true);
                        RectTransform entryRect = currentEntryObj.GetComponent<RectTransform>();
                        float yAddition = 0;
                        if (entryRect != null)
                        {

                            yAddition = entryRect.sizeDelta.y * (i-levelOffset);

                            entryRect.anchoredPosition -= new Vector2(0, yAddition);
                        }

                        LevelSelectCanvasLevelEntry entryScr = currentEntryObj.GetComponent<LevelSelectCanvasLevelEntry>();



                        if (entryScr != null)
                        {
                            if (allLevelPaths_levelsOnly[i] != null)
                            {
                                entryScr.associatedFileInfo = allLevelPaths_levelsOnly[i];

                            }
                            entryScr.associatedLevelData = LevelSaveLoad.LoadLevel(allLevelPaths_levelsOnly[i].Name.ToUpper().Replace("." + LevelSaveLoad.fileType.ToString().ToUpper(), ""), LevelSaveLoad.folderToLoad, false);


                        }

                        levelEntries.Add(currentEntryObj);

                        if (entryScr != null)
                        {
                            if (i == levelOffset)
                            {
                                entryScr.SetLevelToLoad();
                            }


                        }

                    }
                    SelectedLevelVisualUpdate();
                }
            }


        }

    }

    public void EditorUpdate()
    {
        gameObject.name = "levelSelectCanvasLevelLoader";
    }
}
