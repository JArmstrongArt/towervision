﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public enum TowerTypes
{
    Standard,
    Medic,
    Slowdown,
    Spread,
    Blast
}

[Serializable]
public struct TowerInfo
{
    public TowerTypes whatTowerIsThis;
    public GameObject towerPrefab;
    public Sprite towerHUDIcon;
    public Sprite towerHUDIcon_grey;
}





public class PlacableTower : MonoBehaviour
{
    public TowerInfo[] placableTowerList;
    
    private BillboardAnimations animScr;
    private GameObject circleObj;
    private PlayerActions actionScr;
    [HideInInspector]
    public bool refreshCircleCheck;
    // Start is called before the first frame update
    void Awake()
    {
        animScr = gameObject.GetComponent<BillboardAnimations>();
        GenerateCircle();
    }


    
    public GameObject FindTowerByType_towerPrefab(TowerTypes towerT)
    {
        GameObject ret = null;

        for(int i = 0; i < placableTowerList.Length; i++)
        {
            if(placableTowerList[i].whatTowerIsThis == towerT)
            {

                if (placableTowerList[i].towerPrefab != null)
                {
                    ret = placableTowerList[i].towerPrefab;
                    
                    break;
                }
            }
        }

        return ret;
    }

    public Sprite FindTowerByType_towerHUDIcon(TowerTypes towerT)
    {
        Sprite ret = null;

        for (int i = 0; i < placableTowerList.Length; i++)
        {
            if (placableTowerList[i].whatTowerIsThis == towerT)
            {
                if (placableTowerList[i].towerHUDIcon != null)
                {
                    ret = placableTowerList[i].towerHUDIcon;
                    break;
                }
            }
        }

        return ret;
    }

    public Sprite FindTowerByType_towerHUDIcon_grey(TowerTypes towerT)
    {
        Sprite ret = null;

        for (int i = 0; i < placableTowerList.Length; i++)
        {
            if (placableTowerList[i].whatTowerIsThis == towerT)
            {
                if (placableTowerList[i].towerHUDIcon != null)
                {
                    ret = placableTowerList[i].towerHUDIcon_grey;
                    break;
                }
            }
        }

        return ret;
    }
    



    // Update is called once per frame
    void Update()
    {
        if (FindDependencies())
        {


            if (animScr != null)
            {
                switch (actionScr.towerToPlace)
                {
                    case TowerTypes.Standard:
                        animScr.PlayAnimation("standard");
                        break;
                    case TowerTypes.Medic:
                        animScr.PlayAnimation("medic");
                        break;
                    case TowerTypes.Slowdown:
                        animScr.PlayAnimation("slowdown");
                        break;
                    case TowerTypes.Spread:
                        animScr.PlayAnimation("spread");
                        break;
                    case TowerTypes.Blast:
                        animScr.PlayAnimation("blast");
                        break;
                }
            }

            if (refreshCircleCheck == true)
            {
                RefreshCircle();
                refreshCircleCheck = false;
            }
        }

        




    }

    private void OnDestroy()
    {
        DeleteCircle();
    }

    void DeleteCircle()
    {
        if (circleObj != null)
        {
            Destroy(circleObj);
            circleObj = null;
            
        }
    }

    void RefreshCircle()
    {
        DeleteCircle();
        GenerateCircle();
    }

    void GenerateCircle()
    {
        if (FindDependencies())
        {
            if (circleObj == null)
            {

                GameObject relevantPrefab = FindTowerByType_towerPrefab(actionScr.towerToPlace);

                if (relevantPrefab != null)
                {

                    EntityInherit entScr = relevantPrefab.GetComponent<EntityInherit>();
                    if (entScr != null)
                    {
                        float circleRadius = entScr.awarenessRadius;

                        circleObj = CircleDraw.DrawCircle(gameObject, circleRadius, 12);//draw the circle representing the radius around this tower
                    }
                }


            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        if (actionScr == null)
        {
            actionScr = GameObject.FindObjectOfType<PlayerActions>();
        }

        if (actionScr != null)
        {
            ret = true;
        }
        return ret;
    }
}
