﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour
{
    private GameObject playerObj;
    private float minimapCamHeight = 500.0f;
    private Camera camScr;

    [HideInInspector]
    public float circleLifetime;//it being public overrules an inline definition, so adding =0.1f to this would still make it equal to 0 since thats the default value of a float.
    [HideInInspector]
    public float circleLifetime_orig;
    [HideInInspector]
    public float lifetimeLerpVal;
    private bool lateAwake=false;
    // Start is called before the first frame update
    void Awake()
    {
        lateAwake = false;

    }

    private void Update()
    {
        if (FindDependencies())
        {
            if (lateAwake == false)
            {
                circleLifetime = 1.3f;
                circleLifetime_orig = circleLifetime;
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, minimapCamHeight, gameObject.transform.position.z);
                lateAwake = true;
            }
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (FindDependencies())
        {
            if (lateAwake == true)
            {
                Vector3 destPos = new Vector3(playerObj.transform.position.x, minimapCamHeight, playerObj.transform.position.z);
                gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, destPos, 5 * Time.deltaTime);

                camScr.farClipPlane = minimapCamHeight * 2;
                lifetimeLerpVal = 1 - ((float)circleLifetime / (float)circleLifetime_orig);

                if (circleLifetime > 0)
                {
                    circleLifetime -= Time.deltaTime;
                }
                else
                {
                    circleLifetime = circleLifetime_orig;
                }
            }

        }
    }

    bool FindDependencies()
    {
        bool ret = false;
        playerObj = GameObject.FindGameObjectWithTag("playerCam");
        camScr = gameObject.GetComponent<Camera>();
        if (playerObj != null && camScr != null)
        {
            ret = true;
        }
        return ret;
    }
}
