﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicTower : TowerInherit
{

    private float awarenessRadius_orig;
    private Vector3 circleLocalScale = Vector3.one;
    [SerializeField] float healPerAttackExecute;
    [SerializeField] Color healCol;
    private List<TowerInherit> relevantTowers = new List<TowerInherit>();

    [SerializeField] float selfHealBaseMutliple;
    protected override bool NearestMonsterInRange()
    {
        return true;
    }
    protected override void Update()
    {
        if (base.FindResources() && base.FindDependencies())
        {
            base.Update();
            if (base.buildingProgress >= 1)
            {

                base.animScr.PlayAnimation("open");
            }
        }

    }

    void GetRelevantTowers()
    {
        if (base.FindResources() && base.FindDependencies())
        {
            relevantTowers.Clear();
            foreach (TowerInherit tower in EntityTracker.ALL_TOWERS)
            {
                if (tower != null)
                {
                    if (Vector3.Distance(gameObject.transform.position, tower.gameObject.transform.position) <= base.awarenessRadius)
                    {
                        if (tower.gameObject.GetComponent<MedicTower>() == null)
                        {
                            if (tower.buildingProgress >= 1)
                            {
                                relevantTowers.Add(tower);
                            }
                            
                        }

                    }
                }

            }
        }

    }

    protected override void Awake()
    {
        if (base.FindResources() && base.FindDependencies())
        {
            base.Awake();
            awarenessRadius_orig = base.awarenessRadius;
            if (base.circleObj != null)
            {
                circleLocalScale = base.circleObj.transform.localScale;
            }

        }

        
    }

    protected override void OnDestroy()
    {
        if (base.FindResources() && base.FindDependencies())
        {
            base.OnDestroy();

            GetRelevantTowers();
            foreach (TowerInherit tower in relevantTowers)
            {

                BillboardAnimations animScr = tower.gameObject.GetComponent<BillboardAnimations>();
                if (animScr != null)
                {

                    animScr.renderCol = animScr.renderCol_orig;

                }
            }
        }

    }
    protected override void Attack()
    {
        if (base.FindResources() && base.FindDependencies())
        {
            GetRelevantTowers();


            foreach (TowerInherit tower in relevantTowers)
            {
                tower.ShiftHealth(Mathf.Abs( healPerAttackExecute));
                BillboardAnimations animScr = tower.gameObject.GetComponent<BillboardAnimations>();
                if (animScr != null)
                {

                    animScr.renderCol = healCol;


                }
            }

            ShiftHealth(Mathf.Abs( healPerAttackExecute) * selfHealBaseMutliple);


        }

    }

    protected override void UpgradeEffectUpdate()
    {
        if (base.FindResources() && base.FindDependencies())
        {
            base.awarenessRadius = awarenessRadius_orig * base.upgrades[base.upgradeLevel - 1].optionalParameter1;

            if (base.circleObj != null)
            {
                base.circleObj.transform.localScale = new Vector3(circleLocalScale.x * base.upgrades[base.upgradeLevel - 1].optionalParameter1, circleLocalScale.y * base.upgrades[base.upgradeLevel - 1].optionalParameter1, circleLocalScale.z * base.upgrades[base.upgradeLevel - 1].optionalParameter1);
            }
        }

    }



}
