﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum SpawnMode
{
    MainMenu,
    LevelEditor
}
public class LevelSelectCanvas : MonoBehaviour
{
    public GameObject levelEntryObj;

    public SpawnMode spawnEnum;

    private int levelsToDisplayOffset = 0;
    private LevelSelectCanvasLevelLoader levLoaderScr;

    public GameObject noLevelsObj;
    // Start is called before the first frame update


    public void ShiftLevelsToDisplay(int shiftAmount)
    {
        if (FindDependencies())
        {
            if(levelsToDisplayOffset+shiftAmount<= levLoaderScr.levelLimit && levelsToDisplayOffset + shiftAmount >= 0)
            {
                levLoaderScr.RefreshLevelList(levelsToDisplayOffset);

            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        levLoaderScr = GameObject.FindObjectOfType<LevelSelectCanvasLevelLoader>();

        if (levLoaderScr != null)
        {
            ret = true;
        }

        return ret;
    }

}
