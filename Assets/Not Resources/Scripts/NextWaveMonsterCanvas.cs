﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class NextWaveMonsterCanvas : MonoBehaviour
{





    private MatchManager matchManagerScr;

    [SerializeField] TextMeshProUGUI waveStatus_txt;

    private GameObject nextWaveMonsterIcon_prefab;


    private List<GameObject> spawnedNextWaveMonsterIcons = new List<GameObject>();
    [SerializeField] Sprite nextWaveMonster_hudIcon_unknown;
    [SerializeField] RectTransform nextWave_txt_rect;

    private bool fakeAwake = false;
    void DeleteNextWaveMonsterIcons()
    {

        foreach (GameObject icon in spawnedNextWaveMonsterIcons)
        {
            if (icon != null)
            {
                Destroy(icon);
            }
        }
        spawnedNextWaveMonsterIcons.Clear();
    }

    public void SetupNextWaveMonsterIcons()
    {
        if (FindResources() && FindDependencies())
        {
            DeleteNextWaveMonsterIcons();

            for(int i = 0; i < matchManagerScr.acceptableMonsterTypes.Length; i++)
            {
                SetupNextWaveMonsterIcons_IndividualIcon(matchManagerScr.acceptableMonsterTypes[i].ToString());
            }
           
            SetupNextWaveMonsterIcons_IndividualIcon("/unknownmonster");
            RefreshNextWaveMonsterIconPos();
        }
    }
    void SetupNextWaveMonsterIcons_IndividualIcon(string relevantMonsterClass)
    {
        if (FindResources() && FindDependencies())
        {
            Type relevantMonsterClass_asType = Type.GetType(relevantMonsterClass);
            if (relevantMonsterClass_asType != null || relevantMonsterClass == "/unknownmonster")
            {
                if (matchManagerScr.upcomingWaveMonsters_totalQuantityDict != null)
                {
                    if (matchManagerScr.upcomingWaveMonsters_totalQuantityDict.ContainsKey(relevantMonsterClass.ToString()))
                    {

                        if (matchManagerScr.upcomingWaveMonsters_totalQuantityDict[relevantMonsterClass.ToString()] > 0)
                        {
                            Type monsterInheritType = typeof(MonsterInherit);
                            if (monsterInheritType != null)
                            {
                                bool knownMonster = false;
                                if (relevantMonsterClass_asType != null)
                                {
                                    if (relevantMonsterClass_asType.IsSubclassOf(monsterInheritType))//known
                                    {
                                        knownMonster = true;
                                    }
                                }



                                GameObject iconInst = Instantiate(nextWaveMonsterIcon_prefab, gameObject.transform);
                                spawnedNextWaveMonsterIcons.Add(iconInst);
                                NextWaveMonsterIcon iconScr = iconInst.GetComponent<NextWaveMonsterIcon>();
                                if (iconScr != null)
                                {
                                    if (knownMonster == false)
                                    {
                                        iconScr.monster_img.sprite = nextWaveMonster_hudIcon_unknown;

                                    }
                                    else
                                    {
                                        GameObject monsterPrefab = matchManagerScr.FindFirstInstanceOfMonsterOnUpcomingWave(relevantMonsterClass_asType);
                                        if (monsterPrefab != null)
                                        {
                                            MonsterInherit inhScr = monsterPrefab.GetComponent<MonsterInherit>();
                                            if (inhScr != null)
                                            {
                                                iconScr.monster_img.sprite = inhScr.nextWaveMonster_hudIcon;
                                            }

                                        }
                                    }

                                    iconScr.monsterQuantity_txt.text = matchManagerScr.upcomingWaveMonsters_totalQuantityDict[relevantMonsterClass.ToString()].ToString();
                                    ExtendedMaths.TMPTextVerify(iconScr.monsterQuantity_txt);
                                }


                            }

                        }

                    }

                }
            }
        }



    }

    void RefreshNextWaveMonsterIconPos()
    {
        if (FindResources() && FindDependencies())
        {
            if (spawnedNextWaveMonsterIcons.Count > 0)
            {
                int iconIndex = 0;
                foreach (GameObject icon in spawnedNextWaveMonsterIcons)
                {
                    if (icon != null)
                    {
                        RectTransform iconRect = icon.GetComponent<RectTransform>();
                        if (iconRect != null)
                        {
                            float startPos_x = (-iconRect.sizeDelta.x * 0.5f) * (spawnedNextWaveMonsterIcons.Count - 1);
                            iconRect.anchoredPosition = new Vector2(startPos_x + (iconRect.sizeDelta.x * iconIndex), (float)gameObject.GetComponent<RectTransform>().sizeDelta.y / (float)2.554191666666667f);
                        }
                    }
                    iconIndex += 1;
                }
                //nextWave_txt_rect.anchoredPosition = new Vector2(0, (float)gameObject.GetComponent<RectTransform>().sizeDelta.y / (float)2.25f);
                if (nextWave_txt_rect.transform.GetSiblingIndex() < nextWave_txt_rect.transform.parent.childCount - 1)
                {
                    nextWave_txt_rect.SetAsLastSibling();

                }
            }


        }
    }

    void WaveProcessing_NextWaveTitle()
    {
        if (FindResources() && FindDependencies())
        {
            if (matchManagerScr.FindDependencies())
            {
                TextMeshProUGUI nextWave_txt = nextWave_txt_rect.GetComponent<TextMeshProUGUI>();
                if (nextWave_txt != null)
                {
                    if (matchManagerScr.currentWaveIndex < matchManagerScr.matchData.allWaves.Length)
                    {
                        nextWave_txt.enabled = true;
                        if (matchManagerScr.waveInProgress == false)
                        {
                            nextWave_txt.text = "NEXT WAVE";
                        }
                        else
                        {
                            nextWave_txt.text = "THIS WAVE";
                        }
                        ExtendedMaths.TMPTextVerify(nextWave_txt);
                    }
                    else
                    {
                        nextWave_txt.enabled = false;
                    }

                }
            }

        }

    }



    void OnRectTransformDimensionsChange()//this monodevelop function is called whenever the gameobject this script is attached to has its recttransform changed in any way
    {
        if (FindResources() && FindDependencies())
        {

            RefreshNextWaveMonsterIconPos();
        }

    }

    void Update()
    {



        if (FindResources() && FindDependencies())
        {
            if (fakeAwake == false)
            {
                if (matchManagerScr.nextWaveMonsterCanvas_setupComplete == true)
                {
                    SetupNextWaveMonsterIcons();
                    fakeAwake = true;
                }
            }
            WaveProcessing_Status();
            WaveProcessing_NextWaveTitle();

        }
    }


    void WaveProcessing_Status()
    {
        if (FindResources() && FindDependencies())
        {
            if (matchManagerScr.FindDependencies())
            {
                string waveStatus_txt_text_proposed = "";

                if (matchManagerScr.currentWaveIndex < matchManagerScr.matchData.allWaves.Length)
                {
                    if (matchManagerScr.waveInProgress == true)
                    {
                        if (matchManagerScr.automaticWaves == false)
                        {
                            waveStatus_txt_text_proposed = "AUTOMATIC WAVES: DISABLED (PRESS f TO TOGGLE)";
                            waveStatus_txt.color = Color.red;
                        }
                        else
                        {
                            waveStatus_txt_text_proposed = "AUTOMATIC WAVES: ENABLED (PRESS f TO TOGGLE)";
                            waveStatus_txt.color = Color.green;
                        }
                    }
                    else
                    {
                        waveStatus_txt_text_proposed = "press the space bar to begin the next wave";
                        waveStatus_txt.color = Color.white;
                    }
                }
                else
                {
                    waveStatus_txt_text_proposed = "";
                    waveStatus_txt.color = Color.white;
                }

                waveStatus_txt.text = waveStatus_txt_text_proposed.ToUpper();
                ExtendedMaths.TMPTextVerify(waveStatus_txt);

            }

        }

    }

    bool FindResources()
    {
        bool ret = false;



        if (nextWaveMonsterIcon_prefab == null)
        {
            nextWaveMonsterIcon_prefab = Resources.Load<GameObject>("Prefabs/nextWaveMonsterIcon");
        }

        if ( nextWaveMonsterIcon_prefab != null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindDependencies()
    {
        bool ret = false;




        if (matchManagerScr == null)
        {
            matchManagerScr = GameObject.FindObjectOfType<MatchManager>();
        }

        if ( matchManagerScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
