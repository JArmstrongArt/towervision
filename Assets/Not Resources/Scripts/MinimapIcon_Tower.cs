﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapIcon_Tower : MinimapIcon
{
    [HideInInspector]
    public TowerInherit towerScr;
    [HideInInspector]
    public bool towerOnMinimap;
    private Texture iconTex_tower;
    private MinimapCamera minimapScr;
    private LineRenderer existingMinimapLineRenderer;
    private Vector2 scaleRange = new Vector2(0, 400);
    protected override void Update()
    {

        if (FindDependencies() && FindResources())
        {
            base.Update();
            base.iconTex = iconTex_tower;


            if (base.myRend != null)
            {
                if (base.myRend.material != null)
                {
                    float triggerHealth = ((float)towerScr.health_max / (float)100) * 50;
                    if (towerScr.health_current <= triggerHealth)
                    {
                        
                        towerOnMinimap = true;
                        float curHealth = Mathf.Clamp(towerScr.health_current, 0, triggerHealth);
                        float lerpVal = (float)(triggerHealth - curHealth) / triggerHealth;



                        float lerpVal_transparency = Mathf.Lerp(1, 0, minimapScr.lifetimeLerpVal);
                        Color iconCol = Color.Lerp(Color.yellow, Color.red, lerpVal);
                        iconCol = new Color(iconCol.r, iconCol.g, iconCol.b, base.currentIconCol.a);



                        if (existingMinimapLineRenderer != null)
                        {
                            if (existingMinimapLineRenderer.material != null)
                            {
                                if (existingMinimapLineRenderer.material.HasProperty("_Color"))
                                {
                                    existingMinimapLineRenderer.material.SetColor("_Color", iconCol);
                                    existingMinimapLineRenderer.material.EnableKeyword("_EMISSION");
                                    if (existingMinimapLineRenderer.material.HasProperty("_EmissionColor"))
                                    {
                                        existingMinimapLineRenderer.material.SetColor("_EmissionColor", iconCol);
                                    }
                                }
                            }
                        }

                        base.currentIconCol = iconCol;





                        if (minimapScr.circleLifetime <= 0)
                        {
                            if (base.minimapIconObj != null)
                            {


                                GameObject circlePos = new GameObject();

                                circlePos.transform.position = new Vector3(base.minimapIconObj.gameObject.transform.position.x, base.minimapIconObj.gameObject.transform.position.y - 2, base.minimapIconObj.gameObject.transform.position.z);
                                GameObject resultingCircle = CircleDraw.DrawCircle(circlePos, 0.05f, 16, Mathf.Lerp(scaleRange.x, scaleRange.y, lerpVal), minimapScr.circleLifetime_orig);
                                resultingCircle.layer = LayerMask.NameToLayer("MinimapExclusive");

                                existingMinimapLineRenderer = resultingCircle.GetComponent<LineRenderer>();



                                Destroy(circlePos);
                            }


                        }

                    }
                    else
                    {

                        towerOnMinimap = false;
                    }
                    base.myRend.enabled = towerOnMinimap;



                }
            }
        }



    }

    protected override bool FindDependencies()
    {
        bool parRet = base.FindDependencies();
        bool ret = false;




        if (minimapScr == null)
        {
            minimapScr = GameObject.FindObjectOfType<MinimapCamera>();
        }


        if (minimapScr != null && parRet == true)
        {
            ret = true;
        }

        return ret;
    }

    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;

        if (iconTex_tower == null)
        {
            iconTex_tower = Resources.Load<Texture>("Textures/MinimapIcons/tower");
        }

        if (iconTex_tower != null && parRet==true)
        {
            ret = true;
        }
        return ret;
    }
}
