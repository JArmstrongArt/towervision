﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class WinCanvas : MonoBehaviour
{

    public void Quit()
    {
        if (LevelSaveLoad.folderToLoad != LevelSaveLoadFolder.TEST)
        {
            SceneManager.LoadScene("MainMenu");

        }
        else
        {
            LevelBuilderSaveLoad.resumePreviousSession = true;
            SceneManager.LoadScene("LevelEditor");

        }
    }
    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name.ToString());
    }
}
