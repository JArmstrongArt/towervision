﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
[System.Serializable]
struct StoryPage
{
    public Sprite storyPic;
    public string storyText;

    public StoryPage(Sprite pic,string text)
    {
        storyPic = pic;
        storyText = text;
    }
}
public class StoryCanvas : MonoBehaviour
{
    [SerializeField] StoryPage[] allStoryPages;
    private int storyPageIndex = 0;
    [SerializeField] TextMeshProUGUI story_txt;
    [SerializeField] Image story_img;
    private bool navAxisUsed = false;
    private InputManager inputScr;
    [SerializeField] int charactersPerLine;
    private void Awake()
    {
        if (FindDependencies())
        {
            RenderStory();
        }

    }

    private void Update()
    {
        if (FindDependencies())
        {
            float nav = inputScr.GetInputByName("Story Navigation", "StoryNav");
            if (nav != 0)
            {
                if (navAxisUsed == false)
                {
                    if (nav < 0)
                    {

                        ShiftPages(-1);
                    }
                    else
                    {
                        ShiftPages(1);

                    }
                    navAxisUsed = true;
                }
            }else
            {
                navAxisUsed = false;
            }
        }

    }
    public void ShiftPages(int pageCount)
    {
        if (FindDependencies())
        {
            if (pageCount == 1)//if only going forward by one
            {
                if (storyPageIndex + pageCount >= allStoryPages.Length)
                {

                    SceneManager.LoadScene("LevelArea");
                }
            }

            storyPageIndex = Mathf.Clamp(storyPageIndex + pageCount, 0, allStoryPages.Length - 1);
            RenderStory();
        }

    }

    void RenderStory()
    {
        if (FindDependencies())
        {
            if (storyPageIndex >= 0 && storyPageIndex < allStoryPages.Length)
            {
                story_img.sprite = allStoryPages[storyPageIndex].storyPic;
                story_txt.text = allStoryPages[storyPageIndex].storyText;
            }
            else
            {
                story_img.sprite = null;
                story_txt.text = "";
            }
            ExtendedMaths.TMPTextVerify(story_txt, charactersPerLine);
        }


    }

    bool FindDependencies()
    {
        bool ret = false;

        inputScr = gameObject.GetComponent<InputManager>();
        if (inputScr != null)
        {
            ret = true;
        }


        return ret;
    }
}
