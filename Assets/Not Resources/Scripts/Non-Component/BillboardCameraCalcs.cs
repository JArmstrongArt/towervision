﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this is the class that lets a billboard-rendered entity figure out how to do the fake 3D DOOM-style sprite stuff
public class BillboardCameraCalcs : MonoBehaviour
{
    private Vector3 billboardWorldDir;//the ACTUAL direction the object would be facing if it were 3d since the billboard will be facing the camera, required for figuring out the viewing angle to render

    private GameObject mainCam_obj;

    private GameObject lookFromCam_obj;
    [HideInInspector]
    public int resultingAngleIndex;//the sprite index in the angles[] array in the _FrameAngles class of BillboardAnimations to render
    // Start is called before the first frame update
    protected virtual void Awake()
    {
        billboardWorldDir_set(Vector3.right);//by default, just so it doesn't look wrong, set the object to be looking in the Vector3.right direction.

    }

    


    void OnDestroy()
    {
        //remove the new GameObject()s made in this script
        if (lookFromCam_obj != null)
        {
            Destroy(lookFromCam_obj);
        }
    }



    // Update is called once per frame
    protected virtual void Update()
    {

        if (FindDependencies())
        {
            //make the actual physical plane of the sprite look at the camera without tilting up and down
            gameObject.transform.LookAt(new Vector3(mainCam_obj.transform.position.x, gameObject.transform.position.y, mainCam_obj.transform.position.z));

            if (lookFromCam_obj == null)
            {
                lookFromCam_obj = new GameObject();
                lookFromCam_obj.name = gameObject.name.ToString() + "_lookFromCam";
            }

            if (lookFromCam_obj != null)
            {
                //use the lookFromCam obj to calculate a dot product similarity between where the plane of the entity is facing vs where the camera is facing if it were looking right at the entity
                lookFromCam_obj.transform.position = mainCam_obj.transform.position;
                lookFromCam_obj.transform.LookAt(new Vector3(gameObject.transform.position.x, lookFromCam_obj.transform.position.y, gameObject.transform.position.z));

                Vector3 lookFromCam = lookFromCam_obj.transform.forward;
                Vector3 lookFromCam_right = lookFromCam_obj.transform.right;
                float billboardWorldDir_lookFromCam_similarity = Vector3.Dot(billboardWorldDir, lookFromCam);
                float billboardWorldDir_lookFromCam_similarity_right = Vector3.Dot(billboardWorldDir, lookFromCam_right);


                //you will see many 'magic numbers' here, such as 0.92f, 0.38f, etc.
                //these are actually my best approximated guesses at eigths for dot product values through the 360 degree spin cycle of the plane because of the maximum 8 angles of sprite being rendered per entity
                //this slightly awkward set of if-elses determine by comparing both left/right and up/down dot products which angle needs to be rendered based on the camera position
                if (Mathf.Abs(billboardWorldDir_lookFromCam_similarity) >= 0.92f)//up/down sprites
                {
                    if (billboardWorldDir_lookFromCam_similarity < 0)
                    {
                        resultingAngleIndex = 0;
                    }
                    else
                    {
                        resultingAngleIndex = 4;
                    }
                }
                else
                {
                    if (billboardWorldDir_lookFromCam_similarity_right < 0)//left sprites
                    {
                        if(billboardWorldDir_lookFromCam_similarity<=-0.38f && billboardWorldDir_lookFromCam_similarity > -0.92f)
                        {
                            resultingAngleIndex = 1;
                        }

                        if (billboardWorldDir_lookFromCam_similarity <= 0.38f && billboardWorldDir_lookFromCam_similarity > -0.38f)
                        {
                            resultingAngleIndex = 2;
                        }

                        if (billboardWorldDir_lookFromCam_similarity > 0.38f && billboardWorldDir_lookFromCam_similarity < 0.92f)
                        {
                            resultingAngleIndex = 3;
                        }
                    }
                    else//right sprites
                    {
                        if (billboardWorldDir_lookFromCam_similarity <= -0.38f && billboardWorldDir_lookFromCam_similarity > -0.92f)
                        {
                            resultingAngleIndex = 7;
                        }

                        if (billboardWorldDir_lookFromCam_similarity <= 0.38f && billboardWorldDir_lookFromCam_similarity > -0.38f)
                        {
                            resultingAngleIndex = 6;
                        }

                        if (billboardWorldDir_lookFromCam_similarity > 0.38f && billboardWorldDir_lookFromCam_similarity < 0.92f)
                        {
                            resultingAngleIndex = 5;
                        }
                    }
                }

            }
            
        }
        
    }

    public void billboardWorldDir_set(Vector3 dir)
    {
        Vector3 billboardWorldDir_proposed = new Vector3(dir.x, 0.0f, dir.z);
        billboardWorldDir_proposed = billboardWorldDir_proposed.normalized;
        billboardWorldDir = new Vector3(billboardWorldDir_proposed.x, billboardWorldDir_proposed.y, billboardWorldDir_proposed.z);//i don't want billboards to have any upwards or downards direction to them in the world, so y is 0.
    }

    public Vector3 billboardWorldDir_get()
    {
        return billboardWorldDir;
    }

    protected bool FindDependencies()
    {
        bool ret = false;

        if (mainCam_obj == null)
        {
            mainCam_obj = GameObject.FindGameObjectWithTag("playerCam");
        }

        if (mainCam_obj != null)
        {
            ret = true;
        }
        return ret;
    }
}
