﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//i created these lists that are automatically updated with each new tower and monster so that functions that need to access all of a certin type at once don't need to do a costly findall type function.
public class EntityTracker
{
    public static List<TowerInherit> ALL_TOWERS = new List<TowerInherit>();
    public static List<MonsterInherit> ALL_MONSTERS = new List<MonsterInherit>();
    public static List<Vector3> ALL_TOWER_PLACEMENTS = new List<Vector3>();
}
