﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


//a struct containing all the neccesary info to an upgrade
[Serializable]
public struct TowerUpgradeLevel
{
    public int cost;
    public float optionalParameter1;
    public float optionalParameter2;
}




//the class that parents all the types of towers and controls them
public abstract class TowerInherit : EntityInherit
{
    [HideInInspector]
    public MinimapIcon_Tower iconScr;
    private InputManager inputScr;
    public TowerUpgradeLevel[] upgrades;//an array of all upgrades in order
    [HideInInspector]
    public int upgradeLevel;
    protected GameObject monster_nearest;//a gameobject that holds the nearest monster that is also in range of the tower, otherwise it is null
    [SerializeField] protected float attackRate;//the time in seconds that the tower will retrigger its attack function
    protected float attackRate_orig;

    protected GameObject circleObj;//the object that, when not null, holds the scripts needed to draw the radius circle of the tower

    [HideInInspector]
    public float buildingProgress;//0 to 1 value, 1 being a complete building.
    [SerializeField] float timeTakenToBuild;//how long, in seconds, buildingProgress will take to go from 0 to 1
    private float timeTakenToBuild_current;//how far into timeTakenToBuild this is in seconds
    public int cost;//how much it costs to build this tower
    private PlayerActions playerActionScr;

    private PlayerWinLose winLoseScr;

    private GameObject towerSmoke_prefab;
    private GameObject towerSmoke_inst;
    private ParticleSystem towerSmoke;
    private float smokeScale = 0.16322f;

    // Start is called before the first frame update


    protected override void Awake()
    {
        if(FindDependencies() && FindResources())
        {
            base.Awake();
            towerSmoke_inst = Instantiate(towerSmoke_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
            towerSmoke = towerSmoke_inst.GetComponent<ParticleSystem>();
            base.animScr.PlayAnimation("idle");
            health_current = health_max;

            timeTakenToBuild = Mathf.Abs(timeTakenToBuild);//problems will arise if this value is negative
            timeTakenToBuild_current = timeTakenToBuild;
            attackRate_orig = attackRate;




            //put this tower in the list of all exsiting towers
            if (!EntityTracker.ALL_TOWERS.Contains(this))
            {
                EntityTracker.ALL_TOWERS.Add(this);
            }

            //put this towers position in the list of all taken tower positions
            if (!EntityTracker.ALL_TOWER_PLACEMENTS.Contains(gameObject.transform.position))
            {
                EntityTracker.ALL_TOWER_PLACEMENTS.Add(gameObject.transform.position);
            }
        }





    }




    protected virtual void OnDestroy()
    {

        if (towerSmoke_inst != null)
        {
            Destroy(towerSmoke_inst);
        }
        if (EntityTracker.ALL_TOWERS.Contains(this))
        {
            EntityTracker.ALL_TOWERS.Remove(this);
        }

        if (EntityTracker.ALL_TOWER_PLACEMENTS.Contains(gameObject.transform.position))
        {
            EntityTracker.ALL_TOWER_PLACEMENTS.Remove(gameObject.transform.position);
        }
        if(FindDependencies() && FindResources())
        {

                Instantiate(explosion_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);

        }

        if (circleObj != null)
        {
            Destroy(circleObj);//destroy the circle representing this tower if it exists
        }
    }
    void BuildTime()
    {
        if (FindDependencies() && FindResources())
        {
            buildingProgress = Mathf.Abs(-1 + (timeTakenToBuild_current / timeTakenToBuild));//buildingProgress is converted to a progress value from 0 to 1 based on the values of how long this building takes to build and how long its spent doing that building so far.

            if (timeTakenToBuild_current > 0)//if time remains on the building
            {
                timeTakenToBuild_current -= Time.deltaTime;//reduce the time by deltaTime
                base.animScr.SetVisualBuild(1 - buildingProgress);//see BillboardAnimations for more, but basically just sets a UV y value of the material to the inverse of buildingProgress so that the texture rises up along with the building progress as the build animation.
            }
            else
            {
                buildingProgress = 1;//make sure building progress is exactly 1.
                base.animScr.SetVisualBuild(0);//make sure the UV y of the material is now set to 0 as if it were unaltered.
            }
        }

        
    }
    protected virtual void CircleUpdate()
    {
        if (FindDependencies() && FindResources())
        {
            if (playerActionScr.dominantRay == DominantRay.BuildInteract && playerActionScr.towerInteractRay.ray_surface == gameObject)//if the player's dominant ray in the playeraction script is the interaction with towers ray AND that ray is looking at this tower
            {
                if (circleObj == null)
                {
                    circleObj = CircleDraw.DrawCircle(gameObject, awarenessRadius, 12);//draw the circle representing the radius around this tower
                }
            }
            else
            {
                if (circleObj != null)
                {
                    Destroy(circleObj);//destroy the circle representing this tower if it exists
                }
            }
        }
    }


    //this function figures out what you need to multiply transform.right by in order to get the correct alignment with the fake 3d sprite for a bullet animation
    protected float FigureOutShootRightMultiple()
    {

        float rightExtent = 1.0f;//i dont know how to get the unity unit size of a mesh from end to end, but i do know that my billboard mesh is exactly 2 units end to end.
        float retVal = 0.0f;
        if (FindDependencies() && FindResources())
        {
            int resAngIndex_WorkableRange = Mathf.Clamp(base.animScr.resultingAngleIndex, 0, 7);

            switch (resAngIndex_WorkableRange)
            {
                case 0:
                    retVal = 0.0f;
                    break;
                case 1:
                    retVal = ((float)rightExtent / (float)2);
                    break;
                case 2:
                    retVal = rightExtent;
                    break;
                case 3:
                    retVal = ((float)rightExtent / (float)2);
                    break;
                case 4:
                    retVal = 0.0f;
                    break;
                case 5:
                    retVal = -(float)rightExtent / (float)2;
                    break;
                case 6:
                    retVal = -rightExtent;
                    break;
                case 7:
                    retVal = -(float)rightExtent / (float)2;
                    break;
            }
        }


        return retVal;
    }



    protected virtual void Update()
    {
        if(FindDependencies() && FindResources())
        {
            if (towerSmoke_inst != null)
            {
                if(towerSmoke_inst.transform.localScale!= new Vector3(smokeScale, smokeScale, smokeScale))
                {
                    towerSmoke_inst.transform.localScale = new Vector3(smokeScale, smokeScale, smokeScale);
                }
            }
            if (towerSmoke != null)
            {
                if (health_current <= health_max * 0.5f)
                {
                    if (towerSmoke.isPlaying == false)
                    {
                        towerSmoke.Play();
                    }
                }
                else
                {
                    if (towerSmoke.isPlaying == true)
                    {
                        towerSmoke.Stop();
                    }
                }
            }



            if (iconScr == null)
            {
                if (gameObject.GetComponent<MinimapIcon_Tower>() == null)
                {
                    iconScr = gameObject.AddComponent<MinimapIcon_Tower>();
                }
                else
                {
                    iconScr = gameObject.GetComponent<MinimapIcon_Tower>();
                }
            }
            else
            {
                iconScr.towerScr = this;
            }




            if (buildingProgress >= 1)//if the tower has finished building
            {
                base.invulnerable = false;
                if (NearestMonsterInRange())//if a monster exists and is in range of the tower
                {
                    RunAttackTimer();//count down the attackRate variable to trigger the Attack() function in the children of TowerInherit, one that must be there by law of abstract functions.
                }

                CircleUpdate();
            }
            else
            {
                base.invulnerable = true;
                BuildTime();//if the tower isnt finished building, then keep going til it is
            }


            if (upgradeLevel - 1 >= 0 && upgradeLevel - 1 <= upgrades.Length - 1)
            {
                UpgradeEffectUpdate();
            }


            if (inputScr.GetInputByName("DEBUG", "DBG_NEARDEATHTOWERS") > 0)
            {
                health_current = 0.1f;
            }
        }



    }

    //this function counts down and every X seconds it will trigger the attack function in the children of towerinherit using the abstract function Attack() in this class that ensures children have the function too.
    void RunAttackTimer()
    {
        if(FindDependencies() && FindResources())
        {
            attackRate -= Time.deltaTime;
            if (attackRate <= 0)
            {
                if(winLoseScr.gameLost==false && winLoseScr.gameWon == false)
                {
                    attackRate = attackRate_orig;
                    Attack();
                    
                }

            }
        }

    }
    protected abstract void Attack();
    protected abstract void UpgradeEffectUpdate();//this function runs in the update loop of this class, and is an abstract required in the children to say how upgrading the tower affects them

    //Returns true if at least one monster exists and is within the range of the tower
    protected virtual bool NearestMonsterInRange()
    {
        if(FindDependencies() && FindResources())
        {
            FindNearestOpposingMonster();
            if (monster_nearest != null)
            {
                if (Vector3.Distance(monster_nearest.transform.position, gameObject.transform.position) <= base.awarenessRadius)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }


    //finds the nearest tower if one exists
    void FindNearestOpposingMonster()
    {
        if ( FindDependencies() && FindResources())
        {
            float closestDist = float.MaxValue;
            GameObject mon_nearest_proposed = null;

            List<MonsterInherit> relevantMonsters = FindAllMonstersInRange();

            if (relevantMonsters != null)
            {
                if (relevantMonsters.Count > 0)
                {
                    foreach (MonsterInherit mon in relevantMonsters)
                    {
                        if (mon != null)
                        {
                            GameObject monsObj = mon.gameObject;
                            if (Vector3.Distance(monsObj.transform.position, gameObject.transform.position) < closestDist && monsObj != gameObject)
                            {
                                closestDist = Vector3.Distance(monsObj.transform.position, gameObject.transform.position);
                                mon_nearest_proposed = monsObj;

                            }
                        }


                    }
                }
            }


            monster_nearest = mon_nearest_proposed;
        }


    }

    List<MonsterInherit> FindAllMonstersInRange()
    {
        List<MonsterInherit> ret = null;
        if ( FindDependencies() && FindResources())
        {
            

            if (EntityTracker.ALL_MONSTERS != null)
            {
                if (EntityTracker.ALL_MONSTERS.Count > 0)
                {
                    ret = new List<MonsterInherit>();
                    foreach (MonsterInherit mon in EntityTracker.ALL_MONSTERS)
                    {
                        if (mon != null)
                        {
                            GameObject monsObj = mon.gameObject;
                            if (Vector3.Distance(monsObj.transform.position, gameObject.transform.position) <= awarenessRadius && monsObj != gameObject)
                            {
                                ret.Add(mon);

                            }
                        }


                    }
                }
            }

        }
        return ret;
    }




    protected override bool FindDependencies()
    {
        bool parRet = base.FindDependencies();
        bool ret = false;


        if (playerActionScr == null)
        {
            playerActionScr = GameObject.FindObjectOfType<PlayerActions>();
        }

        if (winLoseScr == null)
        {
            winLoseScr = GameObject.FindObjectOfType<PlayerWinLose>();

        }


        if (inputScr == null)
        {
            inputScr = GameObject.FindObjectOfType<InputManager>();
        }

        if (winLoseScr!=null && playerActionScr!=null &&inputScr!=null && parRet==true)
        {
            ret = true;
        }

        return ret;
    }

    protected override bool FindResources()
    {
        bool parRet= base.FindResources();
        bool ret = false;

        if (towerSmoke_prefab == null) 
        {
            towerSmoke_prefab = Resources.Load<GameObject>("Particles/pS_towerSmoke");
        }

        if (towerSmoke_prefab != null && parRet==true)
        {
            ret = true;
        }
        return ret;

    }



}
