﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditorCanvasInputToggles : MonoBehaviour
{
    public void ToggleInput(int tog)
    {
        InputManager inputScr = GameObject.FindObjectOfType<InputManager>();
        bool enableTog = false;
        if (tog >= 1)
        {
            enableTog = true;
        }
        if (inputScr != null)
        {
            inputScr.input_enabled = enableTog;
        }
    }
}
