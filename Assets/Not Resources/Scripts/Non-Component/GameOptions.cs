﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public struct OptionBundle
{
    public int resX;
    public int resY;
    public float musicVol;
    public float soundEffectsVol;
    public bool fullscreen;

    public OptionBundle(int rX = GameOptions.minRes_X, int rY = GameOptions.minRes_Y, float mV = 0.0f, float sEV = 0.0f, bool fM = false)
    {
        resX = rX;
        resY = rY;
        musicVol = mV;
        soundEffectsVol = sEV;
        fullscreen = fM;
    }
}
public class GameOptions : MonoBehaviour
{
    public const int minRes_X = 320;
    public const int maxRes_X = 1920;
    public const int minRes_Y = 240;
    public const int maxRes_Y = 1080;

    public static AudioMixer mixer = null;
    public static void SavePlayerPrefs(OptionBundle bund)
    {
        PlayerPrefs.SetInt("resX", bund.resX);
        PlayerPrefs.SetInt("resY", bund.resY);
        PlayerPrefs.SetFloat("musicVol", bund.musicVol);
        PlayerPrefs.SetFloat("soundEffectsVol", bund.soundEffectsVol);
        PlayerPrefs.SetInt("fullscreen", bund.fullscreen ? 1 : 0);
        PlayerPrefs.Save();
    }

    public static OptionBundle LoadPlayerPrefs()
    {
        OptionBundle defaultBundle = new OptionBundle(GameOptions.minRes_X);

        int fullsc_int = PlayerPrefs.GetInt("fullscreen", defaultBundle.fullscreen ? 1 : 0);
        bool fullsc = ExtendedMaths.IntToBool(fullsc_int);


        Screen.SetResolution(PlayerPrefs.GetInt("resX", defaultBundle.resX), PlayerPrefs.GetInt("resY", defaultBundle.resY), fullsc);


        if (mixer == null)
        {
            mixer = Resources.Load<AudioMixer>("GameMixer");
        }

        if (mixer != null)
        {
            mixer.SetFloat("musicVol", PlayerPrefs.GetFloat("musicVol", defaultBundle.musicVol));
            mixer.SetFloat("soundEffectsVol", PlayerPrefs.GetFloat("soundEffectsVol", defaultBundle.soundEffectsVol));
        }
        //todo: load music and sound effects

        return new OptionBundle(PlayerPrefs.GetInt("resX", defaultBundle.resX), PlayerPrefs.GetInt("resY", defaultBundle.resY), PlayerPrefs.GetFloat("musicVol", defaultBundle.musicVol), PlayerPrefs.GetFloat("soundEffectsVol", defaultBundle.soundEffectsVol), fullsc);
    }
}
