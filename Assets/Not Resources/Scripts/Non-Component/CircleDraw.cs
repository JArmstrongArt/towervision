﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this class uses the linerenderer to draw a circle based on the location of a provided gameObject

public static class CircleDraw
{
    private static Material lineMat;

    public static GameObject DrawCircle(GameObject sourceObj,float radius, int corners,float scaleRate = 0.0f,float lifetime = 0.0f)
    {
        if (FindResources())
        {
            GameObject circleCenter = new GameObject();
            circleCenter.name = "circleCenter";
            circleCenter.transform.position = sourceObj.transform.position;
            circleCenter.transform.rotation = Quaternion.Euler(Vector3.zero);
            LineRenderer line = circleCenter.AddComponent<LineRenderer>();
            line.loop = true;
            line.positionCount = corners;
            line.material = lineMat;
            line.startWidth = 0.3f;
            line.endWidth = line.startWidth;
            line.useWorldSpace = false;
            line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            line.receiveShadows = false;
            float angle = 0;
            int line_positionIndex = 0;
            float addAngle = ExtendedMaths.DivideAmongst360(corners);
            while (angle < 360)
            {
                line.SetPosition(line_positionIndex, circleCenter.transform.forward * radius);
                line_positionIndex += 1;
                angle += addAngle;
                circleCenter.transform.rotation = Quaternion.Euler(circleCenter.transform.eulerAngles.x, angle, circleCenter.transform.eulerAngles.z);
            }

            MaintainOffset offScr = circleCenter.AddComponent<MaintainOffset>();
            offScr.targetObj = sourceObj;
            offScr.AwakeSetup();

            if (scaleRate != 0)
            {
                Scale scaScr = circleCenter.AddComponent<Scale>();
                scaScr.scaleRates = new Vector3(scaleRate, scaleRate, scaleRate);
            }

            if (lifetime != 0)
            {
                if (lifetime < 0)
                {
                    lifetime = 0;
                }

                LifeTimer timeScr = circleCenter.AddComponent<LifeTimer>();
                timeScr.lifetime = lifetime;
            }

            line.enabled = true;
            return circleCenter;
        }
        else
        {
            return null;
        }

    }

    private static bool FindResources()
    {
        bool ret = false;

        if (lineMat == null)
        {
            lineMat = Resources.Load<Material>("Materials/line");
        }

        if (lineMat != null)
        {
            ret = true;
        }

        return ret;
    }

    

    
    

}
