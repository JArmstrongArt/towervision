﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProjectileInherit : MonoBehaviour
{
    private BillboardAnimations animScr;
    [SerializeField] float moveSpeed;
    [HideInInspector]
    public Vector3 moveDir;

    [SerializeField] protected GenericRay collideRay;
    [SerializeField] protected float lifetime;
    [SerializeField] float projectileScale;

    [SerializeField] bool disappearAfterCollision;


    protected virtual void Awake()
    {
        if (FindDependencies())
        {
            animScr.PlayAnimation("loop");



            LifeTimer lifeScr = gameObject.GetComponent<LifeTimer>();

            if (lifeScr == null)
            {
                lifeScr = gameObject.AddComponent<LifeTimer>();
            }
            lifeScr.lifetime = lifetime;

            

            gameObject.transform.localScale = new Vector3(projectileScale, projectileScale, projectileScale);
        }

    }

    protected abstract void React();

    protected virtual void Update()
    {
        if (FindDependencies())
        {

            collideRay.updateType_enum = UpdateType.Regular;

            if (collideRay.ray_surface!=null)
            {
                Contact();
            }
        }
    }

    protected virtual void Contact()
    {
        React();
        if (disappearAfterCollision)
        {
            Destroy(gameObject);
        }
    }

    protected virtual void LateUpdate()
    {
        if (FindDependencies())
        {
            animScr.billboardWorldDir_set(moveDir.normalized);
            gameObject.transform.position += moveDir.normalized * moveSpeed * Time.deltaTime;
            collideRay.customDir_vec = moveDir.normalized;
            collideRay.rayLength = moveSpeed * Time.deltaTime;
        }
    }

    protected virtual bool FindDependencies()
    {
        bool ret = false;
        if (animScr == null)
        {
            animScr = gameObject.GetComponent<BillboardAnimations>();
        }

        if (animScr != null)
        {
            ret = true;
        }
        return ret;
    }

}
