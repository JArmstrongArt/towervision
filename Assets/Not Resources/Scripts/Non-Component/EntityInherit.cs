﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//this class parents both monsterinherit and towerinherit, allowing for even more common functionality between even those two classes
//be warned, this class is pretty packed with variables
public abstract class EntityInherit : MonoBehaviour
{
    public float health_max;
    [HideInInspector]
    public float health_current;

    [HideInInspector]
    public bool invulnerable;
    protected BillboardAnimations animScr;

    public float awarenessRadius;//the radius of how far this entity will react to whatever its interested in fighting
    public float damage;//how much damage this entity does to what it is interested in fighting


    protected GameObject explosion_prefab;


    // Start is called before the first frame update



    protected virtual void Awake()
    {
        if (FindDependencies() && FindResources())
        {
            if (gameObject.GetComponent<Rigidbody>() != null)
            {
                Destroy(GetComponent(typeof(Rigidbody)));
            }

            if (gameObject.GetComponent<Rigidbody>() == null)
            {
                gameObject.AddComponent<Rigidbody>();
            }

            if (gameObject.GetComponent<Rigidbody>() != null)
            {
                Rigidbody myRb = gameObject.GetComponent<Rigidbody>();
                myRb.isKinematic = true;
                myRb.useGravity = false;
                myRb.constraints = RigidbodyConstraints.FreezeAll;
                myRb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
            }

            health_current = health_max;




            //just guess some average values for the collision on this entity, again it can be changed in child awake calls easily
            SphereCollider collComp = null;
            if (gameObject.GetComponent<SphereCollider>() != null)
            {
                collComp = gameObject.GetComponent<SphereCollider>();
            }
            else
            {
                while (gameObject.GetComponent<Collider>() != null)
                {
                    Destroy(GetComponent(typeof(Collider)));
                }
                collComp = gameObject.AddComponent<SphereCollider>();
            }
            collComp.radius = 0.85f;
            collComp.center = new Vector3(0, 0.9f, 0);



        }


    }







    //shift the health value according to the provided float within the range of 0 to maximum health, then make sure health is only added or taken depending on the state of both invulnerability and death
    public void ShiftHealth(float shiftAmt)
    {
        if (FindDependencies() && FindResources())
        {
            float health_current_proposed = health_current + shiftAmt;
            health_current_proposed = Mathf.Clamp(health_current_proposed, 0.0f, health_max);

            if (invulnerable == false || (invulnerable == true && health_current_proposed > health_current))
            {
                health_current = health_current_proposed;
            }



            if (health_current <= 0)
            {
                Destroy(gameObject);



            }
        }


        
    }




    protected virtual bool FindDependencies()
    {
        bool ret = false;
        if (animScr == null)
        {
            animScr = gameObject.GetComponent<BillboardAnimations>();
        }

        if (animScr != null)
        {
            ret = true;
        }

        return ret;
    }

    protected virtual bool FindResources()
    {
        bool ret = false;
        if (explosion_prefab == null)
        {
            explosion_prefab = Resources.Load<GameObject>("Particles/pS_explosion");
        }


        if (explosion_prefab != null )
        {
            ret = true;
        }

        return ret;
    }

}
