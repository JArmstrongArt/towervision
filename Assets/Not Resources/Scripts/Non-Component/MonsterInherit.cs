﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this class parents all the monster types for common functionality
public class MonsterInherit : EntityInherit
{
    protected GameObject tower_nearest;
    [SerializeField] int killBounty;//how much money the player gets for killing the monster
    private PlayerStats statScr;
    private int pathIndex;
    [SerializeField] protected float moveSpeed;
    protected float moveSpeed_orig;
    private float moveSpeed_slowdownMode_multiple = 0.5f;
    [HideInInspector]
    public MonsterPath pathScr;//this must be manually assigned by another object, typically MatchManager
    private float pathLerp_progress;
    protected bool pathLerp_progress_paused=false;
    [SerializeField] float preciousObjectDamageRate;

    private bool killBountyAdded;

    [HideInInspector]
    public MatchManager matchManageScr;//this is set by a matchmanager instance, used in the OnDestroy function to remove it from that matchManager's monster list if it is there

    public Sprite nextWaveMonster_hudIcon;//this is for the hud script to draw from, i thought it would fit neater here than a separate list from monsterinherit derived classes.

    protected Vector3 billboardWorldDir_previous;//this is related to SoldierMonster, it is not altered in this class but needs to be set in this class due to the path system altering billboardworlddir.


    [SerializeField] float entityStatBarRaise;//how far above this entity will the ui for it render
    private GameObject entityStatBar_prefab;//the asset to instantiate for the entity ui
    private GameObject entityStatBar_inst;//the instance instantiated from entityStatBar_prefab
    protected EntityStatCanvas entityCanvScr;//the canvas script attached to entityStatBar_inst
    [SerializeField] string entityCanv_entityName;

    [HideInInspector]
    public bool slowdownMode;

    private GameObject soundEffect_deathScream_prefab;
    private GameObject soundEffect_explode_prefab;

    private GameObject slimeGlob_prefab;

    private intVec2 slimeGlobCountRange = new intVec2(8,16);

    protected override void Awake()
    {
        if (FindResources() && FindDependencies())
        {
            base.Awake();

            moveSpeed_orig = moveSpeed;

            //add this monster to list of all existing monsters
            if (!EntityTracker.ALL_MONSTERS.Contains(this))
            {
                EntityTracker.ALL_MONSTERS.Add(this);
            }


            entityStatBar_inst = Instantiate(entityStatBar_prefab, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + entityStatBarRaise, gameObject.transform.position.z), Quaternion.Euler(Vector3.zero), null);
            entityCanvScr = entityStatBar_inst.GetComponent<EntityStatCanvas>();
            entityCanvScr.entityName_txt.text = entityCanv_entityName;
            ExtendedMaths.TMPTextVerify(entityCanvScr.entityName_txt);

            LookAtCamera entityCamScr = entityStatBar_inst.GetComponent<LookAtCamera>();

            if (entityCamScr != null)
            {
                entityCamScr.flipped = true;//you have to flip the entity ui when it uses the LookAtCamera script bc for some reason it is naturally flipped, therefore making a double negative with this line back to normalcy
            }

            MaintainOffset maintainScr = null;
            if (entityStatBar_inst.GetComponent<MaintainOffset>() == null)
            {
                maintainScr = entityStatBar_inst.AddComponent<MaintainOffset>();
            }
            else
            {
                maintainScr = entityStatBar_inst.GetComponent<MaintainOffset>();
                maintainScr.targetObj = null;
            }

            maintainScr.targetObj = gameObject;
            maintainScr.AwakeSetup();
        }



        


    }

    protected virtual void OnDestroy()
    {

        if (FindResources() && FindDependencies())
        {
            if (entityStatBar_inst != null)
            {
                Destroy(entityStatBar_inst);
            }

            if (killBountyAdded == false)//i had to add this because the destroying of the object and this function happen a frame off from each other, so in some fringe cases, 2 towers were killing the monster at once, causing 2 times as much of a cash drop.
            {
                if (FindResources() && FindDependencies())
                {
                    statScr.ShiftMoney(killBounty);
                    killBountyAdded = true;
                }

            }
            int screamOdds = Random.Range(0, 30);
            if (screamOdds == 0)
            {
                Instantiate(soundEffect_deathScream_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
            }
            Instantiate(soundEffect_explode_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);


            int slimeGlobCount = Random.Range(slimeGlobCountRange.x, slimeGlobCountRange.y+1);//plus 1 bc int rand range is exclusive on the end cap and inclusive on the start cap

            float eulerInterval = (float)360 / (float)slimeGlobCount;
            SlimeGlob globScr = slimeGlob_prefab.GetComponent<SlimeGlob>();
            if (globScr != null)
            {
                GameObject spawnWheelObj = new GameObject();
                spawnWheelObj.transform.position = gameObject.transform.position;
                for (int i = 0; i < slimeGlobCount; i++)
                {
                    GameObject slimeGlob_inst = Instantiate(slimeGlob_prefab, spawnWheelObj.transform.position, Quaternion.Euler(Vector3.zero), null);

                    SlimeGlob instGlob = slimeGlob_inst.GetComponent<SlimeGlob>();
                    instGlob.moveDir = spawnWheelObj.transform.forward;



                    spawnWheelObj.transform.eulerAngles = new Vector3(spawnWheelObj.transform.eulerAngles.x, spawnWheelObj.transform.eulerAngles.y + eulerInterval, spawnWheelObj.transform.eulerAngles.z);
                }
                Destroy(spawnWheelObj);

            }




        }


        if (EntityTracker.ALL_MONSTERS.Contains(this))
        {
            EntityTracker.ALL_MONSTERS.Remove(this);
        }

        if (matchManageScr != null)
        {
            if (matchManageScr.allMonstersFromCurrentWave.Contains(this))
            {
                matchManageScr.allMonstersFromCurrentWave.Remove(this);
            }
        }
    }

    protected virtual void Update()
    {

        if (FindResources() && FindDependencies())
        {
            if (entityCanvScr != null)
            {
                entityCanvScr.healthVal_txt.text = Mathf.CeilToInt(health_current).ToString() + "/" + Mathf.CeilToInt(health_max).ToString();//set the health value of the entity ui to this scripts health value
                ExtendedMaths.TMPTextVerify(entityCanvScr.healthVal_txt);

                float lerpVal = (float)health_current / (float)health_max;
                entityCanvScr.bg_img.color = Color.Lerp(new Color(Color.red.r, Color.red.g, Color.red.b, entityCanvScr.bg_img.color.a), new Color(Color.green.r, Color.green.g, Color.green.b, entityCanvScr.bg_img.color.a), lerpVal);
                entityCanvScr.healthbar_bg_img.color = Color.Lerp(new Color(Color.red.r, Color.red.g, Color.red.b, entityCanvScr.healthbar_bg_img.color.a), new Color(Color.green.r, Color.green.g, Color.green.b, entityCanvScr.healthbar_bg_img.color.a), lerpVal);

            }



            if (slowdownMode)
            {
                moveSpeed = moveSpeed_orig * moveSpeed_slowdownMode_multiple;
                base.animScr.renderCol = Color.cyan;
            }
            else
            {
                moveSpeed = moveSpeed_orig;
                base.animScr.renderCol = Color.white;

            }

            MoveAlongPath();
        }

    }

    void MoveAlongPath()
    {
        if (FindResources() && FindDependencies())
        {
            if (pathScr != null)
            {
                if (pathIndex + 1 < pathScr.allPathCorners.Length)
                {
                    
                    if (pathLerp_progress_paused == false)
                    {
                        gameObject.transform.position = Vector3.Lerp(pathScr.allPathCorners[pathIndex].position, pathScr.allPathCorners[pathIndex + 1].position, pathLerp_progress);
                        pathLerp_progress += (moveSpeed * Time.deltaTime) / Vector3.Distance(pathScr.allPathCorners[pathIndex].position, pathScr.allPathCorners[pathIndex + 1].position);
                    }
                    
                    pathLerp_progress = Mathf.Clamp(pathLerp_progress, 0.0f, 1.0f);
                    if (pathLerp_progress >= 1)
                    {
                        Vector3 newForward = pathScr.allPathCorners[pathIndex + 1].forward;
                        billboardWorldDir_previous = newForward;
                        base.animScr.billboardWorldDir_set(newForward);
                        pathIndex += 1;
                        pathLerp_progress = 0;

                    }
                }
                else
                {

                    gameObject.transform.position = pathScr.allPathCorners[pathScr.allPathCorners.Length - 1].position;
                    base.animScr.billboardWorldDir_set(pathScr.allPathCorners[pathScr.allPathCorners.Length - 1].forward);

                    if (FindResources() && FindDependencies())
                    {
                        statScr.ShiftHealth(-(Mathf.Abs( preciousObjectDamageRate) * Time.deltaTime));
                    }
                }
            }
        }

    }


    //returns true if at least one tower exists and is in the range of the monster
    protected bool NearestTowerInRange()
    {
        if (FindResources() && FindDependencies())
        {
            FindNearestOpposingTower();
            if (tower_nearest != null)
            {
                if (Vector3.Distance(tower_nearest.transform.position, gameObject.transform.position) <= base.awarenessRadius)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }

    //finds the nearest tower if one exists
    void FindNearestOpposingTower()
    {
        if (FindResources() && FindDependencies())
        {
            float closestDist = float.MaxValue;
            GameObject tower_nearest_proposed = null;

            List<TowerInherit> relevantTowers = ExtendedMaths.FindAllTowersInRange(gameObject.transform.position,awarenessRadius,gameObject);

            if (relevantTowers != null)
            {
                if (relevantTowers.Count > 0)
                {
                    foreach (TowerInherit tower in relevantTowers)
                    {
                        if (tower != null)
                        {
                            GameObject tower_obj = tower.gameObject;
                            if (Vector3.Distance(tower_obj.transform.position, gameObject.transform.position) < closestDist && tower_obj != gameObject)
                            {
                                closestDist = Vector3.Distance(tower_obj.transform.position, gameObject.transform.position);
                                tower_nearest_proposed = tower_obj;

                            }
                        }


                    }
                }
            }


            tower_nearest = tower_nearest_proposed;
        }


    }




    protected override bool FindDependencies()
    {
        bool parRet = base.FindDependencies();
        bool ret = false;

        if (statScr == null)
        {
            statScr = GameObject.FindObjectOfType<PlayerStats>();
        }

        if (statScr != null && parRet==true)
        {
            ret = true;

        }

        return ret;
    }

    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;

        if (entityStatBar_prefab == null)
        {
            entityStatBar_prefab = Resources.Load<GameObject>("Prefabs/UI/entityStatCanvas");
        }

        if (soundEffect_deathScream_prefab == null)
        {
            soundEffect_deathScream_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_slimeScream");
        }
        if (soundEffect_explode_prefab == null)
        {
            soundEffect_explode_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_slimeExplode");
        }

        if (slimeGlob_prefab == null)
        {
            slimeGlob_prefab = Resources.Load<GameObject>("Prefabs/slimeGlob");
        }

        if (slimeGlob_prefab!=null && soundEffect_explode_prefab != null && soundEffect_deathScream_prefab != null && parRet == true && entityStatBar_prefab != null)
        {
            ret = true;
        }
        return ret;
    }

}
