﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//this class just keeps misc constants that i can refer to, not really important
public class Constants
{
    public static bool DEBUG_MODE =false;
    public static string MONSTER_DIR = "Prefabs/Monsters/";
    public static string FONT_LOCATION = "Fonts/TowerVision";

}
