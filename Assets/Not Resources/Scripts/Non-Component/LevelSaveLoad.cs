﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public enum LevelBlockData_BlockAngle
{

    NONE,
    UP,
    DOWN,
    LEFT,
    RIGHT
}


[Serializable]
public enum LevelBlockData_BlockType
{
    GENERICSOLID,
    INVISIBLE
}

public enum LevelSaveLoadFolder
{
    OFFICIAL,
    TEST,
    USER
}

[Serializable]
public struct LevelBlockData
{
    public LevelBlockData_BlockAngle blockAng;
    public int blockElev;
    public LevelBlockData_BlockType blockType;
    public int monsterPathIndex;
    public float monsterPathIndex_elevation;
    public string blockTexPath;
    public bool spawnPoint;
    public LevelBlockData(int elev, LevelBlockData_BlockType type, LevelBlockData_BlockAngle ang = LevelBlockData_BlockAngle.NONE,int pI = -1,string texPath = "", bool sP=false)
    {
        if (elev < 0)
        {
            elev = 0;
        }
        blockElev = elev;
        blockAng = ang;
        blockType = type;
        monsterPathIndex = pI;
        monsterPathIndex_elevation = 0;
        blockTexPath = texPath;
        spawnPoint = sP;
    }
}

[Serializable]
public struct LevelData
{
    public LevelBlockData[,] levelGeometry;
    public MonsterWave[] levelWaves;

    public BuilderData_Serializable[,] editor_builderData;
    public List<intVec2> editor_allPathPoints;
    public List<EditorWave> editor_editorWaves;

    public int startMoney;
    public int startHealth;
    public float skyColour_r;
    public float skyColour_g;
    public float skyColour_b;
    public float minimapOrthoScale;
    public LevelData(LevelBlockData[,] geo, MonsterWave[] wav, BuilderData_Serializable[,] bD = null, List<intVec2> aPP = null, List<EditorWave> eW = null,int sM = 100,int sH = 100,float sC_r = 0, float sC_g = 0, float sC_b = 0,float mOS = 1.0f)
    {
        levelGeometry = geo;
        levelWaves = wav;
        editor_builderData = bD;
        editor_allPathPoints = aPP;
        editor_editorWaves = eW;
        startMoney = sM;
        startHealth = sH;
        skyColour_r = sC_r;
        skyColour_g = sC_g;
        skyColour_b = sC_b;
        minimapOrthoScale = mOS;
    }
}

public abstract class LevelSaveLoad:MonoBehaviour
{
    public static string levelToLoad = "BASICARENA";
    public static LevelSaveLoadFolder folderToLoad = LevelSaveLoadFolder.OFFICIAL;
    public static string fileType = "bongo".ToLower();

    public static string SaveLoadFolderToString(LevelSaveLoadFolder sLF)
    {
        string ret = "";
        switch (sLF)
        {
            case LevelSaveLoadFolder.OFFICIAL:
                ret = "official";
                break;
            case LevelSaveLoadFolder.TEST:
                ret = "test";
                break;
            case LevelSaveLoadFolder.USER:
                ret = "user";
                break;
            default:
                ret = "user";
                break;
        }
        return ret;
    }

    public static void SaveLevel(LevelData dataToSave, string fileName,LevelSaveLoadFolder saveLoadFolder)
    {
        fileName = fileName.ToUpper();

        string levelFolder = SaveLoadFolderToString(saveLoadFolder);

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = File.Create(Application.dataPath + "/" +"levels"+"/" +levelFolder+"/" + fileName.ToString() + "."+fileType.ToString());
        formatter.Serialize(stream, dataToSave);
        stream.Close();
    }

    public static LevelData LoadLevel(string fileName, LevelSaveLoadFolder saveLoadFolder, bool generateLevel = true)
    {
        LevelData ret = new LevelData();
        fileName = fileName.ToUpper();

        string levelFolder = "";

        switch (saveLoadFolder)
        {
            case LevelSaveLoadFolder.OFFICIAL:
                levelFolder = "official";
                break;
            case LevelSaveLoadFolder.TEST:
                levelFolder = "test";
                break;
            case LevelSaveLoadFolder.USER:
                levelFolder = "user";
                break;
            default:
                levelFolder = "user";
                break;
        }

        string fileName_full = Application.dataPath + "/" + "levels" + "/" + levelFolder+"/" + fileName.ToString() + "."+fileType.ToString();
        if (File.Exists(fileName_full))
        {
            LevelMeshGenerate meshScr = GameObject.FindObjectOfType<LevelMeshGenerate>();

            if (meshScr == null)
            {
                GameObject meshGenObj = new GameObject();
                meshScr = meshGenObj.AddComponent<LevelMeshGenerate>();
            }

            meshScr.gameObject.transform.position = Vector3.zero;

            BinaryFormatter bF = new BinaryFormatter();
            FileStream fS = File.Open(fileName_full, FileMode.Open);
            LevelData loadedData = (LevelData)bF.Deserialize(fS);
            ret = loadedData;
            fS.Close();

            if (generateLevel == true)
            {
                bool revokeCreation = true;
                if (loadedData.levelGeometry != null && loadedData.levelWaves != null)
                {
                    if (loadedData.levelGeometry.GetLength(0) > 0 && loadedData.levelWaves.Length > 0)
                    {
                        revokeCreation = false;
                        meshScr.allLevelBlockData = loadedData.levelGeometry;
                        meshScr.waveData = loadedData.levelWaves;
                        meshScr.GenerateTheLevel();
                    }
                }

                if (revokeCreation == true)
                {
                    if (meshScr != null)
                    {
                        Destroy(meshScr.gameObject);
                    }
                }
            }

            GameObject statLoaderObj = new GameObject();
            statLoaderObj.AddComponent<LevelLoadCaseDependentStatLoader>().dataToLoadFrom = loadedData;

            if (RenderSettings.skybox != null)
            {

                RenderSettings.skybox.SetColor("_Tint", new Color(loadedData.skyColour_r, loadedData.skyColour_g, loadedData.skyColour_b, 1));
                RenderSettings.skybox.SetColor("_EmissionColor", RenderSettings.skybox.GetColor("_Tint"));
                RenderSettings.skybox.SetColor("_Color", RenderSettings.skybox.GetColor("_Tint"));
            }



        }

        return ret;


    }
}
