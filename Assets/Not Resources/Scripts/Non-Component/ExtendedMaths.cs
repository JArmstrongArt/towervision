﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using TMPro;
using System.Reflection;
using System.Linq;

public enum RectReturnPos
{
    Center,
    TopLeft,
    TopRight,
    Top,
    BottomLeft,
    BottomRight,
    Bottom,
    Left,
    Right
}

public struct monsterIcons
{
    public System.Type sprSource;
    public Sprite spr;
    public string sprSourcePrefabMonster;

    public monsterIcons(System.Type ty,Sprite sp, string pth)
    {
        sprSource = ty;
        spr = sp;
        sprSourcePrefabMonster = pth;
    }
}
//this class exists in case i need some common mathematics not found in mathf
public class ExtendedMaths
{

    public static TMP_SpriteAsset font = null;



    public static void TMPTextVerify(TextMeshProUGUI textComp,int spriteTextCharThreshold = 0)
    {

        if (font == null)
        {
            font = Resources.Load<TMP_SpriteAsset>(Constants.FONT_LOCATION);
        }


        if (font != null)
        {
            textComp.spriteAsset = font;

            textComp.text = ToTMPSpriteText(textComp,spriteTextCharThreshold);


        }


    }

    public static void TMPTextVerify_InputField(TMP_InputField textComp)
    {
        if(textComp.gameObject.GetComponent<TMProInputFieldWorkaround>() != null)
        {
            if (textComp.gameObject.GetComponent<TMProInputFieldWorkaround>().dest != null)
            {
                GameObject.Destroy(textComp.gameObject.GetComponent<TMProInputFieldWorkaround>().dest.gameObject);

            }
        }
        else
        {
            textComp.gameObject.AddComponent<TMProInputFieldWorkaround>();
        }
        
    }

    //i found out this is like the exact same code as inverselerp after i figured out how to inverse lerp from the formula of lerp myself, but i thought i'd keep it here just as proof that it works

    /*For future reference to myself, here's my working:
    lerp(a, b, f) = a + f * (b - a)
    Mathf.Lerp(minVol, maxVol,musicVol_slider.value) = minVol+musicVol_slider.value * (maxVol-minVol)

    if slider was at 0.5, it should equal -40

    lerp = -80+0.5*(0--80) = -80+0.5*80 = -80+40 = -40


    rearrange to get f, l = lerpresult
    a + f * (b - a) = l
    l-a = f*(b-a)

    (l-a)/(b-a) = f


    0.5 = (-40--80)/(0--80) = 40/80 = 0.5
    */

public static float LerpInterpFromResult(float min, float max, float result)
{
    return (result - min) / (max - min);
}
public static List<System.IO.FileInfo> LoadAllLevelsInFolder(string folderPath)
{
    string loadPath = folderPath;

    List<System.IO.FileInfo> allLevelPaths_cleaned = null;
    if (System.IO.Directory.Exists(loadPath))
    {
        System.IO.DirectoryInfo levelList = new System.IO.DirectoryInfo(loadPath);
        System.IO.FileInfo[]  allLevelPaths = levelList.GetFiles();

        if (ExtendedMaths.Array1DTrueEmpty(allLevelPaths) == false)
        {
            allLevelPaths_cleaned = new List<System.IO.FileInfo>();
            for (int i = 0; i < allLevelPaths.Length; i++)
            {
                if (allLevelPaths[i] != null)
                {

                    if (allLevelPaths[i].Extension.ToLower() == "." + LevelSaveLoad.fileType.ToString())
                    {
                        allLevelPaths_cleaned.Add(allLevelPaths[i]);
                    }
                }

            }
        }

    }
    return allLevelPaths_cleaned;

}

public static string ToTMPSpriteText(TextMeshProUGUI regularStringComp,int newLineCharThreshold = 0)
{
    string regularString = regularStringComp.text;
    string ret = "";

    if (font == null)
    {
        font = Resources.Load<TMP_SpriteAsset>(Constants.FONT_LOCATION);
    }


    if (font != null && !regularString.Contains("<sprite index=")) 
    {


        for (int i=0;i<regularString.Length;i++)
        {
            string cStr = regularString[i].ToString();
            cStr = cStr.ToLower();


            if (font.GetSpriteIndexFromName(cStr) >= 0)
            {
                ret += "<sprite index=" + font.GetSpriteIndexFromName(cStr).ToString() + " tint=1>";
            }
            else
            {
                ret += regularString[i];
            }

            if (newLineCharThreshold > 0)
            {

                if (regularString[i]==' ')
                {
                    int jFind = -1;

                    if (i< regularString.Length - 1)
                    {

                        for (int j = i + 1; j < regularString.Length; j++)
                        {
                            if(regularString[j]==' ')
                            {
                                jFind = j;
                                break;
                            }
                        }



                    }

                    if (jFind >= 0)
                    {

                        if (i + Mathf.Abs(jFind - i) > newLineCharThreshold)
                        {
                            ret += "\n";
                            newLineCharThreshold += newLineCharThreshold;
                        }
                    }

                }


            }

        }

        if (regularStringComp.gameObject.GetComponent<TMProCharacterSpacingRetainer>() == null)
        {
            regularStringComp.gameObject.AddComponent<TMProCharacterSpacingRetainer>();
            regularStringComp.gameObject.GetComponent<TMProCharacterSpacingRetainer>().newLineCharacterThreshold = newLineCharThreshold;
        }

    }
    else
    {
        ret = regularString;
    }


    return ret;
}
public static List<GameObject> DestroyGameObjectList(List<GameObject> orig)
{
    if (orig != null)
    {
        foreach(GameObject o in orig)
        {
            if (o != null)
            {
                GameObject.Destroy(o);
            }
        }
        orig.Clear();
    }
    orig = new List<GameObject>();
    return orig;
}

public static List<monsterIcons> LoadAllMonsterIcons()
{

    GameObject[] allMonsters = Resources.LoadAll<GameObject>(Constants.MONSTER_DIR);
    List<System.Type> acceptableClasses = GetAllClassesThatAreSubclassOf(typeof(MonsterInherit));
    List<monsterIcons> ret = new List<monsterIcons>();
    Sprite unknownIcon = Resources.Load<Sprite>("Sprites/Monsters/unknown");

    foreach (GameObject monsObj in allMonsters)
    {
        Sprite destIcon = unknownIcon;
        System.Type destType = null;
        MonsterInherit script = null;
        foreach(System.Type typ in acceptableClasses)
        {
            if (monsObj.GetComponent(typ) != null)
            {
                script = monsObj.GetComponent<MonsterInherit>();
                destType = typ;

                if (script != null)
                {
                    if (script.nextWaveMonster_hudIcon != null)
                    {
                        destIcon = script.nextWaveMonster_hudIcon;
                    }
                }

                monsterIcons addition = new monsterIcons(destType, destIcon, Constants.MONSTER_DIR + monsObj.name.ToString());
                if (!ret.Contains(addition))
                {
                    ret.Add(addition);
                }

                break;
            }
        }








    }

    return ret;
}

//NOT MY CODE!!! Taylor-Libonati on StackOverflow (https://answers.unity.com/questions/812240/convert-hex-int-to-colorcolor32.html)
public static string ColorToHex(Color32 color)//color32 just represents the colour as 0-255 values rather than 0-1.
{
    string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
    hex = ReformatHex(hex);
    return hex;
}

public static string ReformatHex(string hex)
{
    hex = hex.ToUpper();
    hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
    hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
    hex = hex.Replace(" ", "");//in case the string is formatted #FFFFFF

    return hex;
}
public static bool IsItHex(string hex)
{
    hex = ReformatHex(hex);
    List<char> hexChars = new List<char> { '0', '1', '2','3','4','5','6','7','8','9','A','B','C','D','E','F' };
    bool ret = true;
    foreach(char c in hex)
    {
        if (!hexChars.Contains(c))
        {
            ret = false;
            break;
        }
    }
    return ret;
}

//NOT MY CODE!!! Taylor-Libonati on StackOverflow (https://answers.unity.com/questions/812240/convert-hex-int-to-colorcolor32.html)
public static Color HexToColor(string hex)
{
    byte r = 0;
    byte g = 0;
    byte b = 0;
    byte a = 255;
    if (hex.Length >= 6)
    {
        hex = hex.ToUpper();
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF

        if (IsItHex(hex))
        {
            r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            //Only use alpha if the string has enough characters
            if (hex.Length >= 8)
            {
                a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
            }
        }


    }

    return new Color32(r, g, b, a);
}


public static List<TowerInherit> FindAllTowersInRange(Vector3 origin, float radius, GameObject excludeObj = null)
{
    List<TowerInherit> ret = null;

    if (ListTrueEmpty(EntityTracker.ALL_TOWERS) == false)
    {
        ret = new List<TowerInherit>();
        foreach (TowerInherit tower in EntityTracker.ALL_TOWERS)
        {
            if (tower != null)
            {

                GameObject towerObj = tower.gameObject;

                if (Vector3.Distance(towerObj.transform.position, origin) <= radius && towerObj != excludeObj)
                {

                    ret.Add(tower);

                }

            }


        }
    }


    return ret;
}

public static List<MonsterInherit> FindAllMonstersInRange(Vector3 origin, float radius, GameObject excludeObj = null)
{
    List<MonsterInherit> ret = null;

    if (ListTrueEmpty(EntityTracker.ALL_MONSTERS) == false)
    {
        ret = new List<MonsterInherit>();
        foreach (MonsterInherit monster in EntityTracker.ALL_MONSTERS)
        {
            if (monster != null)
            {

                GameObject monsterObj = monster.gameObject;

                if (Vector3.Distance(monsterObj.transform.position, origin) <= radius && monsterObj != excludeObj)
                {

                    ret.Add(monster);

                }

            }


        }
    }


    return ret;
}

public static List<System.Type> GetAllClassesThatAreSubclassOf(System.Type parClass)
{
    System.Type parentType = parClass;
    Assembly assembly = Assembly.GetExecutingAssembly();
    System.Type[] types = assembly.GetTypes();

    IEnumerable<System.Type> retType_enumerable = types.Where(t => t.BaseType == parentType);//gets all classes that are DIRECT descendants to parentType

    List<System.Type> retType_list = new List<System.Type>();
    foreach (System.Type t in retType_enumerable)
    {
        retType_list.Add(t);
    }
    return retType_list;
}
public static Vector2 GetWorldSizeOfRectInUnits(RectTransform rectSrc)
{
    Vector3 left = GetWorldPosOfRectPos(rectSrc, RectReturnPos.Left);
    Vector3 right = GetWorldPosOfRectPos(rectSrc, RectReturnPos.Right);
    Vector3 top = GetWorldPosOfRectPos(rectSrc, RectReturnPos.Top);
    Vector3 bottom = GetWorldPosOfRectPos(rectSrc, RectReturnPos.Bottom);

    float xSpan = Mathf.Abs( right.x - left.x);
    float ySpan = Mathf.Abs( top.y - bottom.y);
    return new Vector2(xSpan, ySpan);
}


public static Vector3 GetWorldPosOfRectPos(RectTransform rectSrc, RectReturnPos posRet)
{
    Vector3 ret = rectSrc.transform.position;

    Vector3[] corners = new Vector3[4];
    rectSrc.GetWorldCorners(corners);

    switch (posRet)
    {
        case RectReturnPos.BottomLeft:
            ret = corners[0];
            break;
        case RectReturnPos.TopLeft:
            ret = corners[1];
            break;
        case RectReturnPos.TopRight:
            ret = corners[2];
            break;
        case RectReturnPos.BottomRight:
            ret = corners[3];
            break;
        case RectReturnPos.Bottom:
            ret = new Vector3((float)(corners[0].x + corners[3].x) / (float)2.0f, (float)(corners[0].y + corners[3].y) / (float)2.0f, (float)(corners[0].z + corners[3].z) / (float)2.0f);
            break;

        case RectReturnPos.Top:
            ret = new Vector3((float)(corners[1].x + corners[2].x) / (float)2.0f, (float)(corners[1].y + corners[2].y) / (float)2.0f, (float)(corners[1].z + corners[2].z) / (float)2.0f);
            break;

        case RectReturnPos.Left:
            ret = new Vector3((float)(corners[0].x + corners[1].x) / (float)2.0f, (float)(corners[0].y + corners[1].y) / (float)2.0f, (float)(corners[0].z + corners[1].z) / (float)2.0f);
            break;

        case RectReturnPos.Right:
            ret = new Vector3((float)(corners[2].x + corners[3].x) / (float)2.0f, (float)(corners[2].y + corners[3].y) / (float)2.0f, (float)(corners[2].z + corners[3].z) / (float)2.0f);
            break;

        case RectReturnPos.Center:
            ret = new Vector3((float)(corners[2].x + corners[3].x+corners[1].x+corners[0].x) / (float)4.0f, (float)(corners[2].y+ corners[3].y + corners[1].y + corners[0].y) / (float)4.0f, (float)(corners[2].z + corners[3].z + corners[1].z + corners[0].z) / (float)4.0f);
            break;

    }



    return ret;
}

public static bool IntToBool(int source)
{

    bool ret = false;
    if (source >= 1)
    {
        ret = true;
    }
    return ret;
}

public static float GetNumFromInputField(TMP_InputField inputSrc, float lowerLimit = 0, float upperLimit = float.MaxValue)
{



    if (upperLimit < lowerLimit)
    {
        float temp = upperLimit;
        upperLimit = lowerLimit;
        lowerLimit = temp;
    }

    float setVal;
    if (inputSrc != null)
    {

        bool setVal_try = float.TryParse(inputSrc.text, out setVal);


        if (setVal_try == true)
        {
            if (setVal < lowerLimit)
            {
                setVal = lowerLimit;

            }

            if (setVal > upperLimit)
            {
                setVal = upperLimit;
            }
        }
        else
        {
            if (inputSrc.text == "")
            {
                setVal = lowerLimit;
            }
            else
            {
                setVal = upperLimit;
            }

        }
        inputSrc.text = setVal.ToString();

        ExtendedMaths.TMPTextVerify_InputField(inputSrc);
    }
    else
    {
        setVal = lowerLimit;
    }

    return setVal;
}

public static bool Array1DTrueEmpty<T>(T[] orig)
{
    bool ret = true;

    if (orig != null)
    {
        if (orig.Length > 0)
        {
            ret = false;
        }
    }

    return ret;
}
public static bool PointerInView(Camera refCam = null)
{
    bool ret = false;
    if (refCam == null)
    {
        refCam = GameObject.FindObjectOfType<Camera>();
    }

    if (refCam != null)
    {
        Vector3 view = refCam.ScreenToViewportPoint(Input.mousePosition);
        bool isOutside = view.x < 0 || view.x > 1 || view.y < 0 || view.y > 1;
        ret = !isOutside;
    }

    return ret;
}

public static List<RectTransform> GetAllRectsUnderPointer()
{
    EventSystem curSys = GameObject.FindObjectOfType<EventSystem>();
    PointerEventData eventData = new PointerEventData(curSys);
    eventData.position = Input.mousePosition;
    List<RaycastResult> results = new List<RaycastResult>();
    curSys.RaycastAll(eventData, results);

    List<RectTransform> ret = new List<RectTransform>();

    foreach (RaycastResult res in results)
    {
        if (res.gameObject.GetComponent<RectTransform>() != null)
        {
            ret.Add(res.gameObject.GetComponent<RectTransform>());

        }
    }

    if (ret.Count <= 0)
    {
        ret = null;
    }

    return ret;

}
public static bool PointerOverUI()
{
    List<RectTransform> allRects = GetAllRectsUnderPointer();

    if (allRects == null)
    {
        return false;
    }
    else
    {
        return true;
    }


}

public static float WrapValue(float val, float addition, float lowerBound, float upperBound)
{
    if (lowerBound == upperBound)
    {
        return lowerBound;
    }
    else
    {
        if (upperBound < lowerBound)
        {
            float temp = upperBound;
            upperBound = lowerBound;
            lowerBound = temp;
        }


        float unwrappedValue = val + addition;


        float resultantValue = 0;

        if (unwrappedValue > upperBound)
        {
            resultantValue = lowerBound + (unwrappedValue - upperBound-1);
        }

        if (unwrappedValue < lowerBound)
        {
            resultantValue = upperBound - (lowerBound - unwrappedValue-1);
        }

        if(unwrappedValue>=lowerBound && unwrappedValue <= upperBound)
        {
            resultantValue = unwrappedValue;
        }

        return resultantValue;
    }

}
public static int[] SegmentedTrisToContinuousTris(int[,] segmentedTris)
{
    if (segmentedTris.GetLength(1) != 3)
    {
        return null;
    }
    else
    {
        int[] continuousTris = new int[segmentedTris.Length * 3];
        int continuousTris_writeIndex = 0;
        for (int i = 0; i < segmentedTris.GetLength(0); i++)
        {
            for (int j = 0; j < segmentedTris.GetLength(1); j++)
            {
                continuousTris[continuousTris_writeIndex] = segmentedTris[i, j];
                continuousTris_writeIndex += 1;
            }
        }
        return continuousTris;
    }


}

public static int[,] ContinuousTrisToSegmentedTris(int[] continuousTris)
{
    if (continuousTris.Length % 3 != 0)
    {
        return null;
    }
    else
    {
        int[,] segmentedTris = new int[Mathf.RoundToInt(continuousTris.Length / 3), 3];

        int segmentedTris_writeIndex_x = 0;
        int segmentedTris_writeIndex_y = 0;
        for (int i = 0; i < continuousTris.Length; i++)
        {
            segmentedTris[segmentedTris_writeIndex_x, segmentedTris_writeIndex_y] = continuousTris[i];
            if (segmentedTris_writeIndex_y >= 2)
            {
                segmentedTris_writeIndex_x += 1;
                segmentedTris_writeIndex_y = 0;
            }
            else
            {
                segmentedTris_writeIndex_y += 1;
            }
        }
        return segmentedTris;
    }


}

public static float DivideAmongst360(float div)
{
    return 360 * (1.0f / (float)div);
}

public static float RoundToNearestX(float value, float x)
{
    return Mathf.Round(value / x) * x;
}


public static float RoundToNearestX_Ceil(float value, float x)
{
    return Mathf.Ceil(value / x) * x;
}

public static float RoundToNearestX_Floor(float value, float x)
{
    return Mathf.Floor(value / x) * x;
}

public static T[,] Copy2DArray<T>(T[,] orig, int newSize_x, int newSize_y)
{
    T[,] ret = null;
    if (newSize_x >0 && newSize_y>0)
    {
        ret = new T[newSize_x, newSize_y];

        for(int i = 0; i < ret.GetLength(0); i++)
        {
            for(int j = 0; j < ret.GetLength(1); j++)
            {
                T writeVal = default(T);
                if (i < orig.GetLength(0) && j<orig.GetLength(1))
                {
                    writeVal = orig[i, j];
                }
                ret[i, j] = writeVal;
            }
        }
    }

    return ret;
}

public static bool Array2DTrueEmpty<T>(T[,] arr)
{
    if (arr == null)
    {
        return true;
    }
    else
    {
        if(arr.GetLength(0)<=0 || arr.GetLength(1) <= 0)
        {
            return true;
        }
    }

    return false;
}

public static T[,] Resize2DArray<T>(T[,] original, int rows, int cols)
{
    T[,] newArray = new T[rows, cols];
    int minRows = Math.Min(rows, original.GetLength(0));
    int minCols = Math.Min(cols, original.GetLength(1));
    for (int i = 0; i < minRows; i++)
        for (int j = 0; j < minCols; j++)
            newArray[i, j] = original[i, j];
    return newArray;
}

public static Vector3 EulerToDir(Vector3 eulerAngles)
{
    return (Quaternion.Euler(eulerAngles) * Vector3.forward).normalized;
}

public static Color EmissiveColorVariant(Color baseCol)
{

    float baseCol_h, baseCol_s, baseCol_v;
    Color.RGBToHSV(baseCol, out baseCol_h, out baseCol_s, out baseCol_v);
    baseCol_v = 1.0f;
    Color returnCol = Color.HSVToRGB(baseCol_h, baseCol_s, baseCol_v);
    return returnCol;
}

public static T[] SubArray<T>(T[] data, int index, int length)
{
    T[] result = new T[length];
    Array.Copy(data, index, result, 0, length);
    return result;
}

public static float GetAverage(List<float> allFloats)
{
    float accumulation = 0;
    foreach(float f in allFloats)
    {
        accumulation += f;
    }
    return (float)accumulation / (float)allFloats.Count;
}

public static Vector3 GetAverage(List<Vector3> allVectors)
{
    List<float> allXValues = new List<float>();
    List<float> allYValues = new List<float>();
    List<float> allZValues = new List<float>();

    foreach(Vector3 vec in allVectors)
    {
        allXValues.Add(vec.x);
        allYValues.Add(vec.y);
        allZValues.Add(vec.z);
    }

    return new Vector3(GetAverage(allXValues), GetAverage(allYValues), GetAverage(allZValues));
}


public static bool ListTrueEmpty<T>(List<T> check)
{
    bool ret = false;

    if (check == null)
    {
        ret = true;
    }
    else
    {
        if (check.Count <= 0)
        {
            ret = true;
        }
    }
    return ret;
}

public static GameObject[] TagWithinRadius(Vector3 position, float radius,string[] validTags)
{
    List<GameObject> ret_list = new List<GameObject>();
    if (validTags != null)
    {
        for (int i = 0; i < validTags.Length; i++)
        {
            GameObject[] allWithTag = GameObject.FindGameObjectsWithTag(validTags[i]);

            for(int j = 0; j < allWithTag.Length; j++)
            {
                if (allWithTag[j] != null)
                {
                    if (Vector3.Distance(position, allWithTag[j].transform.position) <= radius)
                    {
                        ret_list.Add(allWithTag[j]);
                    }
                }

            }
        }
    }

    GameObject[] ret = new GameObject[ret_list.Count];

    if (ret_list.Count <= 0)
    {
        ret = null;
    }
    else
    {
        int retIndex = 0;
        foreach (GameObject retObj in ret_list)
        {
            ret[retIndex] = retObj;
            retIndex += 1;
        }
    }




    return ret;
}

}
