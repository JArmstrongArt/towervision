﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSnap
{
    public static float GRID_SIZE = 1.175f;//i can change this to any value and every object in the game that works off of the grid will adjust, i love it tbh
    



    public static Vector3 SnapToGrid(Vector3 vec, bool centeredOnTile = false, bool snapY=false,bool levelEditorMode=false)
    {


        float retX = vec.x;
        float retZ = vec.z;
        float retY = vec.y;
        if (centeredOnTile == true)
        {

            //check which grid tile to snap to based on which line on the grid the exact vec input is closer to
            float xExtent_upper = ExtendedMaths.RoundToNearestX_Ceil(vec.x, GRID_SIZE) + (GRID_SIZE * 0.5f);
            float xExtent_lower = ExtendedMaths.RoundToNearestX_Ceil(vec.x, GRID_SIZE) - (GRID_SIZE * 0.5f);
            float xExtent_middle = (xExtent_lower+xExtent_upper)*0.5f;
            if (vec.x < xExtent_middle)
            {
                retX = xExtent_lower;
            }
            else
            {
                retX = xExtent_upper;
            }

            float zExtent_upper = ExtendedMaths.RoundToNearestX_Ceil(vec.z, GRID_SIZE) + (GRID_SIZE * 0.5f);
            float zExtent_lower = ExtendedMaths.RoundToNearestX_Ceil(vec.z, GRID_SIZE) - (GRID_SIZE * 0.5f);
            float zExtent_middle = (zExtent_lower + zExtent_upper) * 0.5f;
            if (vec.z < zExtent_middle)
            {
                retZ = zExtent_lower;
            }
            else
            {
                retZ = zExtent_upper;
            }

            if (snapY == true || levelEditorMode==true)
            {
                if (levelEditorMode == false)
                {
                    return new Vector3(retX, ExtendedMaths.RoundToNearestX_Floor(retY, GRID_SIZE), retZ);
                }
                else
                {
                    return new Vector3(retX, ExtendedMaths.RoundToNearestX(retY, GRID_SIZE), retZ);
                }
                
            }
            else
            {
                return new Vector3(retX,retY, retZ);
            }
            
        }
        else
        {
            //if you want to snap to the grid line and not the tiles, its this simple
            if (snapY == true||levelEditorMode==true)
            {
                

                if (levelEditorMode == false)
                {
                    return new Vector3(ExtendedMaths.RoundToNearestX_Ceil(retX, GRID_SIZE), ExtendedMaths.RoundToNearestX_Floor(retY, GRID_SIZE), ExtendedMaths.RoundToNearestX_Ceil(retZ, GRID_SIZE));
                }
                else
                {
                    return new Vector3(ExtendedMaths.RoundToNearestX_Ceil(retX, GRID_SIZE), ExtendedMaths.RoundToNearestX(retY, GRID_SIZE), ExtendedMaths.RoundToNearestX_Ceil(retZ, GRID_SIZE));
                }
            }
            else
            {
                return new Vector3(ExtendedMaths.RoundToNearestX_Ceil(retX, GRID_SIZE), retY, ExtendedMaths.RoundToNearestX_Ceil(retZ, GRID_SIZE));
            }
            
        }
        
    }
}
