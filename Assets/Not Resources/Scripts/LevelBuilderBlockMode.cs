﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelBuilderBlockMode : MonoBehaviour
{



    private InputManager inputScr;








    private Camera levelEditorCam;

    private LevelBuilderSaveLoad builderSaveLoadScr;





    void Update_PlaceRemoveBlocks()
    {
        if (FindDependencies())
        {
            if (ExtendedMaths.PointerOverUI() == false && ExtendedMaths.PointerInView() == true)
            {
                Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                Vector3 spawnPos = GridSnap.SnapToGrid(levelEditorCam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, levelEditorCam.nearClipPlane + 0.5f)), true, true, true);

                intVec2 posFound = builderSaveLoadScr.FindPosInBuilderData(new Vector2(spawnPos.x, spawnPos.y));


                    
                if (inputScr.GetInputByName("Building", "Editor_PlaceBlock") > 0)
                {


                    
                    if (posFound.x != LevelBuilderSaveLoad.invalidIntVec2.x && posFound.y != LevelBuilderSaveLoad.invalidIntVec2.y)
                    {
                        

                        if (ExtendedMaths.Array1DTrueEmpty(builderSaveLoadScr.allBuilderTextures) == false)
                        {
                            string blockTex = "Textures/Level Textures/brick";
                            if (builderSaveLoadScr.allBuilderTextures_index >= 0 && builderSaveLoadScr.allBuilderTextures_index < builderSaveLoadScr.allBuilderTextures.Length)
                            {
                                blockTex = builderSaveLoadScr.allBuilderTextures[builderSaveLoadScr.allBuilderTextures_index].texPath;
                            }

                            if ((builderSaveLoadScr.builderData[posFound.x, posFound.y].block.blockType != LevelBlockData_BlockType.GENERICSOLID) || (builderSaveLoadScr.builderData[posFound.x, posFound.y].block.blockType == LevelBlockData_BlockType.GENERICSOLID && (builderSaveLoadScr.builderData[posFound.x, posFound.y].block.blockTexPath != builderSaveLoadScr.allBuilderTextures[builderSaveLoadScr.allBuilderTextures_index].texPath  || builderSaveLoadScr.builderData[posFound.x, posFound.y].block.blockElev != builderSaveLoadScr.upcomingBlockElev || builderSaveLoadScr.builderData[posFound.x, posFound.y].block.blockAng != builderSaveLoadScr.upcomingBlockSlant)))
                            {
                                
                                builderSaveLoadScr.builderData[posFound.x, posFound.y].block = new LevelBlockData(builderSaveLoadScr.upcomingBlockElev, LevelBlockData_BlockType.GENERICSOLID, builderSaveLoadScr.upcomingBlockSlant, -1, blockTex, builderSaveLoadScr.builderData[posFound.x, posFound.y].block.spawnPoint);

                                builderSaveLoadScr.RenderBuilderData(posFound.x, posFound.y);
                            }
                        }
                        //print(builderSaveLoadScr.builderData[posFound.x, posFound.y].block.blockTexPath.ToString() + Time.deltaTime.ToString());


                    }

                }


                if (inputScr.GetInputByName("Building", "Editor_RemoveBlock") > 0)
                {



                    if (posFound.x != LevelBuilderSaveLoad.invalidIntVec2.x && posFound.y != LevelBuilderSaveLoad.invalidIntVec2.y)
                    {

                        if (builderSaveLoadScr.builderData[posFound.x, posFound.y].block.blockType != LevelBlockData_BlockType.INVISIBLE)
                        {
                            builderSaveLoadScr.builderData[posFound.x, posFound.y].block = new LevelBlockData(1000, LevelBlockData_BlockType.INVISIBLE,LevelBlockData_BlockAngle.NONE,-1,"", builderSaveLoadScr.builderData[posFound.x, posFound.y].block.spawnPoint);

                            builderSaveLoadScr.RenderBuilderData(posFound.x, posFound.y);
                        }

                    }

                }
            }
        }




    }




    private void Update()
    {
        if(FindDependencies())
        {

            Update_PlaceRemoveBlocks();
        }

    }




    bool FindDependencies()
    {
        bool ret = false;

        if (GameObject.FindObjectOfType<LevelBuilderCameraMovement>() != null)
        {

            levelEditorCam = GameObject.FindObjectOfType<LevelBuilderCameraMovement>().GetComponent<Camera>();
        }

        inputScr = GameObject.FindObjectOfType<InputManager>();

        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();
        if (levelEditorCam != null && inputScr != null && builderSaveLoadScr!=null)
        {
            ret = true;
        }

        return ret;
    }
}
