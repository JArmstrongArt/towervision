﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class WaveProgrammerSingleGroupWorldCanvas : LevelEditorCanvasInputToggles
{


    private LevelBuilderSaveLoad builderSaveLoadScr;

    [SerializeField] TMP_InputField appearCount_inp;

    [SerializeField] TMP_InputField betweenDelay_inp;

    [SerializeField] TMP_InputField postDelay_inp;


    [SerializeField] RectTransform groupMonsterBG_rect;

    [SerializeField] GameObject addGroupObj;
    public Image groupMonster_img;

    private Camera waveProgrammerCamera;

    private Canvas canv;

    private GameObject monsterSelectButtonCanvas_prefab;
    private List<GameObject> spawnedMonsterSelectPrefabs = new List<GameObject>();
    private List<monsterIcons> loadedIcons = new List<monsterIcons>();


    [SerializeField] GameObject deleteGroupObj;

    public void AddGroup()
    {
        if (FindDependencies() && FindResources())
        {
            builderSaveLoadScr.AddGroupToWave();

        }
    }

    public void ManualAwake()
    {

        if (FindDependencies() && FindResources())
        {
            loadedIcons = ExtendedMaths.LoadAllMonsterIcons();
            canv.worldCamera = waveProgrammerCamera;
            int canvIndex = FindSelfInCanvasInstances();

            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {


                AppearCount_Get();
                BetweenDelay_Get();
                PostDelay_Get();
                MonsterToSpawnIcon_Get();

                if (canvIndex == 0)
                {
                    deleteGroupObj.SetActive(false);
                }
                else
                {
                    deleteGroupObj.SetActive(true);

                }
            }
        }
    }

    Sprite MonsterToSpawnIcon_Get()
    {
        monsterIcons fallback = loadedIcons[0];
        Sprite ret = fallback.spr;
        if(FindDependencies() && FindResources())
        {
            int canvIndex = FindSelfInCanvasInstances();
            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {
                //you have to copy the variable of the struct to modify it due to structs being a value type. it looks stupid but trust me it has to be this way.
                MonsterGroup curGroup = builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex];
                bool fallbackMode = true;
                foreach(monsterIcons ico in loadedIcons)
                {
                    if(ico.sprSourcePrefabMonster == builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex].monster_prefabDirectory)
                    {
                        ret = ico.spr;
                        fallbackMode = false;
                        break;
                    }
                }
                if (fallbackMode == true)
                {
                    curGroup.monster_prefabDirectory = fallback.sprSourcePrefabMonster;
                    builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex] = curGroup;
                }
            }
        }
        groupMonster_img.sprite = ret;
        return ret;
    }



    public void GroupMonsterSelectAppear()
    {
        if (FindDependencies() && FindResources())
        {
            int canvIndex = FindSelfInCanvasInstances();
            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {
                spawnedMonsterSelectPrefabs = ExtendedMaths.DestroyGameObjectList(spawnedMonsterSelectPrefabs);


                RectTransform selectRect = monsterSelectButtonCanvas_prefab.GetComponent<RectTransform>();
                if (selectRect != null)
                {
                    float prefabWidth = ExtendedMaths.GetWorldSizeOfRectInUnits(selectRect).x;

                    Vector3 appearOrigin = ExtendedMaths.GetWorldPosOfRectPos(groupMonsterBG_rect, RectReturnPos.Bottom);
                    float offsetFromSrc = -(prefabWidth*0.5f);
                    float popOut = 0.5f;
                    appearOrigin = new Vector3(appearOrigin.x, appearOrigin.y + offsetFromSrc, appearOrigin.z - popOut);

                    if (loadedIcons == null || (loadedIcons != null && loadedIcons.Count <= 0))
                    {
                        loadedIcons = ExtendedMaths.LoadAllMonsterIcons();

                    }

                    float startXOffset = -(prefabWidth * ((float)(loadedIcons.Count - 1) / (float)2));

                    foreach (monsterIcons icoData in loadedIcons)
                    {

                        GameObject newButton = Instantiate(monsterSelectButtonCanvas_prefab, new Vector3(appearOrigin.x + startXOffset, appearOrigin.y, appearOrigin.z), Quaternion.Euler(Vector3.zero), null);

                        if (newButton.GetComponent<Canvas>() != null)
                        {
                            newButton.GetComponent<Canvas>().worldCamera = waveProgrammerCamera;
                        }
                        spawnedMonsterSelectPrefabs.Add(newButton);
                        MonsterSelectButtonCanvas selScr = newButton.GetComponent<MonsterSelectButtonCanvas>();

                        if (selScr != null)
                        {
                            selScr.monsterIcon_img.sprite = icoData.spr;
                            selScr.relevantMonsterAssetPath = icoData.sprSourcePrefabMonster;
                            selScr.canvasIndexPassedAlong = canvIndex;
                            selScr.builderSaveLoadScr = builderSaveLoadScr;
                            selScr.waveGroupScr = this;
                        }
                        startXOffset += prefabWidth;
                    }
                }

            }

            
        }


    }

    public void DeleteGroup()
    {
        if (FindDependencies() && FindResources())
        {
            int canvIndex = FindSelfInCanvasInstances();
            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {
                builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.RemoveAt(canvIndex);
                builderSaveLoadScr.SpawnWaveGroupCanvases();
            }
        }
    }

    public void AppearCount_Refresh()
    {
        if (FindDependencies() && FindResources())
        {


            ExtendedMaths.TMPTextVerify_InputField(appearCount_inp);




        }

    }

    public void PostDelay_Refresh()
    {
        if (FindDependencies() && FindResources())
        {


            ExtendedMaths.TMPTextVerify_InputField(postDelay_inp);



        }

    }

    public void BetweenDelay_Refresh()
    {
        if (FindDependencies() && FindResources())
        {

            ExtendedMaths.TMPTextVerify_InputField(betweenDelay_inp);




        }

    }

    public void AppearCount_Set()
    {
        if (FindDependencies() && FindResources())
        {

            int canvIndex = FindSelfInCanvasInstances();
            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {

                int setVal = Mathf.RoundToInt(ExtendedMaths.GetNumFromInputField(appearCount_inp,1,int.MaxValue));


                //you have to copy the variable of the struct to modify it due to structs being a value type. it looks stupid but trust me it has to be this way.
                MonsterGroup curGroup = builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex];
                curGroup.amount = setVal;

                builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex] = curGroup;


            }




        }

    }

    void UpdateRenderStatusOfAddGroup()
    {
        if(FindDependencies() && FindResources())
        {
            int canvIndex = FindSelfInCanvasInstances();
            if(canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {
                if(canvIndex== builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count - 1)
                {
                    addGroupObj.SetActive(true);
                }
                else
                {

                    addGroupObj.SetActive(false);
                }
            }
        }
    }

    int AppearCount_Get()
    {
        int ret = 1;
        if (FindDependencies() && FindResources())
        {

            int canvIndex = FindSelfInCanvasInstances();
            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {
                ret = builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex].amount;



            }

            appearCount_inp.text = ret.ToString();
            ExtendedMaths.TMPTextVerify_InputField(appearCount_inp);




        }
        return ret;
    }

    float BetweenDelay_Get()
    {
        float ret = 0;
        if (FindDependencies() && FindResources())
        {

            int canvIndex = FindSelfInCanvasInstances();
            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {
                ret = builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex].betweenSpawnDelay;



            }

            betweenDelay_inp.text = ret.ToString();
            ExtendedMaths.TMPTextVerify_InputField(betweenDelay_inp);



        }
        return ret;
    }

    public void BetweenDelay_Set()
    {
        if (FindDependencies() && FindResources())
        {

            int canvIndex = FindSelfInCanvasInstances();
            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {

                float setVal = ExtendedMaths.GetNumFromInputField(betweenDelay_inp,0,float.MaxValue);


                //you have to copy the variable of the struct to modify it due to structs being a value type. it looks stupid but trust me it has to be this way.
                MonsterGroup curGroup = builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex];
                curGroup.betweenSpawnDelay = setVal;

                builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex] = curGroup;


            }




        }

    }

    float PostDelay_Get()
    {
        float ret = 0;
        if (FindDependencies() && FindResources())
        {

            int canvIndex = FindSelfInCanvasInstances();
            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {
                ret = builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex].postSpawnDelay;



            }

            postDelay_inp.text = ret.ToString();
            ExtendedMaths.TMPTextVerify_InputField(postDelay_inp);



        }
        return ret;
    }


    public void PostDelay_Set()
    {
        if (FindDependencies() && FindResources())
        {

            int canvIndex = FindSelfInCanvasInstances();
            if (canvIndex >= 0 && canvIndex < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
            {

                float setVal = ExtendedMaths.GetNumFromInputField(postDelay_inp, 0,float.MaxValue);


                //you have to copy the variable of the struct to modify it due to structs being a value type. it looks stupid but trust me it has to be this way.
                MonsterGroup curGroup = builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex];
                curGroup.postSpawnDelay = setVal;

                builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvIndex] = curGroup;


            }




        }

    }

    private void Update()
    {
        UpdateRenderStatusOfAddGroup();


    }

    bool FindDependencies()
    {
        bool ret = false;

        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();

        canv = gameObject.GetComponent<Canvas>();

        if (GameObject.FindObjectOfType<WaveProgrammerCameraMovement>() != null)
        {
            waveProgrammerCamera = GameObject.FindObjectOfType<WaveProgrammerCameraMovement>().GetComponent<Camera>();
        }

        /*
        if (builderSaveLoadScr == null)
        {
            print("builder not found");
        }

        if (canv == null)
        {
            print("canv not found");
        }

        if (waveProgrammerCamera == null)
        {
            print("wavecam not found");
        }
        */

        if (canv!=null && waveProgrammerCamera!=null && builderSaveLoadScr != null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;
        if (monsterSelectButtonCanvas_prefab == null)
        {
            monsterSelectButtonCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/waveProgrammer_monsterSelectButtonCanvas");
        }
        if (monsterSelectButtonCanvas_prefab != null)
        {
            ret = true;
        }
        return ret;
    }

    int FindSelfInCanvasInstances()
    {
        int ret = -1;
        
        if (FindDependencies())
        {

            for(int i=0;i< builderSaveLoadScr.editorWaveGroupCanvasInstances.Count;i++)
            {

                if (builderSaveLoadScr.editorWaveGroupCanvasInstances[i] == gameObject)
                {
                    ret = i;
                    break;
                }
            }


        }

        return ret;
    }
}
