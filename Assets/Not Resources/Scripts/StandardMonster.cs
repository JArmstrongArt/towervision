﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the class for the things specific to the standard monster
public class StandardMonster : MonsterInherit
{
    private GameObject soundEffect_walk_prefab;
    private GameObject soundEffect_walk_inst;
    protected override void Awake()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.Awake();
            base.animScr.PlayAnimation("walk");
            GeneralSoundEffect walkSEScr = soundEffect_walk_prefab.GetComponent<GeneralSoundEffect>();
            if (walkSEScr != null)
            {
                soundEffect_walk_inst = Instantiate(soundEffect_walk_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                soundEffect_walk_inst.GetComponent<GeneralSoundEffect>().ownerObj = gameObject;
            }
        }

    }

    private void LateUpdate()
    {
        if(base.FindDependencies() && FindResources())
        {
            if (soundEffect_walk_inst != null)
            {
                soundEffect_walk_inst.transform.position = gameObject.transform.position;
            }
        }

    }

    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;
        if (soundEffect_walk_prefab == null)
        {
            soundEffect_walk_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_slimeWalk");
        }
        if(soundEffect_walk_prefab!=null && parRet == true)
        {
            ret = true;
        }

        return ret;
    }

}
