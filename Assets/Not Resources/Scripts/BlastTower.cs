﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastTower : TowerInherit
{
    private GameObject projectile_prefab;
    [SerializeField] bool projectile_inheritDamage;
    private float awarenessRadius_orig;
    private Vector3 circleLocalScale = Vector3.one;
    protected override void Awake()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.Awake();
            awarenessRadius_orig = base.awarenessRadius;

            if (base.circleObj != null)
            {
                circleLocalScale = base.circleObj.transform.localScale;
            }

        }
    }
    protected override void Attack()
    {
        if(base.FindDependencies() && FindResources())
        {
            if (monster_nearest != null)
            {
                GameObject blastBomb_inst = Instantiate(projectile_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                BlastProjectile projScr = blastBomb_inst.GetComponent<BlastProjectile>();
                if (projScr != null)
                {
                    Vector3 aimDir = (new Vector3(monster_nearest.transform.position.x, gameObject.transform.position.y, monster_nearest.transform.position.z) - gameObject.transform.position).normalized;
                    projScr.moveDir = aimDir;
                    projScr.srcTower = this;
                    if (projectile_inheritDamage)
                    {
                        projScr.blastDamage = base.damage;
                    }
                    base.animScr.billboardWorldDir_set(aimDir);

                }
                else
                {
                    Destroy(blastBomb_inst);
                }
            }
        }


    }

    protected override void UpgradeEffectUpdate()
    {
        if (FindResources() && base.FindDependencies())
        {
            base.awarenessRadius = awarenessRadius_orig * base.upgrades[base.upgradeLevel - 1].optionalParameter1;

            if (base.circleObj != null)
            {
                base.circleObj.transform.localScale = new Vector3(circleLocalScale.x * base.upgrades[base.upgradeLevel - 1].optionalParameter1, circleLocalScale.y * base.upgrades[base.upgradeLevel - 1].optionalParameter1, circleLocalScale.z * base.upgrades[base.upgradeLevel - 1].optionalParameter1);
            }
        }
    }

    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;
        if (projectile_prefab == null)
        {
            projectile_prefab = Resources.Load<GameObject>("Prefabs/Towers/Projectiles/tower_blast_projectile");
        }

        if (projectile_prefab != null && parRet==true)
        {
            ret = true;
        }

        return ret;
    }
}
