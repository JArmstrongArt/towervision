﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

enum SwitchCanvasMode
{
    TITLE,
    OPTIONS,
    CREDITS,
    LEVELFOLDERSELECT,
    LEVELSELECT
}

public class MainMenuHotbarCanvas : MonoBehaviour
{

    private GameObject titleScreenCanvasObj;
    private GameObject optionsCanvasObj;
    private GameObject creditsCanvasObj;
    private GameObject levelFolderSelectCanvasObj;
    private GameObject levelSelectCanvasObj;



    private SwitchCanvasMode currentCanvas = SwitchCanvasMode.TITLE;

    private bool lateAwake = false;
    private void Awake()
    {
        if (FindDependencies())
        {
            lateAwake = false;

        }
        
    }


    public void SwitchCanvases_PlaySensetive()
    {
        if (FindDependencies())
        {
            List<System.IO.FileInfo> allUserLevels = ExtendedMaths.LoadAllLevelsInFolder(Application.dataPath + "/" + "levels" + "/" + LevelSaveLoad.SaveLoadFolderToString(LevelSaveLoadFolder.USER) + "/");

            if (ExtendedMaths.ListTrueEmpty(allUserLevels))
            {
                LevelSaveLoad.folderToLoad = LevelSaveLoadFolder.OFFICIAL;

                if (currentCanvas != SwitchCanvasMode.LEVELSELECT)
                {
                    SwitchCanvases((int)SwitchCanvasMode.LEVELSELECT);
                }
                else
                {

                    SceneManager.LoadScene("Story");
                }
                

            }
            else
            {
                if (currentCanvas != SwitchCanvasMode.LEVELSELECT)
                {
                    SwitchCanvases((int)SwitchCanvasMode.LEVELFOLDERSELECT);
                }
                else
                {

                    SceneManager.LoadScene("Story");
                }
            }

        }
    }


    
    public void SwitchCanvases_TitleSensetive()
    {
        if (FindDependencies())
        {
            if (currentCanvas == SwitchCanvasMode.TITLE)
            {
                Application.Quit();
            }
            else
            {
                SwitchCanvases((int)SwitchCanvasMode.TITLE);
            }
        }
    }

    private void Update()
    {
        if (FindDependencies())
        {
            if (lateAwake == false)
            {
                SwitchCanvases((int)currentCanvas);
                lateAwake = true;
            }
        }
    }

    public void SwitchCanvases(int newCanvas)
    {
        if (FindDependencies())
        {
            if(System.Enum.IsDefined(typeof(SwitchCanvasMode), newCanvas))
            {
                ToggleDependencies(false);
                currentCanvas = (SwitchCanvasMode)newCanvas;

                switch (currentCanvas)
                {
                    case SwitchCanvasMode.TITLE:
                        titleScreenCanvasObj.SetActive(true);
                        break;
                    case SwitchCanvasMode.OPTIONS:
                        optionsCanvasObj.SetActive(true);
                        break;
                    case SwitchCanvasMode.CREDITS:
                        creditsCanvasObj.SetActive(true);
                        break;
                    case SwitchCanvasMode.LEVELFOLDERSELECT:
                        levelFolderSelectCanvasObj.SetActive(true);
                        break;
                    case SwitchCanvasMode.LEVELSELECT:
                        levelSelectCanvasObj.SetActive(true);

                        break;
                }
            }
        }
    }

    void ToggleDependencies(bool tog)
    {
        if (FindDependencies())
        {
            titleScreenCanvasObj.SetActive(tog);
            optionsCanvasObj.SetActive(tog);
            creditsCanvasObj.SetActive(tog);
            levelFolderSelectCanvasObj.SetActive(tog);
            levelSelectCanvasObj.SetActive(tog);
        }
    }

    public void LoadLevelEditor()
    {
        if (FindDependencies())
        {
            SceneManager.LoadScene("LevelEditor");
        }
        
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (GameObject.FindObjectOfType<TitleScreenCanvas>() != null)
        {
            titleScreenCanvasObj = GameObject.FindObjectOfType<TitleScreenCanvas>().gameObject;
        }

        if (GameObject.FindObjectOfType<OptionsCanvas>() != null)
        {
            optionsCanvasObj = GameObject.FindObjectOfType<OptionsCanvas>().gameObject;
        }

        if (GameObject.FindObjectOfType<CreditsCanvas>() != null)
        {
            creditsCanvasObj = GameObject.FindObjectOfType<CreditsCanvas>().gameObject;
        }

        if (GameObject.FindObjectOfType<LevelFolderSelectCanvas>() != null)
        {
            levelFolderSelectCanvasObj = GameObject.FindObjectOfType<LevelFolderSelectCanvas>().gameObject;
        }
        if (GameObject.FindObjectOfType<LevelSelectCanvas>() != null)
        {
            levelSelectCanvasObj = GameObject.FindObjectOfType<LevelSelectCanvas>().gameObject;
        }


        if (levelSelectCanvasObj != null && levelFolderSelectCanvasObj != null && creditsCanvasObj != null && titleScreenCanvasObj != null && optionsCanvasObj != null)
        {
            ret = true;
        }

        return ret;

    }
}
