﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.SceneManagement;
//defines the possible rays this script can choose to acknowledge as the relevant one.
public enum DominantRay
{
    None,
    Build,
    BuildBlock,
    BuildInteract,
    Attack
}

public enum DominantMode
{
    Build,
    Fight
}

enum TowerHealAmountReturn
{
    Max,
    Actual,
    MoneyToPay
}


//im considering splitting this into separate components, but basically this script handles all the possible actions a player can take during live gameplay.
public class PlayerActions : MonoBehaviour
{
    [SerializeField] GenericRay buildRay;//a ray that scans if there is floor to build upon

    [SerializeField] GenericRay monsterFightRay;
    public GenericRay towerInteractRay;//a ray that only collides with towers


    private InputManager inputScr;

    private PlayerStats statsScr;
    [HideInInspector]
    public DominantRay dominantRay;
    private DominantMode dominantMode;
    private GameObject placableTower_prefab;
    private GameObject placableTower_inst;

    [HideInInspector]
    public TowerTypes towerToPlace;//this is in here and not placabletower as you might expect because the object that placabletower is attached to deletes and respawns frequently when the dominant ray loses and starts being the build ray respectively, i didn't want ppl to have to reselect which tower they will put down over and over again, and i thought having one static variable in an otherwise non-static class would just be weird.


    private bool hotbar_axisUsed;
    private MatchManager matchManagerScr;
    private bool waveControl_axisUsed;
    private bool switchMode_axisUsed;
    private WeaponAnimations weaponAnimScr;
    [SerializeField] float meleeDamage;
    [SerializeField] float healToMoneyConversionMultiplier;
    private Vector3 towerSpawnPoint;
    private GameObject soundEffect_wrenchFail_prefab;
    private GameObject soundEffect_wrenchHit_prefab;
    private GameObject soundEffect_weaponMiss_prefab;
    private GameObject soundEffect_hammerHit_prefab;
    private bool weaponRespondCheck;
    private bool weaponActionCertain;
    private bool spawnWeaponMissInstance;
    private GameObject towerUpgradeAndHealCanvas_inst;
    private GameObject towerUpgradeAndHealCanvas_prefab;
    [SerializeField] float towerHealAmount_percent;

    private bool pauseAxis_used = false;

    private GameObject pauseCanvasObj;

    private TimeScaling timeScaleScr;
    [HideInInspector]
    public bool gamePaused=false;

    private bool lateAwake=false;

    private PlayerWinLose winLoseScr;
    // Start is called before the first frame update

    private void OnDisable()
    {
        if(FindDependencies() && FindResources())
        {
            lateAwake = false;

        }
    }



    void Update_LateAwake()
    {
        if(FindDependencies() && FindResources())
        {



            if (winLoseScr.gameLost == false && winLoseScr.gameWon == false)
            {
                if (lateAwake == false)
                {
                    pauseSet(timeScaleScr.paused);



                }
            }

        }

    }

    void DebugButtons()
    {
        if(FindDependencies() && FindResources())
        {
            if (inputScr.GetInputByName("DEBUG", "DBG_RELOADSCENE")>0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }

            if (towerInteractRay.ray_surface != null)
            {

                TowerInherit towerScr = towerInteractRay.ray_surface.GetComponent<TowerInherit>();
                if (towerScr != null)
                {
                    
                    if(inputScr.GetInputByName("DEBUG", "DBG_DELETETOWER") > 0)
                    {
                        
                        towerScr.ShiftHealth(-Mathf.Abs( towerScr.health_max));
                    }
                }
            }
        }
    }

    void ModeSwitchHandler()
    {
        if (FindDependencies() && FindResources())
        {
            bool inputSuccess = false;

            if (inputScr.GetInputByName("General", "SwitchMode") > 0)
            {
                inputSuccess = true;
            }

            if (inputSuccess)
            {
                if (switchMode_axisUsed == false)
                {
                    int newEnumVal_proposed = (int)dominantMode + 1;
                    if (!Enum.IsDefined(typeof(DominantMode), newEnumVal_proposed))
                    {
                        newEnumVal_proposed = Enum.GetValues(typeof(DominantMode)).Cast<int>().Min();
                    }
                    dominantMode = (DominantMode)newEnumVal_proposed;


                    switch (dominantMode)
                    {
                        case DominantMode.Build:
                            weaponAnimScr.ChangeWeapon("wrench");

                            break;
                        case DominantMode.Fight:
                            weaponAnimScr.ChangeWeapon("hammer");
                            break;
                        default:
                            weaponAnimScr.ChangeWeapon("wrench");
                            break;
                    }

                    switchMode_axisUsed = true;
                }
            }
            else
            {
                switchMode_axisUsed = false;
            }
        }
    }



    //this function is used to determine what dominantRay should be set to, and therefore which of the rays used for world interaction should be acknowledged actively.
    void DecideDominantRay()
    {
        if(FindDependencies() && FindResources())
        {
            switch (dominantMode)
            {
                case DominantMode.Build:
                    if (buildRay.ray_active == false && towerInteractRay.ray_active == false)
                    {
                        dominantRay = DominantRay.None;

                    }

                    if (towerInteractRay.ray_active == false)
                    {
                        if (buildRay.ray_active == true)
                        {

                            if (buildRay.ray_hit.normal.y ==1.0f)
                            {
                                dominantRay = DominantRay.Build;
                            }
                            else
                            {
                                
                                dominantRay = DominantRay.BuildBlock;//if a floor has sides that arent facing up, i dont want the user to be able to build, it'll mess with the grid snapping.
                            }

                        }
                        else
                        {
                            dominantRay = DominantRay.None;//this isnt really neccesary after i said at the start that if literally none of the rays are true then make dominantray into none, but it felt wrong not to include it, bit stupid i know.
                        }
                    }
                    else
                    {
                        dominantRay = DominantRay.BuildInteract;

                    }
                    break;
                case DominantMode.Fight:
                    if (monsterFightRay.ray_active == true)
                    {
                        dominantRay = DominantRay.Attack;
                    }
                    else
                    {
                        dominantRay = DominantRay.None;
                    }

                    break;
            }
        }



    }


    //uses a linerenderer to outline the current gridspace you'll be placing a tower in when relevant, like the little block outline in minecraft.
    void PlaceTowers_DrawGrid()
    {
        if(FindDependencies() && FindResources())
        {
            if (gameObject.GetComponent<LineRenderer>() != null)
            {
                LineRenderer line = gameObject.GetComponent<LineRenderer>();

                if (dominantRay == DominantRay.Build)//if the most relevant ray as decided by DecideDominantRay() is the ray for building towers
                {


                    Vector3 buildRay_hitSnapped = GridSnap.SnapToGrid(buildRay.ray_hitPoint, true);//set the exact build point of the tower to the center of the grid tile.


                    line.positionCount = 4;//there are 4 corners on the square of a grid tile

                    //draw a square around the grid tile
                    //TL
                    line.SetPosition(0, new Vector3(buildRay_hitSnapped.x - (GridSnap.GRID_SIZE * 0.5f), buildRay_hitSnapped.y, buildRay_hitSnapped.z - (GridSnap.GRID_SIZE * 0.5f)));
                    //TR
                    line.SetPosition(1, new Vector3(buildRay_hitSnapped.x + (GridSnap.GRID_SIZE * 0.5f), buildRay_hitSnapped.y, buildRay_hitSnapped.z - (GridSnap.GRID_SIZE * 0.5f)));
                    //BR
                    line.SetPosition(2, new Vector3(buildRay_hitSnapped.x + (GridSnap.GRID_SIZE * 0.5f), buildRay_hitSnapped.y, buildRay_hitSnapped.z + (GridSnap.GRID_SIZE * 0.5f)));
                    //BL
                    line.SetPosition(3, new Vector3(buildRay_hitSnapped.x - (GridSnap.GRID_SIZE * 0.5f), buildRay_hitSnapped.y, buildRay_hitSnapped.z + (GridSnap.GRID_SIZE * 0.5f)));

                    //set a few other little tidbits to make it look the way i want
                    line.loop = true;
                    line.startColor = Color.red;
                    line.endColor = Color.blue;

                    line.enabled = true;
                }
                else
                {
                    //hide the grid drawing if the dominant ray is not the build ray
                    line.enabled = false;
                }
            }
        }



    }

    void LateUpdate_CreatePlacableTowerInst()
    {
        if(FindDependencies() && FindResources())
        {
            if (dominantRay == DominantRay.Build)
            {
                //Vector3 placableTowerPos = GridSnap.SnapToGrid(buildRay.ray_hitPoint, true);
                Vector3 placableTowerPos = buildRay.ray_hitPoint;


                if (placableTower_inst == null)
                {
                    placableTower_inst = Instantiate(placableTower_prefab, placableTowerPos, Quaternion.Euler(Vector3.zero), null);
                }
                else
                {
                    placableTower_inst.transform.position = placableTowerPos;
                    BillboardAnimations placeAnimScr = placableTower_inst.GetComponent<BillboardAnimations>();
                    if (placeAnimScr != null)
                    {
                        placeAnimScr.billboardWorldDir_set(-gameObject.transform.forward);
                    }


                }
            }
            else
            {
                if (placableTower_inst != null)
                {
                    Destroy(placableTower_inst);
                }
            }
        }

    }

    EntityInherit FightMonsters_WeaponReflect_Allowed()
    {
        EntityInherit ret = null;
        if (FindDependencies() && FindResources())
        {

            GameObject monsterToHurt = monsterFightRay.ray_surface;
            if (monsterToHurt != null)
            {

                EntityInherit entScr = monsterToHurt.GetComponent<EntityInherit>();

                if (entScr != null)
                {
                    ret = entScr;

                }
            }
        }


        return ret;
    }

    public void FightMonsters_WeaponReflect()
    {
        if (FindDependencies() && FindResources())
        {

            if (FightMonsters_WeaponReflect_Allowed() != null)
            {
                FightMonsters_WeaponReflect_Allowed().ShiftHealth(-meleeDamage);
                Instantiate(soundEffect_hammerHit_prefab, null);
            }
        }

    }

    public void pauseSet(bool pause)
    {
        if(FindDependencies() && FindResources())
        {
            gamePaused = pause;
            timeScaleScr.paused = gamePaused;

            pauseCanvasObj.SetActive(gamePaused);
            inputScr.input_enabled = !gamePaused;
        }

    }

    void Update_PauseGame()
    {
        if(FindDependencies() && FindResources())
        {
            if(inputScr.GetInputByName("Pausing", "PauseGame",false,true) > 0)
            {
                if (pauseAxis_used == false)
                {
                    if(winLoseScr.gameLost == false && winLoseScr.gameWon== false)
                    {
                        pauseSet(!gamePaused);
                    }



                    pauseAxis_used = true;
                }
            }
            else
            {
                pauseAxis_used = false;
            }
            



        }
    }


    void Update_Weapon_FightMonsters()
    {
        if (FindDependencies() && FindResources())
        {
            monsterFightRay.customDir_vec = gameObject.transform.forward;





            if (inputScr.GetInputByName("General", "Action1") > 0)//if the button for building towers is pressed
            {
                if (WeaponReady())
                {

                    if (dominantRay == DominantRay.None)
                    {
                        weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Null_WeaponReflect";
                        spawnWeaponMissInstance = true;
                    }
                    else
                    {
                        if (dominantRay == DominantRay.Attack)
                        {
                            if (FightMonsters_WeaponReflect_Allowed() != null)
                            {

                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "FightMonsters_WeaponReflect";
                            }
                            else
                            {

                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Preventative_WeaponReflect";

                            }
                            weaponActionCertain = true;
                        }
                        else
                        {
                            if(weaponActionCertain == false)
                            {
                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Preventative_WeaponReflect";

                            }

                        }
                    }

                    weaponRespondCheck = true;
                }
            }

        }
    }

    //a function to run in Update() that controls the system for placing towers
    void Update_Weapon_PlaceTowers()
    {
        if (FindDependencies() && FindResources())
        {
            buildRay.customDir_vec = gameObject.transform.forward;//set the build rays direction to that of gameObject's forward direction, which in the case of the camera, is where you are looking.





            if (inputScr.GetInputByName("General", "Action1") > 0)//if the button for building towers is pressed
            {
                if (WeaponReady())
                {


                    if (dominantRay == DominantRay.None)
                    {
                        weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Null_WeaponReflect";
                        spawnWeaponMissInstance = true;
                    }
                    else
                    {
                        if (dominantRay == DominantRay.Build)
                        {
                            if (PlaceTowers_WeaponReflect_Allowed() != null)
                            {

                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "PlaceTowers_WeaponReflect";
                            }
                            else
                            {
                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Preventative_WeaponReflect";

                            }
                            weaponActionCertain = true;
                        }
                        else
                        {
                            if (weaponActionCertain == false)
                            {
                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Preventative_WeaponReflect";

                            }

                        }
                    }

                    weaponRespondCheck = true;
                }
            }
        }

    }

    void WeaponAnims_Respond()
    {
        if (FindDependencies() && FindResources())
        {

            if (weaponAnimScr.allWeaponAnimations_index >= 0 && weaponAnimScr.allWeaponAnimations_index < weaponAnimScr.allWeaponAnimations.Length)
            {

                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].PlayAnimation();
            }
        }

    }
    void Update_ChooseTowerToPlace()
    {
        if (FindDependencies() && FindResources())
        {
            if (inputScr.GetGroupIndexByName("Hotbar") >= 0)
            {

                if (inputScr.allInputValues[inputScr.GetGroupIndexByName("Hotbar")].HasActivity() == true)
                {



                    if (hotbar_axisUsed == false)
                    {
                        if (dominantMode == DominantMode.Build)
                        {
                            if (placableTower_prefab != null)
                            {
                                PlacableTower placeTowerScr = placableTower_prefab.GetComponent<PlacableTower>();
                                if (placeTowerScr != null)
                                {
                                    int directHotbarAccess_index = -1;
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar1") > 0)
                                    {
                                        directHotbarAccess_index = 0;

                                    }
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar2") > 0)
                                    {
                                        directHotbarAccess_index = 1;

                                    }
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar3") > 0)
                                    {
                                        directHotbarAccess_index = 2;

                                    }
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar4") > 0)
                                    {
                                        directHotbarAccess_index = 3;

                                    }
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar5") > 0)
                                    {
                                        directHotbarAccess_index = 4;

                                    }
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar6") > 0)
                                    {
                                        directHotbarAccess_index = 5;

                                    }
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar7") > 0)
                                    {
                                        directHotbarAccess_index = 6;

                                    }
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar8") > 0)
                                    {
                                        directHotbarAccess_index = 7;

                                    }
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar9") > 0)
                                    {
                                        directHotbarAccess_index = 8;

                                    }
                                    if (inputScr.GetInputByName("Hotbar", "Hotbar0") > 0)
                                    {
                                        directHotbarAccess_index = 9;

                                    }

                                    if (directHotbarAccess_index >= 0)
                                    {
                                        if (placeTowerScr.placableTowerList.Length >= directHotbarAccess_index + 1)
                                        {
                                            if (placeTowerScr.placableTowerList[directHotbarAccess_index].towerPrefab != null)
                                            {
                                                towerToPlace = placeTowerScr.placableTowerList[directHotbarAccess_index].whatTowerIsThis;
                                            }


                                        }
                                    }

                                    if (placableTower_inst != null)
                                    {
                                        PlacableTower placeTowerScr_instVersion = placableTower_inst.GetComponent<PlacableTower>();
                                        if (placeTowerScr_instVersion != null)
                                        {
                                            placeTowerScr_instVersion.refreshCircleCheck = true;
                                        }
                                    }


                                }
                            }
                        }




                        hotbar_axisUsed = true;
                    }
                }
                else
                {
                    hotbar_axisUsed = false;
                }
            }



        }

    }

    PlacableTower PlaceTowers_WeaponReflect_Allowed()
    {
        PlacableTower ret = null;
        if (FindDependencies() && FindResources())
        {
            towerSpawnPoint = GridSnap.SnapToGrid(buildRay.ray_hitPoint, true);//set the tower spawn point to the center of the tile that buildray is hitting.
            if (!EntityTracker.ALL_TOWER_PLACEMENTS.Contains(towerSpawnPoint))//if this upcoming towers place on the grid is not occupied by an existing tower
            {

                if (placableTower_inst != null)
                {
                    PlacableTower placableTowerScr = placableTower_inst.GetComponent<PlacableTower>();


                    if (placableTowerScr != null)
                    {
                        if (placableTowerScr.FindTowerByType_towerPrefab(towerToPlace) != null)
                        {
                            if (statsScr.money - placableTowerScr.FindTowerByType_towerPrefab(towerToPlace).GetComponent<TowerInherit>().cost >= 0)//if you have enough money to pay for the tower
                            {
                                ret = placableTowerScr;


                            }
                        }



                    }
                }

            }
        }
        return ret;
    }
    public void PlaceTowers_WeaponReflect()
    {
        if (FindDependencies() && FindResources())
        {
            if (PlaceTowers_WeaponReflect_Allowed()!=null)
            {
                PlacableTower placableTowerScr = PlaceTowers_WeaponReflect_Allowed();
                statsScr.ShiftMoney(-placableTowerScr.FindTowerByType_towerPrefab(towerToPlace).GetComponent<TowerInherit>().cost);//pay the money for the tower


                GameObject instTower = Instantiate(placableTowerScr.FindTowerByType_towerPrefab(towerToPlace), towerSpawnPoint, Quaternion.Euler(Vector3.zero), null);

                BillboardCameraCalcs camCalsScr = instTower.GetComponent<BillboardCameraCalcs>();//get a component from the tower instance that is inherited from or is the billboardcameracals class
                if (camCalsScr != null)//if the tower instance has that component
                {
                    camCalsScr.billboardWorldDir_set(-gameObject.transform.forward);//set the tower's sprite to be looking at the player when they place it
                }
                Instantiate(soundEffect_wrenchHit_prefab, null);
            }

        }

    }

    float HealTowers_WeaponReflect_towerHealAmount(TowerHealAmountReturn valueToReturn)
    {
        float retVal = -1.0f;
        if (FindDependencies() && FindResources())
        {
            TowerInherit towerScr = towerInteractRay.ray_surface.gameObject.GetComponent<TowerInherit>();
            if (towerScr != null)
            {
                EntityInherit entScr = towerScr.gameObject.GetComponent<EntityInherit>();
                if (entScr != null)
                {
                    float towerHealAmount_max = ((float)entScr.health_max / (float)100) * towerHealAmount_percent;
                    float towerHealAmount_actual = Mathf.Clamp(entScr.health_max - entScr.health_current, 0, towerHealAmount_max);
                    int towerHealAmount_moneyToPay = Mathf.CeilToInt(towerHealAmount_actual * healToMoneyConversionMultiplier);


                    switch (valueToReturn)
                    {
                        case TowerHealAmountReturn.Max:
                            retVal = towerHealAmount_max;
                            break;
                        case TowerHealAmountReturn.Actual:
                            retVal = towerHealAmount_actual;
                            break;
                        case TowerHealAmountReturn.MoneyToPay:
                            retVal = (float)towerHealAmount_moneyToPay;


                            break;
                    }
                    

                }
            }

        }
        return retVal;
    }
    bool HealTowers_WeaponReflect_Allowed()
    {
        bool ret = false;
        if (FindDependencies() && FindResources())
        {
            if (towerInteractRay != null)
            {
                if (towerInteractRay.ray_surface != null)
                {
                    TowerInherit towerScr = towerInteractRay.ray_surface.gameObject.GetComponent<TowerInherit>();//get a component from whatever the interact ray is hitting that is inherited from or is the towerinheret class, basically as long as the thing your hitting is a tower you're good

                    if (towerScr != null)//as long as towerscr successfully returned something
                    {
                        if (towerScr.buildingProgress >= 1)//if the tower is done building
                        {



                            EntityInherit entScr = towerScr.gameObject.GetComponent<EntityInherit>();
                            if (entScr != null)
                            {


                                if (statsScr.money - HealTowers_WeaponReflect_towerHealAmount(TowerHealAmountReturn.MoneyToPay) >= 0)
                                {
                                    if (entScr.health_max > entScr.health_current)
                                    {
                                        ret = true;
                                    }


                                }

                            }

                        }
                    }
                }
            }

        }
        return ret;
    }

    public void HealTowers_WeaponReflect()
    {
        if (FindDependencies() && FindResources())
        {
            
            if (towerInteractRay.ray_surface != null)
            {
                TowerInherit towerScr = towerInteractRay.ray_surface.GetComponent<TowerInherit>();//get a component from whatever the interact ray is hitting that is inherited from or is the towerinheret class, basically as long as the thing your hitting is a tower you're good

                if (towerScr != null)//as long as towerscr successfully returned something
                {
                    EntityInherit entScr = towerScr.gameObject.GetComponent<EntityInherit>();
                    if (entScr != null)
                    {

                        if (HealTowers_WeaponReflect_Allowed())
                        {
                            //must save temporarily to these local variables so that the return process of HealTowers_WeaponReflect_towerHealAmount does not affect the outcome of the 2nd call of it.
                            int newMoney = -Mathf.RoundToInt(HealTowers_WeaponReflect_towerHealAmount(TowerHealAmountReturn.MoneyToPay));
                            float newHealth = Mathf.Abs(HealTowers_WeaponReflect_towerHealAmount(TowerHealAmountReturn.Actual));
                            statsScr.ShiftMoney(newMoney);

                            entScr.ShiftHealth(newHealth);
                            Instantiate(soundEffect_wrenchHit_prefab, null);
                        }

                    }
                }
            }

        }

    }

    bool UpgradeTowers_WeaponReflect_Allowed()
    {
        bool ret = false;

        if (FindDependencies() && FindResources())
        {
            if (towerInteractRay != null)
            {
                if (towerInteractRay.ray_surface != null)
                {
                    TowerInherit towerScr = towerInteractRay.ray_surface.gameObject.GetComponent<TowerInherit>();//get a component from whatever the interact ray is hitting that is inherited from or is the towerinheret class, basically as long as the thing your hitting is a tower you're good

                    if (towerScr != null)//as long as towerscr successfully returned something
                    {
                        if (towerScr.buildingProgress >= 1)//if the tower is done building
                        {
                            if (towerScr.upgradeLevel < towerScr.upgrades.Length)//if there are upgrades available
                            {
                                if (towerScr.upgradeLevel >= 0)//as long as you're on the first upgrade or above
                                {
                                    if (towerScr.upgrades[towerScr.upgradeLevel].cost <= statsScr.money)//as long as you have the money to buy the current upgrade
                                    {
                                        ret = true;
                                    }
                                }

                            }
                        }
                    }
                }
            }


        }
        return ret;
    }
    public void UpgradeTowers_WeaponReflect()
    {
        if (FindDependencies() && FindResources())
        {
            if (UpgradeTowers_WeaponReflect_Allowed())
            {
                TowerInherit towerScr = towerInteractRay.ray_surface.gameObject.GetComponent<TowerInherit>();//get a component from whatever the interact ray is hitting that is inherited from or is the towerinheret class, basically as long as the thing your hitting is a tower you're good

                statsScr.ShiftMoney(-towerScr.upgrades[towerScr.upgradeLevel].cost);//take that money away
                towerScr.upgradeLevel += 1;//upgrade the tower once
                Instantiate(soundEffect_wrenchHit_prefab, null);
            }
        }



    }

    public void Preventative_WeaponReflect()//call this reflection method when you want the weapon to signify it cannot be used even if the dominant ray is valid
    {
        if(FindDependencies() && FindResources())
        {
            Instantiate(soundEffect_wrenchFail_prefab, null);
        }

    }

    public void Null_WeaponReflect()//call this reflection method when you want the weapon to default to doing nothing
    {

    }

    bool WeaponReady()
    {
        bool ret = false;
        if (FindDependencies() && FindResources())
        {
            if (weaponAnimScr.allWeaponAnimations_index >= 0 && weaponAnimScr.allWeaponAnimations_index < weaponAnimScr.allWeaponAnimations.Length)
            {
                if (weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index] != null)
                {
                    if (weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].IsReadyToFire() == true)
                    {
                        ret = true;

                    }
                }
            }
        }

        return ret;
    }

    //this function, ran in the Update() function, handles upgrading towers
    void Update_Weapon_UpgradeTowers()
    {
        if (FindDependencies() && FindResources())
        {
            towerInteractRay.customDir_vec = gameObject.transform.forward;//set the direction of the towerInteractWay to the forward direction of this gameobject, which on the camera, is wherever you are looking.


            if (inputScr.GetInputByName("General", "Action1") > 0)//if the button for interacting with towers is pressed
            {
                if (WeaponReady())
                {

                    if (dominantRay == DominantRay.None)
                    {
                        weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Null_WeaponReflect";
                        spawnWeaponMissInstance = true;
                    }
                    else
                    {
                        if (dominantRay == DominantRay.BuildInteract)
                        {
                            if (UpgradeTowers_WeaponReflect_Allowed())
                            {

                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "UpgradeTowers_WeaponReflect";
                            }
                            else
                            {
                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Preventative_WeaponReflect";

                            }
                            weaponActionCertain = true;
                        }
                        else
                        {
                            if(weaponActionCertain == false)
                            {
                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Preventative_WeaponReflect";
                            }
                            
                        }
                    }

                    weaponRespondCheck = true;

                }
            }

        }
    }


    void Update_Weapon_HealTowers()
    {
        if (FindDependencies() && FindResources())
        {
            towerInteractRay.customDir_vec = gameObject.transform.forward;//set the direction of the towerInteractWay to the forward direction of this gameobject, which on the camera, is wherever you are looking.


            if (inputScr.GetInputByName("General", "Action2") > 0)//if the button for interacting with towers is pressed
            {
                if (WeaponReady())
                {

                    if (dominantRay == DominantRay.None)
                    {
                        weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Null_WeaponReflect";
                        
                        spawnWeaponMissInstance = true;
                    }
                    else
                    {
                        if (dominantRay == DominantRay.BuildInteract)
                        {
                            if (HealTowers_WeaponReflect_Allowed())
                            {

                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "HealTowers_WeaponReflect";
                            }
                            else
                            {
                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Preventative_WeaponReflect";

                            }
                            weaponActionCertain = true;
                        }
                        else
                        {
                            if(weaponActionCertain == false)
                            {
                                weaponAnimScr.allWeaponAnimations[weaponAnimScr.allWeaponAnimations_index].currentMethodToReflect = "Preventative_WeaponReflect";
                            }
                            
                        }
                    }

                    weaponRespondCheck = true;
                }
            }

        }
    }

    void Update_ControlWaves()
    {
        if (FindDependencies() && FindResources())
        {
            if (matchManagerScr.FindDependencies())
            {
                if (inputScr.allInputValues[inputScr.GetGroupIndexByName("Wave Control")].HasActivity() == true)
                {
                    if (waveControl_axisUsed == false)
                    {
                        if (matchManagerScr.currentWaveIndex < matchManagerScr.matchData.allWaves.Length)
                        {
                            if (matchManagerScr.waveInProgress == false)
                            {
                                if (inputScr.GetInputByName("Wave Control", "BeginWave") > 0)
                                {
                                    matchManagerScr.waveInProgress = true;
                                }
                            }
                            else
                            {
                                if (inputScr.GetInputByName("Wave Control", "AutoWaves") > 0)
                                {
                                    matchManagerScr.automaticWaves = !matchManagerScr.automaticWaves;
                                }
                            }
                        }

                        waveControl_axisUsed = true;
                    }
                }
                else
                {
                    waveControl_axisUsed = false;
                }
            }


        }
    }

    void OnDestroy()
    {
        if (towerUpgradeAndHealCanvas_inst != null)
        {
            Destroy(towerUpgradeAndHealCanvas_inst);
        }




    }
    void DrawTowerUpgradeHealCanvas()
    {
        if (FindResources() && FindDependencies())
        {
            if (dominantRay == DominantRay.BuildInteract && towerInteractRay.ray_surface!=null)
            {
                TowerInherit towerScr = towerInteractRay.ray_surface.GetComponent<TowerInherit>();

                if (towerScr != null)
                {
                    if (towerScr.buildingProgress >= 1)
                    {
                        if (towerUpgradeAndHealCanvas_inst == null)
                        {
                            towerUpgradeAndHealCanvas_inst = Instantiate(towerUpgradeAndHealCanvas_prefab, null);
                        }

                        if (towerUpgradeAndHealCanvas_inst != null)
                        {
                            TowerUpgradeAndHealCanvas canvScr = towerUpgradeAndHealCanvas_inst.GetComponent<TowerUpgradeAndHealCanvas>();

                            if (canvScr != null)
                            {

                                if (towerScr.upgradeLevel >= towerScr.upgrades.Length || towerScr.upgrades.Length <= 0)
                                {
                                    canvScr.upgradePanel.alpha = 0;
                                    canvScr.upgradePrice_txt.color = Color.white;
                                }
                                else
                                {
                                    canvScr.upgradePanel.alpha = 1;
                                    canvScr.upgradePrice_txt.text = "$"+towerScr.upgrades[towerScr.upgradeLevel].cost.ToString();
                                    ExtendedMaths.TMPTextVerify(canvScr.upgradePrice_txt);

                                    canvScr.upgradeLevel_txt.text = "LV. " + (towerScr.upgradeLevel + 2).ToString();
                                    ExtendedMaths.TMPTextVerify(canvScr.upgradeLevel_txt);


                                    if (towerScr.upgrades[towerScr.upgradeLevel].cost <= statsScr.money)
                                    {
                                        canvScr.upgradePrice_txt.color = Color.green;
                                    }
                                    else
                                    {
                                        canvScr.upgradePrice_txt.color = Color.red;
                                    }
                                }

                                if (towerScr.health_current >= towerScr.health_max || towerScr.health_max <= 0)
                                {
                                    canvScr.healPanel.alpha = 0;
                                    canvScr.healPrice_txt.color = Color.white;
                                }
                                else
                                {
                                    canvScr.healPanel.alpha = 1;
                                    canvScr.healPercent_txt.text = "+"+towerHealAmount_percent.ToString()+"%";
                                    ExtendedMaths.TMPTextVerify(canvScr.healPercent_txt);

                                    canvScr.healPrice_txt.text = "$"+HealTowers_WeaponReflect_towerHealAmount(TowerHealAmountReturn.MoneyToPay).ToString();
                                    ExtendedMaths.TMPTextVerify(canvScr.healPrice_txt);


                                    if (HealTowers_WeaponReflect_towerHealAmount(TowerHealAmountReturn.MoneyToPay) <= statsScr.money)
                                    {
                                        canvScr.healPrice_txt.color = Color.green;
                                    }
                                    else
                                    {
                                        canvScr.healPrice_txt.color = Color.red;
                                    }
                                    
                                }

                                float healthPercent = ((float)1 / (float)towerScr.health_max) * towerScr.health_current;
                                if (towerScr.health_max > 0)
                                {
                                    canvScr.healProgress_rect.sizeDelta = new Vector2(canvScr.healProgressBG_rect.sizeDelta.x * healthPercent, canvScr.healProgress_rect.sizeDelta.y);
                                }
                                else
                                {
                                    canvScr.healProgress_rect.sizeDelta = new Vector2(canvScr.healProgressBG_rect.sizeDelta.x, canvScr.healProgress_rect.sizeDelta.y);
                                }
                                



                                if (towerScr.upgrades.Length > 0)
                                {


                                    float upgradePercent = ((float)1 / (float)towerScr.upgrades.Length) * towerScr.upgradeLevel;
                                    canvScr.upgradeProgress_rect.sizeDelta = new Vector2(canvScr.upgradeProgressBG_rect.sizeDelta.x * upgradePercent, canvScr.upgradeProgress_rect.sizeDelta.y);
                                }
                                else
                                {
                                    canvScr.upgradeProgress_rect.sizeDelta = new Vector2(canvScr.upgradeProgressBG_rect.sizeDelta.x, canvScr.upgradeProgress_rect.sizeDelta.y);
                                }

                            }
                        }
                    }
                    else
                    {
                        if (towerUpgradeAndHealCanvas_inst != null)
                        {
                            Destroy(towerUpgradeAndHealCanvas_inst);
                        }
                    }

                }

            }
            else
            {
                if (towerUpgradeAndHealCanvas_inst != null)
                {
                    Destroy(towerUpgradeAndHealCanvas_inst);
                }
            }
        }

    }
    //run all of the functions i defined above, i just separated them to be neat n tidy
    void Update()
    {


        
        if (FindDependencies() && FindResources())
        {
            Update_LateAwake();

            spawnWeaponMissInstance = false;
            weaponRespondCheck = false;
            weaponActionCertain = false;
            PlaceTowers_DrawGrid();
            DecideDominantRay();

            Update_Weapon_PlaceTowers();
            Update_Weapon_UpgradeTowers();
            Update_Weapon_HealTowers();
            
            Update_ChooseTowerToPlace();
            Update_ControlWaves();
            Update_Weapon_FightMonsters();
            DrawTowerUpgradeHealCanvas();
            DebugButtons();
            ModeSwitchHandler();


            Update_PauseGame();
            


            if (spawnWeaponMissInstance == true)
            {
                Instantiate(soundEffect_weaponMiss_prefab, null);
            }
            if (weaponRespondCheck == true)
            {

                WeaponAnims_Respond();
                weaponRespondCheck = false;
            }
        }

    }

    private void LateUpdate()
    {
        if (FindDependencies() && FindResources())
        {
            LateUpdate_CreatePlacableTowerInst();
        }
    }

    bool FindDependencies()
    {
        bool ret = false;
        if (inputScr == null)
        {
            inputScr =gameObject.GetComponent<InputManager>();
        }

        if (matchManagerScr == null)
        {
            matchManagerScr = GameObject.FindObjectOfType<MatchManager>();
        }

        if (weaponAnimScr == null)
        {
            WeaponCanvas canvScr = GameObject.FindObjectOfType<WeaponCanvas>();
            if (canvScr != null)
            {
                weaponAnimScr = canvScr.animScr;
            }
            
        }

        if (statsScr == null)
        {
            statsScr = gameObject.GetComponent<PlayerStats>();
        }


        if (pauseCanvasObj == null)
        {
            if (GameObject.FindObjectOfType<PauseCanvas>() != null)
            {
                pauseCanvasObj = GameObject.FindObjectOfType<PauseCanvas>().gameObject;

            }
        }

        if (timeScaleScr == null)
        {
            timeScaleScr = GameObject.FindObjectOfType<TimeScaling>();
        }

        if (winLoseScr == null)
        {
            winLoseScr = gameObject.GetComponent<PlayerWinLose>();
        }

        if (winLoseScr!=null && timeScaleScr != null && pauseCanvasObj != null && inputScr != null && matchManagerScr!=null && weaponAnimScr!=null && statsScr!=null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (placableTower_prefab == null)
        {
            placableTower_prefab = Resources.Load<GameObject>("Prefabs/Towers/tower_placable");
        }

        if (soundEffect_hammerHit_prefab == null)
        {
            soundEffect_hammerHit_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_hammerHit");
        }

        if (soundEffect_wrenchHit_prefab == null)
        {
            soundEffect_wrenchHit_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_wrenchHit");
        }

        if (soundEffect_weaponMiss_prefab == null)
        {
            soundEffect_weaponMiss_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_weaponMiss");
        }

        if (soundEffect_wrenchFail_prefab == null)
        {
            soundEffect_wrenchFail_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_wrenchFail");
        }

        if (towerUpgradeAndHealCanvas_prefab == null)
        {
            towerUpgradeAndHealCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/towerUpgradeAndHealCanvas");
        }

        if(towerUpgradeAndHealCanvas_prefab!=null && placableTower_prefab != null && soundEffect_hammerHit_prefab!=null && soundEffect_wrenchHit_prefab!=null && soundEffect_weaponMiss_prefab != null && soundEffect_wrenchFail_prefab != null)
        {
            ret = true;
        }
        return ret;
    }
}
