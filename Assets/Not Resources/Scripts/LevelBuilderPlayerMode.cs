﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilderPlayerMode : MonoBehaviour
{


    private InputManager inputScr;








    private Camera levelEditorCam;

    private LevelBuilderSaveLoad builderSaveLoadScr;


    private bool buildingAxisUsed;


    void Update_PlaceRemovePlayers()
    {
        if (FindDependencies())
        {
            if (ExtendedMaths.PointerOverUI() == false && ExtendedMaths.PointerInView() == true)
            {
                Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                Vector3 spawnPos = GridSnap.SnapToGrid(levelEditorCam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, levelEditorCam.nearClipPlane + 0.5f)), true, true, true);

                intVec2 posFound = builderSaveLoadScr.FindPosInBuilderData(new Vector2(spawnPos.x, spawnPos.y));

                if (inputScr.allInputValues[inputScr.GetGroupIndexByName("Building")].HasActivity())
                {
                    if (buildingAxisUsed == false)
                    {
                        if (inputScr.GetInputByName("Building", "Editor_PlaceBlock") > 0)
                        {



                            if (posFound.x != LevelBuilderSaveLoad.invalidIntVec2.x && posFound.y != LevelBuilderSaveLoad.invalidIntVec2.y)
                            {


                                builderSaveLoadScr.builderData[posFound.x, posFound.y].block.spawnPoint = true;

                                builderSaveLoadScr.RenderBuilderData(posFound.x, posFound.y);



                            }

                        }


                        if (inputScr.GetInputByName("Building", "Editor_RemoveBlock") > 0)
                        {



                            if (posFound.x != LevelBuilderSaveLoad.invalidIntVec2.x && posFound.y != LevelBuilderSaveLoad.invalidIntVec2.y)
                            {
                                builderSaveLoadScr.builderData[posFound.x, posFound.y].block.spawnPoint = false;

                                builderSaveLoadScr.RenderBuilderData(posFound.x, posFound.y);

                            }

                        }
                        buildingAxisUsed = true;
                    }
                }
                else
                {
                    buildingAxisUsed = false;
                }


            }
        }




    }




    private void Update()
    {
        if (FindDependencies())
        {

            Update_PlaceRemovePlayers();
        }

    }




    bool FindDependencies()
    {
        bool ret = false;

        if (GameObject.FindObjectOfType<LevelBuilderCameraMovement>() != null)
        {
            levelEditorCam = GameObject.FindObjectOfType<LevelBuilderCameraMovement>().GetComponent<Camera>();
        }

        inputScr = GameObject.FindObjectOfType<InputManager>();

        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();
        if (levelEditorCam != null && inputScr != null && builderSaveLoadScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
