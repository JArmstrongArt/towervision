﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct intVec2
{
    public int x;
    public int y;

    public intVec2(int nx, int ny)
    {
        x = nx;
        y = ny;
    }
}

[System.Serializable]
public struct BuilderData_Serializable
{
    public Vec2_Serializable pos;
    public LevelBlockData block;

    public BuilderData_Serializable(Vec2_Serializable p, LevelBlockData b)
    {
        pos = p;
        block = b;
    }
}


public struct BuilderData
{
    public Vector2 pos;
    public LevelBlockData block;
    public GameObject editorMesh_block;
    public GameObject editorMesh_path;
    public GameObject editorMesh_player;
    public GameObject editorMesh_info;
    public BuilderData(Vector2 p)
    {
        pos = p;
        block = new LevelBlockData(1000, LevelBlockData_BlockType.INVISIBLE);
        editorMesh_block = null;
        editorMesh_path = null;
        editorMesh_player = null;
        editorMesh_info = null;
    }

    public BuilderData(Vector2 p, LevelBlockData dat)
    {
        
        pos = p;
        block = dat;
        editorMesh_block = null;
        editorMesh_path = null;
        editorMesh_player = null;
        editorMesh_info = null;

    }
}

[System.Serializable]
public struct Vec2_Serializable
{
    public float x;
    public float y;

    public Vec2_Serializable(float nX, float nY)
    {
        x = nX;
        y = nY;
    }
}



[System.Serializable]
public struct BuilderTexture
{
    public string texPath;

    [HideInInspector]
    public Texture tex;

    public BuilderTexture(string path)
    {
        texPath = path;
        tex = Resources.Load<Texture>(texPath);
    }


}

[System.Serializable]
public struct EditorWave
{
    public List<MonsterGroup> waveGroups;


}


public class LevelBuilderSaveLoad : MonoBehaviour
{
    public string testLevelName = "levelBuilderTest";
    public BuilderData[,] builderData;
    public static intVec2 invalidIntVec2 = new intVec2(-1, -1);

    private Camera levelEditorCam;

    private Material tileMat;
    private Texture gridTex;
    private Mesh tileMesh;
    private Texture pathPointTex;
    private Texture spawnPointTex;

    [HideInInspector]
    public List<EditorWave> editorWaves = new List<EditorWave>();

    [HideInInspector]
    public int editorWaves_index = 0;

    [HideInInspector]
    public List<GameObject> editorWaveGroupCanvasInstances = new List<GameObject>();


    private GameObject editorWaveGroupCanvas_prefab;

    public BuilderTexture[] allBuilderTextures;

    [HideInInspector]
    public int allBuilderTextures_index = 0;

    [HideInInspector]
    public bool awakeDone;

    [HideInInspector]
    public List<intVec2> allPathPoints = new List<intVec2>();

    private InputManager inputScr;

    private bool debugAxisUsed;

    [HideInInspector]
    public int upcomingBlockElev = 0;

    [HideInInspector]
    public LevelBlockData_BlockAngle upcomingBlockSlant = LevelBlockData_BlockAngle.NONE;

    [HideInInspector]
    public int startMoney = 100;

    [HideInInspector]
    public int startHealth = 100;

    [HideInInspector]
    public Color skyColour = Color.black;

    private Sprite blockAng_up;
    private Sprite blockAng_down;
    private Sprite blockAng_left;
    private Sprite blockAng_right;
    private GameObject levelBuilderTileInfoCanvas_prefab;

    private LevelEditorMainCanvas mainCanvScr;

    [HideInInspector]
    public float singleWorldGroupCanvasHeight;

    public static bool resumePreviousSession = false;
    public static string resumePreviousSession_levelName = "";

    private GameObject pathLineRendererObj;
    private Material lineMat;

    private Texture pathPointTex_start;
    private Texture pathPointTex_end;
    private void Awake()
    {

        if (FindResources() && FindDependencies())
        {
            awakeDone = false;

            


        }

    }

    


    public void AddWave()
    {
        editorWaves.Add(new EditorWave());

        EditorWave addedWave = editorWaves[editorWaves.Count - 1];
        addedWave.waveGroups = new List<MonsterGroup>();
        editorWaves[editorWaves.Count - 1] = addedWave;
        AddGroupToWave(editorWaves.Count - 1);

    }

    public intVec2 FindPosInBuilderData(Vector2 pos)
    {
        intVec2 ret = LevelBuilderSaveLoad.invalidIntVec2;

        if (FindResources() && FindDependencies())
        {
            if (!ExtendedMaths.Array2DTrueEmpty(builderData))
            {
                for (int i = 0; i < builderData.GetLength(0); i++)
                {
                    for (int j = 0; j < builderData.GetLength(1); j++)
                    {
                        float xDist = Mathf.Abs(builderData[i, j].pos.x - pos.x);
                        float yDist = Mathf.Abs(builderData[i, j].pos.y - pos.y);
                        if (xDist <= GridSnap.GRID_SIZE * 0.5f && yDist <= GridSnap.GRID_SIZE * 0.5f)//added this comparator because just doing a direct == lead to floating point precision errors very soon into some maps
                        {
                            ret = new intVec2(i, j);

                        }
                    }

                }
            }
        }

        return ret;


    }


    public void LoadLevelIntoEditor(bool basicArenaDebugLoad=false, string loadName = "", bool resumeSessionLoad = false)
    {
        if(FindResources() && FindDependencies())
        {
            LevelSaveLoadFolder loadFolder = LevelSaveLoadFolder.USER;

            if (resumeSessionLoad == false)
            {
                if (basicArenaDebugLoad == true)
                {
                    loadFolder = LevelSaveLoadFolder.OFFICIAL;
                    loadName = "BasicArena";
                }
                else
                {
                    if (loadName == "")
                    {
                        loadName = testLevelName;
                    }
                }
            }
            else
            {
                loadName = testLevelName.ToUpper();


                loadFolder = LevelSaveLoadFolder.TEST;
            }




            LevelData loaded = LevelSaveLoad.LoadLevel(loadName, loadFolder, false);
            RefreshBuilderData(false);

            if (loaded.editor_builderData != null)
            {
                BuilderData_Serializable[,] builderData_serialized = loaded.editor_builderData;

                for (int i = 0; i < builderData.GetLength(0); i++)
                {
                    for (int j = 0; j < builderData.GetLength(1); j++)
                    {
                        builderData[i, j].pos = new Vector2(builderData_serialized[i, j].pos.x, builderData_serialized[i, j].pos.y);
                        builderData[i, j].block = builderData_serialized[i, j].block;

                    }
                }
            }

            if (loaded.editor_allPathPoints == null)
            {
                loaded.editor_allPathPoints = new List<intVec2>();
            }
            allPathPoints = loaded.editor_allPathPoints;

            if (loaded.editor_editorWaves == null)
            {
                loaded.editor_editorWaves = new List<EditorWave>();
                
            }
            editorWaves = loaded.editor_editorWaves;
            while (loaded.editor_editorWaves.Count <= 0)
            {
                AddWave();
            }

            if (resumeSessionLoad)
            {
                mainCanvScr.levelName_inp.text = resumePreviousSession_levelName.ToUpper();

            }
            else
            {
                mainCanvScr.levelName_inp.text = loadName;

            }
            ExtendedMaths.TMPTextVerify_InputField(mainCanvScr.levelName_inp);

            editorWaves_index = 0;



            startMoney = loaded.startMoney;
            startHealth = loaded.startHealth;
            skyColour = new Color(loaded.skyColour_r, loaded.skyColour_g, loaded.skyColour_b,1.0f);



            RenderBuilderData();
            SpawnWaveGroupCanvases();



        }
    }

    float MinimapCamOrthoScale_Get()
    {
        int maxXBreadth = 1;
        int maxYBreadth = 1;

        for(int i = 0; i < builderData.GetLength(0); i++)
        {
            for(int j = 0; j < builderData.GetLength(1); j++)
            {
                if (builderData[i, j].block.blockType != LevelBlockData_BlockType.INVISIBLE)
                {
                    if (i > maxXBreadth)
                    {
                        maxXBreadth = i;
                    }

                    if (j > maxXBreadth)
                    {
                        maxXBreadth = j;
                    }
                }
            }
        }
        int breadthToUse = maxXBreadth;
        if (maxXBreadth < maxYBreadth)
        {
            breadthToUse = maxYBreadth;
        }

        return (float)breadthToUse / 1.604621309370988f;
    }

    public IDictionary<string, bool> SaveLevelOutOfEditor(bool testModeSave=false,string levelName="")
    {
        if (FindResources() && FindDependencies())
        {

            levelName = levelName.ToUpper();
            
            List<char> acceptableNameChars = new List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };


            if (testModeSave == false)
            {
                if (levelName == "")
                {
                    levelName = "unnamedlevel".ToUpper();
                }
                else
                {
                    bool invalidChars = false;

                    foreach(char c in levelName)
                    {
                        if (!acceptableNameChars.Contains(c))
                        {
                            invalidChars = true;
                            break;
                        }
                    }

                    if (invalidChars == false)
                    {
                        levelName = levelName.ToUpper();
                    }
                    else
                    {
                        levelName = "invalidlevelname".ToUpper();
                    }
                }
                resumePreviousSession_levelName = levelName.ToUpper();
            }
            else
            {
                resumePreviousSession_levelName = levelName.ToUpper();
                levelName = testLevelName.ToUpper();
            }


            LevelBlockData[,] saveBlocks = new LevelBlockData[builderData.GetLength(0), builderData.GetLength(1)];
            for (int i = 0; i < saveBlocks.GetLength(0); i++)
            {
                for (int j = 0; j < saveBlocks.GetLength(1); j++)
                {

                    saveBlocks[i, j] = builderData[i, j].block;
                }
            }

            MonsterWave[] saveWaves = new MonsterWave[editorWaves.Count];
            List<MonsterWave> flexibleSaveWaves = new List<MonsterWave>();

            for (int i = 0; i < saveWaves.Length; i++)
            {
                saveWaves[i].allMonsterGroups = new MonsterGroup[editorWaves[i].waveGroups.Count];
                
                for(int j = 0; j < saveWaves[i].allMonsterGroups.Length; j++)
                {
                    saveWaves[i].allMonsterGroups[j] = editorWaves[i].waveGroups[j];
                    //saveWaves[i].allMonsterGroups[j].monster_prefabDirectory = Constants.MONSTER_DIR+"monster_soldier";
                }
                flexibleSaveWaves.Add(saveWaves[i]);
            }

            for(int i=0;i<flexibleSaveWaves.Count;i++)
            {
                if (ExtendedMaths.Array1DTrueEmpty(flexibleSaveWaves[i].allMonsterGroups))
                {
                    flexibleSaveWaves.Remove(flexibleSaveWaves[i]);
                    i -= 1;
                }
            }

            saveWaves = new MonsterWave[flexibleSaveWaves.Count];
            for(int i = 0; i < saveWaves.Length; i++)
            {
                saveWaves[i] = flexibleSaveWaves[i];
            }




            BuilderData_Serializable[,] bd_serial = new BuilderData_Serializable[builderData.GetLength(0), builderData.GetLength(1)];
            for(int i = 0; i < bd_serial.GetLength(0); i++)
            {
                for(int j = 0; j < bd_serial.GetLength(1); j++)
                {

                    bd_serial[i, j].pos = new Vec2_Serializable( builderData[i, j].pos.x,builderData[i,j].pos.y);
                    bd_serial[i, j].block = builderData[i, j].block;


                }
            }
            LevelData finalSave = new LevelData(saveBlocks, saveWaves, bd_serial, allPathPoints,editorWaves,startMoney,startHealth,skyColour.r, skyColour.g, skyColour.b, MinimapCamOrthoScale_Get());

            bool savableFile = true;

            IDictionary<string, bool> reasonsNotToSave = new Dictionary<string, bool>();

            reasonsNotToSave["hasSpawns"] = false;
            reasonsNotToSave["hasPath"] = false;
            reasonsNotToSave["hasWaves"] = false;


            for (int i = 0; i < bd_serial.GetLength(0); i++)
            {
                for (int j = 0; j < bd_serial.GetLength(1); j++)
                {

                    if (bd_serial[i, j].block.spawnPoint == true)
                    {
                        reasonsNotToSave["hasSpawns"] = true;
                        break;
                    }


                }
            }

            for (int i = 0; i < bd_serial.GetLength(0); i++)
            {
                for (int j = 0; j < bd_serial.GetLength(1); j++)
                {

                    if (bd_serial[i, j].block.monsterPathIndex >=0)
                    {
                        reasonsNotToSave["hasPath"] = true;

                        break;
                    }


                }
            }

            if (!ExtendedMaths.Array1DTrueEmpty(saveWaves))
            {
                reasonsNotToSave["hasWaves"] = true;

            }

            foreach (KeyValuePair<string, bool> reason in reasonsNotToSave)
            {
                if (reason.Value == false)
                {
                    savableFile = false;
                    break;
                }
            }


            if (savableFile)
            {
                LevelSaveLoadFolder destFolder = LevelSaveLoadFolder.USER;
                if (testModeSave)
                {
                    destFolder = LevelSaveLoadFolder.TEST;
                }

                
                LevelSaveLoad.SaveLevel(finalSave, levelName, destFolder);
            }

            return reasonsNotToSave;
            
        }
        return null;

    }

    void PrintBuilderData()
    {
        if(FindResources() && FindDependencies())
        {
            string finalString = "";
            if (!ExtendedMaths.Array2DTrueEmpty(builderData))
            {
                for (int i = 0; i < builderData.GetLength(0); i++)
                {
                    for (int j = 0; j < builderData.GetLength(1); j++)
                    {
                        finalString += builderData[i, j].block.blockType;
                        if (j < builderData.GetLength(1) - 1)
                        {
                            finalString += ", ";
                        }
                    }
                    if (i < builderData.GetLength(0) - 1)
                    {
                        finalString += "||";
                    }
                }
            }
            else
            {
                finalString = "builderData empty!";
            }
            print(finalString);
        }

    }



    void RefreshBuilderData(bool renderIt = true)
    {
        if(FindResources() && FindDependencies())
        {
            if (ExtendedMaths.Array2DTrueEmpty(builderData)==false)
            {
                for (int i = 0; i < builderData.GetLength(0); i++)
                {
                    for (int j = 0; j < builderData.GetLength(1); j++)
                    {

                        if(builderData[i, j].editorMesh_block != null)
                        {
                            Destroy(builderData[i, j].editorMesh_block);

                        }

                        if (builderData[i, j].editorMesh_path != null)
                        {
                            Destroy(builderData[i, j].editorMesh_path);

                        }
                        if (builderData[i, j].editorMesh_player != null)
                        {
                            Destroy(builderData[i, j].editorMesh_player);

                        }
                        if (builderData[i, j].editorMesh_info != null)
                        {
                            Destroy(builderData[i, j].editorMesh_info);

                        }

                    }

                }
            }


            allPathPoints = new List<intVec2>();
            Vector2 builderData_size = new Vector2(25, 25);
            Vector3 spacePos = GridSnap.SnapToGrid(Vector3.zero, true, true, true);
            Vector2 spacePos_vec2 = new Vector2(spacePos.x, spacePos.y);

            builderData = new BuilderData[Mathf.RoundToInt(builderData_size.x), Mathf.RoundToInt(builderData_size.y)];

            for (int i = 0; i < builderData.GetLength(0); i++)
            {
                for (int j = 0; j < builderData.GetLength(1); j++)
                {

                    builderData[i, j] = new BuilderData(spacePos_vec2);
                    spacePos_vec2 += new Vector2( GridSnap.GRID_SIZE,0);
                }
                spacePos_vec2 = new Vector2(spacePos.x, spacePos_vec2.y + GridSnap.GRID_SIZE);
            }
            if (renderIt)
            {
                RenderBuilderData();
            }
            
        }

    }


    public void SpawnWaveGroupCanvases()
    {
        if(FindDependencies() && FindResources())
        {
            editorWaveGroupCanvasInstances = ExtendedMaths.DestroyGameObjectList(editorWaveGroupCanvasInstances);


            if(editorWaves_index>=0 && editorWaves_index < editorWaves.Count)
            {
                for (int i = 0; i < editorWaves[editorWaves_index].waveGroups.Count; i++)
                {
                    Vector3 spawnPos = new Vector3(0, - (singleWorldGroupCanvasHeight * i), 0);
                    GameObject spawnInst = Instantiate(editorWaveGroupCanvas_prefab, spawnPos, Quaternion.Euler(Vector3.zero), null);
                    editorWaveGroupCanvasInstances.Add(spawnInst);
                    WaveProgrammerSingleGroupWorldCanvas groupScr = spawnInst.GetComponent<WaveProgrammerSingleGroupWorldCanvas>();
                    if (groupScr != null)
                    {

                        groupScr.ManualAwake();
                    }

                }

            }


        }

    }



    void RenderBuilderData_Individual(int x, int y)
    {
        if(FindResources() && FindDependencies())
        {

            Mathf.Clamp(x, 0, builderData.GetLength(0) - 1);
            Mathf.Clamp(y, 0, builderData.GetLength(1) - 1);

            GameObject renderObj_block = null;
            
            if (builderData[x, y].editorMesh_block == null)
            {
                renderObj_block = new GameObject();
            }
            else
            {
                renderObj_block = builderData[x, y].editorMesh_block;
            }


            renderObj_block.layer = LayerMask.NameToLayer("LevelBuilder");

            MeshFilter renderObj_block_filt = renderObj_block.GetComponent<MeshFilter>();
            if (renderObj_block_filt == null)
            {
                renderObj_block_filt = renderObj_block.AddComponent<MeshFilter>();
            }

            MeshRenderer renderObj_block_rend = renderObj_block.GetComponent<MeshRenderer>();
            if (renderObj_block_rend == null)
            {
                renderObj_block_rend = renderObj_block.AddComponent<MeshRenderer>();
            }


            renderObj_block.transform.position = new Vector3(builderData[x, y].pos.x, builderData[x, y].pos.y, 0);

            renderObj_block_filt.mesh = tileMesh;
            renderObj_block_rend.material = tileMat;
            
            if (builderData[x, y].block.blockType != LevelBlockData_BlockType.INVISIBLE)
            {

                Texture t = Resources.Load<Texture>(builderData[x, y].block.blockTexPath);

                renderObj_block_rend.material.SetTexture("_MainTex", t);
            }
            else
            {
                renderObj_block_rend.material.SetTexture("_MainTex", gridTex);
            }

            builderData[x, y].editorMesh_block = renderObj_block;
            //---------------------


            GameObject renderObj_path = null;
            if (builderData[x, y].editorMesh_path == null)
            {
                renderObj_path = new GameObject();
            }
            else
            {
                renderObj_path = builderData[x, y].editorMesh_path;
            }

            renderObj_path.layer = LayerMask.NameToLayer("LevelBuilder");

            MeshFilter renderObj_path_filt = renderObj_path.GetComponent<MeshFilter>();
            if (renderObj_path_filt == null)
            {
                renderObj_path_filt = renderObj_path.AddComponent<MeshFilter>();
            }

            MeshRenderer renderObj_path_rend = renderObj_path.GetComponent<MeshRenderer>();
            if (renderObj_path_rend == null)
            {
                renderObj_path_rend = renderObj_path.AddComponent<MeshRenderer>();
            }


            renderObj_path.transform.position = new Vector3(builderData[x, y].pos.x, builderData[x, y].pos.y, (float)(levelEditorCam.transform.position.z) / (float)2);


            renderObj_path_filt.mesh = tileMesh;
            renderObj_path_rend.material = tileMat;
            renderObj_path_rend.material.SetTexture("_MainTex", pathPointTex);
            renderObj_path_rend.material.SetColor("_Color", LevelBuilderModeManager.PATHMODECOLOUR);

            builderData[x, y].block.monsterPathIndex = -1;

            intVec2 pointCheck = new intVec2(x, y);
            if (allPathPoints.Contains(pointCheck))
            {

                for(int i = 0; i < allPathPoints.Count; i++)
                {
                    if (allPathPoints[i].x == pointCheck.x && allPathPoints[i].y==pointCheck.y)
                    {
                        builderData[x, y].block.monsterPathIndex = i;

                        if (i<= 0)
                        {
                            renderObj_path_rend.material.SetTexture("_MainTex", pathPointTex_start);

                        }

                        if(i>= allPathPoints.Count - 1)
                        {
                            renderObj_path_rend.material.SetTexture("_MainTex", pathPointTex_end);

                        }
                        break;
                    }
                }
            }


            if (builderData[x, y].block.monsterPathIndex >= 0)
            {
                renderObj_path_rend.enabled = true;
            }
            else
            {
                renderObj_path_rend.enabled = false;
            }

            builderData[x, y].editorMesh_path = renderObj_path;



            //---------------------


            GameObject renderObj_player = null;
            if (builderData[x, y].editorMesh_player == null)
            {
                renderObj_player = new GameObject();
            }
            else
            {
                renderObj_player = builderData[x, y].editorMesh_player;
            }

            renderObj_player.layer = LayerMask.NameToLayer("LevelBuilder");

            MeshFilter renderObj_player_filt = renderObj_player.GetComponent<MeshFilter>();
            if (renderObj_player_filt == null)
            {
                renderObj_player_filt = renderObj_player.AddComponent<MeshFilter>();
            }

            MeshRenderer renderObj_player_rend = renderObj_player.GetComponent<MeshRenderer>();
            if (renderObj_player_rend == null)
            {
                renderObj_player_rend = renderObj_player.AddComponent<MeshRenderer>();
            }


            renderObj_player.transform.position = new Vector3(builderData[x, y].pos.x, builderData[x, y].pos.y, (float)(levelEditorCam.transform.position.z) / (float)1.3f);

            renderObj_player_filt.mesh = tileMesh;
            renderObj_player_rend.material = tileMat;
            renderObj_player_rend.material.SetTexture("_MainTex", spawnPointTex);
            renderObj_player_rend.material.SetColor("_Color", LevelBuilderModeManager.PLAYERMODECOLOUR);



            renderObj_player_rend.enabled = builderData[x, y].block.spawnPoint;

            builderData[x, y].editorMesh_player = renderObj_player;


            //---------------------


            
            if((builderData[x, y].block.blockElev != 0 || builderData[x, y].block.blockAng != LevelBlockData_BlockAngle.NONE) && builderData[x, y].block.blockType != LevelBlockData_BlockType.INVISIBLE)
            {
                GameObject renderObj_info = null;
                if (builderData[x, y].editorMesh_info == null)
                {
                    renderObj_info = Instantiate(levelBuilderTileInfoCanvas_prefab, null);
                }
                else
                {

                    renderObj_info = builderData[x, y].editorMesh_info;
                }

                renderObj_info.layer = LayerMask.NameToLayer("LevelBuilder");



                renderObj_info.transform.position = new Vector3(builderData[x, y].pos.x, builderData[x, y].pos.y, (float)(levelEditorCam.transform.position.z) / (float)1.2f);

                RectTransform renderObj_info_rect = renderObj_info.GetComponent<RectTransform>();

                if (renderObj_info_rect != null)
                {
                    renderObj_info_rect.localScale = new Vector3(GridSnap.GRID_SIZE, GridSnap.GRID_SIZE, 1);
                    renderObj_info_rect.sizeDelta = new Vector2(1, 1);
                }


                LevelBuilderTileInfoCanvas renderObj_infoScr = renderObj_info.GetComponent<LevelBuilderTileInfoCanvas>();
                if (renderObj_infoScr != null)
                {
                    renderObj_infoScr.slope_img.enabled = true;

                    switch (builderData[x, y].block.blockAng)
                    {
                        case LevelBlockData_BlockAngle.UP:
                            renderObj_infoScr.slope_img.sprite = blockAng_up;
                            break;

                        case LevelBlockData_BlockAngle.DOWN:
                            renderObj_infoScr.slope_img.sprite = blockAng_down;

                            break;

                        case LevelBlockData_BlockAngle.LEFT:
                            renderObj_infoScr.slope_img.sprite = blockAng_left;

                            break;

                        case LevelBlockData_BlockAngle.RIGHT:
                            renderObj_infoScr.slope_img.sprite = blockAng_right;

                            break;

                        default:
                            renderObj_infoScr.slope_img.enabled = false;
                            break;
                    }

                    renderObj_infoScr.elevation_txt.text = builderData[x, y].block.blockElev.ToString();
                    ExtendedMaths.TMPTextVerify(renderObj_infoScr.elevation_txt);
                }






                builderData[x, y].editorMesh_info = renderObj_info;
            }
            else
            {
                if(builderData[x, y].editorMesh_info != null)
                {
                    Destroy(builderData[x, y].editorMesh_info);
                }
            }

        }

    }

    void RenderMonsterPath()
    {
        if(FindDependencies() && FindResources())
        {
            if (pathLineRendererObj != null)
            {
                Destroy(pathLineRendererObj);
            }

            pathLineRendererObj = new GameObject();
            pathLineRendererObj.name = "pathLineRendererObj";
            pathLineRendererObj.layer = LayerMask.NameToLayer("LevelBuilder");
            LineRenderer lineScr = pathLineRendererObj.AddComponent<LineRenderer>();
            lineScr.startWidth = 0.35f;
            lineScr.endWidth = lineScr.startWidth;
            lineScr.positionCount = allPathPoints.Count;
            lineScr.material = lineMat;
            lineScr.material.SetColor("_Color", LevelBuilderModeManager.PATHMODECOLOUR);
            lineScr.material.SetColor("_EmissionColor", lineScr.material.GetColor("_Color"));
            for (int i=0;i<allPathPoints.Count;i++)
            {
                lineScr.SetPosition(i, new Vector3(builderData[allPathPoints[i].x, allPathPoints[i].y].pos.x, builderData[allPathPoints[i].x, allPathPoints[i].y].pos.y, -5));
            }
        }


    } 

    public void RenderBuilderData(int row = -1, int column = -1)
    {
        if (FindResources() && FindDependencies())
        {
            if (!ExtendedMaths.Array2DTrueEmpty(builderData))
            {

                if (row >= 0 && column < 0)
                {
                    for (int i = 0; i < builderData.GetLength(1); i++)
                    {
                        RenderBuilderData_Individual(row, i);

                    }
                }

                if (row < 0 && column >= 0)
                {
                    for (int i = 0; i < builderData.GetLength(0); i++)
                    {
                        RenderBuilderData_Individual(i, column);

                    }
                }

                if (row >= 0 && column >= 0)
                {
                    RenderBuilderData_Individual(row, column);
                }

                if (row < 0 && column < 0)
                {
                    for (int i = 0; i < builderData.GetLength(0); i++)
                    {
                        for (int j = 0; j < builderData.GetLength(1); j++)
                        {


                            RenderBuilderData_Individual(i, j);

                        }

                    }
                }
                RenderMonsterPath();

            }
        }

    }
    Mesh GenerateTileMesh()
    {
        if (FindResources() && FindDependencies())
        {
            Vector3[] tileVerts = new Vector3[6];





            tileVerts[0] = new Vector3(0, GridSnap.GRID_SIZE, 0);
            tileVerts[1] = new Vector3(GridSnap.GRID_SIZE, 0, 0);
            tileVerts[2] = Vector3.zero;


            tileVerts[3] = new Vector3(GridSnap.GRID_SIZE, 0, 0);
            tileVerts[4] = new Vector3(0, GridSnap.GRID_SIZE, 0);
            tileVerts[5] = new Vector3(GridSnap.GRID_SIZE, 0, 0) + new Vector3(0, GridSnap.GRID_SIZE, 0);


            for (int i = 0; i < tileVerts.Length; i++)
            {
                tileVerts[i] = new Vector3(tileVerts[i].x - (GridSnap.GRID_SIZE * 0.5f), tileVerts[i].y - (GridSnap.GRID_SIZE * 0.5f), tileVerts[i].z);
            }



            int[,] tileTris_segmented = new int[,]
            {

            { 0,1,2 },
            { 3,4,5 }

            };

            Vector2[] tileUVs = {
            new Vector2(0.3f, 0.0f),
            new Vector2(0.0f, 0.3f),
            new Vector2(0.0f, 0.0f),
            new Vector2(0.0f, 0.3f),
            new Vector2(0.3f, 0.0f),
            new Vector2(0.3f, 0.3f) };



            for (int i = 0; i < tileUVs.Length; i++)
            {
                int mult = 3;
                tileUVs[i] = new Vector2(tileUVs[i].x * mult, tileUVs[i].y * mult);

            }

            /*
            Vector2[] tileUVs_rotateEdit = tileUVs;

            for (int i = 0; i < tileUVs_rotateEdit.Length; i++)
            {
                
                if (i >= 24)
                {
                    tileUVs_rotateEdit[i] += new Vector2(0.05f, 0.09f);
                    Quaternion rot = Quaternion.Euler(0, 0, -90.0f);

                    if (i > 29)
                    {
                        rot = Quaternion.Euler(0.0f, 0.0f, 90.0f);
                    }
                    tileUVs_rotateEdit[i] = rot * tileUVs_rotateEdit[i];
                }
                

                //tileUVs_rotateEdit[i] += new Vector2(0.05f, 0.09f);
                Quaternion rot = Quaternion.Euler(0, 0, -90.0f);

                tileUVs_rotateEdit[i] = rot * tileUVs_rotateEdit[i];
            }
            tileUVs = tileUVs_rotateEdit;
            */


            int[] tileTris = ExtendedMaths.SegmentedTrisToContinuousTris(tileTris_segmented);
            Mesh retTile = new Mesh();
            retTile.vertices = tileVerts;
            retTile.triangles = tileTris;
            retTile.uv = tileUVs;


            return retTile;
        }
        else
        {
            return null;
        }

    }

    Vector2 GridSpaceToLevelDataArrayIndex(Vector3 space)
    {
        if (FindResources() && FindDependencies())
        {
            space = GridSnap.SnapToGrid(space, true, true, true);

            return new Vector2(Mathf.Round(((float)space.x / (float)GridSnap.GRID_SIZE) - (GridSnap.GRID_SIZE * 0.5f)), Mathf.Round((float)space.y / (float)GridSnap.GRID_SIZE));
        }
        else
        {
            return Vector2.zero;
        }

    }

    public void AddGroupToWave(int waveIndex = -1)
    {
        if (editorWaves_index >= 0 && editorWaves_index < editorWaves.Count)
        {
            int index = editorWaves_index;
            if (waveIndex >= 0 && waveIndex<editorWaves.Count)
            {
                index = waveIndex;
            }
            editorWaves[index].waveGroups.Add(new MonsterGroup());
            SpawnWaveGroupCanvases();
        }
    }
    private void Update()
    {
        if(FindDependencies() && FindResources())
        {
            if (awakeDone == false)
            {
                for (int i = 0; i < allBuilderTextures.Length; i++)
                {

                    allBuilderTextures[i].tex = Resources.Load<Texture>(allBuilderTextures[i].texPath);
                }
                tileMesh = GenerateTileMesh();
                RefreshBuilderData();
                singleWorldGroupCanvasHeight =Mathf.Abs(   (float)editorWaveGroupCanvas_prefab.GetComponent<RectTransform>().sizeDelta.y / ( (float)1 / (float)editorWaveGroupCanvas_prefab.GetComponent<RectTransform>().localScale.y) );


                if (resumePreviousSession)
                {
                    LoadLevelIntoEditor(false, "", true);
                    resumePreviousSession = false;
                }


                awakeDone = true;
            }

            if (awakeDone == true)
            {
                if (inputScr.allInputValues[inputScr.GetGroupIndexByName("DEBUG")].HasActivity())
                {
                    if (debugAxisUsed == false)
                    {
                        if (inputScr.GetInputByName("DEBUG", "DBG_ADDGROUP") > 0)
                        {
                            AddGroupToWave();
                        }

                        if (inputScr.GetInputByName("DEBUG", "DBG_LOADLEVELINTOEDITOR") > 0)
                        {
                            LoadLevelIntoEditor(false);
                        }

                        if (inputScr.GetInputByName("DEBUG", "DBG_LOADBASICARENAINTOEDITOR") > 0)
                        {
                            LoadLevelIntoEditor(true);
                        }
                        debugAxisUsed = true;
                    }
                }
                else
                {
                    debugAxisUsed = false;
                }
            }

        }
    }


    bool FindResources()
    {

        bool ret = false;
        if (tileMat == null)
        {
            tileMat = Resources.Load<Material>("Materials/LevelBuilderMat");
        }

        if (gridTex == null)
        {
            gridTex = Resources.Load<Texture>("Textures/Level Editor/gridTex");
        }

        if (pathPointTex == null)
        {
            pathPointTex= Resources.Load<Texture>("Sprites/UI/pathMode_noColour");
        }

        if (pathPointTex_start == null)
        {
            pathPointTex_start = Resources.Load<Texture>("Sprites/UI/pathMode_start_noColour");
        }

        if (pathPointTex_end == null)
        {
            pathPointTex_end = Resources.Load<Texture>("Sprites/UI/pathMode_end_noColour");
        }

        if (spawnPointTex == null)
        {
            spawnPointTex = Resources.Load<Texture>("Sprites/UI/spawnMode_noColour");
        }

        if (editorWaveGroupCanvas_prefab == null)
        {
            editorWaveGroupCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/waveProgrammer_singleGroupWorldCanvas");
        }

        if (levelBuilderTileInfoCanvas_prefab == null)
        {
            levelBuilderTileInfoCanvas_prefab = Resources.Load<GameObject>("Prefabs/UI/levelBuilderTileInfoCanvas");
        }

        if (blockAng_up == null)
        {
            blockAng_up = Resources.Load<Sprite>("Sprites/Level Editor/blockAng_up");
        }

        if (blockAng_down == null)
        {
            blockAng_down = Resources.Load<Sprite>("Sprites/Level Editor/blockAng_down");
        }

        if (blockAng_left == null)
        {
            blockAng_left = Resources.Load<Sprite>("Sprites/Level Editor/blockAng_left");
        }

        if (blockAng_right == null)
        {
            blockAng_right = Resources.Load<Sprite>("Sprites/Level Editor/blockAng_right");
        }

        if (lineMat == null)
        {
            lineMat = Resources.Load<Material>("Materials/editorPathLine");
        }
        if (pathPointTex_start!=null && pathPointTex_end!=null && lineMat != null && blockAng_up != null && blockAng_down != null && blockAng_left != null && blockAng_right != null && levelBuilderTileInfoCanvas_prefab != null && editorWaveGroupCanvas_prefab != null && pathPointTex!=null && tileMat != null && gridTex != null && spawnPointTex!=null)
        {
            ret = true;
        }
        return ret;
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (GameObject.FindObjectOfType<LevelBuilderCameraMovement>() != null)
        {
            levelEditorCam = GameObject.FindObjectOfType<LevelBuilderCameraMovement>().GetComponent<Camera>();
        }



        mainCanvScr = GameObject.FindObjectOfType<LevelEditorMainCanvas>();

        inputScr = GameObject.FindObjectOfType<InputManager>();



        if ( mainCanvScr != null && inputScr != null && levelEditorCam != null )
        {
            ret = true;
        }

        return ret;
    }
}
