﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSaveLoadLive : MonoBehaviour,IEditorFunctionality
{

    private GameObject player_prefab;

    [HideInInspector]
    public List<Vector3> spawnPoints;

    private CanvasSpawner canvSpawnScr;
    public void EditorUpdate()
    {
        gameObject.name = "LevelSaveLoadLive";
    }
    private bool lateAwake=false;
    private void Awake()
    {

        lateAwake = false;



        

    }

    void Update()
    {
        if (FindResources() && FindDependencies())
        {
            if (canvSpawnScr.awakeDone == true)
            {
                if (lateAwake == false)
                {
                    spawnPoints = new List<Vector3>();

                    LevelData minicamData = LevelSaveLoad.LoadLevel(LevelSaveLoad.levelToLoad, LevelSaveLoad.folderToLoad);

                    GameObject playerObj = GameObject.FindGameObjectWithTag("playerCam");
                    if (playerObj == null)
                    {
                        playerObj = Instantiate(player_prefab, null);
                    }
                    Vector3 finalSpawnPos = Vector3.zero;

                    if (!ExtendedMaths.ListTrueEmpty(spawnPoints))
                    {

                        finalSpawnPos = spawnPoints[Random.Range(0, spawnPoints.Count)];
                    }

                    PlayerMovement moveScr = playerObj.GetComponent<PlayerMovement>();
                    if (moveScr != null)
                    {
                        moveScr.playerPos = finalSpawnPos;
                    }
                    
                    if (GameObject.FindObjectOfType<MinimapCamera>() != null)
                    {
                        Camera miniCam = GameObject.FindObjectOfType<MinimapCamera>().gameObject.GetComponent<Camera>();

                        if (miniCam != null)
                        { 
                            miniCam.orthographicSize = minicamData.minimapOrthoScale;
                        }
                    }
                    lateAwake = true;
                }

            }


        }
    }
    //awakeDone
    bool FindDependencies()
    {
        bool ret = false;

        canvSpawnScr = GameObject.FindObjectOfType<CanvasSpawner>();

        if (canvSpawnScr != null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (player_prefab == null)
        {
            player_prefab = Resources.Load<GameObject>("Prefabs/playerCamera");
        }

        if (player_prefab != null)
        {
            ret = true;
        }

        return ret;
    }
}
