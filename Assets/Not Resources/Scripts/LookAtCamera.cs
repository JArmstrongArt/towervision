﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//a script, used primarily on world space ui, to look at the camera at all times.
public class LookAtCamera : MonoBehaviour
{
    private GameObject mainCam_obj;
    public bool flipped;//if true, the object will rotate 180 degrees on the y to face its back to the camera instead of its front.
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (FindDependencies())
        {
            if (flipped == true)
            {
                transform.rotation = Quaternion.LookRotation(transform.position - mainCam_obj.transform.position);
            }
            else
            {
                gameObject.transform.LookAt(mainCam_obj.transform.position);
            }
            
        }
        
    }

    bool FindDependencies()
    {
        bool ret = false;
        if (mainCam_obj == null)
        {
            mainCam_obj = GameObject.FindGameObjectWithTag("playerCam");
        }

        if (mainCam_obj != null)
        {
            ret = true;
        }

        return ret;
    }
}
