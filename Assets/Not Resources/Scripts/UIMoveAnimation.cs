﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UIMoveAnimation : MonoBehaviour,IEditorFunctionality
{
    private RectTransform[] animPoints;
    [HideInInspector]
    public Vector3 movePoint_localPosition;
    [SerializeField] float moveSpeed;
    private int animPoints_moveIndex;
    private float animPoints_currentLerpProgress;
    // Start is called before the first frame update
    void Awake()
    {
        GeneratePath();
    }

    // Update is called once per frame
    void Update()
    {
        if (animPoints != null)
        {
            if (animPoints.Length >= 2)
            {
                if (animPoints_moveIndex < animPoints.Length - 1)
                {
                    movePoint_localPosition = Vector3.Lerp(animPoints[animPoints_moveIndex].localPosition, animPoints[animPoints_moveIndex + 1].localPosition, animPoints_currentLerpProgress);
                }
                else
                {
                    movePoint_localPosition = Vector3.Lerp(animPoints[animPoints.Length-1].localPosition, animPoints[0].localPosition, animPoints_currentLerpProgress);
                }

                animPoints_currentLerpProgress += moveSpeed * Time.deltaTime;
                if (animPoints_currentLerpProgress >= 1)
                {
                    animPoints_currentLerpProgress = 0;
                    if (animPoints_moveIndex < animPoints.Length - 1)
                    {
                        animPoints_moveIndex += 1;
                    }
                    else
                    {
                        animPoints_moveIndex = 0;
                    }
                    
                }
            }
            else
            {
                movePoint_localPosition = animPoints[0].localPosition;
                animPoints_moveIndex = 0;
                animPoints_currentLerpProgress = 0;
            }

        }

    }

    private void OnDrawGizmos()
    {
        if (animPoints != null)
        {
            for (int i = 0; i < animPoints.Length; i++)
            {
                Gizmos.color = Color.Lerp(Color.blue, Color.red, (float)1 / (float)(i + 1));
                if (animPoints.Length >= 2)
                {
                    if (i < animPoints.Length - 1)
                    {
                        Gizmos.DrawLine(animPoints[i].transform.position, animPoints[i + 1].transform.position);
                    }
                    else
                    {
                        Gizmos.DrawLine(animPoints[animPoints.Length - 1].transform.position, animPoints[0].transform.position);
                    }
                }

                Gizmos.DrawSphere(animPoints[i].transform.position, 20);
            }
        }

    }

    public virtual void EditorUpdate()
    {
        GeneratePath();


    }

    void GeneratePath()
    {
        int animPointLength = 0;
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            RectTransform childRect = gameObject.transform.GetChild(i).GetComponent<RectTransform>();
            if (childRect != null)
            {
                animPointLength += 1;
            }
        }
        animPoints = new RectTransform[animPointLength];

        for (int i = 0; i < animPoints.Length; i++)
        {
            RectTransform childRect = gameObject.transform.GetChild(i).GetComponent<RectTransform>();
            if (childRect != null)
            {
                animPoints[i] = childRect;
            }
        }
    }
}
