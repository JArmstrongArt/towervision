﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

public enum MonsterType
{
    Standard,
    Leadhead,
    Sprinter,
    Soldier,
    EMP
}


[Serializable]
public struct MonsterGroup
{
    public string monster_prefabDirectory;
    public int amount;//how many of monster_prefab to spawn
    public float postSpawnDelay;//how long after all in the group have spawned shall the wave wait to send out the next group
    public float betweenSpawnDelay;//how long shall be waited between each monster in the group spawning
}

[Serializable]
public struct MonsterWave
{
    public MonsterGroup[] allMonsterGroups;
}


public class MonsterPath : MonoBehaviour,IEditorFunctionality
{
    [HideInInspector]
    public Transform[] allPathCorners;

    [HideInInspector]
    public MonsterWave[] allWaves;

    void Awake()
    {
        GeneratePath();
    }



    public void EditorUpdate()
    {

        GeneratePath();
    }

    private void OnDrawGizmos()
    {
        float lineWidth = 0.2f;
        if (allPathCorners != null)
        {
            if (allPathCorners.Length > 0)
            {
                for (int i = 0; i < allPathCorners.Length; i++)
                {
                    if (allPathCorners.Length - 1 <= 0)
                    {
                        Gizmos.color = Color.Lerp(Color.blue, Color.red, 0);
                    }
                    else
                    {
                        Gizmos.color = Color.Lerp(Color.blue, Color.red, (float)i / (float)(allPathCorners.Length - 1));
                    }

                    if (i >= 1)
                    {
                        Gizmos.DrawLine(allPathCorners[i - 1].position, allPathCorners[i].position);
                        Gizmos.DrawLine(allPathCorners[i - 1].position + (allPathCorners[i - 1].up * lineWidth), allPathCorners[i].position + (allPathCorners[i].up * lineWidth));
                        Gizmos.DrawLine(allPathCorners[i - 1].position - (allPathCorners[i - 1].up * lineWidth), allPathCorners[i].position + (allPathCorners[i].right * lineWidth));
                        Gizmos.DrawLine(allPathCorners[i - 1].position + (allPathCorners[i - 1].right * lineWidth), allPathCorners[i].position + (allPathCorners[i].right * lineWidth));
                        Gizmos.DrawLine(allPathCorners[i - 1].position - (allPathCorners[i - 1].right * lineWidth), allPathCorners[i].position - (allPathCorners[i].right * lineWidth));
                    }



                    Gizmos.DrawSphere(allPathCorners[i].position, 0.5f);
                }
            }
        }


    }

    public void GeneratePath()
    {
        allPathCorners = new Transform[gameObject.transform.childCount];
        gameObject.transform.position = Vector3.zero;
        for (int i = 0; i < allPathCorners.Length; i++)
        {
            allPathCorners[i] = gameObject.transform.GetChild(i).gameObject.transform;
            allPathCorners[i].position = GridSnap.SnapToGrid(allPathCorners[i].position, true,true);

            if (allPathCorners.Length >= 2)
            {
                if (i < allPathCorners.Length)
                {


                    if (i > 0)
                    {
                        allPathCorners[i - 1].LookAt(allPathCorners[i].position);
                    }

                }

            }

        }

        if (allPathCorners.Length >= 2)
        {
            allPathCorners[allPathCorners.Length - 1].rotation = allPathCorners[allPathCorners.Length - 2].rotation;
        }

        if (allPathCorners.Length == 1)
        {
            allPathCorners[0].rotation = Quaternion.Euler(Vector3.zero);
        }
    }
}
