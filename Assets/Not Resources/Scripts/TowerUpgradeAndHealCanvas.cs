﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TowerUpgradeAndHealCanvas : MonoBehaviour
{
    public RectTransform upgradeProgress_rect;
    public RectTransform upgradeProgressBG_rect;
    public RectTransform healProgress_rect;
    public RectTransform healProgressBG_rect;
    public TextMeshProUGUI upgradePrice_txt;
    public TextMeshProUGUI upgradeLevel_txt;
    public TextMeshProUGUI healPrice_txt;
    public TextMeshProUGUI healPercent_txt;
    public CanvasGroup upgradePanel;
    public CanvasGroup healPanel;
}
