﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//really simple component class that rotates whatever it is attached to at a constant rate.
public class Rotate : MonoBehaviour
{
    public Vector3 spinVals;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(spinVals.x * Time.deltaTime, spinVals.y * Time.deltaTime, spinVals.z * Time.deltaTime);
    }
}
