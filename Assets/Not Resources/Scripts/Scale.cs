﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour
{
    public Vector3 scaleRates;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 scale_proposed = gameObject.transform.localScale + (scaleRates*Time.deltaTime);
        if (scale_proposed.x <= 0.01f)
        {
            scale_proposed.x = 0.01f;
        }

        if (scale_proposed.y <= 0.01f)
        {
            scale_proposed.y = 0.01f;
        }

        if (scale_proposed.z <= 0.01f)
        {
            scale_proposed.z = 0.01f;
        }

        gameObject.transform.localScale = scale_proposed;
    }
}
