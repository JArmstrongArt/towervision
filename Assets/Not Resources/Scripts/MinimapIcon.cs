﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapIcon : MonoBehaviour
{

    [HideInInspector]
    public GameObject minimapIconObj;
    private GameObject minimapIconObj_transformUp;
    private Mesh billboardMesh;
    private Material minimapIconMat;
    [HideInInspector]
    public MeshRenderer myRend;
    protected MeshFilter myFilt;
    public Texture iconTex;

    private MinimapCamera miniCamScr;
    private Camera miniCam;
    private float iconScale=1.25f;

    [HideInInspector]
    public Color currentIconCol = Color.white;


    // Start is called before the first frame update
    void Awake()
    {
        if (FindResources() && FindDependencies())
        {
            if (minimapIconObj == null)
            {
                minimapIconObj = new GameObject();
                minimapIconObj.name = gameObject.name.ToString() + "_minimapIconObj";
            }

            if (minimapIconObj != null)
            {
                if (minimapIconObj.GetComponent<MeshRenderer>() == null)
                {
                    myRend = minimapIconObj.AddComponent<MeshRenderer>();
                }
                else
                {
                    myRend = minimapIconObj.GetComponent<MeshRenderer>();
                }

                if (myRend.material != minimapIconMat)
                {
                    myRend.material = minimapIconMat;
                }
            }
        }



    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (FindResources() && FindDependencies())
        {

            if(iconScale!=(float)miniCam.orthographicSize/ 11.96544f)
            {
                iconScale = miniCam.orthographicSize / 11.96544f;
            }
            if (minimapIconObj == null)
            {
                minimapIconObj = new GameObject();
                minimapIconObj.name = gameObject.name.ToString() + "_minimapIconObj";
            }
            else
            {
                minimapIconObj.transform.localScale = new Vector3(iconScale, iconScale, iconScale);
                if (minimapIconObj.GetComponent<MeshRenderer>() == null)
                {
                    myRend = minimapIconObj.AddComponent<MeshRenderer>();
                }
                else
                {
                    myRend = minimapIconObj.GetComponent<MeshRenderer>();
                }

                if (minimapIconObj.GetComponent<MeshFilter>() == null)
                {
                    myFilt = minimapIconObj.AddComponent<MeshFilter>();
                }
                else
                {
                    myFilt = minimapIconObj.GetComponent<MeshFilter>();
                }

                if (myRend != null && myFilt != null)
                {
                    myFilt.mesh = billboardMesh;
                    myRend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    myRend.receiveShadows = false;



                    Color setCol = new Color(currentIconCol.r, currentIconCol.g, currentIconCol.b, Mathf.Lerp(1, 0, Mathf.Clamp( miniCamScr.lifetimeLerpVal*3,0,1)));
                    myRend.material.SetColor("_Color", setCol);
                    myRend.material.SetTexture("_MainTex", iconTex);
                    myRend.material.EnableKeyword("_EMISSION");
                    myRend.material.SetTexture("_EmissionMap", myRend.material.GetTexture("_MainTex"));


                    Color currentIconCol_emissive = ExtendedMaths.EmissiveColorVariant(setCol);
                    myRend.material.SetColor("_EmissionColor", currentIconCol_emissive);





                }
                if (minimapIconObj_transformUp == null)
                {
                    minimapIconObj_transformUp = new GameObject();
                    minimapIconObj_transformUp.name = minimapIconObj.name.ToString() + "_transformUp";
                    minimapIconObj.transform.LookAt(new Vector3(minimapIconObj.transform.position.x, minimapIconObj.transform.position.y + 1, minimapIconObj.transform.position.z));
                }
                else
                {
                    minimapIconObj_transformUp.transform.LookAt(new Vector3(minimapIconObj.transform.position.x, minimapIconObj.transform.position.y + 1, minimapIconObj.transform.position.z));
                    minimapIconObj.transform.rotation = Quaternion.Euler(minimapIconObj_transformUp.transform.eulerAngles.x, miniCamScr.gameObject.transform.eulerAngles.y - 180, minimapIconObj_transformUp.transform.eulerAngles.z);
                }

                minimapIconObj.layer = LayerMask.NameToLayer("MinimapExclusive");
            }
        }





    }

    private void LateUpdate()
    {
        if(FindResources() && FindDependencies())
        {
            if (minimapIconObj != null)
            {
                minimapIconObj.transform.position = new Vector3(gameObject.transform.position.x, miniCamScr.gameObject.transform.position.y-1, gameObject.transform.position.z);
            }
        }

    }

    private void OnDestroy()
    {
        if (minimapIconObj != null)
        {
            Destroy(minimapIconObj);
        }
    }

    protected virtual bool FindResources()
    {
        bool ret = false;

        if (billboardMesh == null)
        {
            billboardMesh = Resources.Load<Mesh>("Models/minimapIcon");
        }

        if (minimapIconMat == null)
        {
            minimapIconMat = Resources.Load<Material>("Materials/minimapIcon");
        }
        
        if (billboardMesh != null && minimapIconMat!=null)
        {
            ret = true;
        }
        return ret;
    }

    protected virtual bool FindDependencies()
    {
        bool ret = false;
        miniCamScr = GameObject.FindObjectOfType<MinimapCamera>();
        if (miniCamScr != null)
        {

            miniCam = miniCamScr.GetComponent<Camera>();
        }

        if(miniCamScr!=null && miniCam != null)
        {
            ret = true;
        }
        return ret;
    }
}
