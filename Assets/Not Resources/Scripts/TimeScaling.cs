﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaling : MonoBehaviour, IEditorFunctionality
{
    private float timeScale_paused = 0.0f;
    private float timeScale_normal = 1.0f;

    [HideInInspector]
    public bool paused;

    void OnDestroy()
    {
        Time.timeScale = timeScale_normal;
    }

    void Update()
    {
        if (paused)
        {
            Time.timeScale = timeScale_paused;

        }
        else
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, timeScale_normal, 3 * Time.unscaledDeltaTime);

        }
    }


    public void EditorUpdate()
    {
        gameObject.name = "TimeScaling";
    }
}
