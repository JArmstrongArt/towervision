﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LevelSelectCanvasLevelEntry : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI levelName_txt;
    [SerializeField] TextMeshProUGUI waveCount_txt;
    [SerializeField] TextMeshProUGUI money_txt;
    [SerializeField] TextMeshProUGUI health_txt;
    public Image selectEntryBtn_img;
    [HideInInspector]
    public LevelData associatedLevelData;
    [HideInInspector]
    public System.IO.FileInfo associatedFileInfo;

    private LevelSelectCanvasLevelLoader levLoaderScr;

    private LevelBuilderSaveLoad builderSaveLoadScr;
    private LevelEditorMainCanvas editorCanvScr;

    private LevelSelectCanvas levSelCanvScr;
    public void SetLevelToLoad(int doubleClickResponse=0)
    {
        if (FindDependencies())
        {

            string levelToLoad_proposed = "BASICARENA";
            LevelSaveLoadFolder folderToLoad_proposed = LevelSaveLoadFolder.OFFICIAL;
            if (associatedFileInfo != null)
            {
                levelToLoad_proposed = associatedFileInfo.Name.ToUpper().Replace("." + LevelSaveLoad.fileType.ToString().ToUpper(), "");
                folderToLoad_proposed = LevelSaveLoad.folderToLoad;
            }

            if(LevelSaveLoad.levelToLoad!= levelToLoad_proposed || LevelSaveLoad.folderToLoad != folderToLoad_proposed)
            {
                LevelSaveLoad.levelToLoad = levelToLoad_proposed;
                LevelSaveLoad.folderToLoad = folderToLoad_proposed;
            }
            else
            {
                if (doubleClickResponse !=0)
                {
                    switch (levSelCanvScr.spawnEnum)
                    {
                        case SpawnMode.MainMenu:
                            SceneManager.LoadScene("story");
                            break;
                        case SpawnMode.LevelEditor:
                            builderSaveLoadScr.LoadLevelIntoEditor(false, LevelSaveLoad.levelToLoad);
                            editorCanvScr.SwitchTab((int)LevelEditorTab.LEVELBUILDER);
                            break;
                    }
                    
                }

                
            }

            levLoaderScr.SelectedLevelVisualUpdate();
        }

    }

    private void Update()
    {
        if (FindDependencies())
        {
            if (associatedFileInfo != null)
            {
                if (levelName_txt.text != associatedFileInfo.Name.Replace("." + LevelSaveLoad.fileType.ToString(), ""))
                {
                    levelName_txt.text = associatedFileInfo.Name.Replace("." + LevelSaveLoad.fileType.ToString(), "");
                    ExtendedMaths.TMPTextVerify(levelName_txt);
                }
            }

            if (!ExtendedMaths.Array1DTrueEmpty(associatedLevelData.levelWaves))
            {
                if (waveCount_txt.text != associatedLevelData.levelWaves.Length.ToString())
                {
                    waveCount_txt.text = associatedLevelData.levelWaves.Length.ToString();
                    ExtendedMaths.TMPTextVerify(waveCount_txt);
                }
            }
            else
            {
                if (waveCount_txt.text != "UNKNOWN")
                {
                    waveCount_txt.text = "UNKNOWN";
                    ExtendedMaths.TMPTextVerify(waveCount_txt);
                }
            }


            if (health_txt.text != associatedLevelData.startHealth.ToString())
            {
                health_txt.text = associatedLevelData.startHealth.ToString();
                ExtendedMaths.TMPTextVerify(health_txt);
            }

            if (money_txt.text != associatedLevelData.startMoney.ToString())
            {
                money_txt.text = associatedLevelData.startMoney.ToString();
                ExtendedMaths.TMPTextVerify(money_txt);
            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        levSelCanvScr = GameObject.FindObjectOfType<LevelSelectCanvas>();
        switch (levSelCanvScr.spawnEnum)
        {
            case SpawnMode.MainMenu:
                levLoaderScr = GameObject.FindObjectOfType<LevelSelectCanvasLevelLoader>();

                if (levSelCanvScr!=null && levLoaderScr != null)
                {
                    ret = true;
                }
                break;
            case SpawnMode.LevelEditor:
                levLoaderScr = GameObject.FindObjectOfType<LevelSelectCanvasLevelLoader>();
                builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();
                editorCanvScr = GameObject.FindObjectOfType<LevelEditorMainCanvas>();
                if (editorCanvScr!=null && levSelCanvScr != null && builderSaveLoadScr != null && levLoaderScr != null)
                {
                    ret = true;
                }
                break;

        }


        return ret;
    }
}
