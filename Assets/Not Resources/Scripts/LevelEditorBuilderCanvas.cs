﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
public class LevelEditorBuilderCanvas : LevelEditorCanvasInputToggles
{
    [SerializeField] RectTransform buildMode_bg_rect;
    [SerializeField] RectTransform pathMode_bg_rect;
    [SerializeField] RectTransform playerMode_bg_rect;
    [SerializeField] RawImage buildMode_icon;
    [SerializeField] RawImage pathMode_icon;
    [SerializeField] RawImage playerMode_icon;


    [SerializeField] RectTransform blockSelector_bg_rect;
    [SerializeField] RawImage blockPrev_img;

    private LevelBuilderSaveLoad builderSaveLoadScr;

    private LevelBuilderModeManager modeManagerScr;

    [SerializeField] TMP_InputField elevationValue_inp;

    [SerializeField] TextMeshProUGUI slantValue_txt;

    private bool lateAwake;


    // Start is called before the first frame update
    void Awake()
    {
        if (FindDependencies())
        {
            lateAwake = false;
            ModeButtonResize();
            ElevationValueUpdate();
            ChangeSlant(0);
            RecolourModeButtons();
        }
       
    }

    void RecolourModeButtons()
    {
        Image buildMode_bg_img = buildMode_bg_rect.GetComponent<Image>();
        if (buildMode_bg_img != null)
        {
            buildMode_bg_img.color = LevelBuilderModeManager.BUILDMODECOLOUR;
            buildMode_icon.color = buildMode_bg_img.color;

        }


        Image pathMode_bg_img = pathMode_bg_rect.GetComponent<Image>();
        if (pathMode_bg_img != null)
        {
            pathMode_bg_img.color = LevelBuilderModeManager.PATHMODECOLOUR;
            pathMode_icon.color = pathMode_bg_img.color;

        }

        Image playerMode_bg_img = playerMode_bg_rect.GetComponent<Image>();
        if (playerMode_bg_img != null)
        {
            playerMode_bg_img.color = LevelBuilderModeManager.PLAYERMODECOLOUR;
            playerMode_icon.color = playerMode_bg_img.color;

        }
    }

    public void ElevationValueRefresh()
    {
        if (FindDependencies())
        {
            ExtendedMaths.TMPTextVerify_InputField(elevationValue_inp);

        }
    }

    public void ElevationValueUpdate()
    {
        if (FindDependencies())
        {

            builderSaveLoadScr.upcomingBlockElev = Mathf.RoundToInt( ExtendedMaths.GetNumFromInputField(elevationValue_inp, 0, 100));




        }

    }


    public void SwitchMode(int modeEnumRep)
    {
        if (FindDependencies())
        {
            if (System.Enum.IsDefined(typeof(LevelBuilderMode), modeEnumRep))
            {
                modeManagerScr.ChangeModeAndTab((LevelBuilderMode)modeEnumRep, LevelEditorTab.LEVELBUILDER);

            }

        }
    }

    private void OnRectTransformDimensionsChange()
    {
        if (FindDependencies())
        {
            ModeButtonResize();
        }

    }

    private void Update()
    {
        if (FindDependencies())
        {
            if (builderSaveLoadScr.awakeDone == true)
            {
                if (lateAwake ==false)
                {

                    ChangeBuilderTex(0);
                    lateAwake = true;
                }
                
            }
        }
    }


    public void ChangeBuilderTex(int addition)
    {
        if (FindDependencies())
        {
            if (ExtendedMaths.Array1DTrueEmpty(builderSaveLoadScr.allBuilderTextures) == false)
            {
                builderSaveLoadScr.allBuilderTextures_index = Mathf.RoundToInt( ExtendedMaths.WrapValue(builderSaveLoadScr.allBuilderTextures_index, addition, 0, builderSaveLoadScr.allBuilderTextures.Length - 1));

                if(builderSaveLoadScr.allBuilderTextures_index >= 0 && builderSaveLoadScr.allBuilderTextures_index < builderSaveLoadScr.allBuilderTextures.Length)
                {
                    if (builderSaveLoadScr.allBuilderTextures[builderSaveLoadScr.allBuilderTextures_index].tex != null)
                    {
                        blockPrev_img.texture = builderSaveLoadScr.allBuilderTextures[builderSaveLoadScr.allBuilderTextures_index].tex;
                    }
                    

                }
            }

            
        }

        
    }

    public void ChangeSlant(int addition)
    {
        if (FindDependencies())
        {
            int enumMax = (int)System.Enum.GetValues(typeof(LevelBlockData_BlockAngle)).Cast<LevelBlockData_BlockAngle>().Max();
            int enumMin = (int)System.Enum.GetValues(typeof(LevelBlockData_BlockAngle)).Cast<LevelBlockData_BlockAngle>().Min();
            int blockSlant_proposed = (int)builderSaveLoadScr.upcomingBlockSlant;
            blockSlant_proposed = Mathf.RoundToInt(ExtendedMaths.WrapValue(blockSlant_proposed, addition, 0, enumMax));

            if (!System.Enum.IsDefined(typeof(LevelBlockData_BlockAngle), blockSlant_proposed))
            {
                if (addition >= 0)
                {
                    while(!System.Enum.IsDefined(typeof(LevelBlockData_BlockAngle), blockSlant_proposed))
                    {
                        blockSlant_proposed = Mathf.RoundToInt(ExtendedMaths.WrapValue(blockSlant_proposed, 1, enumMin, enumMax));
                    }
                }
                else
                {
                    blockSlant_proposed = Mathf.RoundToInt(ExtendedMaths.WrapValue(blockSlant_proposed, -1, enumMin, enumMax));
                }
            }

            builderSaveLoadScr.upcomingBlockSlant = (LevelBlockData_BlockAngle)blockSlant_proposed;
            slantValue_txt.text = builderSaveLoadScr.upcomingBlockSlant.ToString();
            ExtendedMaths.TMPTextVerify(slantValue_txt);
        }
    }

    void ModeButtonResize()
    {
        if (FindDependencies())
        {
            buildMode_bg_rect.anchoredPosition = new Vector2(0, -buildMode_bg_rect.sizeDelta.y);
            pathMode_bg_rect.anchoredPosition = new Vector2(0, -buildMode_bg_rect.sizeDelta.y);
            playerMode_bg_rect.anchoredPosition = new Vector2(0, -buildMode_bg_rect.sizeDelta.y);

            buildMode_bg_rect.sizeDelta = new Vector2((float)blockSelector_bg_rect.sizeDelta.x / (float)3, buildMode_bg_rect.sizeDelta.y);
            pathMode_bg_rect.sizeDelta = new Vector2((float)blockSelector_bg_rect.sizeDelta.x / (float)3, pathMode_bg_rect.sizeDelta.y);
            playerMode_bg_rect.sizeDelta = new Vector2((float)blockSelector_bg_rect.sizeDelta.x / (float)3, playerMode_bg_rect.sizeDelta.y);
        }


    }

    bool FindDependencies()
    {

        bool ret = false;

        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();
        modeManagerScr = GameObject.FindObjectOfType<LevelBuilderModeManager>();

        if (modeManagerScr!=null && builderSaveLoadScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
