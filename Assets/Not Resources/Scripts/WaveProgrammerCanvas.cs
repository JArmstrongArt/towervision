﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class WaveProgrammerCanvas : MonoBehaviour
{
    private LevelBuilderSaveLoad builderSaveLoadScr;
    [SerializeField] TextMeshProUGUI waveIndex_txt;
    // Start is called before the first frame update
    void Awake()
    {
        if (FindDependencies())
        {
            while (builderSaveLoadScr.editorWaves.Count <= 0)
            {
                AddWave();
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (FindDependencies())
        {
            waveIndex_txt.text = (builderSaveLoadScr.editorWaves_index + 1).ToString() + "/" + builderSaveLoadScr.editorWaves.Count.ToString();
            ExtendedMaths.TMPTextVerify(waveIndex_txt);


        }
    }



    public void ShiftWaveIndex(int addition)
    {
        if (FindDependencies())
        {
            builderSaveLoadScr.editorWaves_index = Mathf.RoundToInt(ExtendedMaths.WrapValue(builderSaveLoadScr.editorWaves_index,addition,0,builderSaveLoadScr.editorWaves.Count-1));
            builderSaveLoadScr.SpawnWaveGroupCanvases();

        }
    }

    public void AddWave()
    {
        if (FindDependencies())
        {
            builderSaveLoadScr.AddWave();
            builderSaveLoadScr.editorWaves_index = builderSaveLoadScr.editorWaves.Count - 1;

            builderSaveLoadScr.SpawnWaveGroupCanvases();
        }
    }

    public void RemoveWave()
    {
        if (FindDependencies())
        {
            if(builderSaveLoadScr.editorWaves.Count>1)
            {
                if(builderSaveLoadScr.editorWaves_index>=0 && builderSaveLoadScr.editorWaves_index < builderSaveLoadScr.editorWaves.Count)
                {
                    builderSaveLoadScr.editorWaves.RemoveAt(builderSaveLoadScr.editorWaves_index);
                    builderSaveLoadScr.editorWaves_index = Mathf.Clamp(builderSaveLoadScr.editorWaves_index, 0, builderSaveLoadScr.editorWaves.Count - 1);
                    builderSaveLoadScr.SpawnWaveGroupCanvases();

                }

            }
            
        }
    }

    bool FindDependencies()
    {
        bool ret = false;

        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();

        if (builderSaveLoadScr != null)
        {
            ret = true;
        }
        return ret;
    }
}
