﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectileCollisionSpawner : MonoBehaviour
{
    private GameObject projectileCollisionInst;

    // Start is called before the first frame update
    void Awake()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (projectileCollisionInst == null)
        {
            projectileCollisionInst = new GameObject();
            projectileCollisionInst.name = "player_projectileCollisionInst";
            PlayerProjectileCollision collScr = projectileCollisionInst.AddComponent<PlayerProjectileCollision>();
            projectileCollisionInst.layer = gameObject.layer;
            collScr.sourcePlayer = gameObject;
            collScr.Setup();
        }
        else
        {
            projectileCollisionInst.transform.position = gameObject.transform.position;
        }
    }
}
