﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowdownTowerFrost : MonoBehaviour
{
    private List<MonsterInherit> allMonstersInTrigger = new List<MonsterInherit>();
    [HideInInspector]
    public SlowdownTower slowdownScr;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (allMonstersInTrigger.Count > 0)
        {
            foreach (MonsterInherit monst in allMonstersInTrigger)
            {
                if (monst != null)
                {
                    if (slowdownScr != null)
                    {
                        if (slowdownScr.sprayMode == true)
                        {
                            monst.slowdownMode = true;
                        }
                        
                    }


                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<MonsterInherit>() != null)
        {
            if (!allMonstersInTrigger.Contains(other.GetComponent<MonsterInherit>()))
            {
                allMonstersInTrigger.Add(other.GetComponent<MonsterInherit>());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<MonsterInherit>() != null)
        {
            if (allMonstersInTrigger.Contains(other.GetComponent<MonsterInherit>()))
            {
                allMonstersInTrigger.Remove(other.GetComponent<MonsterInherit>());
            }
        }
    }
}
