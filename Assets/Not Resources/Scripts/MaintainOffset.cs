﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//a simple script that, once AwakeSetup() has been ran, will maintain the distance between itself and the targetObj, you can run AwakeSetup() again to reset the distance needed to be maintained.
public class MaintainOffset : MonoBehaviour
{
    private Vector3 origPos;
    public GameObject targetObj;
    // Start is called before the first frame update
    void Awake()
    {
        AwakeSetup();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LateUpdate()
    {
        if (targetObj != null)
        {
            transform.position = origPos + targetObj.transform.position;
        }
        
    }

    public void AwakeSetup()
    {
        if (targetObj != null)
        {
            origPos = transform.position - targetObj.transform.position;
        }
        
    }
}
