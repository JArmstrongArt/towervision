﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LevelEditorTab
{
    LEVELBUILDER,
    WAVEPROGRAMMER,
    METADATA,
    LOADLEVEL
}

public enum LevelBuilderMode
{
    BUILD,
    PATH,
    PLAYER
}


public class LevelBuilderModeManager : MonoBehaviour
{
    private LevelEditorTab tab = LevelEditorTab.LEVELBUILDER;
    [HideInInspector]
    public LevelBuilderMode mode = LevelBuilderMode.BUILD;

    private LevelBuilderBlockMode blockModeScr;


    private LevelBuilderPathMode pathModeScr;
    private LevelBuilderPlayerMode playerModeScr;

    private CanvasSpawner canvSpawnScr;

    private GameObject buildCanvObj;

    private GameObject levelBuilderCam_inst;

    private GameObject waveProgrammerCam_inst;
    private GameObject metadataCam_inst;
    private GameObject loadLevelCam_inst;

    private GameObject levelBuilderCam_prefab;

    private GameObject waveProgrammerCam_prefab;
    private GameObject metadataCam_prefab;
    private GameObject loadLevelCam_prefab;


    private LevelBuilderSaveLoad builderSaveLoadScr;
    private GameObject waveProgrammerCanvObj;

    private GameObject levelSelectCanvasObj;


    private GameObject metadataCanvObj;


    private bool lateAwake = false;
    [HideInInspector]
    public bool awakeDone = false;

    private intVec2 origResolution = new intVec2(0,0);

    public static readonly Color BUILDMODECOLOUR = Color.green;
    public static readonly Color PATHMODECOLOUR = Color.red;
    public static readonly Color PLAYERMODECOLOUR = Color.blue;

    private void Awake()
    {


        if (FindDependencies() && FindDependencies_NonTog() && FindResources())
        {


            lateAwake = false;
            awakeDone = true;
        }


    }

    private void Update()
    {
        SpawnCameras();
        if (FindDependencies() && FindDependencies_NonTog() && FindResources())
        {
            if (canvSpawnScr.awakeDone == true && builderSaveLoadScr.awakeDone == true)
            {
                if (lateAwake == false)
                {

                    ChangeModeAndTab(mode, tab);
                    lateAwake = true;
                }

                if (lateAwake == true)
                {


                    if (origResolution.x != Screen.width || origResolution.y != Screen.height)
                    {
                        ReOrthoWaveCamera();
                        origResolution = new intVec2(Screen.width, Screen.height);
                    }
                }
            }


        }
    }
    void ToggleDependencies(bool tog)
    {
        if (FindDependencies() && FindDependencies_NonTog() && FindResources())
        {
            blockModeScr.enabled = tog;
            buildCanvObj.SetActive(tog);
            pathModeScr.enabled = tog;
            playerModeScr.enabled = tog;

            levelBuilderCam_inst.SetActive(tog);
            waveProgrammerCam_inst.SetActive(tog);
            metadataCam_inst.SetActive(tog);

            waveProgrammerCanvObj.SetActive(tog);
            metadataCanvObj.SetActive(tog);
            levelSelectCanvasObj.SetActive(tog);
            loadLevelCam_inst.SetActive(tog);

            foreach (GameObject canv in builderSaveLoadScr.editorWaveGroupCanvasInstances)
            {
                if (canv != null)
                {
                    canv.SetActive(tog);
                }
            }

        }

    }
    public void ChangeModeAndTab(LevelBuilderMode newMode, LevelEditorTab newTab)
    {
        if (FindDependencies() && FindDependencies_NonTog() && FindResources())
        {
            ToggleDependencies(false);
            tab = newTab;
            switch (tab)
            {
                case LevelEditorTab.LEVELBUILDER:
                    mode = newMode;
                    levelBuilderCam_inst.SetActive(true);
                    buildCanvObj.SetActive(true);
                    switch (mode)
                    {
                        case LevelBuilderMode.BUILD:
                            blockModeScr.enabled = true;


                            break;
                        case LevelBuilderMode.PLAYER:
                            playerModeScr.enabled = true;
                            break;
                        case LevelBuilderMode.PATH:
                            pathModeScr.enabled = true;
                            break;
                    }
                    break;
                case LevelEditorTab.WAVEPROGRAMMER:
                    waveProgrammerCam_inst.SetActive(true);
                    waveProgrammerCanvObj.SetActive(true);
                    builderSaveLoadScr.SpawnWaveGroupCanvases();
                    foreach (GameObject canv in builderSaveLoadScr.editorWaveGroupCanvasInstances)
                    {
                        if (canv != null)
                        {
                            canv.SetActive(true);
                        }
                    }

                    break;
                case LevelEditorTab.METADATA:
                    metadataCam_inst.SetActive(true);
                    metadataCanvObj.SetActive(true);
                    break;
                case LevelEditorTab.LOADLEVEL:
                    loadLevelCam_inst.SetActive(true);
                    levelSelectCanvasObj.SetActive(true);
                    break;
            }
        }


    }

    bool FindDependencies_NonTog()
    {
        bool ret = false;


        canvSpawnScr = GameObject.FindObjectOfType<CanvasSpawner>();

        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();


        if (builderSaveLoadScr != null && canvSpawnScr != null)
        {
            ret = true;
        }

        return ret;
    }


    void ReOrthoWaveCamera()
    {
        if(FindDependencies() && FindDependencies_NonTog() && FindResources())
        {
            if (waveProgrammerCam_inst != null)
            {
                Camera programCam = waveProgrammerCam_inst.GetComponent<Camera>();

                if (programCam != null)
                {
                    //HOW I GOT THE 8.79 CONSTANT:


                    /*


1124 by 519 editor resolution = 4.06 ortho size
2.165703275529865:1 aspect = 4.06 ortho size

1/2.165703275529865 = 0.4617437722419929


4.06/0.4617437722419929 = 8.792755298651252

8.792755298651252/2.165703275529865 = 4.06
                     */

                    if (Screen.width >= Screen.height)
                    {

                        programCam.orthographicSize = 8.792755298651252f/(((float)Screen.width / (float)Screen.height)) ;


                    }
                    else
                    {

                        programCam.orthographicSize = 8.792755298651252f / (((float)Screen.height / (float)Screen.width));





                    }
                }

            }
        }

    }
    void SpawnCameras()
    {
        if(FindDependencies() && FindDependencies_NonTog() && FindResources())
        {

            if (levelBuilderCam_inst == null)
            {
                levelBuilderCam_inst = Instantiate(levelBuilderCam_prefab, new Vector3(0, -1, -10), Quaternion.Euler(Vector3.zero), null);

            }

            if (waveProgrammerCam_inst == null)
            {
                waveProgrammerCam_inst = Instantiate(waveProgrammerCam_prefab, new Vector3(0, -1, -10), Quaternion.Euler(Vector3.zero), null);
                ReOrthoWaveCamera();
            }

            if (metadataCam_inst == null)
            {
                metadataCam_inst = Instantiate(metadataCam_prefab, new Vector3(0, -1, -10), Quaternion.Euler(Vector3.zero), null);
            }

            if (loadLevelCam_inst == null)
            {
                loadLevelCam_inst = Instantiate(loadLevelCam_prefab, new Vector3(0, -1, -10), Quaternion.Euler(Vector3.zero), null);
            }
        }


    }

    bool FindResources()
    {
        bool ret = false;

        if (levelBuilderCam_prefab == null)
        {
            levelBuilderCam_prefab = Resources.Load<GameObject>("Prefabs/Level Editor/levelBuilderCamera");
        }

        if (waveProgrammerCam_prefab == null)
        {
            waveProgrammerCam_prefab = Resources.Load<GameObject>("Prefabs/Level Editor/waveProgrammerCamera");
        }

        if (metadataCam_prefab == null)
        {
            metadataCam_prefab = Resources.Load<GameObject>("Prefabs/Level Editor/metadataCamera");
        }

        if (loadLevelCam_prefab == null)
        {
            loadLevelCam_prefab = Resources.Load<GameObject>("Prefabs/Level Editor/loadLevelCamera");
        }

        if (loadLevelCam_prefab!=null && levelBuilderCam_prefab != null && waveProgrammerCam_prefab != null && metadataCam_prefab != null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindDependencies()
    {
        bool ret = false;
        blockModeScr = GameObject.FindObjectOfType<LevelBuilderBlockMode>();


        pathModeScr = GameObject.FindObjectOfType<LevelBuilderPathMode>();

        playerModeScr = GameObject.FindObjectOfType<LevelBuilderPlayerMode>();

        if (GameObject.FindObjectOfType<LevelEditorBuilderCanvas>() != null)
        {
            buildCanvObj = GameObject.FindObjectOfType<LevelEditorBuilderCanvas>().gameObject;
        }


        if (GameObject.FindObjectOfType<WaveProgrammerCanvas>() != null)
        {
            waveProgrammerCanvObj = GameObject.FindObjectOfType<WaveProgrammerCanvas>().gameObject;
        }

        if (GameObject.FindObjectOfType<MetadataCanvas>() != null)
        {
            metadataCanvObj = GameObject.FindObjectOfType<MetadataCanvas>().gameObject;
        }

        if (GameObject.FindObjectOfType<LevelSelectCanvas>() != null)
        {
            levelSelectCanvasObj = GameObject.FindObjectOfType<LevelSelectCanvas>().gameObject;
        }



        if (levelSelectCanvasObj!=null && metadataCanvObj != null && waveProgrammerCanvObj != null  && playerModeScr != null && buildCanvObj != null && pathModeScr != null && blockModeScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
