﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeadheadMonster : MonsterInherit
{

    public bool armouredMode;
    private float moveSpeed_orig_orig;
    protected override void Awake()
    {
        if (base.FindDependencies() && base.FindResources())
        {
            base.Awake();
            moveSpeed_orig_orig = base.moveSpeed_orig;
            ArmouredRoutine();
        }

    }

    protected override void Update()
    {
        if(base.FindDependencies() && base.FindResources())
        {
            base.Update();
            ArmouredRoutine();
        }
        
    }

    void ArmouredRoutine()
    {

        if (armouredMode)
        {
            moveSpeed_orig = moveSpeed_orig_orig;
            base.animScr.PlayAnimation("armoured");

        }
        else
        {
            moveSpeed_orig = moveSpeed_orig_orig * 2;
            base.animScr.PlayAnimation("no armour");

        }
        base.invulnerable = armouredMode;
    }
}
