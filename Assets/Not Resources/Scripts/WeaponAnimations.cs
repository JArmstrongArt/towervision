﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Reflection;

[Serializable]
public class WeaponAnimationInfo
{
    public string weaponName;
    public bool looping;
    public Sprite[] weaponFrames;
    public int reloadEndFrame;
    public int reactionFrame;
    public float playbackSpeed;
    [HideInInspector]
    public int weaponFrames_index;
    [HideInInspector]
    public bool playingAnimation;
    [HideInInspector]
    public float playbackSpeed_timeRemaining;
    [HideInInspector]
    public float playbackSpeed_timeRemaining_orig;
    [HideInInspector]
    public string currentMethodToReflect;
    [HideInInspector]
    public bool currentMethodToReflect_ran;
    public void PlayAnimation()
    {
        ResetAnimation();
        playingAnimation = true;
        playbackSpeed_timeRemaining = (float)1 / (float)playbackSpeed;
        playbackSpeed_timeRemaining_orig = playbackSpeed_timeRemaining;

    }

    public void ResetAnimation()
    {
        weaponFrames_index = 0;
        playingAnimation = false;
        currentMethodToReflect_ran = false;
    }

    public bool IsReadyToFire()
    {
        if (playingAnimation == false)
        {
            return true;
        }
        else
        {
            if (weaponFrames_index >= reloadEndFrame)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
public class WeaponAnimations : MonoBehaviour
{
    public WeaponAnimationInfo[] allWeaponAnimations;

    [HideInInspector]
    public int allWeaponAnimations_index;
    private int allWeaponAnimations_index_previous;
    private Image weaponRenderTarget;
    private PlayerActions actionScr;
    // Start is called before the first frame update
    void Awake()
    {
        allWeaponAnimations_index = -1;
        allWeaponAnimations_index_previous = allWeaponAnimations_index;
        ChangeWeapon("wrench");
    }

    void PlayCurrentAnimation()
    {
        if (FindDependencies())
        {
            if (allWeaponAnimations_index >= 0 && allWeaponAnimations_index < allWeaponAnimations.Length)
            {
                if (allWeaponAnimations[allWeaponAnimations_index].playingAnimation == true)
                {

                    if (allWeaponAnimations[allWeaponAnimations_index].weaponFrames_index < allWeaponAnimations[allWeaponAnimations_index].weaponFrames.Length)
                    {

                        allWeaponAnimations[allWeaponAnimations_index].playbackSpeed_timeRemaining -= Time.deltaTime;

                        if (allWeaponAnimations[allWeaponAnimations_index].playbackSpeed_timeRemaining <= 0)
                        {
                            allWeaponAnimations[allWeaponAnimations_index].playbackSpeed_timeRemaining = allWeaponAnimations[allWeaponAnimations_index].playbackSpeed_timeRemaining_orig;

                            if (allWeaponAnimations[allWeaponAnimations_index].looping == false)
                            {
                                allWeaponAnimations[allWeaponAnimations_index].weaponFrames_index += 1;
                            }
                            else
                            {
                                if(allWeaponAnimations[allWeaponAnimations_index].weaponFrames_index < allWeaponAnimations[allWeaponAnimations_index].weaponFrames.Length - 1)
                                {
                                    allWeaponAnimations[allWeaponAnimations_index].weaponFrames_index += 1;
                                }
                                else
                                {
                                    allWeaponAnimations[allWeaponAnimations_index].weaponFrames_index = 0;
                                }
                            }
                            
                        }
                    }
                    else
                    {
                        allWeaponAnimations[allWeaponAnimations_index].ResetAnimation();
                    }
                }
            }
        }

    }

    // Update is called once per frame
    void Update()
    {


        if (FindDependencies())
        {
            if (allWeaponAnimations_index >= 0 && allWeaponAnimations_index < allWeaponAnimations.Length)
            {
                Mathf.Clamp(allWeaponAnimations_index, 0, allWeaponAnimations.Length - 1);
                Mathf.Clamp(allWeaponAnimations_index_previous, 0, allWeaponAnimations.Length - 1);


                PlayCurrentAnimation();
                int weaponFrames_index_clamp = Mathf.Clamp(allWeaponAnimations[allWeaponAnimations_index].weaponFrames_index, 0, allWeaponAnimations[allWeaponAnimations_index].weaponFrames.Length - 1);



                if (allWeaponAnimations[allWeaponAnimations_index].weaponFrames[weaponFrames_index_clamp] != null)
                {
                    weaponRenderTarget.sprite = allWeaponAnimations[allWeaponAnimations_index].weaponFrames[weaponFrames_index_clamp];
                }

                weaponRenderTarget.enabled = true;

                if (allWeaponAnimations[allWeaponAnimations_index].playingAnimation == true)
                {
                    if (allWeaponAnimations[allWeaponAnimations_index].weaponFrames_index == allWeaponAnimations[allWeaponAnimations_index].reactionFrame)
                    {

                        if (actionScr.GetType().GetMethod(allWeaponAnimations[allWeaponAnimations_index].currentMethodToReflect) != null)
                        {

                            if (allWeaponAnimations[allWeaponAnimations_index].currentMethodToReflect_ran == false)
                            {
                                MethodInfo mi = actionScr.GetType().GetMethod(allWeaponAnimations[allWeaponAnimations_index].currentMethodToReflect);

                                mi.Invoke(actionScr, null);
                                allWeaponAnimations[allWeaponAnimations_index].currentMethodToReflect_ran = true;
                            }

                        }
                    }
                }
            }
            else
            {
                print(allWeaponAnimations_index);
                weaponRenderTarget.enabled = false;
            }


        }

    }

    int FindWeaponIndexByName(string weaponName)
    {
        int ret = -1;
        for (int i = 0; i < allWeaponAnimations.Length; i++)
        {
            if (allWeaponAnimations[i].weaponName == weaponName)
            {
                ret = i;
                break;
            }
        }
        return ret;
    }

    public void ChangeWeapon(string weaponName)
    {

        int indexProposed = FindWeaponIndexByName(weaponName);

        if (indexProposed >= 0 && indexProposed < allWeaponAnimations.Length)
        {
            allWeaponAnimations_index_previous = allWeaponAnimations_index;
            allWeaponAnimations_index = indexProposed;
        }
        allWeaponAnimations[allWeaponAnimations_index].ResetAnimation();
    }

    void ShiftWeapon(int weaponShift)
    {
        int indexProposed = Mathf.RoundToInt( ExtendedMaths.WrapValue(allWeaponAnimations_index,weaponShift,0,allWeaponAnimations.Length-1) );


        allWeaponAnimations_index_previous = allWeaponAnimations_index;
        allWeaponAnimations_index = indexProposed;
        allWeaponAnimations[allWeaponAnimations_index].ResetAnimation();
    }

    void PreviousWeapon()
    {
        int allWeaponAnimations_index_temp = allWeaponAnimations_index_previous;
        allWeaponAnimations_index_previous = allWeaponAnimations_index;
        allWeaponAnimations_index = allWeaponAnimations_index_temp;
        allWeaponAnimations[allWeaponAnimations_index].ResetAnimation();
    }

    bool FindDependencies()
    {
        bool ret = false;


        if (weaponRenderTarget == null)
        {
            weaponRenderTarget = gameObject.GetComponent<Image>();
        }

        if (actionScr == null)
        {
            actionScr = GameObject.FindObjectOfType<PlayerActions>();
        }

        if (weaponRenderTarget != null && actionScr != null)
        {
            ret = true;
        }
        return ret;
    }
}
