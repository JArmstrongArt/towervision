﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMPMonster : MonsterInherit
{
    [SerializeField] GenericRay towerSearchRay;
    private GenericRay[] allSearchRays = new GenericRay[6];
    [SerializeField] float towerCheckRate;
    private float towerCheckRate_orig;

    private TowerInherit targetTowerScr;
    private float lerpToTower_progress;


    private Vector3 lerpToTower_basePos;

    private Vector3 lerpToTower_destPos;
    [SerializeField] float lerpToTower_moveSpeedBoostMultiplier;

    [SerializeField] float waitTimeTilExplode;
    private float waitTimeTilExplode_orig;


    [SerializeField] float explodeRadius;

    private bool prematureReturnMode=false;

    private GameObject soundEffect_detonate_prefab;
    private GameObject soundEffect_walk_prefab;
    private GameObject soundEffect_walk_inst;
    private GameObject soundEffect_detonate_inst;
    protected override void Awake()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.Awake();
            base.animScr.PlayAnimation("roll");
            prematureReturnMode = false;
            waitTimeTilExplode_orig = waitTimeTilExplode;
            towerCheckRate_orig = towerCheckRate;
            float addAngle = ExtendedMaths.DivideAmongst360(allSearchRays.Length);
            float currentAngle = 0;
            for (int i = 0; i < allSearchRays.Length; i++)
            {
                Vector3 spawnPos = towerSearchRay.transform.position;
                Vector3 spawnEuler = new Vector3(0, currentAngle, 0);
                GameObject rayObj = Instantiate(towerSearchRay.gameObject, spawnPos, Quaternion.Euler(spawnEuler), null);
                MaintainOffset offScr = rayObj.AddComponent<MaintainOffset>();
                offScr.targetObj = gameObject;
                offScr.AwakeSetup();
                allSearchRays[i] = rayObj.GetComponent<GenericRay>();
                currentAngle += addAngle;
            }

            GeneralSoundEffect walkSEScr = soundEffect_walk_prefab.GetComponent<GeneralSoundEffect>();
            if (walkSEScr != null)
            {

                soundEffect_walk_inst = Instantiate(soundEffect_walk_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                GeneralSoundEffect walkSEScr_inst = soundEffect_walk_inst.GetComponent<GeneralSoundEffect>();
                walkSEScr_inst.ownerObj = gameObject;
            }
            Destroy(towerSearchRay);
        }

    }

    protected override void OnDestroy()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.OnDestroy();
            if (soundEffect_detonate_inst != null)
            {
                Destroy(soundEffect_detonate_inst);
            }
        }

    }

    private void LateUpdate()
    {
        if(base.FindDependencies() && FindResources())
        {
            if (soundEffect_walk_inst != null)
            {
                soundEffect_walk_inst.transform.position = gameObject.transform.position;

            }
        }
    }

    protected override void Update()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.Update();


            if (targetTowerScr != null || lerpToTower_progress > 0)
            {

                base.pathLerp_progress_paused = true;

                if (prematureReturnMode == false)
                {
                    lerpToTower_progress += (base.moveSpeed * Time.deltaTime * lerpToTower_moveSpeedBoostMultiplier) / Vector3.Distance(lerpToTower_basePos, lerpToTower_destPos);
                }
                else
                {
                    lerpToTower_progress -= (base.moveSpeed * Time.deltaTime * lerpToTower_moveSpeedBoostMultiplier) / Vector3.Distance(lerpToTower_basePos, lerpToTower_destPos);
                }
                
                lerpToTower_progress = Mathf.Clamp(lerpToTower_progress, 0, 1);

                if (lerpToTower_progress >= 1)
                {
                    if (soundEffect_detonate_inst == null)
                    {
                        soundEffect_detonate_inst = Instantiate(soundEffect_detonate_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    }
                    animScr.PlayAnimation("still");
                    if (waitTimeTilExplode > 0)
                    {
                        waitTimeTilExplode -= Time.deltaTime;
                        
                    }
                    else
                    {

                        if (targetTowerScr != null)
                        {
                            List<TowerInherit> damagedTowers = ExtendedMaths.FindAllTowersInRange(gameObject.transform.position, explodeRadius, gameObject);
                            
                            if(ExtendedMaths.ListTrueEmpty(damagedTowers) == false)
                            {
                                List<GameObject> damagedTowers_asObj = new List<GameObject>();
                                
                                foreach (TowerInherit tow in damagedTowers)
                                {
                                    if (tow != null)
                                    {
                                        damagedTowers_asObj.Add(tow.gameObject);
                                        

                                        
                                    }
                                }

                                GameObject explosion_inst = Instantiate(explosion_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                                float explodescale = (float)explodeRadius/(float)2;
                                explosion_inst.transform.localScale = new Vector3(explodescale, explodescale, explodescale);
                                ExplosionEffect effectScr = explosion_inst.GetComponent<ExplosionEffect>();
                                if (effectScr != null)
                                {
                                    effectScr.objectsToDestroy = damagedTowers_asObj;
                                }
                            }

                            Destroy(gameObject);
                        }
                        else
                        {
                            prematureReturnMode = true;
                        }
                    }




                }
                else
                {

                    waitTimeTilExplode = waitTimeTilExplode_orig;
                }
                gameObject.transform.position = Vector3.Lerp(lerpToTower_basePos, lerpToTower_destPos, lerpToTower_progress);
                base.animScr.billboardWorldDir_set((lerpToTower_destPos - lerpToTower_basePos).normalized);



            }
            else
            {
                prematureReturnMode = false;
                waitTimeTilExplode = waitTimeTilExplode_orig;
                base.pathLerp_progress_paused = false;

                lerpToTower_progress = 0;
                if (towerCheckRate > 0)
                {
                    towerCheckRate -= Time.deltaTime;
                }
                else
                {

                    for (int i = 0; i < allSearchRays.Length; i++)
                    {
                        if (allSearchRays[i] != null)
                        {
                            if (allSearchRays[i].ray_active == true)
                            {
                                if (allSearchRays[i].ray_surface != null)
                                {
                                    if (allSearchRays[i].ray_surface.gameObject.layer == LayerMask.NameToLayer("Tower"))
                                    {

                                        TowerInherit targetScr = allSearchRays[i].ray_surface.GetComponent<TowerInherit>();
                                        if (targetScr != null)
                                        {
                                            if (targetScr.buildingProgress >= 1)
                                            {
                                                lerpToTower_basePos = gameObject.transform.position;

                                                lerpToTower_destPos = new Vector3(targetScr.gameObject.transform.position.x, gameObject.transform.position.y, targetScr.gameObject.transform.position.z);
                                                targetTowerScr = targetScr;
                                                allSearchRays[i].ray_disabled = true;
                                            }

                                        }



                                    }
                                }

                            }
                        }
                    }


                    towerCheckRate = towerCheckRate_orig;
                }


            }
        }


    }

    protected override bool FindResources()
    {
        bool parRet = base.FindResources();
        bool ret = false;
        if (soundEffect_detonate_prefab == null)
        {
            soundEffect_detonate_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_empDetonate");
        }

        if (soundEffect_walk_prefab == null)
        {
            soundEffect_walk_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_empWalk");
        }

        if (soundEffect_detonate_prefab != null && soundEffect_walk_prefab!=null && parRet==true)
        {
            ret = true;
        }

        return ret;
    }
}
