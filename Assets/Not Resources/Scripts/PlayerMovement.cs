﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this code handles basic player movement and looking around
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float wallCollideRadius;
    [SerializeField] GenericRay wallCollideRay;
    [SerializeField] GenericRay wallCollideRay_right;
    [SerializeField] GenericRay wallCollideRay_left;
    private GenericRay wallCollideRay_lower;
    private GenericRay wallCollideRay_left_lower;
    private GenericRay wallCollideRay_right_lower;

    private GenericRay wallCollideRay_upper;
    private GenericRay wallCollideRay_left_upper;
    private GenericRay wallCollideRay_right_upper;

    [SerializeField] Vector2 lookSensetivity;//multiplies the mouse X and Y speed to make the camera turn quicker
    [SerializeField] float moveSpeed;
    [SerializeField] float runMultiple;//how much moveSpeed is multiplied by to give the run speed
    private float runMultiple_current;//the current value of runMultiple, since it lerps from 1 to runMultiple
    [SerializeField] GenericRay groundRayScr;//a ray that detects the floor directly below you
    [HideInInspector]
    public Vector3 playerPos;//the vector3 that is built upon to give the final position the player should exist in after all the move calculations
    [HideInInspector]
    public Vector3 moveDirectionPlusMagnitude;
    private GameObject camFlatForward;//the forward direction of the camera without the y, that way the move code only tries to move you forwards/backwards and left/right


    private InputManager inputScr;
    private Camera camComp;
    private float cam_origFOV = -1.0f;//set to -1.0 by default so that once the camera class is found by FindDependencies(), we can use this impossible FOV value to set it one time only to a possible one in Update().

    public float playerHeight;


    public float playerSlowdownTimer_orig;
    public float playerSlowdownMultiple_orig;
    [HideInInspector]
    public float playerSlowdownTimer;
    private float playerSlowdownMultiple_current = 1.0f;
    [HideInInspector]
    public float playerSlowdownMultiple = 1.0f;
    private GameObject wallNormalObj;

    private bool stuckInCorner;

    // Start is called before the first frame update
    void Awake()
    {



        playerPos = gameObject.transform.position;


    }


    //get rid of all the new GameObject()s made.
    void OnDestroy()
    {
        if (camFlatForward != null)
        {
            Destroy(camFlatForward);
        }

        if (wallNormalObj != null)
        {
            Destroy(wallNormalObj);

        }

        if (wallCollideRay_lower != null)
        {
            Destroy(wallCollideRay_lower);
        }

        if (wallCollideRay_left_lower != null)
        {
            Destroy(wallCollideRay_left_lower);
        }

        if (wallCollideRay_right_lower != null)
        {
            Destroy(wallCollideRay_right_lower);
        }

        if (wallCollideRay_upper != null)
        {
            Destroy(wallCollideRay_upper);
        }

        if (wallCollideRay_left_upper != null)
        {
            Destroy(wallCollideRay_left_upper);
        }

        if (wallCollideRay_right_upper != null)
        {
            Destroy(wallCollideRay_right_upper);
        }

    }

    bool FindDependencies()
    {
        bool ret = false;
        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (camComp == null)
        {
            camComp = gameObject.GetComponent<Camera>();
        }

        if (inputScr != null && camComp != null)
        {
            ret = true;
        }



        return ret;
    }



    void LateUpdate()
    {
        if (FindDependencies())
        {

            if (cam_origFOV < 0)
            {
                cam_origFOV = camComp.fieldOfView;
            }

            Vector3 eulerAngles_proposed = gameObject.transform.eulerAngles + new Vector3((inputScr.GetInputByName("General", "Mouse Y") * lookSensetivity.y * -1), (inputScr.GetInputByName("General", "Mouse X") * lookSensetivity.x), 0.0f);//DO NOT MULTIPLY BY DELTATIME because "Mouseaxis is in display units per frame. By that, the lower the framerate, the higher the time.deltatime" according to a helpful feller on reddit.

            Vector3 eulerAngles_dir = ExtendedMaths.EulerToDir(eulerAngles_proposed);
            eulerAngles_dir = new Vector3(eulerAngles_dir.x, Mathf.Clamp(eulerAngles_dir.y, -0.8f, 0.8f), eulerAngles_dir.z).normalized;
            //gameObject.transform.eulerAngles = eulerAngles_proposed;
            gameObject.transform.forward = eulerAngles_dir;



            //if there is ground directly below you, glide the player down to the ground at a point that is playerHeight units above that floor
            if (groundRayScr.ray_active == true)
            {
                if (groundRayScr.ray_hit.normal.y != 0.0f)
                {
                    playerPos = new Vector3(playerPos.x, Mathf.Lerp(playerPos.y, groundRayScr.ray_hitPoint.y + playerHeight, Time.deltaTime * (3 + ((float)1 / (float)Vector3.Distance(groundRayScr.ray_hitPoint, gameObject.transform.position)))), playerPos.z);

                }

            }

            if (camFlatForward == null)
            {
                camFlatForward = new GameObject();
                camFlatForward.name = "camFlatForward";
            }

            if (camFlatForward != null)
            {
                //if you are trying to run
                if (inputScr.GetInputByName("General", "Run") > 0 && playerSlowdownTimer <= 0 && (inputScr.GetInputByName("General","Horizontal")!=0 || inputScr.GetInputByName("General", "Vertical") != 0))
                {
                    runMultiple_current = Mathf.Lerp(runMultiple_current, runMultiple, 7 * Time.deltaTime);//gradually but swiftly take runMultiple_current to the value of runMultiple
                    camComp.fieldOfView = Mathf.Lerp(camComp.fieldOfView, cam_origFOV * 1.3f, 3.5f * Time.deltaTime);//more gradually increase the fov of the camera for a heightened sense of speed
                }
                else
                {
                    runMultiple_current = Mathf.Lerp(runMultiple_current, 1.0f, 7 * Time.deltaTime);//quickly glide the speed back down to a multiple of 1 so as to have no ultimate effect on move speed
                    camComp.fieldOfView = Mathf.Lerp(camComp.fieldOfView, cam_origFOV, 7 * Time.deltaTime);//quickly glide the fov back to normal
                }

                //simply put, this is just the camera's y rotation only.
                camFlatForward.transform.rotation = Quaternion.Euler(0.0f, gameObject.transform.eulerAngles.y, 0.0f);

                if (playerSlowdownTimer > 0)
                {
                    playerSlowdownMultiple_current = Mathf.Lerp(playerSlowdownMultiple_current, playerSlowdownMultiple, 7 * Time.deltaTime);
                    playerSlowdownTimer -= Time.deltaTime;
                }
                else
                {
                    playerSlowdownMultiple_current = Mathf.Lerp(playerSlowdownMultiple_current, 1, 7 * Time.deltaTime);
                    playerSlowdownTimer = 0;
                }


                if (wallCollideRay_lower == null)
                {
                    wallCollideRay_lower = Instantiate(wallCollideRay.gameObject, null).GetComponent<GenericRay>();
                }

                if (wallCollideRay_left_lower == null)
                {
                    wallCollideRay_left_lower = Instantiate(wallCollideRay_left.gameObject, null).GetComponent<GenericRay>();
                }

                if (wallCollideRay_right_lower == null)
                {
                    wallCollideRay_right_lower = Instantiate(wallCollideRay_right.gameObject, null).GetComponent<GenericRay>();
                }

                if (wallCollideRay_upper == null)
                {
                    wallCollideRay_upper = Instantiate(wallCollideRay.gameObject, null).GetComponent<GenericRay>();
                }

                if (wallCollideRay_left_upper == null)
                {
                    wallCollideRay_left_upper = Instantiate(wallCollideRay_left.gameObject, null).GetComponent<GenericRay>();
                }

                if (wallCollideRay_right_upper == null)
                {
                    wallCollideRay_right_upper = Instantiate(wallCollideRay_right.gameObject, null).GetComponent<GenericRay>();
                }



                if (wallCollideRay_lower != null && wallCollideRay_left_lower != null && wallCollideRay_right_lower != null && wallCollideRay_upper != null && wallCollideRay_left_upper != null && wallCollideRay_right_upper != null)
                {
                    //move the player forward along camFlatForward's directions according to the vertical input along with the moveSpeed multiplied by the run speed, and do the same for side-to-side movement.
                    moveDirectionPlusMagnitude = (camFlatForward.transform.forward * Time.deltaTime * moveSpeed * inputScr.GetInputByName("General", "Vertical") * runMultiple_current * playerSlowdownMultiple_current) + (camFlatForward.transform.right * Time.deltaTime * moveSpeed * inputScr.GetInputByName("General", "Horizontal") * runMultiple_current * playerSlowdownMultiple_current);



                    wallCollideRay.customDir_vec = moveDirectionPlusMagnitude.normalized;
                    wallCollideRay_lower.customDir_vec = wallCollideRay.customDir_vec;
                    wallCollideRay_upper.customDir_vec = wallCollideRay.customDir_vec;

                    wallCollideRay.rayLength = wallCollideRadius;
                    wallCollideRay_lower.rayLength = wallCollideRay.rayLength;
                    wallCollideRay_upper.rayLength = wallCollideRay.rayLength;


                    wallCollideRay_right.customDir_vec = gameObject.transform.right;
                    wallCollideRay_right_lower.customDir_vec = wallCollideRay_right.customDir_vec;
                    wallCollideRay_right_upper.customDir_vec = wallCollideRay_right.customDir_vec;

                    wallCollideRay_left.customDir_vec = -wallCollideRay_right.customDir_vec;
                    wallCollideRay_left_lower.customDir_vec = wallCollideRay_left.customDir_vec;
                    wallCollideRay_left_upper.customDir_vec = wallCollideRay_left.customDir_vec;
                    //wallCollideRay_right.rayLength = wallCollideRadius*2;
                    wallCollideRay_right.rayLength = wallCollideRay.rayLength*2;
                    wallCollideRay_right_lower.rayLength = wallCollideRay_right.rayLength;
                    wallCollideRay_right_upper.rayLength = wallCollideRay_right.rayLength;
                    wallCollideRay_left.rayLength = wallCollideRay_right.rayLength;
                    wallCollideRay_left_lower.rayLength = wallCollideRay_left.rayLength;
                    wallCollideRay_left_upper.rayLength = wallCollideRay_left.rayLength;

                    wallCollideRay_lower.transform.position = gameObject.transform.position - new Vector3(0, playerHeight * 0.5f, 0);
                    wallCollideRay_upper.transform.position = gameObject.transform.position + new Vector3(0, playerHeight * 0.5f, 0);
                    wallCollideRay_left_lower.transform.position = gameObject.transform.position - new Vector3(0, playerHeight * 0.5f, 0);
                    wallCollideRay_left_upper.transform.position = gameObject.transform.position + new Vector3(0, playerHeight * 0.5f, 0);
                    wallCollideRay_right_lower.transform.position = gameObject.transform.position - new Vector3(0, playerHeight * 0.5f, 0);
                    wallCollideRay_right_upper.transform.position = gameObject.transform.position + new Vector3(0, playerHeight * 0.5f, 0);

                    bool hitAWall_main = false;

                    if(wallCollideRay.ray_active==true && wallCollideRay.ray_hit.normal.y == 0.0f)
                    {
                        hitAWall_main = true;
                    }


                    bool hitAWall_lower = false;

                    if (wallCollideRay_lower.ray_active == true && wallCollideRay_lower.ray_hit.normal.y == 0.0f)
                    {
                        hitAWall_lower = true;
                    }


                    bool hitAWall_upper = false;

                    if (wallCollideRay_upper.ray_active == true && wallCollideRay_upper.ray_hit.normal.y == 0.0f)
                    {
                        hitAWall_upper = true;
                    }

                    if ((hitAWall_main==false && hitAWall_lower==false && hitAWall_upper==false))
                    {

                        stuckInCorner = false;
                        playerPos += moveDirectionPlusMagnitude;
                        if (wallNormalObj != null)
                        {
                            Destroy(wallNormalObj);
                            wallNormalObj = null;
                        }
                    }
                    else
                    {
                        
                        if (wallNormalObj == null)
                        {
                            wallNormalObj = new GameObject();
                            if (hitAWall_main==true)
                            {
                                wallNormalObj.transform.forward = wallCollideRay.ray_hit.normal;

                            }

                            if (hitAWall_lower == true)
                            {
                                wallNormalObj.transform.forward = wallCollideRay_lower.ray_hit.normal;
                            }

                            if (hitAWall_upper == true)
                            {
                                wallNormalObj.transform.forward = wallCollideRay_upper.ray_hit.normal;
                            }

                        }

                        if (wallNormalObj != null)
                        {

                            
                            Vector3 wallDirToFollow = wallNormalObj.transform.right;
                            if (Vector3.Dot(wallNormalObj.transform.right, wallCollideRay.customDir_vec) < 0)
                            {
                                wallDirToFollow = -wallDirToFollow;
                            }

                            float wallGlide = Mathf.Abs(Vector3.Dot(wallNormalObj.transform.forward, moveDirectionPlusMagnitude.normalized));

                            if (stuckInCorner == false)
                            {

                                playerPos += wallDirToFollow * moveDirectionPlusMagnitude.magnitude * (1 - wallGlide);
                            }


                            //getting into a corner and drifting outside of collision solver
                            if ((wallCollideRay_left.ray_active == true && wallCollideRay_left.ray_hit.normal.y == 0.0f && wallCollideRay_right.ray_active == true && wallCollideRay_right.ray_hit.normal.y == 0.0f) || (wallCollideRay_left_lower.ray_active == true && wallCollideRay_left_lower.ray_hit.normal.y == 0.0f && wallCollideRay_right_lower.ray_active == true && wallCollideRay_right_lower.ray_hit.normal.y == 0.0f) || (wallCollideRay_left_upper.ray_active == true && wallCollideRay_left_upper.ray_hit.normal.y == 0.0f && wallCollideRay_right_upper.ray_active == true && wallCollideRay_right_upper.ray_hit.normal.y == 0.0f))
                            {
                                stuckInCorner = true;

                            }

                            if ((wallCollideRay.ray_active == true && wallCollideRay_right.ray_active == true && wallCollideRay.ray_hit.normal.x != wallCollideRay_right.ray_hit.normal.x))
                            {
                                stuckInCorner = true;
                            }

                            if ((wallCollideRay_lower.ray_active == true && wallCollideRay_right_lower.ray_active == true && wallCollideRay_lower.ray_hit.normal.x != wallCollideRay_right_lower.ray_hit.normal.x))
                            {
                                stuckInCorner = true;
                            }

                            if ((wallCollideRay_upper.ray_active == true && wallCollideRay_right_upper.ray_active == true && wallCollideRay_upper.ray_hit.normal.x != wallCollideRay_right_upper.ray_hit.normal.x))
                            {
                                stuckInCorner = true;
                            }

                            if ((wallCollideRay.ray_active == true && wallCollideRay_left.ray_active == true && wallCollideRay.ray_hit.normal.x != wallCollideRay_left.ray_hit.normal.x))
                            {
                                stuckInCorner = true;
                            }

                            if ((wallCollideRay_lower.ray_active == true && wallCollideRay_left_lower.ray_active == true && wallCollideRay_lower.ray_hit.normal.x != wallCollideRay_left_lower.ray_hit.normal.x))
                            {
                                stuckInCorner = true;
                            }

                            if ((wallCollideRay_upper.ray_active == true && wallCollideRay_left_upper.ray_active == true && wallCollideRay_upper.ray_hit.normal.x != wallCollideRay_left_upper.ray_hit.normal.x))
                            {
                                stuckInCorner = true;
                            }


                            /*
                            //'stuck in corner' tight hallway resolver
                            //this resolver has been removed because whilst it does work, it makes it so that when you go into a dead end corner, you can wriggle through the wall super easily. It's better that I just let tight hallways make u a bit sticky to the walls than let you fall through those walls.
                            if (wallCollideRay_left.ray_active == true && wallCollideRay_right.ray_active == true)
                            {
                                if (Mathf.Abs(wallCollideRay_left.ray_hit.normal.x) == Mathf.Abs(wallCollideRay_right.ray_hit.normal.x) || Mathf.Abs(wallCollideRay_left.ray_hit.normal.z) == Mathf.Abs(wallCollideRay_right.ray_hit.normal.z))
                                {
                                    stuckInCorner = false;
                                }
                            }

                            if (wallCollideRay_left_lower.ray_active == true && wallCollideRay_right_lower.ray_active == true)
                            {
                                if (Mathf.Abs(wallCollideRay_left_lower.ray_hit.normal.x) == Mathf.Abs(wallCollideRay_right_lower.ray_hit.normal.x) || Mathf.Abs(wallCollideRay_left_lower.ray_hit.normal.z) == Mathf.Abs(wallCollideRay_right_lower.ray_hit.normal.z))
                                {
                                    stuckInCorner = false;
                                }
                            }

                            if (wallCollideRay_left_upper.ray_active==true && wallCollideRay_right_upper.ray_active == true)
                            {
                                if(Mathf.Abs( wallCollideRay_left_upper.ray_hit.normal.x)== Mathf.Abs(wallCollideRay_right_upper.ray_hit.normal.x) || Mathf.Abs(wallCollideRay_left_upper.ray_hit.normal.z) == Mathf.Abs(wallCollideRay_right_upper.ray_hit.normal.z))
                                {
                                    stuckInCorner = false;
                                }
                            }
                            */

                        }


                    }
                }



                if (inputScr.GetInputByName("DEBUG", "DBG_ZIPFORWARD") > 0)
                {
                    Vector3 moveDirZip = (camFlatForward.transform.forward * 0.2f);
                    playerPos += moveDirZip;
                }



                //set the position to playerPos after all it has had done to it
                gameObject.transform.position = playerPos;


            }
        }




    }
}
