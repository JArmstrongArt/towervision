﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MonsterSelectButtonCanvas : MonoBehaviour
{
    public Image monsterIcon_img;
    [HideInInspector]
    public string relevantMonsterAssetPath;

    [HideInInspector]
    public int canvasIndexPassedAlong =-1;

    [HideInInspector]
    public LevelBuilderSaveLoad builderSaveLoadScr;

    [HideInInspector]
    public WaveProgrammerSingleGroupWorldCanvas waveGroupScr;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            List<RectTransform> allRects = ExtendedMaths.GetAllRectsUnderPointer();
            if (allRects==null || (allRects!=null && !allRects.Contains(gameObject.GetComponent<RectTransform>())))
            {
                Destroy(gameObject);
            }
        }
    }

    public void SetMonsterToMine()
    {
        if(canvasIndexPassedAlong >= 0 && canvasIndexPassedAlong < builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups.Count)
        {
            //you have to copy the variable of the struct to modify it due to structs being a value type. it looks stupid but trust me it has to be this way.
            MonsterGroup curGroup = builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvasIndexPassedAlong];
            curGroup.monster_prefabDirectory = relevantMonsterAssetPath;

            builderSaveLoadScr.editorWaves[builderSaveLoadScr.editorWaves_index].waveGroups[canvasIndexPassedAlong] = curGroup;

            waveGroupScr.groupMonster_img.sprite = monsterIcon_img.sprite;

        }
    }
}
