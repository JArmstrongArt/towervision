﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEffect : MonoBehaviour,IEditorFunctionality
{
    [SerializeField] FlashyObj flashyScr;
    private GameObject flashyObj;
    private float lerpProgress = 0.0f;
    [SerializeField] float progressionRate;
    [SerializeField] float finalScaleMultiple;
    private Vector3 destScale;
    private Vector3 baseScale;
    private float baseAlpha = 1.0f;
    private float destAlpha = 0.0f;

    [HideInInspector]
    public List<GameObject> objectsToDestroy = new List<GameObject>();

    [HideInInspector]
    public bool reducedDamageMode = false;

    [HideInInspector]
    public float reducedDamageMode_damagePoints = 0.0f;

    
    [HideInInspector]
    public bool disarmsLedheads = false;
    

    public void EditorUpdate()
    {
        progressionRate = Mathf.Abs(progressionRate);
    }

    // Start is called before the first frame update
    void Awake()
    {
        progressionRate = Mathf.Abs(progressionRate);
        flashyObj = flashyScr.gameObject;
        baseScale = flashyObj.transform.localScale;
        destScale = new Vector3(baseScale.x*finalScaleMultiple, baseScale.y* finalScaleMultiple, baseScale.z* finalScaleMultiple);


    }

    // Update is called once per frame
    void Update()
    {
        if (ExtendedMaths.ListTrueEmpty(objectsToDestroy) == false)
        {
            if (reducedDamageMode == false)
            {
                foreach (GameObject obj in objectsToDestroy)
                {
                    if (obj != null)
                    {
                        if (disarmsLedheads == true)
                        {
                            if (obj.GetComponent<LeadheadMonster>() != null)
                            {
                                if (obj.GetComponent<LeadheadMonster>().armouredMode == true)
                                {
                                    obj.GetComponent<LeadheadMonster>().armouredMode = false;
                                }
                                else
                                {
                                    Destroy(obj);
                                }
                            }
                            else
                            {
                                Destroy(obj);
                            }
                        }
                        else
                        {
                            if(obj.GetComponent<LeadheadMonster>() != null)
                            {
                                if (obj.GetComponent<LeadheadMonster>().armouredMode == false)
                                {
                                    Destroy(obj);
                                }
                            }
                            else
                            {
                                Destroy(obj);
                            }
                            

                        }

                    }

                }

                objectsToDestroy.Clear();
            }
            else
            {
                foreach (GameObject obj in objectsToDestroy)
                {
                    if (obj != null)
                    {
                        if (obj.GetComponent<EntityInherit>() != null)
                        {
                            if (disarmsLedheads == true)
                            {
                                if (obj.GetComponent<LeadheadMonster>() != null)
                                {
                                    if (obj.GetComponent<LeadheadMonster>().armouredMode == true)
                                    {
                                        obj.GetComponent<LeadheadMonster>().armouredMode = false;
                                    }
                                    else
                                    {
                                        obj.GetComponent<EntityInherit>().ShiftHealth(-Mathf.Abs(reducedDamageMode_damagePoints));
                                    }
                                }
                                else
                                {
                                    obj.GetComponent<EntityInherit>().ShiftHealth(-Mathf.Abs(reducedDamageMode_damagePoints));
                                }
                            }
                            else
                            {
                                if (obj.GetComponent<LeadheadMonster>() != null)
                                {
                                    if (obj.GetComponent<LeadheadMonster>().armouredMode == false)
                                    {
                                        obj.GetComponent<EntityInherit>().ShiftHealth(-Mathf.Abs(reducedDamageMode_damagePoints));
                                    }
                                }
                                else
                                {
                                    obj.GetComponent<EntityInherit>().ShiftHealth(-Mathf.Abs(reducedDamageMode_damagePoints));
                                }
                                

                            }


                        }
                        else
                        {
                            if (disarmsLedheads == true)
                            {
                                if (obj.GetComponent<LeadheadMonster>() != null)
                                {
                                    if (obj.GetComponent<LeadheadMonster>().armouredMode == true)
                                    {
                                        obj.GetComponent<LeadheadMonster>().armouredMode = false;
                                    }
                                    else
                                    {
                                        Destroy(obj);
                                    }
                                }
                                else
                                {
                                    Destroy(obj);
                                }
                            }
                            else
                            {
                                if (obj.GetComponent<LeadheadMonster>() != null)
                                {
                                    if (obj.GetComponent<LeadheadMonster>().armouredMode == false)
                                    {
                                        Destroy(obj);
                                    }
                                }
                                else
                                {
                                    Destroy(obj);
                                }
                            }

                            
                        }
                    }

                }
                objectsToDestroy.Clear();
            }
        }

        /*
            if (reducedDamageMode == false)
            {
                objectsToDestroy = ExtendedMaths.DestroyGameObjectList(objectsToDestroy);
            }
            else
            {
                foreach (GameObject obj in objectsToDestroy)
                {
                    if (obj != null)
                    {
                        if (obj.GetComponent<EntityInherit>() != null)
                        {
                            obj.GetComponent<EntityInherit>().ShiftHealth(-Mathf.Abs(reducedDamageMode_damagePoints));
                        }
                        else
                        {
                            Destroy(obj);
                        }
                    }

                }
                objectsToDestroy.Clear();
            }

        */

        flashyObj.transform.localScale = Vector3.Lerp(baseScale, destScale, lerpProgress);
        if(flashyScr.flashColours_index>=0 && flashyScr.flashColours_index < flashyScr.flashColours.Length)
        {
            flashyScr.flashColours[flashyScr.flashColours_index].a = Mathf.Lerp(baseAlpha, destAlpha, lerpProgress);
            flashyScr.SetCol();
        }
        

        float lerpProgress_proposed = lerpProgress + (Time.deltaTime * progressionRate);
        lerpProgress = Mathf.Clamp(lerpProgress_proposed, 0, 1);

        if (lerpProgress >=1)
        {
            Destroy(gameObject);
        }
    }
}
