﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastProjectile : ProjectileInherit
{

    [SerializeField] float blastRadius;
    public float blastDamage;
    private GameObject explosion_prefab;

    [HideInInspector]
    public TowerInherit srcTower;
    protected override void Awake()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.Awake();

            if (gameObject.GetComponent<LifeTimer>() != null)
            {
                
                Destroy(gameObject.GetComponent<LifeTimer>());
            }
        }
    }



    protected override void Update()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.Update();

            if (lifetime > 0)
            {
                lifetime -= Time.deltaTime;

                if (srcTower != null)
                {
                    if (Vector3.Distance(gameObject.transform.position, srcTower.gameObject.transform.position) >= srcTower.awarenessRadius)
                    {
                        base.Contact();
                    }
                }
            }
            else
            {
                base.Contact();

            }


        }
        
    }
    protected override void React()
    {
        if (base.FindDependencies() && FindResources())
        {
            List<MonsterInherit> affectedMonsters = ExtendedMaths.FindAllMonstersInRange(gameObject.transform.position, blastRadius,gameObject);
            List<GameObject> affectedMonsters_obj = new List<GameObject>();
            if (ExtendedMaths.ListTrueEmpty(affectedMonsters) == false)
            {
                foreach (MonsterInherit mons in affectedMonsters)
                {
                    if (mons != null)
                    {
                        affectedMonsters_obj.Add(mons.gameObject);
                    }

                }
            }

            GameObject explosion_inst = Instantiate(explosion_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
            ExplosionEffect explodeScr = explosion_inst.GetComponent<ExplosionEffect>();

            if (explodeScr != null)
            {
                explodeScr.reducedDamageMode = true;
                explodeScr.disarmsLedheads = true;
                explodeScr.reducedDamageMode_damagePoints = blastDamage;
                explodeScr.objectsToDestroy = affectedMonsters_obj;

            }
        }


        
    }


    protected virtual bool FindResources()
    {
        bool ret = false;
        if (explosion_prefab == null)
        {
            explosion_prefab = Resources.Load<GameObject>("Particles/pS_explosion");
        }


        if (explosion_prefab != null)
        {
            ret = true;
        }

        return ret;
    }

}
