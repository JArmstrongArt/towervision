﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//the functionality specific to the standard tower
public class StandardTower : TowerInherit
{
    private LineRenderer shootLine;//the white line drawn from this tower to the target monster for a few frames to visualize a bullet.
    [SerializeField] float shootLine_duration;//how long in seconds the white line is drawn for.
    private float shootLine_duration_orig;
    private float attackRate_orig_orig;//standard towers upgrade increases its attack speed, so i need to alter the orig attack speed in towerinherit and keep that value's orig value safe here.


    private GameObject soundEffect_shoot_prefab;

    protected override void Awake()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.Awake();
            attackRate_orig_orig = base.attackRate_orig;
            shootLine = gameObject.GetComponent<LineRenderer>();
            shootLine_duration_orig = shootLine_duration;
        }


    }

    //implementation of how upgrades affect this specific tower, it increases its fire rate
    protected override void UpgradeEffectUpdate()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.attackRate_orig = attackRate_orig_orig * base.upgrades[base.upgradeLevel - 1].optionalParameter1;
        }

    }
    // Update is called once per frame

    protected override void Update()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.Update();



            //if the tower is finished being built, drain the duration of the bullet line being rendered should the timer for that bullet line ever be above 0.
            if (base.buildingProgress >= 1)
            {
                if (shootLine_duration > 0)
                {
                    shootLine_duration -= Time.deltaTime;
                }
                else
                {
                    if (shootLine != null)
                    {
                        shootLine.enabled = false;
                    }

                }
            }
        }


    }

    //overrides an abstract class in towerinherit so that towerinherit can run Attack() even though its in one of it's children.
    protected override void Attack()
    {
        if (base.FindDependencies() && FindResources())
        {
            //if there is at least one monster in the game and its within range of this tower
            if (base.monster_nearest != null)
            {
                Vector3 myPos_billboardSet = gameObject.transform.position;
                Vector3 monsterPos_billboardSet = new Vector3(base.monster_nearest.transform.position.x, gameObject.transform.position.y, base.monster_nearest.transform.position.z);
                base.animScr.billboardWorldDir_set((monsterPos_billboardSet - myPos_billboardSet).normalized);//have the direction of the tower be facing the monster if it were a real 3d object

                //if one of the monsters components is or inherits from EntityInherit
                if (base.monster_nearest.GetComponent<EntityInherit>() != null)
                {
                    EntityInherit monsterScr = base.monster_nearest.GetComponent<EntityInherit>();
                    monsterScr.ShiftHealth(-Mathf.Abs(base.damage));//minus the monsters health by however much damage this tower does on attack, using the shifthealth function of entityinherit to keep it clamped.
                }

                //set up some simple rendering prerequisites for the bullet line.
                Vector3 myPos_bulletHeight = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1.3f, gameObject.transform.position.z);
                Vector3 myPos_lineSet = myPos_bulletHeight + (gameObject.transform.right * FigureOutShootRightMultiple());
                //Vector3 monsterPos_lineSet = new Vector3(base.monster_nearest.transform.position.x, gameObject.transform.position.y + 1.3f, base.monster_nearest.transform.position.z);
                Vector3 monsterPos_lineSet = myPos_lineSet + ((monsterPos_billboardSet - myPos_billboardSet).normalized * Vector3.Distance(myPos_bulletHeight, new Vector3(base.monster_nearest.transform.position.x, myPos_bulletHeight.y, base.monster_nearest.transform.position.z)));

                Instantiate(soundEffect_shoot_prefab, myPos_lineSet, Quaternion.Euler(Vector3.zero), null);

                if (shootLine != null)
                {

                    shootLine.positionCount = 2;
                    shootLine.SetPosition(0, myPos_lineSet);
                    shootLine.SetPosition(1, monsterPos_lineSet);
                    shootLine_duration = shootLine_duration_orig;
                    shootLine.enabled = true;
                }
            }
        }

    }

    protected override bool FindResources()
    {
        bool parRet = base.FindResources();

        bool ret = false;

        if (soundEffect_shoot_prefab == null)
        {
            soundEffect_shoot_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_standardTowerShoot");
        }

        if(soundEffect_shoot_prefab!=null && parRet == true)
        {
            ret = true;
        }
        return ret;
    }




}
