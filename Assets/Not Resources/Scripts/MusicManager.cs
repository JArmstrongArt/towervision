﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum SongFadeStage
{
    Playing,
    FadingOut,
    FadingIn
}
[System.Serializable]
struct Song
{
    public string name;
    public AudioClip snd;
}

public class MusicManager : MonoBehaviour,IEditorFunctionality
{
    private AudioSource musicSrc;
    [SerializeField] Song[] songs;

    private SongFadeStage fadeStageEnum = SongFadeStage.Playing;
    private AudioClip nextSong;
    private float musicFadeRate = 0.4f;
    private bool stopSongMode;

    private float musicVol = 0.35f;
    private void Awake()
    {
        if (FindDependencies())
        {
            DefaultTheSrc();

        }

    }

    void DefaultTheSrc()
    {
        if (FindDependencies())
        {
            if (GameOptions.mixer != null)
            {
                musicSrc.outputAudioMixerGroup = GameOptions.mixer.FindMatchingGroups("Music")[0];
            }
            musicSrc.dopplerLevel = 0;
            musicSrc.spread = 0;
            musicSrc.pitch = 1;
            musicSrc.panStereo = 0;
            musicSrc.spatialBlend = 0;
            musicSrc.reverbZoneMix = 1;
            musicSrc.mute = false;
            musicSrc.playOnAwake = false;
            musicSrc.loop = true;
            musicSrc.Stop();
        }

    }
    private void Update()
    {
        if (FindDependencies())
        {

            

            switch (fadeStageEnum)
            {
                case SongFadeStage.Playing:
                    musicSrc.clip = nextSong;
                    if (musicSrc.isPlaying == false)
                    {
                        musicSrc.Play();
                    }

                    break;

                case SongFadeStage.FadingIn:
                    musicSrc.clip = nextSong;
                    if (musicSrc.isPlaying == false)
                    {
                        musicSrc.Play();
                    }
                    if (musicSrc.volume < musicVol)
                    {
                        musicSrc.volume += musicFadeRate * Time.deltaTime*musicVol;
                    }
                    else
                    {
                        musicSrc.volume = musicVol;
                        fadeStageEnum = SongFadeStage.Playing;
                    }

                    break;

                case SongFadeStage.FadingOut:
                    if (musicSrc.volume > 0)
                    {
                        musicSrc.volume -= musicFadeRate * Time.deltaTime* musicVol;
                    }
                    else
                    {
                        musicSrc.volume = 0;
                        if (stopSongMode == false)
                        {
                            fadeStageEnum = SongFadeStage.FadingIn;
                        }
                        else
                        {
                            musicSrc.Stop();
                        }

                    }
                    break;
            }
        }

    }
    public void PlaySong(string nm)
    {
        if (FindDependencies())
        {
            nextSong = FindSongByName(nm);

            if (nextSong != musicSrc.clip)
            {

                if (musicSrc.isPlaying == false)
                {

                    musicSrc.volume = 0;
                    fadeStageEnum = SongFadeStage.FadingIn;
                }
                else
                {
                    fadeStageEnum = SongFadeStage.FadingOut;
                }


                stopSongMode = false;
            }


        }

    }

    public void StopSong()
    {
        if (FindDependencies())
        {
            fadeStageEnum = SongFadeStage.FadingOut;
            stopSongMode = true;
        }

    }

    AudioClip FindSongByName(string nm)
    {
        if (FindDependencies())
        {
            for (int i = 0; i < songs.Length; i++)
            {
                if (songs[i].name == nm)
                {
                    return songs[i].snd;
                }
            }
        }

        return null;
    }

    bool FindDependencies()
    {
        bool ret = false;

        musicSrc = gameObject.GetComponent<AudioSource>();

        if (musicSrc != null)
        {
            ret = true;
        }
        return ret;
    }

    public void EditorUpdate()
    {
        gameObject.name = "MusicManager";
    }
}
