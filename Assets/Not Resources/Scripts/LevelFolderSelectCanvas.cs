﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFolderSelectCanvas : MonoBehaviour
{
    // Start is called before the first frame update
    private MainMenuHotbarCanvas hotbarScr;



    public void OfficialPackClicked()
    {
        if (FindDependencies())
        {
            LevelSaveLoad.folderToLoad = LevelSaveLoadFolder.OFFICIAL;
            hotbarScr.SwitchCanvases((int)SwitchCanvasMode.LEVELSELECT);
        }
    }

    public void UserPackClicked()
    {
        if (FindDependencies())
        {
            LevelSaveLoad.folderToLoad = LevelSaveLoadFolder.USER;
            hotbarScr.SwitchCanvases((int)SwitchCanvasMode.LEVELSELECT);

        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        hotbarScr = GameObject.FindObjectOfType<MainMenuHotbarCanvas>();

        if (hotbarScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
