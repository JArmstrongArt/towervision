﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum FlashOrder
{
    Random,
    RandomNoRepeat,
    Sequenced,
    SequencedNoLoop
}
public class FlashyObj : MonoBehaviour,IEditorFunctionality
{
    private MeshRenderer rend;
    private MeshFilter filt;

    private Material flashyMat;
    [SerializeField] float flashRate;
    private float flashRate_orig;
    public Color[] flashColours;
    [SerializeField] FlashOrder orderEnum;
    public int flashColours_index;
    // Start is called before the first frame update
    void Awake()
    {
        if (FindResources())
        {
            flashColours_index = Mathf.Clamp(flashColours_index, 0, flashColours.Length - 1);
            filt = gameObject.GetComponent<MeshFilter>();

            rend = gameObject.GetComponent<MeshRenderer>();
            flashRate_orig = flashRate;

            if (rend != null)
            {
                rend.material = flashyMat;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (FindResources())
        {
            if (filt != null && rend != null)
            {
                if (filt.mesh != null)
                {
                    rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    rend.receiveShadows = false;

                    

                    if (flashRate > 0)
                    {
                        flashRate -= Time.deltaTime;
                    }
                    else
                    {
                        switch (orderEnum)
                        {
                            case FlashOrder.Random:
                                flashColours_index = Random.Range(0, flashColours.Length);
                                break;

                            case FlashOrder.RandomNoRepeat:
                                int prev = flashColours_index;
                                while (flashColours_index == prev)
                                {
                                    flashColours_index = Random.Range(0, flashColours.Length);
                                }
                                
                                break;

                            case FlashOrder.Sequenced:
                                flashColours_index = Mathf.RoundToInt( ExtendedMaths.WrapValue(flashColours_index, 1, 0, flashColours.Length - 1));
                                break;

                            case FlashOrder.SequencedNoLoop:
                                flashColours_index = Mathf.Clamp(flashColours_index + 1, 0, flashColours.Length - 1);
                                break;
                        }
                        SetCol();
                        
                        flashRate = flashRate_orig;
                    }
                }
                

            }
        }

    }

    public void SetCol()
    {
        if (flashColours_index >= 0 && flashColours_index < flashColours.Length)
        {
            rend.material.SetColor("_Color", flashColours[flashColours_index]);
        }
    }

    bool FindResources()
    {
        bool ret = false;

        if (flashyMat == null)
        {
            flashyMat = Resources.Load<Material>("Materials/flashyMat");
        }

        if (flashyMat != null)
        {
            ret = true;
        }
        return ret;
    }

    public void EditorUpdate()
    {
        if (!ExtendedMaths.Array1DTrueEmpty(flashColours))
        {
            flashColours_index = Mathf.Clamp(flashColours_index, 0, flashColours.Length - 1);
        }
        else
        {
            flashColours_index = 0;
        }
        
    }
}
