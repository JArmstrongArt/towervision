﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TMProCharacterSpacingRetainer : MonoBehaviour,IEditorFunctionality
{
    private float origSpacing = float.MaxValue;
    private float origFontSize = float.MaxValue;
    private TextMeshProUGUI textComp;
    [HideInInspector]
    public int newLineCharacterThreshold;


    // Start is called before the first frame update
    void Awake()
    {
        if (FindDependencies())
        {
            ExtendedMaths.TMPTextVerify(textComp, newLineCharacterThreshold);
            if (origSpacing >= float.MaxValue)
            {
                origSpacing = textComp.characterSpacing;
            }

            if (origFontSize >= float.MaxValue)
            {
                origFontSize = textComp.fontSize;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (FindDependencies())
        {
            //31.71
            if (textComp.characterSpacing != origSpacing - 28)
            {
                textComp.characterSpacing = origSpacing - 28;
            }

            if(textComp.fontSize!=origFontSize* 1.1388f)
            {
                textComp.fontSize = origFontSize * 1.1388f;
            }



        }

    }

    bool FindDependencies()
    {
        bool ret = false;
        textComp = gameObject.GetComponent<TextMeshProUGUI>();

        if (textComp != null)
        {
            ret = true;
        }
        return ret;
    }

    public void EditorUpdate()
    {
        newLineCharacterThreshold = Mathf.Clamp(newLineCharacterThreshold, 0, int.MaxValue);
    }
}
