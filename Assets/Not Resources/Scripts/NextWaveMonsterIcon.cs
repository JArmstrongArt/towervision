﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class NextWaveMonsterIcon : MonoBehaviour
{
    public TextMeshProUGUI monsterQuantity_txt;
    public Image monster_img;
}
