﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class OptionsCanvas : MonoBehaviour
{
    [SerializeField] Toggle fullscreen_tog;
    [SerializeField] TMP_InputField resolutionX_inp;

    [SerializeField] TMP_InputField resolutionY_inp;

    [SerializeField] Slider musicVol_slider;
    [SerializeField] Slider soundEffectsVol_slider;
    private float minVol = -80.0f;
    private float maxVol = 0.0f;

    [HideInInspector]
    public bool closeButton = false;
    [SerializeField] GameObject closeButtonObj;

    public void Close()
    {
        if (closeButton)
        {
            Canvas canv = gameObject.GetComponent<Canvas>();
            if (canv != null)
            {
                canv.enabled = false;
            }

        }
    }

    private void Update()
    {
        if(closeButtonObj.activeSelf != closeButton)
        {
            closeButtonObj.SetActive(closeButton);
        }
    }

    private void Awake()
    {
        SetOptionsAccordingToPlayerPrefs();

    }
    public void ApplySettings()
    {
        bool fullscreenMode = fullscreen_tog.isOn;
        int resX = ResX_Verify();
        int resY = ResY_Verify();
        float musicVol = Mathf.Lerp(minVol, maxVol,musicVol_slider.value);
        float soundEffectsVol = Mathf.Lerp(minVol,maxVol, soundEffectsVol_slider.value);

        OptionBundle save = new OptionBundle(resX, resY, musicVol, soundEffectsVol, fullscreenMode);
        GameOptions.SavePlayerPrefs(save);
        GameOptions.LoadPlayerPrefs();
    }

    void SetOptionsAccordingToPlayerPrefs()
    {
        OptionBundle loadedOptions = GameOptions.LoadPlayerPrefs();
        fullscreen_tog.isOn = loadedOptions.fullscreen;
        resolutionX_inp.text = loadedOptions.resX.ToString();
        resolutionY_inp.text = loadedOptions.resY.ToString();
        ExtendedMaths.TMPTextVerify_InputField(resolutionX_inp);
        ExtendedMaths.TMPTextVerify_InputField(resolutionY_inp);





        musicVol_slider.value = ExtendedMaths.LerpInterpFromResult(minVol,maxVol,loadedOptions.musicVol);
        soundEffectsVol_slider.value = ExtendedMaths.LerpInterpFromResult(minVol, maxVol, loadedOptions.soundEffectsVol);
        ApplySettings();
    }

    public void ResX_Verify_NoReturn()//this function and the one below it only exists because the inspector GUI for button events is so outdated and limited, just stating facts.
    {
        ResX_Verify();
    }

    public void ResY_Verify_NoReturn()
    {
        ResY_Verify();
    }

    public void ResX_Refresh()
    {
        ExtendedMaths.TMPTextVerify_InputField(resolutionX_inp);

    }

    public void ResY_Refresh()
    {
        ExtendedMaths.TMPTextVerify_InputField(resolutionY_inp);

    }

    int ResX_Verify()
    {
        int resX = Mathf.RoundToInt(ExtendedMaths.GetNumFromInputField(resolutionX_inp, GameOptions.minRes_X, GameOptions.maxRes_X));


        resolutionX_inp.text = resX.ToString();
        ExtendedMaths.TMPTextVerify_InputField(resolutionX_inp);
        return resX;
    }

    int ResY_Verify()
    {
        int resY = Mathf.RoundToInt(ExtendedMaths.GetNumFromInputField(resolutionY_inp, GameOptions.minRes_Y, GameOptions.maxRes_Y));

        resolutionY_inp.text = resY.ToString();
        ExtendedMaths.TMPTextVerify_InputField(resolutionY_inp);

        return resY;
    }
}
