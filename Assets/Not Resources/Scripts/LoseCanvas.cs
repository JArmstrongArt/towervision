﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoseCanvas : MonoBehaviour
{
    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name.ToString());
    }

    public void Quit()
    {
        if (LevelSaveLoad.folderToLoad != LevelSaveLoadFolder.TEST)
        {
            SceneManager.LoadScene("MainMenu");

        }
        else
        {
            LevelBuilderSaveLoad.resumePreviousSession = true;

            SceneManager.LoadScene("LevelEditor");

        }
    }
}
