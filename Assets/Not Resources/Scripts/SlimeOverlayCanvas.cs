﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlimeOverlayCanvas : MonoBehaviour
{
    [SerializeField] CanvasGroup slimeCanvGroup;
    private PlayerMovement moveScr;
    [SerializeField] Image slimeImg;

    [SerializeField] RectTransform slimeOverlayRect;
    [SerializeField] RectTransform slimeOverlayColourRect;
    [SerializeField] WeaponAnimations animScr;
    private bool manualAwakeDone = false;
    private void Awake()
    {


    }



    private void Update()
    {
        if (FindDependencies())
        {
            if (manualAwakeDone == false)
            {
                slimeImg.type = Image.Type.Tiled;

                resizeOverlay();
                manualAwakeDone = true;
            }
            else
            {
                if (animScr.allWeaponAnimations_index >= 0 && animScr.allWeaponAnimations_index < animScr.allWeaponAnimations.Length)
                {
                    if (animScr.allWeaponAnimations[animScr.allWeaponAnimations_index].playingAnimation == false)
                    {
                        animScr.allWeaponAnimations[animScr.allWeaponAnimations_index].PlayAnimation();
                    }
                }

                slimeCanvGroup.alpha = (float)moveScr.playerSlowdownTimer / (float)moveScr.playerSlowdownTimer_orig;
            }

        }
        else
        {
            manualAwakeDone = false;
        }


        
    }
    void OnRectTransformDimensionsChange()
    {
        if (FindDependencies())
        {
            resizeOverlay();
        }

    }

    void resizeOverlay()
    {
        if (FindDependencies())
        {
            slimeOverlayRect.sizeDelta = gameObject.GetComponent<RectTransform>().sizeDelta;
            slimeOverlayColourRect.sizeDelta = slimeOverlayRect.sizeDelta;
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        if (moveScr == null)
        {
            moveScr = GameObject.FindObjectOfType<PlayerMovement>();
        }

        if (moveScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
