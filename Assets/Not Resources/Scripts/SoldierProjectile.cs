﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierProjectile : ProjectileInherit
{


    private GameObject soundEffect_splat_prefab;
    public float towerHealthDamage;




    bool FindResources()
    {
        bool ret = false;

        if (soundEffect_splat_prefab == null)
        {
            soundEffect_splat_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_splat");
        }

        if (soundEffect_splat_prefab != null)
        {
            ret = true;
        }
        return ret;
    }

    protected override void React()
    {
        if(FindResources() && base.FindDependencies())
        {
            GameObject relevantSurface = base.collideRay.ray_surface;

            if (relevantSurface.layer == LayerMask.NameToLayer("Player"))
            {
                PlayerProjectileCollision playerProjCollScr = relevantSurface.GetComponent<PlayerProjectileCollision>();
                if (playerProjCollScr != null)
                {
                    if (playerProjCollScr.sourcePlayer != null)
                    {
                        PlayerMovement moveScr = playerProjCollScr.sourcePlayer.GetComponent<PlayerMovement>();
                        if (moveScr != null)
                        {
                            moveScr.playerSlowdownTimer = moveScr.playerSlowdownTimer_orig;
                            moveScr.playerSlowdownMultiple = moveScr.playerSlowdownMultiple_orig;

                        }
                        Instantiate(soundEffect_splat_prefab, null);
                    }
                }

            }

            if (relevantSurface.layer == LayerMask.NameToLayer("Tower"))
            {
                EntityInherit towerEntScr = relevantSurface.GetComponent<EntityInherit>();
                if (towerEntScr != null)
                {
                    towerEntScr.ShiftHealth(-Mathf.Abs(towerHealthDamage));
                }
            }
        }


    }
}
