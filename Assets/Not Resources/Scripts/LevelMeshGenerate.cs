﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;

struct MeshGenerateReturn
{
    public Mesh returnedMesh;
    public Mesh returnedMesh_minimap;
    public LevelBlockData_BlockType representedBlockType;
    public string representedBlockTexPath;
    public Vector3 spawnPos;
    public List<Vector3> playerSpawnPos;
}

struct PathIndexNormalizerData
{
    public intVec2 dataIndex;
    public int pathIndex;

    public PathIndexNormalizerData(int pI, int x, int y)
    {
        pathIndex = pI;
        dataIndex = new intVec2(x, y);
    }
}



public class LevelMeshGenerate : MonoBehaviour,IEditorFunctionality
{
    private GameObject matchManagerObj_inst;
    private GameObject monsterPathObj_inst;


    [HideInInspector]
    public LevelBlockData[,] allLevelBlockData;

    private LevelBlockData[,] allLevelBlockData_withInvisibleWalls;



    private Material levelMeshMat;
    private Material minimapMat;
    private Material levelMeshMat_invisible;

    [HideInInspector]
    public MonsterWave[] waveData;

    private List<GameObject> objectsSpawnedDuringLevelGeneration = new List<GameObject>();

    private LevelSaveLoadLive saveLoadLiveScr;
    private GameObject player_prefab;
    // Start is called before the first frame update

    void OnDestroy()
    {
        foreach(GameObject comp in objectsSpawnedDuringLevelGeneration)
        {
            if (comp != null)
            {
                Destroy(comp);
            }
        }
    }

    public void GenerateTheLevel()
    {
        if (FindResources() && FindAndSubstituteDependencies())
        {
            gameObject.transform.position = GridSnap.SnapToGrid(new Vector3(0,0,0), false, true);

            NormalizeMonsterPathIndexes();
            AddInvisibleWallsToLevelData();
            HandleAndGenerateMeshes();
            InstantiateMatchManagerAndMonsterPath();
        }
    }

    

    void InstantiateMatchManagerAndMonsterPath()
    {
        if (FindResources() && FindAndSubstituteDependencies())
        {
            if (monsterPathObj_inst == null)
            {
                monsterPathObj_inst = new GameObject();
            }

            if (monsterPathObj_inst != null)
            {
                objectsSpawnedDuringLevelGeneration.Add(monsterPathObj_inst);
                MonsterPath pathScr = monsterPathObj_inst.AddComponent<MonsterPath>();
                pathScr.allWaves = waveData;


                List<int> allPathIndexesFromLevelData = new List<int>();

                for (int i = 0; i < allLevelBlockData_withInvisibleWalls.GetLength(0); i++)
                {
                    for (int j = 0; j < allLevelBlockData_withInvisibleWalls.GetLength(1); j++)
                    {
                        if (allLevelBlockData_withInvisibleWalls[i, j].monsterPathIndex >= 0)
                        {
                            allPathIndexesFromLevelData.Add(allLevelBlockData_withInvisibleWalls[i, j].monsterPathIndex);
                        }
                    }
                }

                allPathIndexesFromLevelData.Sort();

                Vector3 startPos = new Vector3(gameObject.transform.position.x + (GridSnap.GRID_SIZE * 0.5f), gameObject.transform.position.y, gameObject.transform.position.z + (GridSnap.GRID_SIZE * 0.5f));

                for (int i = 0; i < allPathIndexesFromLevelData.Count; i++)
                {
                    Vector3 currentPos = startPos;
                    for (int j = 0; j < allLevelBlockData_withInvisibleWalls.GetLength(0); j++)
                    {
                        for (int k = 0; k < allLevelBlockData_withInvisibleWalls.GetLength(1); k++)
                        {
                            if (allLevelBlockData_withInvisibleWalls[j, k].monsterPathIndex == allPathIndexesFromLevelData[i])
                            {

                                GameObject newPoint = new GameObject();

                                newPoint.transform.position = new Vector3(currentPos.x, currentPos.y + allLevelBlockData_withInvisibleWalls[j, k].monsterPathIndex_elevation, currentPos.z);
                                newPoint.transform.parent = pathScr.gameObject.transform;
                            }
                            currentPos += new Vector3(GridSnap.GRID_SIZE, 0, 0);


                        }
                        currentPos = new Vector3(startPos.x, currentPos.y, currentPos.z + GridSnap.GRID_SIZE);

                    }
                }

                pathScr.GeneratePath();

                if (matchManagerObj_inst == null)
                {
                    matchManagerObj_inst = new GameObject();
                }

                if (matchManagerObj_inst != null)
                {
                    MatchManager manageScr = matchManagerObj_inst.AddComponent<MatchManager>();
                    objectsSpawnedDuringLevelGeneration.Add(matchManagerObj_inst);

                }

            }
        }





    }

    void NormalizeMonsterPathIndexes()
    {

        if (FindResources() && FindAndSubstituteDependencies())
        {
            List<PathIndexNormalizerData> pathIndexesRetrieved = new List<PathIndexNormalizerData>();

            for (int i = 0; i < allLevelBlockData.GetLength(0); i++)
            {
                for(int j = 0; j < allLevelBlockData.GetLength(1); j++)
                {
                    if (allLevelBlockData[i, j].monsterPathIndex >= 0)
                    {
                        pathIndexesRetrieved.Add(new PathIndexNormalizerData(allLevelBlockData[i, j].monsterPathIndex,i,j));
                    }
                }
            }

            pathIndexesRetrieved.Sort((p1, p2) => p1.pathIndex.CompareTo(p2.pathIndex));



            for (int i = 0; i < pathIndexesRetrieved.Count; i++) 
            {
                allLevelBlockData[pathIndexesRetrieved[i].dataIndex.x, pathIndexesRetrieved[i].dataIndex.y].monsterPathIndex = i;
            }

        }
    }

    void AddInvisibleWallsToLevelData()
    {
        if (FindResources() && FindAndSubstituteDependencies())
        {
            List<LevelBlockData[]> dataAsList = LevelBlockDataAsList(allLevelBlockData);

            if (dataAsList != null)
            {

                for(int i = 0; i < dataAsList.Count; i++)
                {
                    List<LevelBlockData> dataAsList_indexAsList = new List<LevelBlockData>();

                    for(int j = 0; j < dataAsList[i].Length; j++)
                    {
                        dataAsList_indexAsList.Add(dataAsList[i][j]);
                    }

                    dataAsList_indexAsList.Insert(0, new LevelBlockData(1000, LevelBlockData_BlockType.INVISIBLE));
                    dataAsList_indexAsList.Add(new LevelBlockData(1000, LevelBlockData_BlockType.INVISIBLE));

                    dataAsList[i] = dataAsList_indexAsList.ToArray();
                }


                LevelBlockData[] invisibleWallChunk = new LevelBlockData[dataAsList[0].Length];

                for (int i = 0; i < invisibleWallChunk.Length; i++)
                {
                    invisibleWallChunk[i] = new LevelBlockData(1000, LevelBlockData_BlockType.INVISIBLE);
                }

                dataAsList.Insert(0, invisibleWallChunk);
                dataAsList.Add(invisibleWallChunk);
            }

            allLevelBlockData_withInvisibleWalls = new LevelBlockData[dataAsList.Count, dataAsList[0].Length];

            for(int i=0;i< dataAsList.Count; i++)
            {

                for (int j = 0; j < allLevelBlockData_withInvisibleWalls.GetLength(1); j++)
                {
                    allLevelBlockData_withInvisibleWalls[i,j] = dataAsList[i][j];
                }

            }


        }


    }

    List<LevelBlockData[]> LevelBlockDataAsList(LevelBlockData[,] sourceArray)
    {
        if (FindResources() && FindAndSubstituteDependencies())
        {
            List<LevelBlockData[]> ret = new List<LevelBlockData[]>();
            for (int i = 0; i < sourceArray.GetLength(0); i++)
            {
                LevelBlockData[] listAdd = new LevelBlockData[sourceArray.GetLength(1)];

                for (int j = 0; j < listAdd.Length; j++)
                {
                    listAdd[j] = sourceArray[i, j];
                }

                ret.Add(listAdd);
            }
            return ret;
        }
        else
        {
            return null;
        }

    }

    Mesh GenerateCubeMesh_Minimap()
    {
        if (FindResources() && FindAndSubstituteDependencies())
        {
            Vector3[] planeVerts = new Vector3[6];




            //top
            planeVerts[0] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            planeVerts[1] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, 0) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            planeVerts[2] = Vector3.zero + new Vector3(0, GridSnap.GRID_SIZE, 0);


            planeVerts[3] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, 0) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            planeVerts[4] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            planeVerts[5] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0);






            int[,] planeTris_segmented = new int[,]
            {

            { 0,1,2 },
            { 3,4,5 }

            };

            Vector2[] planeUVs = {
            new Vector2(0.3f, 0.0f),
            new Vector2(0.0f, 0.3f),
            new Vector2(0.0f, 0.0f),
            new Vector2(0.0f, 0.3f),
            new Vector2(0.3f, 0.0f),
            new Vector2(0.3f, 0.3f) };









            int[] planeTris = ExtendedMaths.SegmentedTrisToContinuousTris(planeTris_segmented);
            Mesh retPlane = new Mesh();
            retPlane.vertices = planeVerts;
            retPlane.triangles = planeTris;
            retPlane.uv = planeUVs;


            return retPlane;
        }
        else
        {
            return null;
        }

    }


    Mesh GenerateCubeMesh() 
    {
        if (FindResources() && FindAndSubstituteDependencies())
        {
            Vector3[] cubeVerts = new Vector3[36];


            //bottom
            cubeVerts[0] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, 0);
            cubeVerts[1] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE);
            cubeVerts[2] = Vector3.zero;




            cubeVerts[3] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE);
            cubeVerts[4] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, 0);
            cubeVerts[5] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, GridSnap.GRID_SIZE);


            //top
            cubeVerts[6] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            cubeVerts[7] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, 0) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            cubeVerts[8] = Vector3.zero + new Vector3(0, GridSnap.GRID_SIZE, 0);


            cubeVerts[9] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, 0) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            cubeVerts[10] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            cubeVerts[11] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0);

            //left
            cubeVerts[12] = Vector3.zero;
            cubeVerts[13] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0);

            cubeVerts[14] = Vector3.zero + new Vector3(0, GridSnap.GRID_SIZE, 0);

            cubeVerts[15] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            cubeVerts[16] = Vector3.zero;

            cubeVerts[17] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE);

            //right
            cubeVerts[18] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0) + new Vector3(GridSnap.GRID_SIZE, 0, 0);
            cubeVerts[19] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, 0);


            cubeVerts[20] = Vector3.zero + new Vector3(0, GridSnap.GRID_SIZE, 0) + new Vector3(GridSnap.GRID_SIZE, 0, 0);


            cubeVerts[21] = Vector3.zero + new Vector3(GridSnap.GRID_SIZE, 0, 0);
            cubeVerts[22] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0) + new Vector3(GridSnap.GRID_SIZE, 0, 0);


            cubeVerts[23] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(GridSnap.GRID_SIZE, 0, 0);

            //front
            cubeVerts[24] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(GridSnap.GRID_SIZE, 0, 0) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            cubeVerts[25] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE);
            cubeVerts[26] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(GridSnap.GRID_SIZE, 0, 0);

            cubeVerts[27] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE);
            cubeVerts[28] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(GridSnap.GRID_SIZE, 0, 0) + new Vector3(0, GridSnap.GRID_SIZE, 0);
            cubeVerts[29] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0);

            //back
            cubeVerts[30] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) - new Vector3(0, 0, GridSnap.GRID_SIZE);
            cubeVerts[31] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(GridSnap.GRID_SIZE, 0, 0) + new Vector3(0, GridSnap.GRID_SIZE, 0) - new Vector3(0, 0, GridSnap.GRID_SIZE);
            cubeVerts[32] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(GridSnap.GRID_SIZE, 0, 0) - new Vector3(0, 0, GridSnap.GRID_SIZE);

            cubeVerts[33] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(GridSnap.GRID_SIZE, 0, 0) + new Vector3(0, GridSnap.GRID_SIZE, 0) - new Vector3(0, 0, GridSnap.GRID_SIZE);
            cubeVerts[34] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) - new Vector3(0, 0, GridSnap.GRID_SIZE);
            cubeVerts[35] = Vector3.zero + new Vector3(0, 0, GridSnap.GRID_SIZE) + new Vector3(0, GridSnap.GRID_SIZE, 0) - new Vector3(0, 0, GridSnap.GRID_SIZE);




            int[,] cubeTris_segmented = new int[,]
            {
            { 0,1,2 },
            { 3,4,5 },
            { 6,7,8 },
            { 9,10,11 },
            { 12,13,14 },
            { 15,16,17 },
            { 18,19,20 },
            { 21,22,23 },
            { 24,25,26 },
            { 27,28,29 },
            { 30,31,32 },
            { 33,34,35 }

            };

            Vector2[] cubeUVs = {
            new Vector2(0.3f, 0.7f),
            new Vector2(0.0f, 0.3f),
            new Vector2(0.3f, 0.3f),
            new Vector2(0.0f, 0.3f),
            new Vector2(0.3f, 0.7f),
            new Vector2(0.0f, 0.7f),
            new Vector2(0.3f, 0.0f),
            new Vector2(0.0f, 0.3f),
            new Vector2(0.0f, 0.0f),
            new Vector2(0.0f, 0.3f),
            new Vector2(0.3f, 0.0f),
            new Vector2(0.3f, 0.3f),
            new Vector2(0.3f, 0.3f),
            new Vector2(0.7f, 0.7f),
            new Vector2(0.3f, 0.7f),
            new Vector2(0.7f, 0.7f),
            new Vector2(0.3f, 0.3f),
            new Vector2(0.7f, 0.3f),
            new Vector2(0.3f, 0.3f),
            new Vector2(0.7f, 0.0f),
            new Vector2(0.7f, 0.3f),
            new Vector2(0.7f, 0.0f),
            new Vector2(0.3f, 0.3f),
            new Vector2(0.3f, 0.0f),
            new Vector2(0.7f, 0.3f),
            new Vector2(1.0f, 0.0f),
            new Vector2(1.0f, 0.3f),
            new Vector2(1.0f, 0.0f),
            new Vector2(0.7f, 0.3f),
            new Vector2(0.7f, 0.0f),
            new Vector2(0.0f, 0.7f),
            new Vector2(0.3f, 1.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(0.3f, 1.0f),
            new Vector2(0.0f, 0.7f),
            new Vector2(0.3f, 0.7f) };




            Vector2[] cubeUVs_rotateEdit = cubeUVs;

            for (int i = 0; i < cubeUVs_rotateEdit.Length; i++)
            {
                if (i >= 24)
                {
                    cubeUVs_rotateEdit[i] += new Vector2(0.05f, 0.09f);
                    Quaternion rot = Quaternion.Euler(0, 0, -90.0f);
                    if (i > 29)
                    {
                        rot = Quaternion.Euler(0.0f, 0.0f, 90.0f);
                    }
                    cubeUVs_rotateEdit[i] = rot * cubeUVs_rotateEdit[i];
                }

            }
            cubeUVs = cubeUVs_rotateEdit;

            for (int i = 0; i < cubeUVs.Length; i++)
            {
                int mult = 3;
                cubeUVs[i] = new Vector2(cubeUVs[i].x * mult, cubeUVs[i].y * mult);

            }





            int[] cubeTris = ExtendedMaths.SegmentedTrisToContinuousTris(cubeTris_segmented);
            Mesh retCube = new Mesh();
            retCube.vertices = cubeVerts;
            retCube.triangles = cubeTris;
            retCube.uv = cubeUVs;


            return retCube;
        }
        else
        {
            return null;
        }

    }




    void HandleAndGenerateMeshes()
    {
        if (FindResources() && FindAndSubstituteDependencies())
        {
            if (allLevelBlockData_withInvisibleWalls == null)
            {
                AddInvisibleWallsToLevelData();
            }
            if (allLevelBlockData_withInvisibleWalls != null)
            {
                MeshGenerateReturn[] allMeshes = GenerateMeshes();

                foreach (MeshGenerateReturn mesh in allMeshes)
                {
                    GameObject meshObj = new GameObject();
                    objectsSpawnedDuringLevelGeneration.Add(meshObj);
                    meshObj.transform.position = gameObject.transform.position+mesh.spawnPos;
                    MeshFilter meshObj_filt = meshObj.AddComponent<MeshFilter>();
                    MeshRenderer meshObj_rend = meshObj.AddComponent<MeshRenderer>();
                    meshObj_filt.mesh = mesh.returnedMesh;
                    meshObj_rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    meshObj_rend.receiveShadows = false;
                    if (mesh.representedBlockType != LevelBlockData_BlockType.INVISIBLE)
                    {
                        meshObj_rend.material = levelMeshMat;

                        Texture blockTex = Resources.Load<Texture>(mesh.representedBlockTexPath);
                        meshObj_rend.material.SetTexture("_MainTex", blockTex);
                    }
                    else
                    {
                        meshObj_rend.material = levelMeshMat_invisible;
                        meshObj_rend.material.SetTexture("_MainTex", null);
                    }
                    meshObj.layer = LayerMask.NameToLayer("WallOrFloor");




                    MeshCollider meshObj_coll = meshObj.AddComponent<MeshCollider>();
                    meshObj_coll.sharedMesh = meshObj_filt.mesh;





                    //----------------------


                    GameObject meshObj_minimap = new GameObject();
                    objectsSpawnedDuringLevelGeneration.Add(meshObj_minimap);
                    meshObj_minimap.transform.position = meshObj.transform.position;
                    MeshFilter meshObj_minimap_filt = meshObj_minimap.AddComponent<MeshFilter>();
                    MeshRenderer meshObj_minimap_rend = meshObj_minimap.AddComponent<MeshRenderer>();
                    meshObj_minimap_filt.mesh = mesh.returnedMesh_minimap;
                    meshObj_minimap_rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    meshObj_minimap_rend.receiveShadows = false;

                    meshObj_minimap_rend.material = minimapMat;

                    Color minimapColour = Color.cyan;
                    meshObj_minimap.layer = LayerMask.NameToLayer("MinimapExclusive");
                    meshObj_minimap_rend.material.SetColor("_Color", minimapColour);


                    MeshCollider meshObj_minimap_coll = meshObj_minimap.AddComponent<MeshCollider>();
                    meshObj_minimap_coll.sharedMesh = meshObj_minimap_filt.mesh;
                }
            }

        }
    }

    MeshGenerateReturn[] GenerateMeshes()
    {
        if (FindResources() && FindAndSubstituteDependencies())
        {
            List<MeshGenerateReturn> allMeshReturns = new List<MeshGenerateReturn>();
            int blocksPerChunk = 5;
            LevelBlockData[,] levelBlockRef = allLevelBlockData_withInvisibleWalls;

            List<string> allTexPaths = new List<string>();

            for(int i = 0; i < levelBlockRef.GetLength(0); i++)
            {
                for(int j = 0; j < levelBlockRef.GetLength(1); j++)
                {
                    if (!allTexPaths.Contains(levelBlockRef[i, j].blockTexPath))
                    {
                        allTexPaths.Add(levelBlockRef[i, j].blockTexPath);
                    }
                }
            }

            foreach (LevelBlockData_BlockType blockType in Enum.GetValues(typeof(LevelBlockData_BlockType)))
            {
                foreach (string blockTexPath in allTexPaths)
                {
                    Vector3 relativeSpawnPos = Vector3.zero;


                    int rangeX_index = 0;
                    int rangeY_index = 0;

                    while (rangeX_index < levelBlockRef.GetLength(0))
                    {
                        rangeY_index = 0;
                        while (rangeY_index < levelBlockRef.GetLength(1))
                        {
                            MeshGenerateReturn ret = new MeshGenerateReturn();
                            ret.representedBlockType = blockType;
                            ret.representedBlockTexPath = blockTexPath;

                            ret.returnedMesh = GenerateMesh_Submesh(ret.representedBlockType, ret.representedBlockTexPath, levelBlockRef, rangeX_index, rangeX_index + blocksPerChunk, rangeY_index, rangeY_index + blocksPerChunk, relativeSpawnPos);
                            ret.returnedMesh_minimap = GenerateMesh_Submesh_Minimap(levelBlockRef, rangeX_index, rangeX_index + blocksPerChunk, rangeY_index, rangeY_index + blocksPerChunk);
                            ret.spawnPos = relativeSpawnPos;

                            allMeshReturns.Add(ret);
                            rangeY_index += blocksPerChunk;
                            relativeSpawnPos += new Vector3(GridSnap.GRID_SIZE * blocksPerChunk, 0, 0);
                        }
                        rangeX_index += blocksPerChunk;
                        relativeSpawnPos = new Vector3(0, relativeSpawnPos.y, relativeSpawnPos.z + (GridSnap.GRID_SIZE * blocksPerChunk));
                    }
                }




            }

            return allMeshReturns.ToArray();
        }
        else
        {
            return null;
        }

    }



    Mesh GenerateMesh_Submesh_Minimap(LevelBlockData[,] sourceData, int rangeX_lower = -1, int rangeX_upper = -1, int rangeY_lower = -1, int rangeY_upper = -1)
    {
        if (FindResources() && FindAndSubstituteDependencies())
        {
            if (rangeX_lower < 0)
            {
                rangeX_lower = 0;
            }

            if (rangeX_upper < 0 || rangeX_upper > sourceData.GetLength(0))
            {
                rangeX_upper = sourceData.GetLength(0);
            }

            if (rangeY_lower < 0)
            {
                rangeY_lower = 0;
            }

            if (rangeY_upper < 0 || rangeY_upper > sourceData.GetLength(1))
            {
                rangeY_upper = sourceData.GetLength(1);
            }



            Vector3 currentPlanePos = Vector3.zero;
            List<Vector3> meshVert = new List<Vector3>();
            List<int> meshTri = new List<int>();
            List<Vector2> meshUV = new List<Vector2>();

            int vertOffset = 0;
            for (int i = rangeX_lower; i < rangeX_upper; i++)
            {

                for (int j = rangeY_lower; j < rangeY_upper; j++)
                {
                    int elevVal = sourceData[i, j].blockElev;

                    if (elevVal > 0 && sourceData[i,j].blockType!=LevelBlockData_BlockType.INVISIBLE)
                    {
                        Mesh planeMesh = GenerateCubeMesh_Minimap();
                        Vector3[] planeMesh_vert = planeMesh.vertices;
                        int[] planeMesh_tri = planeMesh.triangles;
                        Vector2[] planeMesh_uv = planeMesh.uv;

                        for (int k = 0; k < planeMesh_vert.Length; k++)
                        {
                            meshVert.Add(planeMesh_vert[k] + currentPlanePos);
                        }

                        for (int k = 0; k < planeMesh_tri.Length; k++)
                        {
                            meshTri.Add(planeMesh_tri[k] + vertOffset);
                        }

                        for (int k = 0; k < planeMesh_uv.Length; k++)
                        {
                            meshUV.Add(planeMesh_uv[k]);
                        }
                        vertOffset += planeMesh.vertices.Length;
                    }


                    currentPlanePos += new Vector3(GridSnap.GRID_SIZE, 0, 0);


                }
                currentPlanePos = new Vector3(0, currentPlanePos.y, currentPlanePos.z + GridSnap.GRID_SIZE);
            }

            Mesh retMesh = new Mesh();
            retMesh.vertices = meshVert.ToArray();
            retMesh.triangles = meshTri.ToArray();
            retMesh.uv = meshUV.ToArray();
            retMesh.RecalculateNormals();
            return retMesh;
        }
        else
        {
            return null;
        }
    }


    Mesh GenerateMesh_Submesh(LevelBlockData_BlockType meshType, string meshTexPath, LevelBlockData[,] sourceData,int rangeX_lower = -1,int rangeX_upper = -1, int rangeY_lower = -1, int rangeY_upper = -1, Vector3 spawnPosOffset = default(Vector3))
    {
        

        if (FindResources() && FindAndSubstituteDependencies())
        {
            if (rangeX_lower < 0)
            {
                rangeX_lower = 0;
            }

            if (rangeX_upper < 0||rangeX_upper>sourceData.GetLength(0))
            {
                rangeX_upper = sourceData.GetLength(0);
            }

            if (rangeY_lower < 0)
            {
                rangeY_lower = 0;
            }

            if (rangeY_upper < 0 || rangeY_upper > sourceData.GetLength(1))
            {
                rangeY_upper = sourceData.GetLength(1);
            }



            Vector3 currentCubePos = Vector3.zero;
            List<Vector3> meshVert = new List<Vector3>();
            List<int> meshTri = new List<int>();
            List<Vector2> meshUV = new List<Vector2>();

            int vertOffset = 0;
            for (int i = rangeX_lower; i < rangeX_upper; i++)
            {

                for (int j = rangeY_lower; j < rangeY_upper; j++)
                {


                    if(sourceData[i, j].blockType == meshType && sourceData[i,j].blockTexPath==meshTexPath)
                    {
                        Mesh levelCube = GenerateCubeMesh();
                        Vector3[] levelCube_vert = levelCube.vertices;
                        int[] levelCube_tri = levelCube.triangles;
                        Vector2[] levelCube_uv = levelCube.uv;

                        List<int> upperVertices = new List<int> {6,7,8,9,10,11, 13, 14, 15, 18, 20, 22, 24, 28, 29, 31, 33, 35 };

                        int elevInt = sourceData[i, j].blockElev;

                        

                        for (int k = 0; k < levelCube_vert.Length; k++)
                        {
                            if (k>11)
                            {
                                levelCube_uv[k] = new Vector2(levelCube_uv[k].x, levelCube_uv[k].y * (elevInt+1));
                            }

                            if (upperVertices.Contains(k))
                            {

                                levelCube_vert[k] = new Vector3(levelCube_vert[k].x, levelCube_vert[k].y + (GridSnap.GRID_SIZE * elevInt), levelCube_vert[k].z);
                                switch (sourceData[i, j].blockAng)
                                {
                                    case LevelBlockData_BlockAngle.UP:


                                        if (k == 6 || k == 10 || k == 11 || k == 13 || k == 15 || k == 18 || k == 22 || k == 24 || k == 28 || k == 29)
                                        {
                                            levelCube_vert[k] = new Vector3(levelCube_vert[k].x, levelCube_vert[k].y + GridSnap.GRID_SIZE, levelCube_vert[k].z);
                                        }



                                        break;
                                    case LevelBlockData_BlockAngle.DOWN:

                                        if (k == 8 || k == 7 || k == 9 || k == 14 || k == 20 || k == 31 || k == 33 || k == 35)
                                        {
                                            levelCube_vert[k] = new Vector3(levelCube_vert[k].x, levelCube_vert[k].y + GridSnap.GRID_SIZE, levelCube_vert[k].z);


                                        }




                                        break;
                                    case LevelBlockData_BlockAngle.LEFT:
                                        if (k == 6 || k == 8 || k == 10 || k == 13 || k == 14 || k == 15 || k == 29 || k == 35)
                                        {
                                            levelCube_vert[k] = new Vector3(levelCube_vert[k].x, levelCube_vert[k].y + GridSnap.GRID_SIZE, levelCube_vert[k].z);
                                        }



                                        break;
                                    case LevelBlockData_BlockAngle.RIGHT:
                                        if (k == 7 || k == 9 || k == 11 || k == 18 || k == 20 || k == 22 || k == 24 || k == 28 || k == 31 || k == 33)
                                        {
                                            levelCube_vert[k] = new Vector3(levelCube_vert[k].x, levelCube_vert[k].y + GridSnap.GRID_SIZE, levelCube_vert[k].z);
                                        }




                                        break;


                                }

                            }

                        }

                        Vector3[] avgArray = ExtendedMaths.SubArray(levelCube_vert, 6, 5);
                        List<Vector3> avgArray_list = new List<Vector3>();
                        foreach(Vector3 vec in avgArray)
                        {
                            avgArray_list.Add(vec);
                        }
                        Vector3 blockAvgTop = ExtendedMaths.GetAverage(avgArray_list);

                        sourceData[i, j].monsterPathIndex_elevation = blockAvgTop.y;

                        if (sourceData[i, j].spawnPoint == true)
                        {
                            Vector3 sPVec = blockAvgTop;
                            PlayerMovement moveScr =  player_prefab.GetComponent<PlayerMovement>();
                            if (moveScr != null)
                            {
                                sPVec += new Vector3(0, moveScr.playerHeight, 0);
                            }
                            sPVec += currentCubePos;
                            sPVec += gameObject.transform.position;
                            sPVec += spawnPosOffset;


                            saveLoadLiveScr.spawnPoints.Add(sPVec);
                        }

                        for (int k = 0; k < levelCube_vert.Length; k++)
                        {
                            meshVert.Add(levelCube_vert[k] + currentCubePos);
                        }

                        for (int k = 0; k < levelCube_tri.Length; k++)
                        {
                            meshTri.Add(levelCube_tri[k] + vertOffset);
                        }

                        for (int k = 0; k < levelCube_uv.Length; k++)
                        {
                            meshUV.Add(levelCube_uv[k]);
                        }
                        vertOffset += levelCube.vertices.Length;


                        
                    }
                    currentCubePos += new Vector3(GridSnap.GRID_SIZE, 0, 0);


                }
                currentCubePos = new Vector3(0, currentCubePos.y, currentCubePos.z + GridSnap.GRID_SIZE);

            }




            Mesh retMesh = new Mesh();
            retMesh.vertices = meshVert.ToArray();
            retMesh.triangles = meshTri.ToArray();
            retMesh.uv = meshUV.ToArray();
            retMesh.RecalculateNormals();
            return retMesh;
        }
        else
        {
            return null;
        }



    }

    bool FindAndSubstituteDependencies()
    {
        bool ret = false;
        if (saveLoadLiveScr == null)
        {
            saveLoadLiveScr = GameObject.FindObjectOfType<LevelSaveLoadLive>();
        }

        if (saveLoadLiveScr == null)
        {
            GameObject saveLoadLiveScr_obj = new GameObject();
            saveLoadLiveScr = saveLoadLiveScr_obj.AddComponent<LevelSaveLoadLive>();
        }

        if (saveLoadLiveScr != null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (levelMeshMat == null)
        {
            levelMeshMat = Resources.Load<Material>("Materials/levelMeshMat");
        }

        if (levelMeshMat_invisible == null)
        {
            levelMeshMat_invisible = Resources.Load<Material>("Materials/levelMeshMat_invisible");
        }

        if (minimapMat == null)
        {
            minimapMat = Resources.Load<Material>("Materials/minimapMat");
        }

        if (player_prefab == null)
        {
            player_prefab = Resources.Load<GameObject>("Prefabs/playerCamera");
        }



        if (player_prefab!=null && minimapMat != null && levelMeshMat != null && levelMeshMat_invisible != null)
        {
            ret = true;
        }
        return ret;
    }

    public void EditorUpdate()
    {
        gameObject.name = "LevelMeshGenerator";
    }
}
