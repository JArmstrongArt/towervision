﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilderCameraMovement : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    [SerializeField] float zoomSpeed;
    private InputManager inputScr;
    private float baseZoom;

    private Camera camScr;

    private LevelBuilderSaveLoad builderSaveLoadScr;
    // Start is called before the first frame update
    void Awake()
    {
        if (FindDependencies())
        {
            camScr.orthographic = true;
            baseZoom = camScr.orthographicSize;
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (FindDependencies())
        {
            float speedUpMultiple = 1;
            if (inputScr.GetInputByName("Camera", "Editor_CamSpeedUp") > 0)
            {
                speedUpMultiple = 2;
            }
            Vector3 position_proposed = gameObject.transform.position+ new Vector3(inputScr.GetInputByName("Camera", "Editor_LeftRight") * moveSpeed * Time.deltaTime* speedUpMultiple, inputScr.GetInputByName("Camera", "Editor_UpDown") * moveSpeed * Time.deltaTime* speedUpMultiple, 0);

            if (builderSaveLoadScr.builderData != null)
            {
                if (builderSaveLoadScr.builderData.GetLength(0) >= 1)
                {
                    if (builderSaveLoadScr.builderData.GetLength(1) >= 1)
                    {
                        Vector2 minLimit = builderSaveLoadScr.builderData[0, 0].pos;
                        Vector2 maxLimit = builderSaveLoadScr.builderData[builderSaveLoadScr.builderData.GetLength(0)-1, builderSaveLoadScr.builderData.GetLength(1)-1].pos;

                        position_proposed = new Vector3(Mathf.Clamp(position_proposed.x, minLimit.x, maxLimit.x), Mathf.Clamp(position_proposed.y, minLimit.y, maxLimit.y), position_proposed.z);
                    }
                }
            }
            

            gameObject.transform.position = position_proposed;

            camScr.orthographicSize += (inputScr.GetInputByName("Camera", "Editor_CamZoom") * zoomSpeed*-1);
            camScr.orthographicSize = Mathf.Clamp(camScr.orthographicSize, baseZoom * 0.5f, baseZoom * 2.0f);
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        inputScr = GameObject.FindObjectOfType<InputManager>();

        camScr = gameObject.GetComponent<Camera>();
        builderSaveLoadScr = GameObject.FindObjectOfType<LevelBuilderSaveLoad>();
        if (builderSaveLoadScr!=null && inputScr != null && camScr!=null)
        {
            ret = true;
        }

        return ret;
    }
}
