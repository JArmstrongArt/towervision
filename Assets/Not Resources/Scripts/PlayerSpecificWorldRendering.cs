﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpecificWorldRendering : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        RenderSettings.fogColor = RenderSettings.skybox.GetColor("_Tint");
        RenderSettings.fogMode = FogMode.Linear;
        RenderSettings.fogStartDistance = 0;
        RenderSettings.fogEndDistance = 18;
        RenderSettings.fog = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
