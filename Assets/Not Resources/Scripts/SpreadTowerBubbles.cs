﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadTowerBubbles : MonoBehaviour
{
    private List<MonsterInherit> allMonstersInTrigger = new List<MonsterInherit>();
    [HideInInspector]
    public SpreadTower spreadScr;

    private GameObject soundEffect_bubbles_prefab;
    private GameObject soundEffect_bubbles_inst;
    // Start is called before the first frame update
    void Awake()
    {
        if (FindResources())
        {
            GeneralSoundEffect bubbleSEScr = soundEffect_bubbles_prefab.GetComponent<GeneralSoundEffect>();
            if (bubbleSEScr != null)
            {
                soundEffect_bubbles_inst = Instantiate(soundEffect_bubbles_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                soundEffect_bubbles_inst.GetComponent<GeneralSoundEffect>().ownerObj = gameObject;
            }
        }

    }

    private void LateUpdate()
    {
        if (FindResources())
        {
            soundEffect_bubbles_inst.transform.position = gameObject.transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (FindResources())
        {
            if (allMonstersInTrigger.Count > 0)
            {
                foreach (MonsterInherit monst in allMonstersInTrigger)
                {
                    if (monst != null)
                    {
                        if (spreadScr != null)
                        {
                            monst.ShiftHealth(-Mathf.Abs(spreadScr.damage));
                        }

                    }
                }
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (FindResources())
        {
            if (other.GetComponent<MonsterInherit>() != null)
            {
                if (!allMonstersInTrigger.Contains(other.GetComponent<MonsterInherit>()))
                {
                    allMonstersInTrigger.Add(other.GetComponent<MonsterInherit>());
                }
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (FindResources())
        {
            if (other.GetComponent<MonsterInherit>() != null)
            {
                if (allMonstersInTrigger.Contains(other.GetComponent<MonsterInherit>()))
                {
                    allMonstersInTrigger.Remove(other.GetComponent<MonsterInherit>());
                }
            }
        }

    }

    bool FindResources()
    {

        bool ret = false;


        if (soundEffect_bubbles_prefab == null)
        {
            soundEffect_bubbles_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_spreadTower");
        }

        if (soundEffect_bubbles_prefab != null )
        {
            ret = true;
        }
        return ret;
    }
}
