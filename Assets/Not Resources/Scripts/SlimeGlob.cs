﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeGlob : MonoBehaviour
{
    [HideInInspector]
    public Vector3 moveDir;
    private BillboardAnimations animScr;


    [SerializeField] float moveSpeed;
    [SerializeField] float gravityRate;
    [SerializeField] float gravityStart;
    private float gravityCurrent;
    [SerializeField] Vector2 scaleRange;
    [SerializeField] GenericRay collideRay;

    private GameObject globSplatObj_prefab;
    // Start is called before the first frame update
    void Awake()
    {
        if (FindDependencies() && FindResources())
        {
            float scale = Random.Range(scaleRange.x, scaleRange.y);
            gameObject.transform.localScale = new Vector3(scale, scale, scale);
            gravityCurrent = gravityStart;
            moveDir = new Vector3(moveDir.x, 0, moveDir.z).normalized;

            LifeTimer timeScr = gameObject.GetComponent<LifeTimer>();
            if (timeScr == null)
            {
                timeScr = gameObject.AddComponent<LifeTimer>();
            }
            timeScr.lifetime = 10.0f;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (FindDependencies() && FindResources())
        {
            moveDir = moveDir.normalized;
            if (gravityCurrent>= 8)
            {
                animScr.PlayAnimation("60");
            }else if(gravityCurrent>3 && gravityCurrent< 8)
            {
                animScr.PlayAnimation("30");
            }else if(gravityCurrent<-3 && gravityCurrent>-8)
            {
                animScr.PlayAnimation("-30");
            }else if (gravityCurrent <= -8)
            {
                animScr.PlayAnimation("-60");

            }
            else
            {
                animScr.PlayAnimation("0");
            }

            gravityCurrent -= Mathf.Abs( gravityRate * Time.fixedDeltaTime);
            Vector3 finalMoveCalc = (moveDir * moveSpeed * Time.fixedDeltaTime) + (Vector3.up * gravityCurrent * Time.fixedDeltaTime);
            gameObject.transform.position += finalMoveCalc;
            animScr.billboardWorldDir_set(finalMoveCalc.normalized);

            collideRay.customDir_vec = finalMoveCalc.normalized;
            collideRay.rayLength = finalMoveCalc.magnitude;

            if (collideRay.ray_surface !=null)
            {
                GameObject globSplatObj_inst = Instantiate(globSplatObj_prefab, collideRay.ray_hitPoint+(collideRay.ray_hit.normal*0.05f), Quaternion.Euler(Vector3.zero), null);
                globSplatObj_inst.transform.forward = collideRay.ray_hit.normal;
                globSplatObj_inst.transform.localScale = gameObject.transform.localScale;
                Destroy(gameObject);
            }


        }
    }

    bool FindDependencies()
    {
        bool ret = false;
        animScr = gameObject.GetComponent<BillboardAnimations>();
        if (animScr != null)
        {
            ret = true;
        }
        return ret;
    }

    bool FindResources()
    {
        bool ret = false;
        if (globSplatObj_prefab == null)
        {
            globSplatObj_prefab = Resources.Load<GameObject>("Prefabs/globSplatObj");
        }

        if (globSplatObj_prefab != null)
        {
            ret = true;
        }
        return ret;
    }
}
