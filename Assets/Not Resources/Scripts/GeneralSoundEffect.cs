﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum LoopMode
{
    None,
    LoopUntilGone,
    LoopUntilOwnerGone,
    LoopForever
}
public class GeneralSoundEffect : MonoBehaviour,IEditorFunctionality
{
    [SerializeField] AudioClip[] allSoundEffects;
    [SerializeField] Vector2 pitchRangePercent;
    private Vector2 pitchRangeActual;
    private AudioSource soundSource;
    private float timeBeforeDestruction=-1.0f;
    private bool soundPlayed;
    private int selectedSoundIndex;
    [SerializeField] LoopMode loopMode;
    [SerializeField] bool spatial;
    [SerializeField] float spatialRadius;



    public GameObject ownerObj;

    public void EditorUpdate()
    {
        pitchRangePercent = new Vector2(Mathf.Abs(pitchRangePercent.x), Mathf.Abs(pitchRangePercent.y));
    }

    // Start is called before the first frame update
    void Awake()
    {
        gameObject.transform.parent = null;
        for(int i = 0; i < allSoundEffects.Length; i++)
        {
            if (allSoundEffects[i] != null)
            {
                if (allSoundEffects[i].length > timeBeforeDestruction)
                {

                    timeBeforeDestruction = allSoundEffects[i].length;
                }
            }

        }

        if (gameObject.GetComponent<AudioSource>() != null)
        {
            soundSource = gameObject.GetComponent<AudioSource>();
        }
        else
        {
            soundSource = gameObject.AddComponent<AudioSource>();
        }
        LifeTimer lifeScr = gameObject.GetComponent<LifeTimer>();

        if (loopMode!= LoopMode.LoopForever && loopMode!=LoopMode.LoopUntilOwnerGone)
        {
            if (lifeScr == null)
            {
                lifeScr = gameObject.AddComponent<LifeTimer>();
            }

            lifeScr.lifetime = timeBeforeDestruction;
        }
        else
        {
            if (lifeScr != null)
            {
                Destroy(lifeScr);
            }
        }


        soundSource.playOnAwake = false;
        if (GameOptions.mixer != null)
        {
            soundSource.outputAudioMixerGroup = GameOptions.mixer.FindMatchingGroups("Sound Effects")[0];
        }
        
        selectedSoundIndex = Random.Range(0, allSoundEffects.Length);
        pitchRangeActual = new Vector2((float)pitchRangePercent.x / (float)100, (float)pitchRangePercent.y / (float)100);
    }

    // Update is called once per frame
    void Update()
    {

        if (allSoundEffects[selectedSoundIndex] != null)
        {
            if (soundPlayed == false)
            {
                if (loopMode != LoopMode.None)
                {
                    soundSource.loop = true;
                }
                else
                {
                    soundSource.loop = false;
                }

                if (spatial == true)
                {
                    soundSource.spatialBlend = 1.0f;
                    soundSource.dopplerLevel = 0.0f;
                    soundSource.spread = 0.0f;
                    soundSource.minDistance = 0.0f;
                    soundSource.maxDistance = spatialRadius;
                    soundSource.rolloffMode = AudioRolloffMode.Linear;
                    
                }
                else
                {
                    soundSource.spatialBlend = 0.0f;
                }
                soundSource.clip = allSoundEffects[selectedSoundIndex];
                soundSource.pitch = Random.Range(pitchRangeActual.x, pitchRangeActual.y);
                soundSource.Play();
                soundPlayed = true;

            }
            else
            {
                if (loopMode == LoopMode.LoopUntilOwnerGone)
                {
                    if (ownerObj == null)
                    {
                        Destroy(gameObject);
                    }
                }
            }

        }
        else
        {
            Destroy(gameObject);
        }




    }


}
