﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobSplat : MonoBehaviour
{
    [SerializeField] Texture[] texturePool;
    private void Awake()
    {
        int texturePoolIndex = Random.Range(0, texturePool.Length);
        MeshRenderer rend = gameObject.GetComponent<MeshRenderer>();
        if (rend != null)
        {
            Material mat = rend.material;
            if (mat != null)
            {
                mat.SetTexture("_MainTex", texturePool[texturePoolIndex]);
            }
        }
    }
}
