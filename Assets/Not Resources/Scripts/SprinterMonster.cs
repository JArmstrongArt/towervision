﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprinterMonster : MonsterInherit
{
    [SerializeField] GenericRay towerSearchRay;
    private GenericRay[] allSearchRays = new GenericRay[6];
    [SerializeField] float towerCheckRate;
    private float towerCheckRate_orig;

    private TowerInherit targetTowerScr;
    private float lerpToTower_progress;

    private bool lerpToTower_returning;
    private Vector3 lerpToTower_basePos;
    private Vector3 lerpToTower_baseWorldDir;
    private Vector3 lerpToTower_destPos;
    [SerializeField] float lerpToTower_moveSpeedBoostMultiplier;
    [SerializeField] float postTargetCooldownLength;
    private float postTargetCooldownLength_current = 0;
    protected override void Awake()
    {
        if(base.FindDependencies() && base.FindResources())
        {
            base.Awake();
            base.animScr.PlayAnimation("run");

            towerCheckRate_orig = towerCheckRate;
            float addAngle = ExtendedMaths.DivideAmongst360(allSearchRays.Length);
            float currentAngle = 0;
            for (int i = 0; i < allSearchRays.Length; i++)
            {
                Vector3 spawnPos = towerSearchRay.transform.position;
                Vector3 spawnEuler = new Vector3(0, currentAngle, 0);
                GameObject rayObj = Instantiate(towerSearchRay.gameObject, spawnPos, Quaternion.Euler(spawnEuler), null);
                MaintainOffset offScr = rayObj.AddComponent<MaintainOffset>();
                offScr.targetObj = gameObject;
                offScr.AwakeSetup();
                allSearchRays[i] = rayObj.GetComponent<GenericRay>();
                currentAngle += addAngle;
            }

            Destroy(towerSearchRay);
        }

    }

    protected override void Update()
    {
        if (base.FindDependencies() && base.FindResources())
        {
            base.Update();


            if (targetTowerScr != null || lerpToTower_progress > 0)
            {

                base.pathLerp_progress_paused = true;

                if (lerpToTower_returning == false)
                {
                    lerpToTower_progress += (base.moveSpeed * Time.deltaTime * lerpToTower_moveSpeedBoostMultiplier) / Vector3.Distance(lerpToTower_basePos, lerpToTower_destPos);
                    lerpToTower_progress = Mathf.Clamp(lerpToTower_progress, 0, 1);
                    if (lerpToTower_progress >= 1)
                    {
                        if (targetTowerScr != null)
                        {
                            targetTowerScr.ShiftHealth(-Mathf.Abs(base.damage));
                        }

                        lerpToTower_returning = true;
                    }
                    base.animScr.billboardWorldDir_set((lerpToTower_destPos - lerpToTower_basePos).normalized);
                }
                else
                {
                    lerpToTower_progress -= (base.moveSpeed * Time.deltaTime * lerpToTower_moveSpeedBoostMultiplier) / Vector3.Distance(lerpToTower_basePos, lerpToTower_destPos);
                    if (lerpToTower_progress > 0)
                    {
                        base.animScr.billboardWorldDir_set((lerpToTower_basePos - lerpToTower_destPos).normalized);
                    }
                    else
                    {
                        base.animScr.billboardWorldDir_set(lerpToTower_baseWorldDir);
                        targetTowerScr = null;
                    }

                }

                gameObject.transform.position = Vector3.Lerp(lerpToTower_basePos, lerpToTower_destPos, lerpToTower_progress);

            }
            else
            {
                base.pathLerp_progress_paused = false;
                lerpToTower_returning = false;
                lerpToTower_progress = 0;
                if (postTargetCooldownLength_current > 0)
                {
                    postTargetCooldownLength_current -= Time.deltaTime;
                }
                else
                {
                    postTargetCooldownLength_current = 0;

                    if (towerCheckRate > 0)
                    {
                        towerCheckRate -= Time.deltaTime;
                    }
                    else
                    {

                        for (int i = 0; i < allSearchRays.Length; i++)
                        {
                            if (allSearchRays[i] != null)
                            {
                                if (allSearchRays[i].ray_active == true)
                                {
                                    if (allSearchRays[i].ray_surface != null)
                                    {
                                        if (allSearchRays[i].ray_surface.gameObject.layer == LayerMask.NameToLayer("Tower"))
                                        {

                                            TowerInherit targetScr = allSearchRays[i].ray_surface.GetComponent<TowerInherit>();
                                            if (targetScr != null)
                                            {
                                                if (targetScr.buildingProgress >= 1)
                                                {
                                                    lerpToTower_basePos = gameObject.transform.position;
                                                    lerpToTower_baseWorldDir = base.animScr.billboardWorldDir_get();
                                                    lerpToTower_destPos = new Vector3(targetScr.gameObject.transform.position.x, gameObject.transform.position.y, targetScr.gameObject.transform.position.z);
                                                    targetTowerScr = targetScr;
                                                    postTargetCooldownLength_current = postTargetCooldownLength;
                                                    allSearchRays[i].ray_disabled = true;
                                                }

                                            }



                                        }
                                    }

                                }
                            }
                        }


                        towerCheckRate = towerCheckRate_orig;
                    }

                }



            }
        }



    }
}
