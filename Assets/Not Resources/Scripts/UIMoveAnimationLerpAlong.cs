﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMoveAnimationLerpAlong : MonoBehaviour
{
    [SerializeField] UIMoveAnimation moveAnimScr;
    [SerializeField] float lerpSpeed;

    [SerializeField] Vector2 finalPositionOffset;
    private Vector3 origLocalPos;

    private PlayerMovement moveScr;

    private bool manualAwakeDone = false;
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        if (FindDependencies())
        {
            if (manualAwakeDone == false)
            {
                RectTransform myRect = gameObject.GetComponent<RectTransform>();
                if (myRect != null)
                {
                    origLocalPos = myRect.localPosition;
                }
                manualAwakeDone = true;
            }
            else
            {
                if (moveAnimScr != null)
                {
                    RectTransform myRect = gameObject.GetComponent<RectTransform>();
                    if (myRect != null)
                    {
                        Vector3 lerpTarget = new Vector3(moveAnimScr.movePoint_localPosition.x + finalPositionOffset.x, moveAnimScr.movePoint_localPosition.y + finalPositionOffset.y, moveAnimScr.movePoint_localPosition.z);

                        if (moveScr.moveDirectionPlusMagnitude.magnitude != 0)
                        {
                            myRect.localPosition = Vector3.Lerp(myRect.localPosition, lerpTarget, lerpSpeed * Time.deltaTime);
                        }
                        else
                        {
                            myRect.localPosition = Vector3.Lerp(myRect.localPosition, origLocalPos, lerpSpeed * Time.deltaTime);
                        }

                    }
                }
            }

        }
        else
        {
            manualAwakeDone = false;
        }


    }

    bool FindDependencies()
    {
        bool ret = false;

        if (moveScr == null)
        {
            moveScr = GameObject.FindObjectOfType<PlayerMovement>();
        }



        if (moveScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
