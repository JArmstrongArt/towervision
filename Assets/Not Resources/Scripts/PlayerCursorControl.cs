﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCursorControl : MonoBehaviour
{
    private PlayerWinLose winLoseScr;
    private PlayerActions actionScr;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnDestroy()
    {

        SetCursorLocked(false);
    }
    // Update is called once per frame
    void Update()
    {
        if (FindDependencies())
        {
            if (winLoseScr.gameWon == true || winLoseScr.gameLost == true || actionScr.gamePaused == true)
            {
                SetCursorLocked(false);
            }
            else
            {
                SetCursorLocked(true);

            }
        }

    }

    void SetCursorLocked(bool lockState)
    {
        Cursor.visible = !lockState;

        if (lockState == false)
        {
            Cursor.lockState = CursorLockMode.None;

        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;

        }


    }

    bool FindDependencies()
    {
        bool ret = false;

        winLoseScr = GameObject.FindObjectOfType<PlayerWinLose>();
        actionScr = GameObject.FindObjectOfType<PlayerActions>();

        if(winLoseScr!=null && actionScr != null)
        {
            ret = true;
        }
        return ret;
    }
}
