﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SoldierMonster : MonsterInherit
{
    private GameObject soldierProjectile_prefab;
    [SerializeField] float fireRate;
    private float fireRate_orig;

    private float billboardRefreshTime;
    private bool billboardRefreshed=true;

    private GameObject soundEffect_walk_prefab;
    private GameObject soundEffect_walk_inst;
    [SerializeField] bool projectilesInheritDamage;
    protected override void Awake()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.Awake();
            fireRate_orig = fireRate;
            base.animScr.PlayAnimation("walk");
            GeneralSoundEffect walkSEScr = soundEffect_walk_prefab.GetComponent<GeneralSoundEffect>();
            if (walkSEScr != null)
            {
                soundEffect_walk_inst = Instantiate(soundEffect_walk_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                soundEffect_walk_inst.GetComponent<GeneralSoundEffect>().ownerObj = gameObject;
            }
        }

    }
    private void LateUpdate()
    {
        if (base.FindDependencies() && FindResources())
        {
            if (soundEffect_walk_inst != null)
            {
                soundEffect_walk_inst.transform.position = gameObject.transform.position;
            }
        }

    }
    protected override void Update()
    {

        if (base.FindDependencies() && FindResources())
        {
            base.Update();
            if (fireRate > 0)
            {
                fireRate -= Time.deltaTime;
            }
            else
            {
                bool playerAim = false;
                GameObject pla = GetPlayerInRange();
                if(pla != null)
                {
                    PlayerMovement moveScr = pla.GetComponent<PlayerMovement>();

                    if (moveScr != null)
                    {
                        if (moveScr.playerSlowdownTimer <=0)
                        {
                            playerAim = true;
                        }
                    }
                    else
                    {
                        playerAim = true;
                    }
                }
                if (playerAim==true)
                {
                    GameObject relevantPlayer = GetPlayerInRange();
                    if (relevantPlayer != null)
                    {
                        GameObject soldierProjectile_inst = Instantiate(soldierProjectile_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        ProjectileInherit projScr = soldierProjectile_inst.GetComponent<ProjectileInherit>();
                        if (projScr != null)
                        {

                            Vector3 aimDir = (new Vector3(relevantPlayer.transform.position.x, gameObject.transform.position.y, relevantPlayer.transform.position.z) - gameObject.transform.position).normalized;
                            projScr.moveDir = aimDir;
                            if (projectilesInheritDamage)
                            {
                                SoldierProjectile soldProjScr = projScr.gameObject.GetComponent<SoldierProjectile>();
                                if (soldProjScr != null)
                                {
                                    soldProjScr.towerHealthDamage = damage;
                                }
                            }

                            billboardRefreshed = false;
                            base.billboardWorldDir_previous = base.animScr.billboardWorldDir_get();
                            base.animScr.billboardWorldDir_set(aimDir);
                            billboardRefreshTime = fireRate_orig * 0.25f;
                        }
                    }




                }
                else
                {
                    if (NearestTowerInRange())
                    {
                        if (base.tower_nearest != null)
                        {
                            if (Vector3.Distance(new Vector3(base.tower_nearest.transform.position.x, gameObject.transform.position.y, base.tower_nearest.transform.position.z), gameObject.transform.position) <= awarenessRadius)
                            {
                                GameObject soldierProjectile_inst = Instantiate(soldierProjectile_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                                ProjectileInherit projScr = soldierProjectile_inst.GetComponent<ProjectileInherit>();
                                if (projScr != null)
                                {

                                    Vector3 aimDir = (new Vector3(base.tower_nearest.transform.position.x, gameObject.transform.position.y, base.tower_nearest.transform.position.z) - gameObject.transform.position).normalized;
                                    projScr.moveDir = aimDir;
                                    billboardRefreshed = false;
                                    base.billboardWorldDir_previous = base.animScr.billboardWorldDir_get();
                                    base.animScr.billboardWorldDir_set(aimDir);
                                    billboardRefreshTime = fireRate_orig * 0.25f;
                                }
                            }
                        }
                    }

                }
                fireRate = fireRate_orig;
            }

            if (billboardRefreshTime > 0)
            {
                billboardRefreshTime -= Time.deltaTime;
            }
            else
            {
                if (billboardRefreshed == false)
                {
                    base.animScr.billboardWorldDir_set(base.billboardWorldDir_previous);
                    billboardRefreshed = true;
                }
            }

        }


    }

    GameObject GetPlayerInRange()
    {

        GameObject ret = null;
        if (base.FindDependencies() && FindResources())
        {
            GameObject playerObj = GameObject.FindGameObjectWithTag("playerCam");
            if (playerObj != null)
            {
                if (Vector3.Distance(new Vector3(playerObj.transform.position.x, gameObject.transform.position.y, playerObj.transform.position.z), gameObject.transform.position) <= awarenessRadius)
                {
                    ret = playerObj;
                }
            }
        }
        return ret;
    }




    protected override bool FindResources()
    {
        bool parRet= base.FindResources();
        bool ret = false;
        
        if (soldierProjectile_prefab == null)
        {
            soldierProjectile_prefab = Resources.Load<GameObject>(Constants.MONSTER_DIR +"Projectiles/monster_soldier_projectile");
        }

        if (soundEffect_walk_prefab == null)
        {
            soundEffect_walk_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/soundEffect_slimeWalk");
        }

        if (soundEffect_walk_prefab!=null && soldierProjectile_prefab != null && parRet==true)
        {
            ret = true;
        }
        return ret;
    }
}
