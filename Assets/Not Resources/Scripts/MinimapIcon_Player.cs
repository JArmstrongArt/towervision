﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapIcon_Player : MinimapIcon
{
    private Texture iconTex_player;


    protected override void Update()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.Update();
            base.iconTex = iconTex_player;
            if (base.myRend != null && base.myFilt != null)
            {
                
                bool renderThisIcon = false;

                foreach (TowerInherit tower in EntityTracker.ALL_TOWERS)
                {
                    if (tower != null)
                    {
                        if (tower.iconScr != null)
                        {
                            if (tower.iconScr.towerOnMinimap == true)
                            {
                                renderThisIcon = true;
                            }
                        }

                    }
                }

                myRend.enabled = renderThisIcon;
            }
        }


    }



    protected override bool FindResources()
    {
        bool parRet= base.FindResources();
        bool ret = false;

        if (iconTex_player == null)
        {
            iconTex_player = Resources.Load<Texture>("Textures/MinimapIcons/player");
        }

        if (iconTex_player != null && parRet==true)
        {
            ret = true;
        }
        return ret;
    }
}
